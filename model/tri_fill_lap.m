function v = tri_fill_lap(t,v)
%TRI_FILL_LAP Fill NaNs in v by laplacian approach (iterative averaging of neighbours).
%
%  t     : ntx3 array of triangles as indices, [n11,n12,n13; 
%                                              n21,n22,n23; etc].
%  v     : [n m] values at nodes (NaNs are the same for all columns)
%
% See also TRI_SMOOTHFIELD_LAP

% Based on 
% Mesh2d v24/smoothmesh.m by Darren Engwirda - 2007
%  SMOOTHMESH: Smooth a triangular mesh using Laplacian smoothing.

[n,m] = size(v);
S = sparse(t(:,[1,1,2,2,3,3]),t(:,[2,3,1,3,1,2]),1,n,n); % Sparse connectivity matrix
% S has 1's for boundary neighbours and 2's for internal neighbours
S(S==2) = 1; % replace 2's in S with 1's, so all neighbours have same weight

ifix = ~isnan(v(:,1)); % NaNs are the same for all columns
while any(~ifix)
    Sf = S(~ifix,ifix); % to keep only valid neighbours
    W = sum(Sf,2); % number of valid neighbours
    vv = (Sf*v(ifix,:)) ./ (W*ones(1,m)); % Laplacian smoothing; average of neighbour values
    
    v(~ifix,:) = vv; % fill in the new values
    ifix = ~isnan(v(:,1)); % update valid nodes list
end