function tr = fvcom_transect(xt,yt,x,y,tri,h,ns,siglay,xc,yc,nbe)
% Vertical transect of a triangular grid along a piecewise-linear line [xt,yt]
% for tracer (node-based) and velocity (element-based) quantity.
%
% Horizontal resolution of the resulting transect is defined by the points of
% intersection with the grid edges; vertical resolution is defined by the model
% sigma-layers.
%
% INPUTS:
%   xt,yt   coordinates of the transect line
%   x,y     [n,1] model grid nodal coordinates
%   tri     [k,3] triangulation array
%   h       [n,1] model depths
%   ns      number of sigma-layers
%   siglay  [n,ns] sigma-layers
%   xc,yc   [k,1] grid element centroids
%   nbe     [k,3] elements surrounding each element (indices into rows of tri); 
%           can be obtained from the fvcom output variable 'nbe';
%           contains 1 zero value for each triangle with a boundary edge
% 
% Model grid metrics can be obtained with 
% [x,y,h,tri,ns,siglay,xc,yc,nbe] = fvcom_ncgrid(outfile)
% 
% OUTPUT:
%   tr      struct with fields tn,te for the node-based and element-based 
%           quantities, respectively.
%       tn,te structures with fields:
%           d     distance along the transect
%           h     bathymetry along the transect
%           siglay  model sigma layers along the transect
%           x,y   transect point coordinates
%           nid,w interpolant for evaluating scalar fields on the transect:
%               nid   [nt,ni] indices into grid nodes array
%               w     [nt,ni] corresponding weights: all(sum(w,2))==1, where
%                   nt -- number of transect points
%                   ni -- number of grid nodes used for interpolation of each point
%           theta   transect direction

olab_addpath('geometry\triangulation') % tri_transect, grid_interp_eval

% ensure dimensions [node, nsiglay] for siglay
% (chn test case: fvcom2 does not expand siglay to nodes dimension)
if size(siglay,2)==1
    siglay = ones(size(h))*siglay';
end
zsig = (h*ones(1,ns)) .* siglay; % layer depths for zeta=0

% transect with interpolant
% [pti,nid,w] = tri_transect(double([xt yt]),double([x y]),double(tri));
[ptn,nid,wn,pte,eid,we,theta] = ...
    tri_transect(double([xt yt]),double([x y]),double(tri),double(xc),double(yc),double(nbe));


% for node-based quantities
hn = grid_interp_eval(nid,wn,h); % bathy profile
% sign = grid_interp_eval(nid,wn,zsig); % sigma layer depths on the transect
sign = grid_interp_eval(nid,wn,siglay); % sigma layers on the transect
dn = [0; cumsum(sum(diff(ptn).^2,2).^.5)]; % distances along the transect


% for element-based quantities
%%% this produces discontinuities at grid edges, use depths from hn
% hc = mean(h(tri),2); % depths at centroids
% he = grid_interp_eval(eid,we,hc); % bathy profile
he = [hn hn]'; % simply duplicate points of the node-based transect to get element-based transect
he = he(:); %NOTE: this relies on the same procedure in tri_transect

%%% this also produces discontinuities at grid edges
%%%% these are not needed; 
zsigc = zsig(tri',:); % [ntri*3, ns]
zsigc = reshape(zsigc,[3 size(tri,1) ns]); % [3, ntri, ns]
zsigc = squeeze(mean(zsigc)); % [ntri, ns]
sige = grid_interp_eval(eid,we,zsigc); % sigma layers on the transect

de = [0; cumsum(sum(diff(pte).^2,2).^.5)]; % distances along the transect

tr.tn = transect_struct(dn,hn,sign,ptn,nid,wn,theta(1:2:end)); % for node-based quantities
tr.te = transect_struct(de,he,sige,pte,eid,we,theta); % for element-based quantities


function tr = transect_struct(d,h,sig,pti,nid,w,theta)
% Structure used for interpolation and displaying of a transect.
tr = struct('d',d,'h',h,'siglay',sig,'x',pti(:,1),'y',pti(:,2),'nid',nid,'w',w,'theta',theta);