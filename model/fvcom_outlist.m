function [files,name,casename] = fvcom_outlist(folder,casename)
%FVCOM_OUTLIST -- List FVCOM output files in the specified folder with the specified casename.
% If casename is omitted or empty, tries to determine the casename automatically.
% 
% Output:
%     files     struct array as returned by dir, with fields:
%               name    -- full name, including path and extension
%               date
%               bytes
%               isdir
%               datenum
%
%     name      cell array of file names without path and extension

olab_addpath('system') % listfiles

files = listfiles(folder,'nc');
fname = {files.name}; % including path
[~,name] = cellfun(@fileparts,fname,'UniformOutput',false); % strip path and extension


% pick files corresponding to fvcom output pattern

% ending with an underscore and 4 digits
igood  = cellfun(@(x) x(end-4)=='_' & all(x(end-3:end)>='0' & x(end-3:end)<='9'), name);

if exist('casename','var') && ~isempty(casename)
    caselen = length(casename);
    icase = strncmp(name,casename,caselen); % starting with casename
    ilen  = cellfun(@(x) length(x)==caselen+5, name); % of required length
    igood = igood & icase & ilen;
else
    % strip trailing underscore and number
    casename = unique(cellfun(@(x) x(1:end-5),name(igood),'UniformOutput',false));
    if length(casename)>1
        error('More than one possible casename is found. Specify a unique casename.')
    end
end


% sort by name
name = name(igood);
[name,isort] = sort(name);

% output structure
files = files(igood);
files = files(isort);