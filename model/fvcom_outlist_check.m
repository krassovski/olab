function files = fvcom_outlist_check(files)
%FVCOM_OUTLIST_CHECK -- Ensure cell array of file names.
% Helper for functions reading from a series of fvcom output files.
%
% Input:
%   files   can be a file name in a string, folder name, or cell array of file names
% 
% Output:
%   files   cell array of file names

if ischar(files)
    files = {files};
end

% generate file list if a folder supplied
if isdir(files{1})
    if length(files)==1
        files = [files,{[]}];
    end
    files = fvcom_outlist(files{1},files{2});
    files = {files.name}';
end