function v = fvcom_var(files,varname,start,count,stride)
%FVCOM_VAR -- Read a variable from a series of fvcom output files.
% 
% Input:
%   files   can be a file name in a string, folder name, or cell array of file names
%   varname fvcom variable name
%   start,count,stride  are as for ncread (1-based); to default omit or specify empty.
%                       Use Inf in count to read to the end.

files = fvcom_outlist_check(files);
vi = ncinfo(files{1},varname);
timedim = find(strcmp({vi.Dimensions.Name},'time'));

% form cell arrays for input to cellfun

rtype = 1; % do not use start,count,stride in ncread
if exist('start','var') && ~isempty(start) && ...
   exist('count','var') && ~isempty(count)
    rtype = 2; % use start,count in ncread
    
    % determine files to read
    nt = cellfun(@get_nt, files);
    tbr = cumsum(nt)-nt;

    % ... 1st file to read and start index
    ist = start(timedim);
    tst = ist - tbr;
    tst(tst<=0) = Inf; % consider only positive values
    fb = find(tst==min(tst)); % 1st file to read
    tst = tst(fb); % start index for 1st file to read

    % ... last file to read and end index
    if isinf(count(timedim))
        fe = length(files);
        ten = nt(end);
    else
        iend = start(timedim)+count(timedim)-1;
        ten = iend - tbr;
        ten(ten<=0) = Inf; % consider only positive values
        fe = find(ten==min(ten)); % last file to read
        ten = ten(fe); % end index (count) for last file to read
    end

    files = files(fb:fe);
    nf = length(files);
    
    % to read entire time dimension (start and count for 1st and last files are adjusted later)
    start(timedim) = 1;
    count(timedim) = Inf;
    
    % form cell arrays for input to cellfun
    start = repmat({start},nf,1);
    count = repmat({count},nf,1);
    
    % adjust start and count for 1st and last files
    start{1}(timedim) = tst;
    if fb==fe % only one file to read
        ten = ten-tst+1;
    end
    count{nf}(timedim) = ten;
end

if exist('stride','var') && ~isempty(stride)
    rtype = 3; % use start,count,stride in ncread
    
    % form cell array for input to cellfun
    stride = repmat({stride},length(files),1);
end

nf = length(files);
varname = repmat({varname},nf,1);

switch rtype
    case 1
        v = cellfun(@ncread,files,varname,                    'UniformOutput',false);
    case 2
        v = cellfun(@ncread,files,varname,start,count,        'UniformOutput',false);
    case 3
        v = cellfun(@ncread,files,varname,start,count,stride, 'UniformOutput',false);
end

v = cat(timedim,v{:});


function nt = get_nt(file)
% Get time dimension length for an fvcom output file.
vi = ncinfo(file);
nt = vi.Dimensions(strcmp({vi.Dimensions.Name},'time')).Length;