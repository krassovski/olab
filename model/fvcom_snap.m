function fvcom_snap(k,varname1,timein,clim,ival,fgridname,vscale,dthres,usegeo)
%FVCOM_SNAP -- Snapshot of an fvcom variable for the surface layer.
% Plots in current axes.
% 
% Signatures:
%   Initialization:
%   fvcom_snap(filename,varname1,timein,clim,ival,fgridname,vscale,dthres,usegeo);
% 
%   Iteration for k-th time step:
%   fvcom_snap(k);
% 
% Inputs:
%   filename    file name in a string, folder name, or cell array of file names
%   varname     fvcom variable name
%   timein      can be obtained with fvcom_time
%   clim        color scale limits for plotting
%   ival        number of valid time steps in a file (in case of unfinished fvcom file)
%   fgridname   nc file to read grid information from; by defau;t reads from
%               the 1st file in the filename list
%   vscale      velocity vector scaling for quiver
%   dthres      distance threshold to decimate quiver arrows
%   usegeo      plot in geographic coordinates, scaling y-axis based on latitude

olab_addpath('model/mesh') % bathy_decimate

persistent varname

if ~exist('vscale','var')
    vscale = 1;
end
if ~exist('dthres','var')
    dthres = [];
end
if ~exist('usegeo','var')
    usegeo = [];
end

if exist('varname1','var') % initialization call
    varname = varname1;
    if strcmp(varname,'v') || strcmp(varname,'V10')
        fvcom_snapvel(k,varname,timein,clim,ival,fgridname,vscale,dthres,usegeo); %%%
    else
        fvcom_snapfield(k,varname,timein,clim,ival,fgridname,usegeo);
    end
else % update call
    if strcmp(varname,'v') || strcmp(varname,'V10')
        fvcom_snapvel(k);
    else
        fvcom_snapfield(k);
    end
end


function timeout = fvcom_snapfield(k,varname1,timein,clim,nt,fgridname,usegeo)

persistent el s hax hp time varname x y tri xc yc hq

element_based = {'ww','partition','wet_cells'};

if iscell(k) % ischar(k) % initialization call
    fname = k;
    
    varname = varname1;
    
    try
        [x,y,~,tri,~,~,xc,yc] = fvcom_ncgrid(fgridname);
        if usegeo
            x = ncread(fgridname,'lon');
            y = ncread(fgridname,'lat');
            xc = ncread(fgridname,'lonc');
            yc = ncread(fgridname,'latc');
        end
    catch
        error('No grid information in the specified file.')
    end

    fi = ncinfo(fname{1});
%     dname = {fi.Dimensions.Name};
%     dlen = [fi.Dimensions.Length];
%     nn = dlen(strcmp(dname,'node'));
%     nt = dlen(strcmp(dname,'time'));
%     nl = dlen(strcmp(dname,'siglay'));
    kl = 1; % layer to read (1-based) %%% take surface layer
    
    ncvar = fi.Variables(strcmp({fi.Variables.Name},varname));
    sz = ncvar.Size;
    
    if length(sz)==3
%         s = squeeze(ncread(fname,varname,[1 kl 1],[sz(1) 1 nt])); % node x siglay x time
        s = squeeze(fvcom_var(fname,varname,[1 kl 1],[sz(1) 1 nt])); % nele x siglay x time
    else
%         s = ncread(fname,varname); % node x time
        s = fvcom_var(fname,varname); % node x time
    end
    s = double(s);

    time = timein;
    
    timeout = time;
    
    hax = gca;
    if usegeo
        set(gca,'DataAspectRatio',[1 sin(mean(y)*pi/180) 1]) % scale axes according to latitiude
    else
        axis equal
    end

    % hout = tri_plot(x,y,tri,'Color',[.7 .7 .7]);

    % smoothly colored bathymetry
    el = [tri tri(:,1)]'; % closed triangle patches
    sc = s(:,1);
    if ismember(varname,element_based)
        sc = ones(4,1)*sc'; % vert velocity is element-centered
    else
        sc = sc(el); % the other variables are node-centered
    end
    hp = patch(x(el),y(el),sc,'EdgeColor','none');
    
    % fixed color limits
    if ~isempty(clim)
        set(hax,'CLim',clim)
    end
    
    % colorbar
    au = strcmp({ncvar.Attributes.Name},'units');
    if any(au)
        unitsbraketed = [' (' ncvar.Attributes(au).Value ')'];
    else
        unitsbraketed = '';
    end
    hcb = colorbar;
    hcb.Label.String = [varname unitsbraketed];
    
%     % show surface slope gradient for zeta with arrows
%     [xn,yn] = tri_grad(x,y,s(:,1),tri);
%     xn = xn';
%     yn = yn';
%     hold on
%     hq = quiver(xc,yc,xn,yn,10);
    
else % update frame call
%     initflag = false;
    sc = s(:,k);
    if ismember(varname,element_based) % strcmp(varname,'ww')
        sc = ones(4,1)*sc'; % vert velocity is element-centered
    else
        sc = sc(el); % the other variables are node-centered
    end
    set(hp,'CData',sc);
    title(hax,datestr(time(k),'yyyy-mmm-dd HH:MM'))
    
    timeout = [];
    
%     [xn,yn] = tri_grad(x,y,s(:,k),tri);
%     xn = xn';
%     yn = yn';
%     set(hq,'UData',xn,'VData',yn);    
end

% nt = length(time);
% for k = 1:nt
%     pause(0.5)
%     sc = s(:,1,k);
%     set(hp,'CData',sc(el));
%     title(datestr(time(k)))
% end


function timeout = fvcom_snapvel(k,varname1,timein,clim,nt,fgridname,vscale,dthres,usegeo)
% Velocity amplitude and vector viewer.

persistent el u v a varname hax hp hq time autoscale inv

if iscell(k) % ischar(k) % initialize
    fname = k;
    
    varname = varname1;

    try
        [x,y,~,tri,~,~,xcm,ycm] = fvcom_ncgrid(fgridname);
        if usegeo
            x = ncread(fgridname,'lon');
            y = ncread(fgridname,'lat');
            xc = ncread(fgridname,'lonc');
            yc = ncread(fgridname,'latc');
        else
            xc = xcm;
            yc = ycm;
        end
    catch
        error('No grid information in the specified file.')
    end

    fi = ncinfo(fname{1});
%     dname = {fi.Dimensions.Name};
%     dlen = [fi.Dimensions.Length];
%     nn = dlen(strcmp(dname,'nele'));
%     nt = dlen(strcmp(dname,'time'));
%     nl = dlen(strcmp(dname,'siglay'));
%     kl = nl; % layer to read (1-based) %%% take bottom layer
    kl = 1; % layer to read (1-based) %%% take surface layer
    
    if strcmp(varname,'v')
        ncvar = fi.Variables(strcmp({fi.Variables.Name},'v'));
        sz = ncvar.Size;
%         u = squeeze(ncread(fname,'u',[1 kl 1],[sz(1) 1 nt])); % nele x siglay x time
%         v = squeeze(ncread(fname,'v',[1 kl 1],[sz(1) 1 nt])); % nele x siglay x time
        u = squeeze(fvcom_var(fname,'u',[1 kl 1],[sz(1) 1 nt])); % nele x siglay x time
        v = squeeze(fvcom_var(fname,'v',[1 kl 1],[sz(1) 1 nt])); % nele x siglay x time
    elseif strcmp(varname,'V10') % wind forcing file
        u = ncread(fname{1},'U10'); % nele x time
        v = ncread(fname{1},'V10'); % nele x time
    end
    
    if any(ismember('wet_cells',{fi.Variables.Name}'))
        wet = fvcom_var(fname,'wet_cells',[1 1],[sz(1) nt]); % nele x time
    else
        wet = ones(size(u)); % nele x time
    end
    
    time = timein;
    
%     %%% hack for viewing daily-averaged velocities
%     addpathrel('Tools\Signal\Filters') % resample_filt
%     [~,   ud] = resample_filt(time,double(u)',1);
%     [time,vd] = resample_filt(time,double(v)',1);
%     u = ud';
%     v = vd';
%     %%%
    
    timeout = time;
    
    a = (u.^2 + v.^2).^0.5; % amplitude
    a(wet==0) = NaN;
    
    inv = bathy_decimate(xcm,ycm,dthres);
    xc(inv) = []; yc(inv) = []; u(inv,:) = []; v(inv,:) = [];
    
    u = u*vscale;
    v = v*vscale;
    
    hax = gca;
    if usegeo
        set(gca,'DataAspectRatio',[1 sin(mean(y)*pi/180) 1]) % scale axes according to latitiude
    else
        axis equal
    end

%     hout = tri_plot(x,y,tri,'Color',[.7 .7 .7]);

    % smoothly colored amplitude
    el = [tri tri(:,1)]'; % closed triangle patches
%     ac = a(:,1);
%     hp = patch(x(el),y(el),ac(el),'EdgeColor','none');
    hp = patch(x(el),y(el),ones(4,1)*a(:,1)','EdgeColor','none');
    
%     load cmap_fvcom_vel cmap % cmap
    load cmap_fvcom_vel_magenta cmap % cmap
    set(gcf,'Colormap',cmap)
%     climnan(gcf,[1 1 1]*.7)
    
    hold on
%     hq = quiver(xc,yc,squeeze(u(:,1)),squeeze(v(:,1)));
    % fix the scaling of arrows based on maximum vector length for each point
    amp = (u.^2 + v.^2).^.5;
    [~,kmax] = max(amp,[],2); % ignores NaNs (for at least R2016a and up)
    szu = size(u);
    lmax = sub2ind(szu,(1:szu(1))',kmax);
    hq = quiver(xc,yc,u(lmax),v(lmax),'Color',[0 113 188]/255,'Visible','off'); 
    autoscale = get(hq,'AutoScaleFactor');
    set(hq,'UData',u(:,1),'VData',v(:,1),'AutoScaleFactor',autoscale,'Visible','on');
    
    % fixed color limits
    if ~isempty(clim)
        set(hax,'CLim',clim)
    end
    
    % colorbar
    au = strcmp({ncvar.Attributes.Name},'units');
    if any(au)
        unitsbraketed = [' (' ncvar.Attributes(au).Value ')'];
    else
        unitsbraketed = '';
    end
    hcb = colorbar;
    hcb.Label.String = ['Speed' unitsbraketed];
else
%     initflag = false;
%     ac = a(:,k);
%     set(hp,'CData',ac(el));
    set(hp,'CData',ones(4,1)*a(:,k)');
    set(hq,'UData',u(:,k),'VData',v(:,k),'AutoScaleFactor',autoscale);
    title(hax,datestr(time(k),'yyyy-mmm-dd HH:MM UTC')) %%%%
%     title(hax,[datestr(time(k)-7/24,'yyyy-mmm-dd HH:MM') ' PDT']) %%%% hack for fvcom_vel_snapshots
    
    timeout = []; %%%
end