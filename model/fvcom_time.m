function time = fvcom_time(files,start,count,stride)
%FVCOM_TIME -- Read time from a series of fvcom output files.
% 
% Input:
%   files   can be a file name in a string, folder name, or cell array of file names
%   start,count,stride  are scalars for ncread for reading along time dimension; 
%                       to default omit or specify empty.
%                       Use Inf in count to read to the end.

%TODO: Deal with reading through multiple files as in fvcom_var.

files = fvcom_outlist_check(files);

rtype = 1; % do not use start,count,stride in ncread
if exist('start','var') && ~isempty(start) && ...
   exist('count','var') && ~isempty(count)
    startt = [1   start];
    countt = [Inf count];
    rtype = 2; % use start,count in ncread
end
if exist('stride','var') && ~isempty(stride)
    stridet = [1 stride];
    rtype = 3; % use start,count,stride in ncread
end

switch rtype
    case 1
%         time = cellfun(@(x) ncread(x,'Times'),                       files, 'UniformOutput',false);
        time = fvcom_var(files,'Times');
    case 2
%         time = cellfun(@(x) ncread(x,'Times',startt,countt),         files, 'UniformOutput',false);
        time = fvcom_var(files,'Times',startt,countt);
    case 3
%         time = cellfun(@(x) ncread(x,'Times',startt,countt,stridet), files, 'UniformOutput',false);
        time = fvcom_var(files,'Times',startt,countt,stridet);
end

% time = cat(2,time{:})';
time = time';
ps = time(:,20:26); % 2016-04-27T16:08:00.000000
ps = str2double(cellstr(ps))/60/60/24; % partial seconds
time = datenum(time,'yyyy-mm-ddTHH:MM:SS');
time = time + ps;