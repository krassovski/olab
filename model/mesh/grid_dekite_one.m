function [p,t,err] = grid_dekite_one(p,t,id,ibnd)
% De-kite one "kite" in a grid [p,t] remove node with index id.
% id should not be a boundary node; no check for this is performed inside, it is
% a responsibility of the caller.
% ibnd  numerical indices of boundary nodes (optional); if supplied then tries
%       to avoid creating edges connecting boundary segments ("bridges");
%       can also be a logical flag - if set to true then boundary nodes are
%       detected internally

if length(id)~=1 || id<1 || id>size(p,1)
    err = 'ID must be a single numerical index into p; no de-kite was performed.';
    return
end

lt = any(t'==id)'; % logical indices of triangles containing node id
if length(find(lt))~=4
    err = 'Not a kite-node was specified; no de-kite was performed.';
    return
end

err = false;

edg = sort(reshape(t(lt,[1 2 2 3 3 1])',2,[])', 2); % all edges in the four triangles; each edge is a row
in = setdiff(unique(edg),id); % neighbours of id
tnew = delaunay(p(in,1),p(in,2)); % triangulate the four neighbours
tnew = in(tnew); % indices into the original p

% try to avoid "bridges"
if exist('ibnd','var') && ~isempty(ibnd)
    if islogical(ibnd)
        ibnd = grid_boundarynodes(t);
    end
    
    edgnew = sort(reshape(tnew(:,[1 2 2 3 3 1])',2,[])', 2); % all edges in the new triangles; each edge is a row
    ediag = setdiff(edgnew,edg,'rows'); % one "diagonal" edge in the two newly created triangles
    
    if any(ibnd==ediag(1)) && any(ibnd==ediag(2)) % it is a "bridge" %%%%%assuming id is not a boundary node
        % alternative diagonal edge
        ediagalt = setdiff(nchoosek(in,2), [ediag; edg], 'rows'); % nchoosek gives all combinatons
        
        if all(ibnd~=ediagalt(1)) || all(ibnd~=ediagalt(2)) % alternative edge is not a "bridge"
            [tnew,err] = grid_edge_exchange(tnew,ediag);
            if err
                return
            end
        end
    end
end

% remove de-kited node and substitute new triangles
p(id,:) = [];
t = [t(~lt,:); tnew];
t(t>id) = t(t>id)-1; % adjust indices