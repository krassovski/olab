function ihang = tri_hangingnode(tri,n)
%Check for hanging nodes. 
% n     number of nodes in the grid

tri = double(tri);

S = sparse(tri(:,[1,1,2,2,3,3]),tri(:,[2,3,1,3,1,2]),1,n,n); % Sparse connectiity matrix
W = sum(S,2);
ihang = find(W==0);