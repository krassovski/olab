function res = grid_validmove(p,tri,inode)
% True if node with index inode in triangular grid [p,tri] does not violate
% integrity of the grid (the edges with this node do not intersect any other
% edges of the grid).

[ebnd,eint] = grid_edge(tri);
e = [ebnd; eint]; % all edges

for iin = 1:length(inode)
    ia = find(any(e==inode(iin), 2)); % "adjacent" edges, the ones containg inode
    exy = [p(e(:,1),:), p(e(:,2),:)]; % 4-column [x1 y1 x2 y2] array of edge coordinates
    res = true;
    for iia = 1:length(ia)
        ic =  all( e~=e(ia(iia),1), 2)  &  all( e~=e(ia(iia),2), 2); % all edges not containing the two nodes from the current edge
        out = lineSegmentIntersect(exy(ic,:), exy(ia(iia),:));
        if any(out.intAdjacencyMatrix)
            res = false;
            return
        end
    end
end