function [xx,yy,zz,ccode] = incres(x,y,d,z,code)
% Increase resolution (node spacing) by inserting more nodes on the long edges.
% Edges are between consequitive points in [x,y].
% Each original edge is divided in to floor(edgelen/d)+1 equal pieces.
% Optional Z value is linearly interpolated between successive nodes.
% Optional code value is taken as the minimum of the two neighbouring original values.

doz = exist('z','var'); % z is supplied
docode = exist('code','var'); % code is supplied

dx = diff(x);
dy = diff(y);
if doz
    dz = diff(z);
    if docode
        code = code(:)'; % ensure row
%         dcode = nanmin([code(1:end-1); code(2:end)]);
        dcode = min([code(1:end-1); code(2:end)],[],'omitnan');
    end
end
l = (dx.^2+dy.^2).^.5; % edge lengths
% n = floor(l/d); % number of points to insert for each edge
n = ceil(l/d) - 1; % number of points to insert for each edge; space them at least at d
n(isnan(n)) = 0; % do not insert nodes before and after the terminating NaN
ni = sum(n);
if ni==0
    xx = x;
    yy = y;
    if doz
        zz = z;
        if docode
            ccode = code;
        end
    end
    return
end

nx = length(x);
xx = zeros(1,nx+ni); % preallocate %%%%+ 1 for NaN
yy = xx;
if doz
    zz = xx;
    if docode
        ccode = xx;
    end
end
ie = 1; % end of stack
for k = 1:nx-1
    xx(ie) = x(k); yy(ie) = y(k); % original node
    if doz
        zz(ie) = z(k);
        if docode
            ccode(ie) = code(k);
        end
    end
    if n(k)~=0 % additional nodes are required
        % linear interpolation of all coordinates
        xx(ie+(1:n(k))) = x(k)+(1:n(k))*dx(k)/(n(k)+1);
        yy(ie+(1:n(k))) = y(k)+(1:n(k))*dy(k)/(n(k)+1);
        if doz
            zz(ie+(1:n(k))) = z(k)+(1:n(k))*dz(k)/(n(k)+1);
            if docode
                ccode(ie+(1:n(k))) = dcode(k); % pre-calculated min of neighbours
            end
        end
    end
    ie = ie+n(k)+1;
end
xx(end) = x(end);
yy(end) = y(end);
if doz
    zz(end) = z(end);
    if docode
        ccode(end) = code(end);
    end
end