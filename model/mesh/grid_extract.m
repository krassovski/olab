function [p,tri,z,code] = grid_extract(p,tri,z,code,ikeep)

% iall = 1:size(p,1);
% irem = setdiff(iall,ikeep);
% p(irem,:) = [];
% z(irem) = [];
% code(irem) = [];
% tri(any(ismember(tri,irem),2), :) = [];
% for ia = sort(irem,'descend')'
%     tri(tri>ia) = tri(tri>ia) - 1; % adjust indices
% end

if isempty(ikeep) % nothing to extract
    p = [];
    tri = [];
    z = [];
    code = [];
else
    % vectorized version
    ikeep = sort(ikeep);
    iall = (1:size(p,1))';
    irem = setdiff(iall,ikeep);
    [iback,iback] = intersect([ikeep; irem],iall);
    tri = iback(tri); % indices into the new set of nodes
    itr = tri>length(ikeep);
    tri(any(itr,2),:) = [];
    p(irem,:) = [];
    z(irem) = [];
    code(irem) = [];

    [p,tri,z,code] = grid_fix(p,tri,z,code);
    % The following checks are performed:
    %  1. Nodes not refereneced in tri are removed.
    %  2. Duplicate nodes are removed.
    %  3. Triangles are ordered counter-clockwise.
    %  4. Triangles with an area less than 1.0e-10*eps*norm(A,'inf') are removed
end