function [p,t,pfun,tfun] = grid_smooth(p,t,maxit,tol,pfun,tfun,obnl,ifix)
%GRID_SMOOTH: Smooth a triangular mesh using Laplacian smoothing.
%
% Laplacian smoothing is an iterative process that generally leads to an
% improvement in the quality of the elements in a triangular mesh.
%
%  [p,t] = grid_smooth(p,t);
%
%  p     : Nx2 array of nodal XY coordinates, [x1,y1; x2,y2; etc].
%  t     : Mx3 array of triangles as indices, [n11,n12,n13; 
%                                              n21,n22,n23; etc].
%  maxit : Maximum allowable iterations.
%  tol   : Convergence tolerance (Percentage change in edge length must be 
%          less than TOL).
%
%mk pfun,tfun   are for fixmesh only, no interpolation of pfun into new
%               nodes after smothing.
%mk obnl    open boundary node logical indices: if supplied, the nodes adjacent to
%           obn are fixed. Used for FVCOM grids with right angle triangles
%           at the open boundary.
%mk ifix    numeric indices of nodes which shouldn't be moved 
%mk Uses fixmesh from Mesh2d v24
%
% If MAXIT or TOL are left empty the default values MAXIT = 20 and TOL =
% 0.01 are used.
%
%  EXAMPLE:
%
%  [p,t] = grid_smooth(p,t,10,0.05);
%
% See also MESH2D, REFINE

% Modified SMOOTHMESH code by Darren Engwirda - 2007.
% MK: added option to avoid smooting near open boundary

%mk
% if nargin<4
%    tol = [];
%    if nargin<3
%       maxit = [];
%       if nargin<2
%          error('Incorrect number of inputs.');
%       end
%    end
% else
if nargin>8 %mk
   error('Incorrect number of inputs.')
end
if nargout>4 %mk
   error('Incorrect number of outputs.');
end
if ~exist('tol','var') || isempty(tol)
   tol = 0.01;
end
if ~exist('maxit','var') ||  isempty(maxit)
   maxit = 20;
end
if ~exist('pfun','var')
   pfun = [];
end
if ~exist('tfun','var')
   tfun = [];
end
if ~exist('obnl','var') || isempty(obnl) 
    obflag = false;
else
    obflag = true; % indicates that obn is supplied
    pfun = [pfun obnl]; % add to node variables
end
if ~exist('ifix','var')
   ifix = [];
end

[p,t,pfun,tfun] = fixmesh(p,t,pfun,tfun); % Ensure consistent mesh

n = size(p,1);
S = sparse(t(:,[1,1,2,2,3,3]),t(:,[2,3,1,3,1,2]),1,n,n);                   % Sparse connectiity matrix
W = sum(S,2);
if any(W==0)
   error('Invalid mesh. Hanging nodes found.');
end

edge = [t(:,[1,2]); t(:,[1,3]); t(:,[2,3])];                               % Non-unique edges
edge = sortrows(sort(edge,2));                                             % Put shared edges next to each other
idx = all(diff(edge)==0,2);                                                % Find shared edges
idx = [idx;false]|[false;idx];                                             % True for all shared edges
bnde = edge(~idx,:);                                                       % Boundary edges
edge = edge(idx,:);                                                        % Internal edges
edge = [bnde; edge(1:2:end-1,:)];                                          % Unique edges
bnd = unique(bnde);                                                        % Boundary nodes

%mk make obn neighbours fixed as well
if obflag
    obn = find(pfun(:,end)); % may have changed after fixmesh
    pfun(:,end) = [];
    obnn = unique(edge(any(ismember(edge,obn),2),:)); % obn and their neighbours
    bnd = unique([bnd; obnn]);
end

bnd = unique([bnd; ifix]);

L = max(sqrt(sum((p(edge(:,1),:)-p(edge(:,2),:)).^2,2)),eps);              % Edge length

for iter = 1:maxit
   pnew = (S*p)./[W,W];                                                    % Laplacian smoothing
   pnew(bnd,:) = p(bnd,:);                                                 % Dont let BND nodes move
   p = pnew;

   Lnew = max(sqrt(sum((p(edge(:,1),:)-p(edge(:,2),:)).^2,2)),eps);        % Edge length
   move = norm((Lnew-L)./Lnew,inf);                                        % Percentage change
   if move<tol
      break
   end
   L = Lnew;
end
if iter==maxit
   disp('WARNING: Maximum number of iterations reached, solution did not converge!');
end

end      % smoothmesh()
