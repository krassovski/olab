function ebr = grid_bridge(tri)
% "Bridge" edges (connecting two non-consequitive boundary nodes).

[ebnd,eint] = grid_edge(tri);
bn = unique(ebnd);
ebr = eint(sum(ismember(eint,bn),2)==2,:);