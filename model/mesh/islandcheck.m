function [r,ixisl,gapp] = islandcheck(r,par,or,coordtype)
% Check if the segmented curve is an island (closed curve) and orient
% clockwise if or ~= -1 and counter-clockwise if or == -1.
% r         same as r but islands are correctly oriented (but may not be closed)
% ixisl     for each r ixisl is true if it is an island
% gapp      for each r, connection edge: [x(1) y(1) x(end) y(end)]

if or ~= -1
    or = 1;
end

nr = length(r);
gapp = NaN(nr,4);
ixisl = false(nr,1);

for ix = 1:nr
    x = r(ix).x(1:end-1); % discard the last point (NaN)
    y = r(ix).y(1:end-1);

    if length(x) < 3
        ixisl = false;
        return
    end

    % % ixisl = x(1) == x(end) & y(1) == y(end); 
    % if strncmpi(coordtype,'geo',3)
    %     d = m_lldist([x(1) x(end)], [y(1) y(end)]); 
    % else
    %     d = sqrt((x(1)- x(end)).^2 + (y(1)-y(end)).^2);
    % end
    % ixisl = d<=tres;

    % take 1% of the smallest distance between conseq. nodes as a threshold
    if strncmpi(coordtype,'geo',3)
        if isempty(which('m_lldist'))
            olab_addpath('plot\m_map1.4') % m_lldist
        end
        d = m_lldist([x x(1)], [y y(1)]); 
    else
        d = sqrt(diff([x x(1)]).^2 + diff([y y(1)]).^2);
    end
    ixisl(ix) = d(end)<min(d(1:end-1))*0.01;

    % check for orientation
    if ixisl(ix)
        if d>0
            gapp(ix,:) = [x(1) y(1) x(end) y(end)];
        end
        [xp,yp] = seg_parallel([x(1) x(2)], [y(1) y(2)], or, eps*par); 
                          % par is empirically chosen (doesn't work for d<=eps*100)
        if inpolygon(min(xp)+abs(diff(xp))/2, min(yp)+abs(diff(yp))/2, x, y)
    %             % plot to check %%%%%%%%%%%%%
    %             plot(x,y,'.-')
    %             text(x,y,num2str([1:length(x)]'))
    %             hold on
    %             plot(xp,yp,'.:k')
    %             hold off

            r(ix).x = [fliplr(x) NaN]; % flip and move NaN to the end
            r(ix).y = [fliplr(y) NaN];
        end
    end
end