function [x,y,z,code,nnum,con,offset_scale,comment] = readngh(fname)
% Read Trigrid NGH file.

fid = fopen(fname,'r');

newflag = false;
comment = [];
while 1
    tline = fgetl(fid);
    if ischar(tline) & tline(1) == '#'
        if strcmp(tline,'#NGH')
            newflag = true;
        end
        comment = [comment; {tline}];
    else
        break
    end
%     if ~ischar(tline) | tline(1) ~= '#',   break,   end
end

if newflag
    offset_scale = str2num(tline); % read limits here
    n = str2num(fgetl(fid));
    ncon = str2num(fgetl(fid));
else
    n = str2num(tline);
    ncon = str2num(fgetl(fid));
    offset_scale = str2num(fgetl(fid)); % read limits here
end

% determine number of columns
dpos = ftell(fid);
% count sequensies of non-whitespace characters:
ncon = length(regexp(fgetl(fid), '\S*', 'match'))-5;
fseek(fid,dpos,'bof');

% read data
formatstr = ['%d %f %f %d %f' repmat(' %d',1,ncon)];
% formatstr = ['%f %f %f %f %f' repmat(' %f',1,ncon)]; % read as double 
     % % con2tri doesn't work with doubles
C = textscan(fid, formatstr, n);

nnum = C{1};
x = C{2};
y = C{3};
z = C{5};
code = C{4};
con = [C{6:ncon+5}];

fclose(fid);