function [x,y,wrn] = moveclose(x,y,xr,yr,tol)
% Move nodes in x,y which are close to any node in xr,yr to match the close
% nodes.

if ~exist('tol','var') || isempty(tol)
    % default tolerance is 1% of the minimum edge length in x,y
    tol = 1e-8;
end

wrn = 0; % no warning by default

n = length(x);
for in = 1:n
    d = ((xr-x(in)).^2 + (yr-y(in)).^2).^0.5;
    iclose = find(d<tol & d>0);
    nc = length(iclose);
%     if nc==0 % check for no match first - the most probable outcome, saves time
%         continue
%     else
    if nc>0
        if nc>1
            wrn = 'Close node pairs found in the reference dataset.';
            iclose = iclose(1);
        end
        x(in) = xr(iclose);
        y(in) = yr(iclose);
        disp(['Node moved: ' num2str(in)]) %%%%%%%%%%
    end
end