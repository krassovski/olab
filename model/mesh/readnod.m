function [xyzc,xyzu,s] = readnod(fname)
% Read Trigrid NOD file. 
% Should work for both old and new format (untested for new format).
% xyzc  ordered (contour or boundary) nodes separated by rows of NaNs
%       by the Trigrid convention, 1st boundary is the open boundary and
%       oriented counterclockwise, the rest of the boundarieds are islands and
%       oriented clockwise.
% xyzu  unordered nodes, e.g. internal or scattered bathymetry nodes
% s     offset and scale

fid = fopen(fname,'r');

tline = fgetl(fid);
if tline(1) == '#'
    C = textscan(fid, '%d %d %d %d %d', 1); % read x0,y0,xscale,yscale,igridtype here
    s.x0 = C{1}; s.y0 = C{2}; s.xscale = C{3}; s.yscale = C{4}; s.gridtype = C{5};
    
    ntotal = str2double(fgetl(fid));
    
    Nb = textscan(fid, '%d %d', 1);
    nb = Nb{1};
else
    s = [];
    ntotal = str2double(tline);
    nb = str2double(fgetl(fid));
end

fgetl(fid); % skip number of nodes for the 1st contour
xyz = cell2mat(textscan(fid, '%f %f %f'));
    % this leaves the number of nodes for each contour in the 1st column and NaNs in
    % the 2nd and 3rd columns

fclose(fid);

if size(xyz,1)-nb ~= ntotal % number of nodes for the 1st boundary was skipped
    warning('Total number of nodes does not agree.')
end

if nb == 0 % all nodes are unassigned
    xyzc = zeros(0,3); % Empty matrix: 0-by-3
    xyzu = xyz;
else
    inan = find(isnan(xyz(:,2))); % NaNs in the 2nd column
    xyz(inan,1) = NaN; % put terminating NaNs in the 1st column
    if length(inan) == nb-1 % no unassigned nodes
        xyzc = [xyz; NaN(1,3)]; % terminate the last contour
        xyzu = zeros(0,3); % Empty matrix: 0-by-3
    else
        xyzc = xyz(1:inan(end),:); % including the last terminating NaN
        xyzu = xyz(inan(end)+1:end,:); % everything after the last terminating NaN
    end
end