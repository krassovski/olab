function [rmw,rlw,rhw] = coast_mwl_x(rlw,rhw,zlw,zhw)
% Find mean water level line interpolating between LW and HW lines.
% All contours must be closed, i.e. islands.
% !!! rlw should not be nested !!! i.e. "main coast" should be processed
% separately.
% This version of the coast_mwl deals with an imperfect coastlines, e.g.
% those which may cross each other as a result of being simplified
% independently.
% Coordinates are 'xy' (equal axes scale).
% zlw,zhw   difference between water line level and MWL, if zlw==zhw (default)
%           interpolation is performed mid-way between LW and HW.
% Returns:
% rmw       interpolated segments
% rlw,rhw   "orphan" lw and hw segments, i.e. lw with no hw inside
%           and hw not inside of any lw contours

if isempty(rlw)
    return
end

if ~exist('zlw','var') || isempty(zlw); zlw = 1; zhw = -1; end

anynodeflag = true;

% initialize with empty element
rmw = rlw(1);
rmw(1) = []; % fields remain as in rlw

% deal with islands one at a time
% all orphan HW islands are returned in rhw
ldel = true(size(rlw));
for ix = 1:length(rlw)
    disp(['========== Island:  ' num2str(ix) ' of ' num2str(length(rlw))])
    
    % find enclosed HW islands
    in = coast_islandsin(rhw, [rlw(ix).x(1:end-1)' rlw(ix).y(1:end-1)'],anynodeflag);
    
    if any(in)
        rmw = [rmw coast_islandinterp_x(rlw(ix),rhw(in),zlw,zhw)]; %#ok<AGROW>
        %%%%% loops are removed in coast_islandinterp_x: can we loose
        %%%%% "satellite islands"?
        rhw(in) = []; % discard used HW segments
    else
        % no HW, nothing to interpolate between
        ldel(ix) = false; % save this LW segment to return
    end
end
rlw(ldel) = []; % discard used LW segments, keep "orphans" to return


function rmisl = coast_islandinterp_x(rlisl,rhisl,zlw,zhw)

rlisl = islandcheck(rlisl,1e11,-1,'xy'); % orient counter-clockwise
rhisl = islandcheck(rhisl,1e11,-1,'xy'); % orient counter-clockwise

try
% do interp for broken contour
[rmisl,rmisl_] = landinterp_v3proj(rlisl,rhisl,zlw,zhw); %%%%%%%%%%
%%%%% if only closed contours are accepted, then we can repair HW by moving
%%%%% outside node to the LW polygon and then use the regular interpolation
%%%%% routine (from Kyuquot)
catch
    '';
end

if length(rmisl)~=1
    '';
end

% close the island by joining start and end nodes (can produce sharp angles
% when start and end are overshooting)
if rmisl.x(1) ~= rmisl.x(end-1) || rmisl.y(1) ~= rmisl.y(end-1)
    rmisl.x = [rmisl.x(1:end-1) rmisl.x(1) NaN];
    rmisl.y = [rmisl.y(1:end-1) rmisl.y(1) NaN];
end

% ensure clockwise orientation
rmisl = islandcheck(rmisl,1e11,0,'xy');

rmisl = [rmisl rmisl_];