function [p,tri,z,code,perr] = grid_cleave_n(p,tri,icl,z,code)
% Cleave nodes with indices icl.
% p     [x y] node coordinates
% tri   triangulation array (see dalaunay)
% icl   nodes to cleave (numerical indices into p)
% code  node codes; the new nodes are coded same as the cleaved ones
% Returns updated p,tri,z,code.

perr = [];

n = length(icl);
% hwait = waitbar(0,'Cleaving: 0% done. ETA: ','CreateCancelBtn','delete(gcbf)');
% tst = now;
for ic = 1:n % cleave nodes one at a time
    [p,tri,z,code,err] = grid_cleave(p,tri,icl(ic),z,code);
    if ~err
        icl = icl-1; % one was removed, new nodes are always at the end
    else
        perr(end+1,:) = p(icl(ic),:);
    end
    
%     pdone = ic/n;
%     eta = tst + (now-tst)/pdone;
%     waitbar(pdone,hwait,['Cleaving: ' num2str(pdone*100,'%2.2f') '% done. ETA: ' datestr(eta)])
end
% delete(hwait);