function ib = grid_boundarynodes(tri)
% Identify boundary nodes (all outer and "island" boundary nodes in bulk (not
% connected consequitevely).

% make each edge a row; array is sorted in both dimensions:
e = sortrows( sort([tri(:,[1,2]); tri(:,[1,3]); tri(:,[2,3])],2) );

idx = all(diff(e,1)==0,2);     % Find shared edges
idx = [idx;false]|[false;idx]; % True for all shared edges
ebnd = e(~idx,:);              % Boundary edges
ib = unique(ebnd);