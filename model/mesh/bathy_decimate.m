function ir = bathy_decimate(x,y,r,m,convertzone)
% Decimate samplings within the radius r from each point in x,y.
% If m supplied, break the dataset into pieces of m values, process
% separately each piece and combine after bathy_dominate with radius of
% suppression r. This approach assumes continous pieces in the list x,y
% represent clouds of points which are closest on the plain, i.e. not
% scattered randomly all over the area. This will also leave points closer
% than r at the seams between pieces.
% iremove   logical indices (in x,y) of points to remove.
% convertzone   use this to supply UTM zone to convert to in case x,y are
%               geographic coordinates; 0 means use the default zone; 
%               omitted, empty, or NaN produces no conversion: (x,y) should
%               be in units of r (e.g. metres).

% create a waitbar
hwait = waitbar(0,'Decimating bathymetry. 0% done. ETA: ','CreateCancelBtn','delete(gcbf)');
tst = now;

n = length(x);
if ~exist('m','var') || isempty(m) || isnan(m)
    m = n; % process entire series by default
end
nm = ceil(n/m); % number of pieces to break into

if ~exist('convertzone','var') || isempty(convertzone) || isnan(convertzone)
    convertzone = []; % no UTM conversion by default
else
    olab_addpath('geodesy')
end

ir = false(size(x));
for im = 1:nm
    iv = 1 + m*(im-1) : min(n,m*im); % current piece indices in the x,y array
    pwait = [iv(1)-1 length(iv)]/n; % start point and fraction of the waitbar for this piece
    ircur = bathy_decimate_entire(x(iv),y(iv),r,hwait,tst,pwait,convertzone);
%     ircur = bathy_decimate_entire_alg2(x(iv),y(iv),r,hwait,tst,pwait,convertzone);
    
    if isnan(ircur)
        ir = NaN; % Cancel was pressed on the waitbar
        return
    else
        ir(iv) = ircur;
    end
end

delete(hwait);


function iremove = bathy_decimate_entire(x,y,r,hwait,tst,pwait,convertzone)
% Consecutively remove all points within radius r from each point in x,y.

if ~isempty(convertzone)
    [x,y] = geo2utm(y,x,convertzone);
end

n = length(x);
i = 1;
dchk = n*0.01; % waitbar step in num of values
chkdone = dchk; % next checkpoint for waitbar
while ~isempty(i) && i<n
%     disp([' bathy_decimate: ' num2str(i) ' of ' num2str(n)]) %%%%%
    d = ((x(i+1:end)-x(i)).^2 + (y(i+1:end)-y(i)).^2).^0.5; % dist from 1st point to all the rest
    ir = i + find(d<r);
    x(ir) = NaN;
    y(ir) = NaN;
    i = i + find(~isnan(x(i+1:end)),1,'first'); % empty if no more valid nodes after previous i
    
    if ~ishandle(hwait) % waitbar Cancel pressed
        iremove = NaN;
        return
    end
    
    if i>chkdone
        pdone = pwait(1) + pwait(2)*chkdone/n;
        eta = tst + (now-tst)/pdone;
        waitbar(pdone,hwait,['Decimating... ' num2str(pdone*100,'%2.2f') '% done. ETA: ' datestr(eta)])
        chkdone = chkdone+dchk;
    end
end
iremove = isnan(x);