function [x,y,tri,z,code] = grid_removenodefixhole(x,y,tri,ir,z,code) % ,ib
% Remove one node and fix the resulting hole by triangulation so that the grid
% is still good.
% X,Y   node coordinates
% TRI   triangulation TRI array (see
%       dalaunay).
% IR    numerical index of the node to remove (index in X,Y)

%%%%% This is not implemented:
% % IB    numerical indices of boundary nodes (optional); if supplied then tries
% %       to avoid creating edges connecting boundary segments ("bridges")
% 
% % if ~exist('ib','var') || isempty(ib)
%     nobridgesflag = false;
% else
%     nobridgesflag = true;
% end

% ensure column vectors
[nrow,ncol] = size(x);
if ncol~=1
    x = x';
    y = y';
end

if islogical(ir)
    ir = find(ir);
end
if length(ir)>1
    warning('Only one node at a time can be deleted. Using 1st specified index.')
    ir = ir(1);
end

[c,itrin] = grid_polynei(tri,ir);

% if boundary node is being removed, exclude it from the list
c(c==ir) = [];

% if size(con,1) ~= length(x)
%     if size(con,2) ~= 3
%         error('Third argument should be a connectivity array (see NGH format) or TRI array (see dalaunay).')
%     end
%     % triangle array specified
%     triflag = true;
%     tri = con;
%     [irow,icol] = find(tri==ir);
%     c = tri(irow,:);
%     con = tri2con(c,length(x)); % temporary assignment for forming a polygon to triangulate
% %     con = ele2nei(c,x,y); % faster equivalent %%%%% works only on "consistent" grids
%     c = setdiff(unique(c),ir); % connections of the deleted node
% else % connectivity array specified
%     triflag = false;
%     c = con(ir,:); % connections of the deleted node
%     c = c(c~=0);
% end
% 
% % form a polygon to triangulate
% nc = length(c);
% p = c(1);
% c(1) = [];
% for ic = 2:nc
%     [nextcon,gbg,ifound] = intersect(con(p(ic-1),:), c);
%     if isempty(nextcon) % if boundary node is being deleted and we came to a neighbouring boundary node
%         p = fliplr(p); % go the other way from the initial polygon vertex
%         [nextcon,gbg,ifound] = intersect(con(p(ic-1),:), c);
%         if ~isempty(nextcon) % if we are not at the second boundary node
%             p(ic) = nextcon(1);
%             c(ifound(1)) = [];
%         end % otherwise do nothing: this must be the last iteration
%     else
%         p(ic) = nextcon(1);
%         c(ifound(1)) = [];
%     end
% end
% c = p;

% t = delaunay(x(c),y(c)); % triangulate the polygon of the nodes connected to the deleted node
% in = edgein(x(c),y(c),t,x(c),y(c));
% t(~in,:) = []; % remove triangles with any edges outside the triangulated polygon

nc = length(c);
t = cdt_mk([x(c) y(c)],[x(c) y(c)],[(1:nc)' [2:nc 1]']); % constrained delaunay

tri(itrin,:) = []; % delete old triangles
c = c'; % make it a row, necessary for the case of one new triangle
tri = [tri; c(t)]; % append new triangles

tri(tri>ir) = tri(tri>ir) - 1; % adjust indices
x(ir) = []; % remove the node
y(ir) = [];
z(ir) = [];
code(ir) = [];

% return to original shape
if ncol~=1
    x = x';
    y = y';
end