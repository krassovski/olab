function [ebnd,eint,tib] = grid_edge(t)
% Get the boundary and internal edges in a triangulation t.
% Based on getedges in meshpoly by Darren Engwirda.
% tib   indices of triangles containing at least one boundary edge.

% % use this to get all unique edges with each consequitive 3 edges being part of
% % same triangle:
% e = sort([t(:,[1,2]); t(:,[1,3]); t(:,[2,3])],2); % each edge is a row, sorted in both dimensions
% % e = sort(reshape(t(:,[1 2 2 3 3 1])',2,[])', 2); %%%%%mk: same, what is faster?
% eu = unique(e); % all unique edges; each consequitive 3 edges are part of same triangle
% e = sortrows(e);

% make each edge a row; array is sorted in both dimensions:
[e,esort] = sortrows( sort([t(:,[1,2]); t(:,[1,3]); t(:,[2,3])],2) );
% e = sortrows( sort(reshape(t(:,[1 2 2 3 3 1])',2,[])', 2) ); %%%%%mk: same, what is faster?

idx = all(diff(e,1)==0,2);     % Find shared edges
idx = [idx;false]|[false;idx]; % True for all shared edges
ebnd = e(~idx,:);              % Boundary edges
eint = e(idx,:);               % Internal edges
eint = eint(1:2:end-1,:);      % unique internal edges

% triange indices
ti = (1:size(t,1))';
ti = [ti; ti; ti];
ti = ti(esort);
% tib = ti(~idx); % indices of triangles corresponding to ebnd (as in Tools/Geometry/Triangulation/tri_edge.m)
tib = unique(ti(~idx)); % indices of triangles containing at least one boundary edge


% % another version (1.5 times slower)
% ii=[t(:,1); t(:,2); t(:,3)];
% jj=[t(:,2); t(:,3); t(:,1)];
% a = sparse(ii,jj,1);
% a = a+a';
% [eb1,eb2] = find(a==1);
% [ei1,ei2] = find(a==2);
% eint = unique(sort([ei1 ei2],2),'rows');
% ebnd = unique(sort([eb1 eb2],2),'rows');