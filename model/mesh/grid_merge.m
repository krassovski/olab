function [p,t,z,code,in1,in2,wrn] = grid_merge(p1,t1, p2,t2, z1,z2, code1,code2)
% Merge grids at coincident nodes. New triangles are generated where nodes
% of one grid fall on the coincident edges of another grid.
% z1,code1 override z2,code2 in the resulting grid.
% Grid overlap test is performed and node indices of nodes of one grid
% within another grid are returned in in1 and in2. In case of grid overlap
% empty arrays for p,t,z,code are returned.

% adjust positions for very close boundary nodes
e1 = grid_edge(t1); % boundary edges
e2 = grid_edge(t2);
% set tolerance at 10% of the minimum edge length in Grid 2
tol = 0.10 * min(sum((p2(e2(:,1),:) - p2(e2(:,2),:)).^2,2).^.5);
bn1 = unique(e1(:)); % boundary nodes
bn2 = unique(e2(:));
[p2(bn2,1),p2(bn2,2),wrn] = moveclose(p2(bn2,1),p2(bn2,2),p1(bn1,1),p1(bn1,2),tol);

% split edges where necessary
if nargout>2
    [p1,t1,p2,z1,code1] = grid_matchseam(p1,t1,p2, z1,z2, code1); % in Grid 1
    [p2,t2,p1,z2,code2] = grid_matchseam(p2,t2,p1, z2,z1, code2); % in Grid 2
else
    [p1,t1,p2] = grid_matchseam(p1,t1,p2); % in Grid 1
    [p2,t2,p1] = grid_matchseam(p2,t2,p1); % in Grid 2
end

% grid overlap test %%%%% tsearch works only for convex shapes, use inpoly
e = grid_edge(t2); % boundary edges in Grid 2
[inp, onp] = inpoly(p1, p2, e);
% %%% inpoly sometimes incorrectly determines nodes on the vertices as in, but not on; 
% %%% use inpolygon with the outer boundary loop instead of inpoly
% loop = edge_extract_loop(e); %%%% assuming it pickes up the OUTER boundary loop
% [inp,onp] = inpolygon(p1(:,1),p1(:,2), p2(loop,1),p2(loop,2));
% in1 = find(inp & ~onp, 1);
in1 = find(inp & ~onp); % show all overlapping nodes

e = grid_edge(t1); % boundary edges in Grid 1
[inp, onp] = inpoly(p2, p1, e);
% loop = edge_extract_loop(e); %%%% assuming it pickes up the OUTER boundary loop
% [inp,onp] = inpolygon(p2(:,1),p2(:,2), p1(loop,1),p1(loop,2));
% in2 = find(inp & ~onp, 1);
in2 = find(inp & ~onp); % show all overlapping nodes

if ~isempty(in1) || ~isempty(in2)
    p = []; t = []; z = []; code = [];
    return
end


[p,t] = fixmesh([p1; p2],[t1; t2+size(p1,1)]); % merge

% adjust z and code
if nargout>2
    z = zeros(size(p,1),1);
    code = zeros(size(p,1),1);
    
    % do Grid 2 first, so Grid 1 values will overwrite Grid 2 values in the
    % combined arrays
    [i2,i2,i22] = intersect(p,p2,'rows'); %#ok<ASGLU>
    z(i2) = z2(i22);
    code(i2) = code2(i22);
    
    [i1,i1,i11] = intersect(p,p1,'rows'); %#ok<ASGLU>
    z(i1) = z1(i11);
    code(i1) = code1(i11);
end


% % there is a bug in the following
% [gbg,c2,c1] = intersect(p2,p1,'rows'); % indices of common nodes in p2
% [c2,isort] = sort(c2,'descend');
% c1 = c1(isort);
% 
% p2(c2,:) = []; % remove common points
% 
% % shift indices for t2 and c2
% n1 = size(p1,1);
% c2 = c2 + n1;
% t2 = t2 + n1;
% 
% % adjust indices in triangles array
% for ic = length(c2):-1:1
%     t2(t2==c2(ic)) = c1(ic);
%     t2(t2>c2(ic)) = t2(t2>c2(ic)) - 1;
% end
% 
% p = [p1; p2];
% t = [t1; t2];


function [p1,t1,p2,z1,code1] = grid_matchseam(p1,t1,p2, z1,z2, code1)
% Split edges in Grid 1 which have nodes from Grid 2 on them.

codeint = 0; % code for internal nodes

% match coincident boundaries by splitting edges with extra nodes in the
% other grid
e1 = grid_edge(t1); % boundary edges in Grid 1
[c1,c1,c2] = intersect(p1,p2,'rows'); %#ok<ASGLU> % common nodes

lc2 = false(size(p2,1),1);
lc2(c2) = true; % convert to logical

ec1 = e1(sum(ismember(e1,c1),2)==2,:); % coincident edges (both nodes are common)

% tolerance for on-segment test is 1e-3 of the shortest edge in p1's seam
tol = 1e-3 * min(sum((p1(ec1(:,1),:) - p1(ec1(:,2),:)).^2,2).^.5);

np = size(p1,1); % keep track of number of points in p1
lrem = false(size(t1,1),1);
for iec = 1:size(ec1,1)
    ecc = ec1(iec,:); % currently processed edge
    [dis,pos,proj] = seg_dist(p1(ecc(1),:)', p1(ecc(2),:)', p2');
    lins = dis<tol & ~lc2;
    if any(lins)
        ispl = find(sum(ismember(t1,ecc),2)==2); % triangle to split
        lrem(ispl) = true; % mark triangle to split
        n3 = setdiff(t1(ispl,:),ecc); % 3rd node in the triangle to split
        
        pos = pos(lins);
        proj = proj(lins,:);
        p2(lins,:) = proj; % correct positions in p2 (before sorting proj!)
        [isort,isort] = sort(pos); %#ok<ASGLU>
        proj = proj(isort,:);
        nins = length(pos); % number of nodes to insert
        tnew = [repmat(n3,nins+1,1) [ecc(1); np+(1:nins)'] [np+(1:nins)'; ecc(2)]];
        t1 = [t1; tnew]; %#ok<AGROW> % append new triangles
        p1 = [p1; proj]; %#ok<AGROW> % append new nodes
        np = np + nins; % update number of nodes
        if nargout>3
            %%% keeping z from p2 (is it better to interpolate them on the p1's ecc?)
            z1 = [z1 z2(lins)]; %#ok<AGROW>
            % all new nodes are internal
            code1 = [code1 repmat(codeint,1,nins)]; %#ok<RPMT0,AGROW>
        end
    end
end
t1(find(lrem),:) = []; %#ok<FNDSB>