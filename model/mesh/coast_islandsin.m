function [in,isfullyin] = coast_islandsin(r, poly, anyflag)
% Logical indices of coastal segments r enclosed in polygon poly.

if ~exist('anyflag','var') || isempty(anyflag); anyflag = false; end

in = repmat(false,size(r));  % initialize
isfullyin = in; % initialize
for ix = 1:length(r)
    if ~isempty(r(ix).x)
        inp = inpolygon(r(ix).x(1:end-1),r(ix).y(1:end-1),poly(:,1),poly(:,2));
        if all(inp)
            isfullyin(ix) = true;
        end
        if any(inp)
            in(ix) = true;
        end
    end
end
if ~anyflag
    in = isfullyin;
end