function [c,ltn] = grid_polynei(tri,icl)
% Extract a polygon c consisting of nodes neighbouring node icl from
% triangilation array tri. Also returns indices of triangles containing icl.

ltn = any(tri==icl,2); % logical indices of neighbouring triangles
trin = tri(ltn,:);
ebnd = grid_edge(trin); % boundary edges only

% form a polygon to triangulate (edges in consequitive order)
ne = size(ebnd,1);
c = zeros(ne,1); % initialization: c contains indices into p
enext = ebnd(1,:);
c(1:2) = enext'; % 1st edge
n1 = enext(1); % start of the current edge
n2 = enext(2); % end of the current edge
for ie = 3:ne
    enext = ebnd(any(ebnd==n2,2) & all(ebnd~=n1,2), :); % should be only one such edge
    if isempty(enext)
        c = 0;
        return % return 0 on error
    end
    n1 = n2;
    n2 = enext(enext~=n2);
    c(ie,:) = n2;
end