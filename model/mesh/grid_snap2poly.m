function [p,tri,z,code] = grid_snap2poly(po,trio,zo,codeo,poly,codesub)

[p,tri,nremoved,nkept] = tri_snap2poly(po,trio,poly);
% number of nodes may have been reduced due to node merging in tri_snap2poly
% only internal edges should have been merged
if any(codeo(nremoved)) || any(codeo(nkept))
    warning('Check the algorithm...')
end
codes = codeo;
codes(nremoved) = []; % codes now corresponds to p in length
zs = zo;
zs(nremoved) = []; % zs now corresponds to p in length

[~,im] = setdiff(p,po,'rows'); % indices of changed nodes

% substitute codes according to the table
if ~isempty(codesub)
    for is = 1:size(codesub,1)
        isub = codes(im)==codesub(is,1);
        codes(im(isub)) = codesub(is,2);
    end
end

% interpolate depths
trio = double(trio); % neigbouring triangles (containing new nodes)
k = tsearchsafe_mk(po(:,1),po(:,2),trio,p(im,1),p(im,2));
zs(im) = tinterp(po, trio, zo(:), p(im,:), k); % zo now corresponds to p in length

% zero-area triangles can appear as a result of the cut; fix this
[p,tri,z,code] = grid_fix(p,tri,zs,codes);
% [pf,tri] = fixmesh(p,tri); % removes zero-area triangles
% [~,ic,io] = intersect(pf,p,'rows');
% code = zeros(1,size(pf,1));
% code(ic) = codes(io);
% z = zeros(1,size(pf,1));
% z(ic) = zs(io);
% p = pf;


function [p,tri,nremoved,nkept] = tri_snap2poly(p,tri,poly)
% Move nodes on grid edges that intersect the polygon to the polygon edge.
% Used in grid_cut.

x = p(:,1); y = p(:,2);

[ebnd,eint] = grid_edge(tri);
ipb = unique(ebnd);
ged = [ebnd; eint]; % all edges
ibe = 1:size(ebnd,1);

% ipb = grid_boundarynodes(tri); % identify boundary nodes (these are not to be moved)
% 
% % create array of grid edges
% ged = reshape(tri(:,[1 2 2 3 3 1])',2,[])'; % each edge is a row
% 
% % identify boundary edges (occur only once)
% ged = sort(ged,2); % sort each row
% [iq,iq] = unique(ged,'rows');
% inq = setdiff(1:size(ged,1),iq);
% [gedb,ibe] = setdiff(ged(iq,:),ged(inq,:),'rows'); % edges that occur only once
% ibe = iq(ibe); % indices into the edge array, ged
% % itr = ceil(ibe/3);   % convert to triangle indices

npoly = size(poly,1);
for ie = 1:npoly-1 % work each edge of the polygon
    iignore = ...
    all(x(ged')>max(poly(ie,1),poly(ie+1,1))) |...
    all(x(ged')<min(poly(ie,1),poly(ie+1,1))) |...
    all(y(ged')>max(poly(ie,2),poly(ie+1,2))) |...
    all(y(ged')<min(poly(ie,2),poly(ie+1,2)));
    if all(iignore)
        continue
    end
    
    px = intersectEdges([p(ged(~iignore,1),:) p(ged(~iignore,2),:)], [poly(ie,:) poly(ie+1,:)]);
    iposs = find(~iignore);
    ipx = ~isnan(px(:,1));
    if all(~ipx)
        continue
    end
    iex = iposs(ipx); % indices of grid edges that intersect the polygon edge
    iexb = intersect(iex,ibe); % boundary edges that intersect the polygon edge
    px = px(ipx,:);
    cx = px(:,1); % crossing coordinates
    cy = px(:,2);
    
    % end points of intersecting edges
    ix1 = ged(iex,1);
    ix2 = ged(iex,2);
    x1 = x(ix1); y1 = y(ix1);
    x2 = x(ix2); y2 = y(ix2);
    
    % distances to polygon crossing
    d1 = sqrt((x1-cx).^2 + (y1-cy).^2);
    d2 = sqrt((x2-cx).^2 + (y2-cy).^2);
    
    % nodes to move
    ipm = [ix1(d1<d2); ix2(d1>=d2)];
%     ipm = count(ipm);
%     nc = ipm(:,2);
%     ipm = ipm(:,1);
    ipm = unique(ipm); % no need to count when using the simple algorithm
    
    % deal with boundary nodes - move along the boundary edge
%     ibpm = find(mod(nc,2)==1); % odd nc - boundary node
    [ipmb,iipmb] = intersect(ipm,ipb); % boundary nodes to move
    for iib = 1:length(ipmb)
        [fm,~] = find(ged(iex,:)==ipmb(iib)); % fm - crossing edges that include the node to move
        [~,ifm] = intersect(iex(fm),iexb); % take the boundary edge
        %%%%%%%%%%%% what if there are several boundary edges with this node
        %%%%%%%%%%%% that cross the polygon???
        if isempty(ifm) % a boundary is near-by but not crossing the polygon
            % add the other end-points of the crossing edges to the node list to
            % move
            ipmadd = ged(iex(fm),:);
            ipmadd = setdiff(ipmadd(:),ipmb(iib));
            % append at the end not to mess up the list
            for iipmadd = 1:length(ipmadd)
                if all(ipm~=ipmadd(iipmadd))
                    ipm(end+1) = ipmadd(iipmadd);
                end
            end
        else
            p(ipmb(iib),:) = px(fm(ifm(1)),:);
        end
    end
%     nc(iipmb) = [];
    ipm(iipmb) = [];
    
    % for the interior nodes: move to the proj point on the polygon edge
    [~, ~, p(ipm,:)] = seg_dist(poly(ie,:)',poly(ie+1,:)', p(ipm,:)');
  
%     %%% fancier algorythm:
%     % each non-boundary edge occurs twice
%     nc = nc/2;
%     
%     % for nodes with multiple intersections (nc>1), move them to their
%     % projection on the ploygon edge
%     im = find(nc>1);
%     [gbg, gbg, p(ipm(im),:)] = seg_dist(poly(ie,:)',poly(ie+1,:)', p(ipm(im),:)');
%     ipm(im) = [];
%     
%     % nodes with single intersecting edge: move the OUTSIDE node along the
%     % edge to the crossing point
%     gedx = ged(iex,:);
%     
%     for iipm = 1:length(ipm) % move nodes to the polygon
%         [fm,gbg] = find(ged(iex,:)==ipm(iipm)); % fm - crossing edges that include the node to move
%         inodein = inpoly(p(ged(iex(fm)),:),poly);
%         %%%%%%%%%% UNFINISHED HERE
%     end
end

% deal with internal zero-area triangles by merging nodes belonging to the
% shortest edge in the triangle; boundary zero-area triangles will be deleted by
% fixmesh in grid_snap2poly
[p,tri,nremoved,nkept] = tri_merge0area(p,tri);