function [p,tri,z,code,err] = grid_cleave(p,tri,icl,z,code)
% Cleave node with index icl in triangular grid [p,tri].
% Cleave allowed only on internal nodes.
% p     [x y] node coordinates
% tri   triangulation array (see dalaunay)
% n     cleave nodes with more than n neighbours
% z     function of (x,y), e.g. z-coordinate
% code  node codes; the new nodes are assigned the same code the cleaved node
% Returns updated p,tri,z,code.

% calls routines in: addpath('D:\Max\Projects\Femgrids\Tools\geom2d')

err = false;

np = size(p,1);

[c,itrin] = grid_polynei(tri,icl);
if ~c
    err = true;
    return % return unchanged grid and a positive error flag
end
    
% ltn = any(tri==icl,2); % logical indices of neighbouring triangles
% trin = tri(ltn,:);
% ebnd = grid_edge(trin); % boundary edges only
% 
% % form a polygon to triangulate (edges in consequitive order)
% ne = size(ebnd,1);
% c = zeros(ne,1); % initialization: c contains indices into p
% enext = ebnd(1,:);
% c(1:2) = enext'; % 1st edge
% n1 = enext(1); % start of the current edge
% n2 = enext(2); % end of the current edge
% for ie = 3:ne
%     enext = ebnd(any(ebnd==n2,2) & all(ebnd~=n1,2), :); % should be only one such edge
%     if isempty(enext)
%         err = true;
%         return % return unchanged grid and a positive error flag
%     end
%     n1 = n2;
%     n2 = enext(enext~=n2);
%     c(ie,:) = n2;
% end
% % c(end) = c(1); % close the polygon

[pnew,tnew] = grid_fillhole2(p(c,:)); % returns two new nodes and all new triangles

% interpolate depths
trir = double(tri(itrin,:)); % neigbouring triangles (containing new nodes)
k = tsearch(p(:,1),p(:,2),trir,pnew(:,1),pnew(:,2));
z(np+1:np+2) = tinterp(p, trir, z', pnew, k);

% update node arrays: p,z,code
p(np+1:np+2,:) = pnew; % append new nodes to the end of the list
code(np+1:np+2) = code(icl); % append codes for the new nodes to the end of the list
p(icl,:) = []; % remove the cleaved node
code(icl) = []; % and its code
z(icl) = []; % and its z

% update triangle array
tri(itrin,:) = []; % delete old triangles
c(end+1:end+2) = np + [1 2]; % append indices for the new nodes
tri = [tri; c(tnew)]; % append new triangles
tri(tri>icl) = tri(tri>icl) - 1; % adjust indices to account for the removed node


function [pnew,t] = grid_fillhole2(p)
% Place two nodes in the polygon p and triangulate the entire set of nodes.

% principal axis
[EV,ED] = eig(cov(p)); % eigenvectors and eigenvalues of the polygon nodes
pc = polygonCentroid(p);
line = createLine(pc(1), pc(2), EV(2,1), EV(2,2)); % princ. axis going through the centroid
ps = intersectLinePolygon(line, p); % intersection of this line with polygon
if size(ps,1) ~= 2
    error('grid_fillhole2 doesn''t work for this polygon shape.')
end
pnew = ps+(diff(ps)'*[1/3 -1/3])'; % place new nodes on the line at 1/3 of the distance between intersection points

pall = [p; pnew];

t = delaunay(pall(:,1),pall(:,2));

% impose geometry constraints
pc = (pall(t(:,1),:)+pall(t(:,2),:)+pall(t(:,3),:))/3.0;
in = inpolygon(pc(:,1),pc(:,2),p(:,1),p(:,2)); % triangles with internal centroids
t = t(in,:);

% smooth
fixflag = false;
[p,t] = mk_smoothmesh(pall,t,[],[],fixflag);
pnew = p(end-1:end,:);