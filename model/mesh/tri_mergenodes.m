function [p,tri,irem] = tri_mergenodes(p,tri,im)
% Merge two connected nodes in triangular grid p,tri.
% im    two node indices

p(im(1),:) = mean(p(im,:)); % new node midway between the original two
p(im(2),:) = [];

n = sum(ismember(tri,im),2); % count of merging nodes in each triangle
irem = n==2; % triangles to remove
tri(irem,:) = [];

tri(tri==im(2)) = im(1); % replace 2nd node with 1st in tri
tri(tri>im(2)) = tri(tri>im(2))-1; % adjust indices