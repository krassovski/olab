function [p,t,perr] = grid_dekite_tri(p,t,nobridgesflag)
% De-kite the entire grid, i.e. remove all non-boundary nodes with four neighbours.
% p     [x y] node coordinates
% t     triangulation array (see dalaunay)
% nobridgesflag (optional)   if supplied and set to true then de-kite avoids creating edges
%                            connecting boundary segments ("bridges")
% Returns updated p and t, and a set of nodes for which de-kite operation
% failed.

if ~exist('nobridgesflag','var') || isempty(nobridgesflag)
    nobridgesflag = false;
end

perr = [];
while true % iterate until all nn==4 nodes are removed
    
    [ebnd,eint] = grid_edge(t);
    b = unique(ebnd);
    
    eu = [ebnd; eint];
    nodenum = unique(eu); % unique node numbers in t
    nn = histc(eu(:),nodenum); % number of neighbours (number of edges) for each node
    k = nodenum(nn==4); % nodes to remove

    % do not remove boundary nodes
    k = setdiff(k,b);
    
    if isempty(k)
        break % quit if no more nodes to remove
    end

    ptarg = p(k(1),:);
    if nobridgesflag
        [p,t,erm] = grid_dekite_one(p,t,k(1),b);
    else
        [p,t,erm] = grid_dekite_one(p,t,k(1));
    end
    if erm
        perr = [perr; ptarg];
    end
    disp(['de-kite:   ' num2str(length(k)) ' nodes remain to de-kite'])
end