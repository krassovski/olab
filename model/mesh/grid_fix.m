function [p,tri,z,code] = grid_fix(po,trio,zo,codeo)
% Do fixmesh for triangular grid po,trio and adjust z and code for the fixed grid.
% The following checks are performed:
%  1. Nodes not refereneced in tri are removed.
%  2. Duplicate nodes are removed.
%  3. Triangles are ordered counter-clockwise.
%  4. Triangles with an area less than 1.0e-10*eps*norm(A,'inf') are removed

[p,tri,pfun] = fixmesh(po,trio,[zo(:) codeo(:)]); % routine by Darren Engwirda
z = pfun(:,1);
code = pfun(:,2);

% if nargout>2
%     z = zeros(size(p,1),1);
%     code = zeros(size(p,1),1);
%     [im,im,io] = intersect(p,po,'rows');
%     z(im) = zo(io);
%     code(im) = codeo(io);
% end