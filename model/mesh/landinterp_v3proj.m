function [rm,rmi] = landinterp_v3proj(rl,rh,zl,zh)
% One rl (LW contour), multiple rh (HW contours).

z0 = 0; % default value
% ptol = 0.01; % 1% of edge length

polyl = [rl.x(1:end-1); rl.y(1:end-1)]';
% nl = size(polyl,1);

% determne crossing points between LW and HW contours
disp(['Adding x-points   ' datestr(now)])
polyh = [[rh.x]' [rh.y]']; % NaN-separated HW segments
[px,posl,posh] = intersections_nan(polyl, polyh);
%mk: NOTE: for the intersection points which fall on the nodes of polylines,
%mk: coordinates returned in px may differ from original node corrdinates by
%mk: tolerance amount (tol is set to 1e-9 in intersections.m)

% % insert in LW
% ival = abs(posl-round(posl))>1e-9; % exclude existing nodes
% polyl = [polyl; px(ival,:)];
% [gbg,isort] = sort([0:nl-1 posl(ival)']);
% polyl = polyl(isort,:);
    
% nh = size(polyh,1);
nl = size(polyl,1);

% original normalized positions
poslorig = (0:nl-1)';

% original contour edges
eo = [polyl(1:end-1,:) polyl(2:end,:); polyh(1:end-1,:) polyh(2:end,:)];
eo(any(isnan(eo),2),:) = []; % remove invalid edges

% polylo = polyl; % reserve a copy to keep original contour; polyl will be changed below

% projection of LW nodes on the closest HW segment
disp(['Projecting on HW   ' datestr(now)])
[projposl,gbg,projl] = projPointOnPolyline(polyl, polyh);

% load landinterp_v3proj_test01 %%%%%%%%

% discard pair connections going to the wrong side of the contour
% this is needed only in case LW and HW contours intersect
invs = whichside(polyl,polyh,px,posl,posh,projl);

% discard pair connections that cross any original contour
%%%%%% or go on the wrong side
%%%%%%% do not discard any nodes in orig contours, correct connections instead...
disp(['Intersection test   ' datestr(now)])
inv = geom_crosstest([polyl projl],eo); % picks some LW-HW crossing points
invl = invs | inv; % remember LW nodes with discarded projections
% [gbg,ix] = intersect(polyl(inv,:),px,'rows'); % do not discard LW-HW crossing points
% inv = find(inv);
% inv(ix) = []; % do not discard LW-HW crossing points
polyl(invl,:) = []; % remove invalid projections
projl(invl,:) = [];
projposl(invl) = [];
poslorig(invl) = [];

% 
% % adjust projections which positions are within ptol from HW nodes to avoid
% % repeating nodes in the interpolated contour
% iadj = abs(projposl-round(projposl)) < ptol;             
% projposl(iadj) = round(projposl(iadj));
% projl(iadj,:) = polyh(projposl(iadj)+1,:);
% [gbg,iu] = unique([poslorig projposl],'rows');
% polyl = polyl(iu,:); % remove repeating pairs
% projl = projl(iu,:);
% poslorig = poslorig(iu);
% projposl = projposl(iu);
% 
% %%%% this is ensured by not extending beyond LW ends
% % % remember starting and ending connection edges: the interpolated contour will
% % % be cut at these edges
% % constart = [polyl(1,:) projl(1,:)];
% % conend = [polyl(end,:) projl(end,:)];
% 
% % % projection of HW nodes on the LW segment
% % % disp(['Projecting on LW   ' datestr(now)])
% % projposh = projPointOnPolyline(polyh, polyl);
% % projh = polylinePoint(polyl, projposh);
% 
% % Instead of projecting HW nodes on LW contour, find HW "reflection"
% % on LW contour within the trapezia bound by neighbouring connections, LW edge 
% % in between, and connection between neghbouring projections on HW contour.
% % This is done to prevent connections from crossing each other.
% % HW nodes which are not the projection points in projl
% disp(['Adding HW projections   ' datestr(now)])
% posh2proj = setdiff((0:nh-1)', projposl);
% nodeh2proj = polylinePoint(polyh, posh2proj);
% [nodeh2proj,iu] = unique(nodeh2proj,'rows');
% posh2proj = posh2proj(iu);
% inv = isnan(nodeh2proj(:,1));
% nodeh2proj(inv,:) = [];
% posh2proj(inv) = [];
% nhp = length(posh2proj);
% projh = ones(nhp,2)*NaN; % array of projections of HW on LW
% for ip = 1:nhp
%     df = projposl-posh2proj(ip);
%     if any(df<0) && any(df>0) % if there is a proj on HW before and after this HW node
%         il1 = find(df == max(df(df<0))); % positions in polyl, but not in polylo
%         il2 = find(df == min(df(df>0)));
%         
%         % select a pair of positions wich differ by 1
%         il1e = il1*ones(size(il2))';
%         il2e = ones(size(il1))*il2';
%         [irow,icol] = find(abs(il1e-il2e)==1);
%         il1 = il1(irow);
%         il2 = il2(icol);
%         if isempty(il1) || isempty(il2) % e.g. for LW embayments shadowed from HW
%             continue
%         end
%         d1 = sqrt(sum((projl(il1,:)-nodeh2proj(ip,:)).^2)); % part 1 in HW
%         d2 = sqrt(sum((projl(il2,:)-nodeh2proj(ip,:)).^2)); % part 2 in HW
%         projtr = polyl(il1,:) + (polyl(il2,:)-polyl(il1,:)).*d1./(d1+d2); % projection on trapezia side bound by LW nodes
% %%%%%         projh(ip,:) = % prolong the edge [nodeh2proj(ip,:) projtr] to the LW
%     else % it's a node near the start or end of HW contour that extends beyond LW
%         %%% do not extend beyond LW ends - for neat seams if several pieces
%         %%% of coastline will have to be joined 
% %         projposh_ = projPointOnPolyline(nodeh2proj(ip,:), polylo);
% %         projh(ip,:) = polylinePoint(polylo, projposh_);
%     end
% end
% inv = isnan(projh(:,1));
% nodeh2proj(inv,:) = [];
% projh(inv,:) = [];
% posh2proj(inv) = [];
% projposh = projPointOnPolyline(projh, polylo); % positions of the "reflections" on LW
% 
% % discard pair connections that cross any original contour or connections
% % generated by LW nodes
% disp(['Intersection test   ' datestr(now)])
% inv = geom_crosstest([nodeh2proj projh],[eo; polyl projl]);
% nodeh2proj(inv,:) = []; % remove invalid projections
% projh(inv,:) = [];
% projposh(inv) = [];
% posh2proj(inv) = [];
% invh = find(inv); % remember HW nodes with discarded projections
% 
% % discard projections which positions are within ptol from LW nodes to avoid
% % repeating nodes in the interpolated contour
% iadj = abs(projposh-round(projposh)) < ptol;
% projposh(iadj) = round(projposh(iadj));
% projh(iadj,:) = polylo(projposh(iadj)+1,:);
% [gbg,iu] = unique([posh2proj projposh],'rows');
% nodeh2proj = nodeh2proj(iu,:); % remove repeating pairs
% projh = projh(iu,:);
% % posh2proj = posh2proj(iu);
% projposh = projposh(iu);
% 
% % combine original and corresponding projection points
% posl = [poslorig; projposh]; % positions
% % posh = [projposl; posh2proj];
% nodel = [polyl; projh]; % coordinates
% nodeh = [projl; nodeh2proj];
% 
% % % discard projections which are close (within ptol of the edge length) to LW nodes to avoid repeating nodes in
% % % the interpolated contour
% % iadj = abs(posh-round(posh)) < ptol;
% % posh(iadj) = round(posh(iadj));
% % nodeh(iadj,:) = polyh(posh(iadj)+1,:);
% % iadj = abs(posl-round(posl)) < ptol;
% % posl(iadj) = round(posl(iadj));
% % nodel(iadj,:) = polylo(posl(iadj)+1,:);
% % % remove repeating pairs
% % [gbg,iu] = unique([posl posh],'rows');
% % posl = posl(iu);
% % % posh = posh(iu);
% % nodel = nodel(iu,:);
% % nodeh = nodeh(iu,:);

% include intersection points
ival = abs(posl-round(posl))>1e-9; % do not include intersection points very close to existing nodes
nodel = [polyl; px(ival,:)]; 
nodeh = [projl; px(ival,:)];
posl = [poslorig; posl(ival)]; % positions

% sort all nodes according to the position on LW contour
[posl,isort] = sort(posl);
% posh = posh(isort);
nodel = nodel(isort,:);
nodeh = nodeh(isort,:);

% linear interpolation of coordinates weighted by z-difference
disp(['Interpolating   ' datestr(now)])
nodei = nodel + (nodeh-nodel)*(z0-zl)/(zh-zl);
inv = isnan(nodei(:,1));
nodei(inv,:) = []; % remove invalid nodes
% nodel(inv,:) = []; % remove corresponding LW-HW pairs
% nodeh(inv,:) = [];
% posl(inv) = [];
% posh(inv) = [];

% % separate out the parts of the intrpolated contour that cross any original contour
% %%% these can only occur if LW intersects HW???
% %%%%%%%% suppose 1st node is good, otherwise the result will be incorrect %%%%%%
% disp(['Removing nodes beyond intersection points   ' datestr(now)])
% [gbg,pos] = intersections_nan(nodei,[polylo; NaN NaN; polyh]); % pos1 is sorted
% ni = size(nodei,1);
% pos = [0; pos; ni];
% [gbg,bin] = histc(0:ni-1, pos);
% nodei(mod(bin,2)==0, :) = []; % remove nodes which fall into even intervals between intersection points

%%%%%% done earlier for LW projections; there shouldn't be any more crossing
%%%%%% connections produced after that
% % discard pair connections that cross any original contour
% % eo = [nodel(1:end-1,:) nodel(2:end,:); nodeh(1:end-1,:) nodeh(2:end,:)]; % original contour edges
% % disp(['Intersection test   ' datestr(now)])
% inv = geom_crosstest([nodel nodeh],eo);
% nodei(inv,:) = []; % remove invalid nodes
% nodel(inv,:) = []; % remove corresponding LW-HW pairs
% nodeh(inv,:) = [];
% posl(inv) = [];
% posh(inv) = [];

% %%%%%% wrong!!!
% % discard pair connections that come to any other node closer than the
% % distance from the interpolant node to the interpolated node
% dl = sqrt(sum((nodei-nodel).^2, 2)); % sum each row
% dh = sqrt(sum((nodei-nodeh).^2, 2)); % sum each row
% nn = size(nodei,1);
% inv = repmat(false,nn,1);
% for ip = 1:nn
%     ipnot = setdiff(1:nn,ip);
%     dc = distancePointEdge([nodel(ipnot,:); nodeh(ipnot,:)], [nodel(ip,:) nodeh(ip,:)]); 
%         % another routine: [d, t, p] = seg_dist(v1, v2, x);
%     if any(dc<dl(ip) | dc<dh(ip))
%         inv(ip) = true;
%     end
% end
% nodei(inv,:) = []; % remove invalid nodes
% nodel(inv,:) = []; % remove corresponding LW-HW pairs
% nodeh(inv,:) = [];
% posl(inv) = [];
% posh(inv) = [];


% %%%%%% wrong results: corrects where not required (produces rugged
% %%%%%% contours); wrong treatment of intersection points
% % extend edges in the interpolated contour that come too close to LW or HW
% % contours
% % project original LW nodes on the interpolated contour
% projposl = projPointOnPolyline(polyl, nodei);
% projl = polylinePoint(nodei, projposl);
% dc = sqrt(sum((polyl-projl).^2, 2)); % sum each row; Euclidean distance
% dl = sqrt(sum((nodel-nodei).^2, 2)); % sum each row; Euclidean distance
% dtest = ones(size(projposl))*NaN;
% for ip = 1:length(projposl)
%     posdif = abs(posl-(ip-1)); % ip-1 is the position of current LW node
%     dtest(ip) = min(dl(posdif==min(posdif))); % closest interpolant node in LW (min if there are several at the same distance)
% end
% icor = dtest-dc > dtest*0.05; % projection points too close to LW contour
% if any(icor)
%     dr = repmat((dtest(icor)./dc(icor)),1,2); % extension factor
%     nodecorl = polyl(icor,:) + (projl(icor,:)-polyl(icor,:)).*dr;
%     posi = [(0:size(nodei,1)-1)'; projposl(icor)];
%     nodei = [nodei; nodecorl];
%     [posi,isort] = sort(posi);
%     nodei = nodei(isort,:);
% end


% %%%%%%%%%%%%%
% % adjust coordinates of interpolated points that are closer to any node in LW or
% % HW contours than the distance to their intrpolant nodes:
% % move the interpolated point to the center of mass of adjusted points, 
% % each adjusted point is extended to the distance 
% nn = size(nodei,1);
% while true
%     dl = sqrt(sum((nodei-nodel).^2, 2)); % sum each row
%     dh = sqrt(sum((nodei-nodeh).^2, 2)); % sum each row
%     nodea = ones(nn,2)*NaN;
%     nicor = repmat(false,nn,1);
%     for ii = 1:nn
%         % LW 
%         dc = sqrt(sum((repmat(nodei(ii,:),nn,1)-nodel).^2, 2)); % sum each row
% %         icor = dc<dc(ii);
%         icor = dc(ii)-dc > dc(ii)*0.05;
%         if any(icor)
%             dr = repmat((dl(icor)./dc(icor)),1,2);
%             nodecorl = nodel(icor,:) + (repmat(nodei(ii,:),length(find(icor)),1)-nodel(icor,:)).*dr;
%         else
%             nodecorl = [];
%         end
% 
%         % HW
%         dc = sqrt(sum((repmat(nodei(ii,:),nn,1)-nodeh).^2, 2)); % sum each row
% %         icor = dc<dc(ii);
%         icor = dc(ii)-dc > dc(ii)*0.05;
%         if any(icor)
%             dr = repmat((dh(icor)./dc(icor)),1,2);
%             nodecorh = nodeh(icor,:) + (repmat(nodei(ii,:),length(find(icor)),1)-nodeh(icor,:)).*dr;
%         else
%             nodecorh = [];
%         end
% 
%         % center of mass, i.e. mean coordinate
%         nodecor = [nodecorl; nodecorh];
%         if ~isempty(nodecor)
%             nodea(ii,:) = mean(nodecor,1);
%             nicor(ii) = true;
%         end
%     end
%     if all(~nicor) || all(all((nodei(nicor,:) - nodea(nicor,:)==0)))
%         break
%     end
%     nodei(nicor,:) = nodea(nicor,:);
% end

% % remove repeating nodes -- done in geom_removeloops
% nodei(all(diff(nodei)==0,2), :) = [];

% load landinterp_v3proj_test02
disp(['Removing loops   ' datestr(now)])
nodei = geom_removeloops(nodei); 

rm = struct_emptyfields(rl(1)); % initialize with dummy element
rm.x = [nodei(:,1)' NaN];
rm.y = [nodei(:,2)' NaN];

rmi = struct_emptyfields(rl(1)); % initialize with dummy element
rmi(1) = []; % make it empty

% load landinterp_v3proj_land02

% % add projection of HW nodes on the LW segment to the LW segment
% for ih = 1:nhs
%     lproj = [rlproj; addprojpoint(polyh{ih},polyl)];
% end