function [ok,algorithm,dmin,dmax,reduc,xyflag,smisltreat,inflag] = dlgSimplify()
%dlgSimplify dialog to set parameters for simplification of a polyline. 
%   Called by VisualGrid.
%   Creates a modal dialog box which allows setting the parameters.
% 
%   Input:
%
%   Output:
%   OK          is 1 if you push the OK button, or 0 if you push the Cancel
%               button or close the figure
%   algorithm   routine to use
%   dmin        dmin for Douglas-Peuker and Opheim algorithms
%   dmax        dmax for Opheim algorithm
%   reduc       reduction factor set by user
%   xyflag      'xy' or 'geo' coordinate type
%   smisltreat  small island treatment
%   inflag      true/false flag indicating whether operation should be
%               performed on the part inside a polygon or outside; or NaN
%               (default) which means working on the entire object
%   All outputs are set to [] if Cancel is pressed
%
% M.Krassovski, 2011
% Parts of code are based on the standard LISTDLG function.

figname = 'Polyline Simplification Parameters';
okstring = 'OK';
cancelstring = 'Cancel';
algorithm = 1;
dmin = 10;
dmax = Inf;
reduc = 2; % default reduction: reduce to half the original number of nodes
smisltreat = 1;
inflag = NaN;

fus = 8;  % frame/uicontrol spacing, in pixels
ffs = 8;  % frame/figure spacing, in pixels
uh = 120; % uicontrol button horizontal dimension (width), in pixels
uv = 22;  % uicontrol button vertical dimension (height), in pixels

nv = 13; % number of ui elements (e.g. buttons or edit fields) in the vertical
nh = 2; % -- in the horizontal

fp = get(0,'defaultfigureposition');
h = 2*ffs + (nh-1)*fus + nh*uh;
v = 2*ffs + nv*fus + nv*uv;
fp = [fp(1) fp(2)+fp(4)-v h v];  % keep upper left corner fixed

fig_props = { ...
    'name'                   figname ...
    'color'                  get(0,'defaultUicontrolBackgroundColor') ...
    'resize'                 'off' ...
    'numbertitle'            'off' ...
    'menubar'                'none' ...
    'windowstyle'            'modal' ...
    'visible'                'off' ...
    'createfcn'              ''    ...
    'position'               fp   ...
    'closerequestfcn'        'delete(gcbf)' ...
%     'WindowButtonDownFcn'    @wbd
            };

fig = figure(fig_props{:});

btn_wid = (fp(3)-2*(ffs+fus)-fus)/2;


% GUI layout
%============

irow = 1; % 1st row from top
%---------------------------
ad.ui.alg(1) = uicontrol('Style','text','String','Algorithm:',...
    'HorizontalAlignment','left',...
    'position',[ffs+fus ffs+(fus+uv)*(nv-irow) btn_wid*0.7 uv]);

algstr = {'Radial distance','Opheim','Douglas-Peuker'};
ad.ui.alg(2) = uicontrol('style','popupmenu',...
    'string',algstr,...
    'position',[ffs+2*fus+btn_wid*0.7 ffs+(fus+uv)*(nv-irow) btn_wid*1.2 uv],...
    'callback',@cbAlgorithm);
               

irow = 2; % 2nd row from top (Douglas-Peucker)
%---------------------------
ad.ui.reduc(1) = uicontrol('Style','text','String','Reduce number of nodes',...
    'HorizontalAlignment','left',...
    'position',[ffs+fus ffs+(fus+uv)*(nv-irow) btn_wid*1.1 uv],'visible','off');

ad.ui.reduc(2) = uicontrol('style','edit',...
    'string',num2str(reduc),...
    'position',[ffs+2*fus+btn_wid*1.1 ffs+(fus+uv)*(nv-irow) btn_wid*0.4 uv],...
    'callback',@cbReduc,'visible','off');

ad.ui.reduc(3) = uicontrol('Style','text','String',' times',...
    'HorizontalAlignment','left',...
    'position',[ffs+fus+3*fus+btn_wid*1.4 ffs+(fus+uv)*(nv-irow) btn_wid*0.4 uv],...
    'visible','off');


irow = 3; % 3rd row from top (Radial Distance and Opheim)
%---------------------------
ad.ui.dmin(1) = uicontrol('Style','text','String','Minimum edge length:',...
    'HorizontalAlignment','left',...
    'position',[ffs+fus ffs+(fus+uv)*(nv-irow) btn_wid*1.1 uv]);

ad.ui.dmin(2) = uicontrol('style','edit',...
    'string',num2str(dmin),...
    'TooltipString','Minimum edge length in the resulting polyline',...
    'position',[ffs+2*fus+btn_wid*1.1 ffs+(fus+uv)*(nv-irow) btn_wid*0.4 uv],...
    'callback',@cbDmin);


irow = 4; % 4th row from top (Opheim)
%---------------------------
ad.ui.dmax(1) = uicontrol('Style','text','String','Maximum edge length:',...
    'HorizontalAlignment','left','visible','off',...
    'position',[ffs+fus ffs+(fus+uv)*(nv-irow) btn_wid*1.1 uv]);

ad.ui.dmax(2) = uicontrol('style','edit',...
    'string',num2str(dmax),...
    'TooltipString','Maximum edge length in the resulting polyline',...
    'position',[ffs+2*fus+btn_wid*1.1 ffs+(fus+uv)*(nv-irow) btn_wid*0.4 uv],...
    'callback',@cbDmax,'visible','off');


irow = 3; % 3rd row from top (Douglas-Peucker)
%---------------------------
% Create the button group.
grpos = [ffs+fus ffs+fus*2+(fus+uv)*(nv-irow-2) btn_wid*2 uv*3];
ad.ui.coordtype_gr = uibuttongroup('visible','off','Units','pixels',...
    'Position',grpos,'Title','Coordinate type:');

% Create radio buttons in the button group.
pos = [fus fus+uv btn_wid*1.5 uv];
coordtype_xy = uicontrol('Style','Radio','String','XY',...
    'pos',pos,'parent',ad.ui.coordtype_gr,'HandleVisibility','off');
pos = [fus fus      btn_wid*1.5 uv];
coordtype_geo = uicontrol('Style','Radio','String','Geographic',...
    'pos',pos,'parent',ad.ui.coordtype_gr,'HandleVisibility','off');

% Initialize button group properties.
set(ad.ui.coordtype_gr,'SelectionChangeFcn',@cbCoordtype);
set(ad.ui.coordtype_gr,'SelectedObject',coordtype_xy);  % Default is XY
set(ad.ui.coordtype_gr,'Visible','off');


% irow = 6; % 6th row from top
% %---------------------------
% ad.ui.hexagon(1) = uicontrol('Style','text','String','Minimum hexagon edge length',...
%     'HorizontalAlignment','left',...
%     'position',[ffs+fus ffs+(fus+uv)*(nv-irow) btn_wid*1.4 uv],...
%     'TooltipString','Sortest edge of hexagon approximating a small island');
% 
% ad.ui.hexagon(2) = uicontrol('style','edit',...
%                    'string',num2str(mhel),...
%                    'position',[ffs+2*fus+btn_wid*1.4 ffs+(fus+uv)*(nv-irow) btn_wid*0.5 uv],...
%                    'callback',@cbMHEL);
               
               
irow = 6; % 6th row from top
%---------------------------
% Create the button group.
grpos = [ffs+fus ffs+fus+(fus+uv)*(nv-irow-2) btn_wid*2 uv*4];
ad.ui.smisltreat_gr = uibuttongroup('visible','off','Units','pixels',...
    'Position',grpos,'Title','Small island treatment:');

% Create radio buttons in the button group.
pos = [fus fus+uv*2 btn_wid*1.5 uv];
ad.ui.smisltreat_1 = uicontrol('Style','Radio','String','Delete',...
    'pos',pos,'parent',ad.ui.smisltreat_gr,'HandleVisibility','off');
pos = [fus fus+uv       btn_wid*1.5 uv];
ad.ui.smisltreat_2 = uicontrol('Style','Radio','String','Leave unchanged',...
    'pos',pos,'parent',ad.ui.smisltreat_gr,'HandleVisibility','off');
pos = [fus fus          btn_wid*1.5 uv];
ad.ui.smisltreat_3 = uicontrol('Style','Radio','String','Approximate with hexagons',...
    'pos',pos,'parent',ad.ui.smisltreat_gr,'HandleVisibility','off');

% Initialize button group properties.
set(ad.ui.smisltreat_gr,'SelectionChangeFcn',@cbSmIslTreat);
set(ad.ui.smisltreat_gr,'SelectedObject',ad.ui.smisltreat_1);  % Default is 1 - delete small islands
set(ad.ui.smisltreat_gr,'Visible','on');


irow = 9; % 9th row from top
%---------------------------
% Create the button group.
pos = [ffs+fus ffs-fus+(fus+uv)*(nv-irow) btn_wid*2 uv];
ad.ui.inflag_chk = uicontrol('Style','Checkbox','String','Define an area to work on',...
    'TooltipString','You will be prompted to specify a polygon after pressing OK',...
    'Value',false,'Callback',@cbInflagChk,...
    'pos',pos,'HandleVisibility','off');


irow = 10; % 10th row from top
%---------------------------
% Create the button group.
grpos = [ffs+fus ffs+fus+(fus+uv)*(nv-irow-1.8) btn_wid*2 uv*3];
ad.ui.inflag_gr = uibuttongroup('visible','off','Units','pixels','Position',grpos,...
    'Title','Which part of the polyline to work on:');
% Create radio buttons in the button group.
pos = [fus fus+uv btn_wid*1.5 uv];
ad.ui.inflag_out = uicontrol('Style','Radio','String','Outside of the polygon',...
    'pos',pos,'parent',ad.ui.inflag_gr,'HandleVisibility','off','Enable','off');
pos = [fus fus      btn_wid*1.5 uv];
ad.ui.inflag_in = uicontrol('Style','Radio','String','Inside of the polygon',...
    'pos',pos,'parent',ad.ui.inflag_gr,'HandleVisibility','off','Enable','off');
% Initialize button group properties.
set(ad.ui.inflag_gr,'SelectionChangeFcn',@cbInflag);
set(ad.ui.inflag_gr,'SelectedObject',ad.ui.inflag_out);  % Default is outside
set(ad.ui.inflag_gr,'Visible','on');


% bottom row
%-----------
ad.ui.ok_btn = uicontrol('style','pushbutton',...
                   'string',okstring,...
                   'position',[ffs+fus ffs+fus btn_wid uv],...
                   'callback',@doOK);

ad.ui.cancel_btn = uicontrol('style','pushbutton',...
                       'string',cancelstring,...
                       'position',[ffs+2*fus+btn_wid ffs+fus btn_wid uv],...
                       'callback',@doCancel);


set([fig, ad.ui.ok_btn, ad.ui.cancel_btn], 'keypressfcn', @doKeypress);

set(fig,'position',getnicedialoglocation(fp, get(fig,'Units')));

setdefaultbutton(fig, ad.ui.ok_btn); % make ok_btn the default button

% make sure we are on screen
movegui(fig)
set(fig, 'visible','on'); drawnow;

% default parameters
ad.value = 0;
ad.algorithm = algorithm;
ad.dmin = dmin;
ad.dmax = dmax;
ad.reduc = reduc;
ad.xyflag = true;
ad.inflag = inflag;
ad.smisltreat = smisltreat;
setappdata(0,'dlgSimplifyInPolyAppData__',ad);

try
    % Give default focus to the OK button *after* the figure is made visible
    uicontrol(ad.ui.ok_btn);
    uiwait(fig);
catch
    if ishandle(fig)
        delete(fig)
    end
end

if isappdata(0,'dlgSimplifyInPolyAppData__')
    ad = getappdata(0,'dlgSimplifyInPolyAppData__');
    ok = ad.value;
    algorithm = ad.algorithm;
    dmin = ad.dmin;
    dmax = ad.dmax;
    reduc = ad.reduc;
    xyflag = ad.xyflag;
    inflag = ad.inflag;
    smisltreat = ad.smisltreat;
    rmappdata(0,'dlgSimplifyInPolyAppData__')
else
    % figure was deleted
    ok = 0;
    algorithm = [];
    dmin = [];
    dmax = [];
    reduc = [];
    xyflag = [];
    inflag = [];
    smisltreat = [];
end


% figure, OK and Cancel KeyPressFcn
function doKeypress(src, evd) %#ok<INUSL>
switch evd.Key
 case 'escape'
  doCancel([],[]);
end


% OK callback
function doOK(src, evd) %#ok<INUSD>

if isappdata(0,'dlgSimplifyInPolyAppData__')
    ad = getappdata(0,'dlgSimplifyInPolyAppData__');
else
    error('No required appdata found') %%%%%%%%%%
end
ad.value = 1;
setappdata(0,'dlgSimplifyInPolyAppData__',ad);
delete(gcbf);


% Cancel callback
function doCancel(cancel_btn, evd) %#ok<INUSD>
ad.value = 0;
ad.algorithm = [];
ad.dmin = [];
ad.dmax = [];
ad.reduc = [];
ad.xyflag = [];
ad.inflag = [];
ad.smisltreat = [];
setappdata(0,'dlgSimplifyInPolyAppData__',ad)
delete(gcbf);


function cbAlgorithm(src,evd) %#ok<INUSD>
if isappdata(0,'dlgSimplifyInPolyAppData__')
    ad = getappdata(0,'dlgSimplifyInPolyAppData__');
end

val = get(src,'Value');
ad.algorithm = val;

% adjust gui layout based on the choice
switch val
    case 1
        set([ad.ui.reduc ad.ui.coordtype_gr ad.ui.dmax],'Visible','off')
        set(ad.ui.dmin,'Visible','on')
    case 2
        set([ad.ui.reduc ad.ui.coordtype_gr],'Visible','off')
        set([ad.ui.dmin ad.ui.dmax],'Visible','on')
    case 3
        set([ad.ui.dmin ad.ui.dmax],'Visible','off')
        set([ad.ui.reduc ad.ui.coordtype_gr],'Visible','on')
end     

setappdata(0,'dlgSimplifyInPolyAppData__',ad)


function cbDmin(src,evd) %#ok<INUSD>
if isappdata(0,'dlgSimplifyInPolyAppData__')
    ad = getappdata(0,'dlgSimplifyInPolyAppData__');
end
ad.dmin = eval(get(src,'String'));
setappdata(0,'dlgSimplifyInPolyAppData__',ad)


function cbDmax(src,evd) %#ok<INUSD>
if isappdata(0,'dlgSimplifyInPolyAppData__')
    ad = getappdata(0,'dlgSimplifyInPolyAppData__');
end
ad.dmax = eval(get(src,'String'));
setappdata(0,'dlgSimplifyInPolyAppData__',ad)


function cbReduc(src,evd) %#ok<INUSD>
if isappdata(0,'dlgSimplifyInPolyAppData__')
    ad = getappdata(0,'dlgSimplifyInPolyAppData__');
end
ad.reduc = eval(get(src,'String'));
setappdata(0,'dlgSimplifyInPolyAppData__',ad)


function cbCoordtype(src,evd) %#ok<INUSD>
if isappdata(0,'dlgSimplifyInPolyAppData__')
    ad = getappdata(0,'dlgSimplifyInPolyAppData__');
end
hsel = get(src,'SelectedObject');
if strcmp(get(hsel,'String'),'XY') && get(hsel,'Value')
    ad.xyflag = true;
else
    ad.xyflag = false;
end
setappdata(0,'dlgSimplifyInPolyAppData__',ad)


function cbSmIslTreat(src,evd) %#ok<INUSD>
if isappdata(0,'dlgSimplifyInPolyAppData__')
    ad = getappdata(0,'dlgSimplifyInPolyAppData__');
end
hsel = get(src,'SelectedObject');
if     get(ad.ui.smisltreat_1,'Value')
    ad.smisltreat = 1;
elseif get(ad.ui.smisltreat_2,'Value')
    ad.smisltreat = 2;
else
    ad.smisltreat = 3;
end
setappdata(0,'dlgSimplifyInPolyAppData__',ad)


function cbInflagChk(src,evd) %#ok<INUSD>
if isappdata(0,'dlgSimplifyInPolyAppData__')
    ad = getappdata(0,'dlgSimplifyInPolyAppData__');
end

if get(src,'Value')
    set([ad.ui.inflag_out ad.ui.inflag_in],'Enable','on')
    if get(ad.ui.inflag_out,'Value')
        ad.inflag = false;
    else
        ad.inflag = true;
    end
else
    set([ad.ui.inflag_out ad.ui.inflag_in],'Enable','off')
    ad.inflag = NaN;
end

setappdata(0,'dlgSimplifyInPolyAppData__',ad)


function cbInflag(src,evd) %#ok<INUSD>
if isappdata(0,'dlgSimplifyInPolyAppData__')
    ad = getappdata(0,'dlgSimplifyInPolyAppData__');
end
hsel = get(src,'SelectedObject');
if strcmp(get(hsel,'String'),'Inside of the polygon') && get(hsel,'Value')
    ad.inflag = true;
else
    ad.inflag = false;
end
setappdata(0,'dlgSimplifyInPolyAppData__',ad)


function cbMHEL(src,evd) %#ok<INUSD>
if isappdata(0,'dlgSimplifyAppData__')
    ad = getappdata(0,'dlgSimplifyAppData__');
end
ad.mhel = eval(get(src,'String'));
setappdata(0,'dlgSimplifyAppData__',ad)