function [ok,sel] = dlgCodeSelect(codeu)
%dlgCodeSelect dialog to select codes from codeu list. 
%   Called by VisualGrid.
%   Creates a modal dialog box which allows setting checkmarks for desired codes.
% 
%   Input:
%       codeu   unique codes to select from
%   Output:
%   OK          is 1 if you push the OK button, or 0 if you push the Cancel
%               button or close the figure
%   sel         selected codes
%   All outputs are set to [] if Cancel is pressed
%
% M.Krassovski, 2011
% Parts of code are based on the standard LISTDLG function.

figname = 'Code select';
okstring = 'OK';
cancelstring = 'Cancel';

fus = 8;  % frame/uicontrol spacing, in pixels
ffs = 8;  % frame/figure spacing, in pixels
uh = 120; % uicontrol button horizontal dimension (width), in pixels
uv = 22;  % uicontrol button vertical dimension (height), in pixels

nv = length(codeu) + 2; % number of ui elements (e.g. buttons or edit fields) in the vertical
nh = 2; % -- in the horizontal

fp = get(0,'defaultfigureposition');
h = 2*ffs + (nh-1)*fus + nh*uh;
v = 2*ffs + nv*fus + nv*uv;
fp = [fp(1) fp(2)+fp(4)-v h v];  % keep upper left corner fixed

fig_props = { ...
    'name'                   figname ...
    'color'                  get(0,'defaultUicontrolBackgroundColor') ...
    'resize'                 'off' ...
    'numbertitle'            'off' ...
    'menubar'                'none' ...
    'windowstyle'            'modal' ...
    'visible'                'off' ...
    'createfcn'              ''    ...
    'position'               fp   ...
    'closerequestfcn'        'delete(gcbf)' ...
%     'WindowButtonDownFcn'    @wbd
            };

fig = figure(fig_props{:});

btn_wid = (fp(3)-2*(ffs+fus)-fus)/2;


% GUI layout
%============

for irow = 1:nv-2 % row number from top
    pos = [ffs+fus ffs-fus+(fus+uv)*(nv-irow) btn_wid uv];
    ad.ui.inflag_chk = uicontrol('Style','Checkbox','String',num2str(codeu(irow)),...
        'Value',false,'Callback',{@cbSetCheck,irow},...
        'pos',pos,'HandleVisibility','off');
end

% bottom row
%-----------
ad.ui.ok_btn = uicontrol('style','pushbutton',...
                   'string',okstring,...
                   'position',[ffs+fus ffs+fus btn_wid uv],...
                   'callback',@doOK);

ad.ui.cancel_btn = uicontrol('style','pushbutton',...
                       'string',cancelstring,...
                       'position',[ffs+2*fus+btn_wid ffs+fus btn_wid uv],...
                       'callback',@doCancel);


set([fig, ad.ui.ok_btn, ad.ui.cancel_btn], 'keypressfcn', @doKeypress);

set(fig,'position',getnicedialoglocation(fp, get(fig,'Units')));

setdefaultbutton(fig, ad.ui.ok_btn); % make ok_btn the default button

% make sure we are on screen
movegui(fig)
set(fig, 'visible','on'); drawnow;

% default parameters
ad.value = 0;
ad.sel = [];
setappdata(0,'dlgSimplifyInPolyAppData__',ad);

try
    % Give default focus to the OK button *after* the figure is made visible
    uicontrol(ad.ui.ok_btn);
    uiwait(fig);
catch
    if ishandle(fig)
        delete(fig)
    end
end

if isappdata(0,'dlgSimplifyInPolyAppData__')
    ad = getappdata(0,'dlgSimplifyInPolyAppData__');
    ok = ad.value;
    sel = ad.sel;
    rmappdata(0,'dlgSimplifyInPolyAppData__')
else
    % figure was deleted
    ok = 0;
    sel = [];
end


% figure, OK and Cancel KeyPressFcn
function doKeypress(src, evd) %#ok<INUSL>
switch evd.Key
 case 'escape'
  doCancel([],[]);
end


% OK callback
function doOK(src, evd) %#ok<INUSD>

if isappdata(0,'dlgSimplifyInPolyAppData__')
    ad = getappdata(0,'dlgSimplifyInPolyAppData__');
else
    error('No required appdata found') %%%%%%%%%%
end
ad.value = 1;
setappdata(0,'dlgSimplifyInPolyAppData__',ad);
delete(gcbf);


% Cancel callback
function doCancel(cancel_btn, evd) %#ok<INUSD>
ad.value = 0;
ad.sel = [];
setappdata(0,'dlgSimplifyInPolyAppData__',ad)
delete(gcbf);


function cbSetCheck(src,evd,checknum) %#ok<INUSL>
if isappdata(0,'dlgSimplifyInPolyAppData__')
    ad = getappdata(0,'dlgSimplifyInPolyAppData__');
end

if get(src,'Value')
    ad.sel = union(ad.sel,checknum);
else
    ad.sel = setdiff(ad.sel,checknum);
end

setappdata(0,'dlgSimplifyInPolyAppData__',ad)