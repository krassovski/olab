function utmzone = dlgUTMzone(defzone)
% Dialog for entering a UTM zone.

if isnan(defzone)
    defzone = '';
elseif isnumeric(defzone)
    defzone = num2str(defzone);
end

fus = 8;  % frame/uicontrol spacing, in pixels
ffs = 8;  % frame/figure spacing, in pixels
uh = 120; % uicontrol button horizontal dimension (width), in pixels
uv = 22;  % uicontrol button vertical dimension (height), in pixels

h = dialog;

fp = get(h,'Position');
fp(3) = fus*2 + uh*3 + ffs*2;
fp(4) = fus*2 + uv*3 + ffs*2;
set(h,'Position',fp);

htxt = uicontrol('style','text',...
                   'string','UTM zone:',...
                   'position',[fus fus+ffs*2+uv*2 uh uv]);

tipstr = ['Signed integer (negative for southern hemisphere) ',...
          'or four-character string, e.g. ''09 U''. ',...
          'No effect if Default is pressed.'];
hedit = uicontrol('style','edit',...
                   'string',defzone,...
                   'position',[fus+ffs+uh fus+ffs*2+uv*2 uh uv],...
                   'callback',@doSetZone,...
                   'tooltip',tipstr);


% bottom row
%-----------
hok = uicontrol('style','pushbutton',...
                   'string','OK',...
                   'position',[fus fus uh uv],...
                   'callback',@doOK);

tipstr = ['Press to determine UTM zone automatically ',...
          '(based on central meridian of the object).'];
hdefault = uicontrol('style','pushbutton',...
                   'string','Default',...
                   'position',[fus+ffs+uh fus uh uv],...
                   'callback',@doDefault,'tooltip',tipstr);

hcancel = uicontrol('style','pushbutton',...
                       'string','Cancel',...
                       'position',[fus+ffs*2+uh*2 fus uh uv],...
                       'callback',@doCancel);
                   
                   
set([h hok hdefault hcancel], 'keypressfcn', @doKeypress);

% default parameters
setappdata(0,'dlgUTMzone__',defzone);

try
    % Give default focus to the OK button *after* the figure is made visible
    uicontrol(hok);
    uiwait(h);
catch
    if ishandle(h)
        delete(h)
    end
end

if isappdata(0,'dlgUTMzone__')
    utmzone = getappdata(0,'dlgUTMzone__');
    rmappdata(0,'dlgUTMzone__')
else
    % figure was deleted
    utmzone = false;
end


function doKeypress(src, evd) %#ok<INUSL>
switch evd.Key
 case 'escape'
  doCancel([],[]);
end


% OK callback
function doOK(src, evd) %#ok<INUSD>
delete(gcbf);


% Default button callback
function doDefault(cancel_btn, evd) %#ok<INUSD>
utmzone = [];
setappdata(0,'dlgUTMzone__',utmzone)
delete(gcbf);


% Cancel callback
function doCancel(cancel_btn, evd) %#ok<INUSD>
utmzone = false;
setappdata(0,'dlgUTMzone__',utmzone)
delete(gcbf);


function doSetZone(src,evd) %#ok<INUSD>
utmzone = get(src,'String');
setappdata(0,'dlgUTMzone__',utmzone)