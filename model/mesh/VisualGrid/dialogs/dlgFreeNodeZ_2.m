function [ok,z] = dlgFreeNodeZ_2(z)
%dlgFreeNodeZ_2 dialog to set z for subsequently inserted nodes.
%   Creates a modal dialog box which allows setting the parameters.
% 
%   Input:
%   z     default z value.  
%
%   Output:
%   OK is 1 if you push the Continue button, or 0 if you push the Finalize Object button or
%   close the dialog.
%   Returns z set by user [] if OK is 0.
%
% M.Krassovski, 2011
% Parts of code are based on the standard LISTDLG function.


figname = 'New Z Value';
okstring = 'Continue';
cancelstring = 'Finalize Object';

fus = 8;  % frame/uicontrol spacing, in pixels
ffs = 8;  % frame/figure spacing, in pixels
uh = 120; % uicontrol button horizontal dimension (width), in pixels
uv = 22;  % uicontrol button vertical dimension (height), in pixels

nv = 5; % number of ui elements (e.g. buttons or edit fields) in the vertical
nh = 2; % -- in the horizontal

fp = get(0,'defaultfigureposition');
h = 2*ffs + (nh-1)*fus + nh*uh;
v = 2*ffs + nv*fus + nv*uv;
fp = [fp(1) fp(2)+fp(4)-v h v];  % keep upper left corner fixed

fig_props = { ...
    'name'                   figname ...
    'color'                  get(0,'defaultUicontrolBackgroundColor') ...
    'resize'                 'off' ...
    'numbertitle'            'off' ...
    'menubar'                'none' ...
    'windowstyle'            'modal' ...
    'visible'                'off' ...
    'createfcn'              ''    ...
    'position'               fp   ...
    'closerequestfcn'        'delete(gcbf)' ...
            };

fig = figure(fig_props{:});

btn_wid = (fp(3)-2*(ffs+fus)-fus)/2;

irow = 1; % 1st row from top
%---------------------------
annotstr = {'Specify Z for the subsequently inserted nodes.',...
            'When finished inserting nodes with current Z ',...
            'press Space or Enter to specify different Z.'};
uicontrol('Style','text','String',annotstr,'HorizontalAlignment','left',...
    'position',[ffs+fus ffs+fus+(fus+uv)*(nv-irow-1) btn_wid*2+fus uv*2])

irow = 3; % 2nd row from top
%---------------------------
uicontrol('Style','text','String','Type new Z:','HorizontalAlignment','left',...
    'position',[ffs+fus ffs+fus+(fus+uv)*(nv-irow) btn_wid uv])

ad.z_edit = uicontrol('Style','text','String',num2str(z),'HorizontalAlignment','left',...
    'position',[ffs+2*fus+btn_wid ffs+fus+(fus+uv)*(nv-irow) btn_wid uv]);

ad.zchanged = false;

% z_edit = uicontrol('style','edit',...
%                    'string',num2str(z),...
%                    'position',[ffs+2*fus+btn_wid*0.5 ffs+fus+(fus+uv)*(nv-irow) btn_wid*0.5 uv],...
%                    'callback',@doSetZ);


% bottom row
%-----------
ok_btn = uicontrol('style','pushbutton',...
                   'string',okstring,...
                   'position',[ffs+fus ffs+fus btn_wid uv],...
                   'callback',@doOK);

cancel_btn = uicontrol('style','pushbutton',...
                       'string',cancelstring,...
                       'position',[ffs+2*fus+btn_wid ffs+fus btn_wid uv],...
                       'callback',@doCancel);


set([fig, ok_btn, cancel_btn], 'keypressfcn', @doKeypress);

set(fig,'position',getnicedialoglocation(fp, get(fig,'Units')));

setdefaultbutton(fig, ok_btn); % make ok_btn the default button

% make sure we are on screen
movegui(fig)
set(fig, 'visible','on'); drawnow;

% default plotting parameters for objects
ad.value = 0;
ad.z = z;
setappdata(0,'dlgPlotParamAppData__',ad);

try
    % Give default focus to the OK button *after* the figure is made visible
    uicontrol(ok_btn);
    uiwait(fig);
catch
    if ishandle(fig)
        delete(fig)
    end
end

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
    ok = ad.value;
    z = ad.z;
    rmappdata(0,'dlgPlotParamAppData__')
else
    % figure was deleted
    ok = 0;
    z = [];
end


% figure, OK and Cancel KeyPressFcn
function doKeypress(src, evd) %#ok<INUSL>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
else
    error('No required appdata found') %%%%%%%%%%
end

switch evd.Key
    case 'enter'
        doOK([],[]);
    case 'escape'
        doCancel([],[]);
    case 'backspace'
        zstr = get(ad.z_edit,'String');
        if ~isempty(zstr)
            if ~ad.zchanged % first edit: clear the entire string
                zstr = '';
                ad.zchanged = true;
            else % not the first edit: clear the last character
                zstr(end) = [];
            end
            set(ad.z_edit,'String',zstr);
            if isempty(zstr)
                ad.value = 0;
            end
        end
    otherwise
        if length(evd.Character)==1 && ...
           ((evd.Character >= '0' && evd.Character <= '9')...
           || evd.Character == '.' || evd.Character == '-')
            ad.value = 1;
            if ~ad.zchanged
                zstr = '';
                ad.zchanged = true;
            else
                zstr = get(ad.z_edit,'String');
            end
            if evd.Character ~= '.' || isempty(strfind(zstr,'.')) % do not allow several periods
                set(ad.z_edit,'String',[zstr evd.Character]);
            end
        end
end

setappdata(0,'dlgPlotParamAppData__',ad);


% OK callback
function doOK(src, evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
else
    error('No required appdata found') %%%%%%%%%%
end
ad.value = 1;
zstr = get(ad.z_edit,'String');
if isempty(zstr)
    ad.z = [];
    ad.value = 0;
else
    ad.z = eval(zstr);
end
setappdata(0,'dlgPlotParamAppData__',ad);
delete(gcbf);


% Cancel callback
function doCancel(cancel_btn, evd) %#ok<INUSD>
ad.value = 0;
ad.z = [];
setappdata(0,'dlgPlotParamAppData__',ad)
delete(gcbf);


function doSetZ(src,evd) %#ok<INUSD>
if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.z = eval(get(src,'String'));
setappdata(0,'dlgPlotParamAppData__',ad)