function [ok,action,codesub] = dlgGridSlashParam(codesubdef)
%dlgGridSlashParam dialog to set parameters for slashing a grid by a polygon in VisualGrid.
%   Creates a modal dialog box which allows setting action type and code substitution table.
% 
%   Input:
%   codesubdef is a N-by-2 array of default code substitution table.
%
%   Output:
%   OK is 1 if you push the OK button, or 0 if you push the Cancel button or
%   close the figure.
%   action    a value 0 to 3
%   codesub   N-by-2 array of specified code substitution pairs
%
% M.Krassovski, 2011
% Parts of code are based on the standard LISTDLG function.

matversion = version;
idot = find(matversion == '.');
ad.version = str2double(matversion(1:idot(2)-1));

figname = 'Grid Slash Options';
tablename = 'Node code substitution pairs';
okstring = 'OK';
cancelstring = 'Cancel';

fus = 7;  % frame/uicontrol spacing, in pixels
ffs = 8;  % frame/figure spacing, in pixels
uh = 140; % uicontrol button horizontal dimension (width), in pixels
uv = 22;  % uicontrol button vertical dimension (height), in pixels

nv = 12; % number of ui elements (e.g. buttons or edit fields) in the vertical
nh = 2; % -- in the horizontal

fp = get(0,'defaultfigureposition');
h = 2*ffs + (nh-1)*fus + nh*uh;
v = 2*ffs + nv*fus + nv*uv;
fp = [fp(1) fp(2)+fp(4)-v h v];  % keep upper left corner fixed

fig_props = { ...
    'name'                   figname ...
    'color'                  get(0,'defaultUicontrolBackgroundColor') ...
    'resize'                 'off' ...
    'numbertitle'            'off' ...
    'menubar'                'none' ...
    'windowstyle'            'modal' ...
    'visible'                'off' ...
    'createfcn'              ''    ...
    'position'               fp   ...
    'closerequestfcn'        'delete(gcbf)' ...
    'WindowButtonDownFcn'    @wbd
            };

fig = figure(fig_props{:});

btn_wid = (fp(3)-2*(ffs+fus)-fus)/2;
% btn_wid = uh; %%%%%%%%%%%

% Create the button group
hbg = uibuttongroup('visible','off','Position',[0.05 0.6 .9 .35],'Title','Treatment of cross-connections:');
% Create radio buttons in the button group
u0 = uicontrol('Style','Radio','String','Discard connecting edges','Tag','0',...
    'pos',[fus fus+(fus+uv*0.8)*3 uh*1.7 uv],'parent',hbg,'HandleVisibility','off');
u1 = uicontrol('Style','Radio','String','Keep connecting edges in the internal part','Tag','1',...
    'pos',[fus fus+(fus+uv*0.8)*2 uh*1.7 uv],'parent',hbg,'HandleVisibility','off');
u2 = uicontrol('Style','Radio','String','Keep connecting edges in the external part','Tag','2',...
    'pos',[fus fus+(fus+uv*0.8) uh*1.7 uv],'parent',hbg,'HandleVisibility','off');
u3 = uicontrol('Style','Radio','String','Move nodes to polygon','Tag','3',...
    'pos',[fus fus uh*1.7 uv],'parent',hbg,'HandleVisibility','off');
% Initialize some button group properties. 
set(hbg,'SelectionChangeFcn',@selectionChangeCallback,'Visible','on');


% code substitution table
tbl = num2cell(codesubdef);
% if ad.version<=7.5
    tbl = cellfun(@num2str,tbl,'UniformOutput',0); % convert to strings so table content is all strings
% end
% tbl{1,1} = 'abcd'; %%%%%%%%%%%
defh = size(codesubdef,1);
tblh = max(10,defh);
tbl = [tbl; cell(tblh-defh,2)]; % append empty cells at the bottom of the table

if verLessThan('matlab', '7.6')
    ad.htbtitle = uicontrol('Style','Text','String',tablename,'Position',[fus+uh/2 fus*2+uv*7 uh uv],'Visible','off');
else
    ad.htbtitle = uicontrol('Style','Text','String',tablename,'Position',[fus+uh/2 fus*2+uv*7 uh uv],'Enable','off');
end

% % uitable from fileexchange
% ad.tblcontainer = axes('units','pixels','position',[fus fus*2+uv*4 uh*2 uv*5]);
% tprop.modal = 0;
% tprop.nVisibleRows = 5;
% tprop.nVisibleCols = 2;
% tprop.colLabels = {'Original','Substitute'};
% tprop.fontsize = 10;
% tprop.precision = '%d';
% tprop.highlightCell = 0;
% tprop.highlightRow = 0;
% tprop.highlightCol = 0;
% ad.htb = uitable(ad.tblcontainer, tbl, tprop);

% native uitable
% ad.tblcontainer = uipanel(fig,'units','pixels',);
colLabels = {'Original','Substitute'};
if verLessThan('matlab', '7.6')
    ad.htb = uitable('Data',tbl,'ColumnNames',colLabels,...
        'Position',[fus+uh/2 fus*2+uv*2 uh+10 uv*5],'ColumnWidth',uh/2-13); % Matlab 7.5.0
    set(ad.htb,'Visible',0) % ,'DataChangedCallback',@htbCallback
else
    ad.htb = uitable('Data',tbl,'ColumnName',colLabels,...
        'Position',[fus+uh/4 fus*2+uv*2 uh*1.5-16 uv*5],...
        'ColumnWidth',{uh/2,uh/2},'Enable','off','ColumnEditable',[true true],...
        'ColumnFormat',{'char','char'},'FontSize',10,'CellEditCallback',@cellEditCallback); % Matlab 7.7.0
end

% bottom row
%-----------
ok_btn = uicontrol('style','pushbutton',...
                   'string',okstring,...
                   'position',[ffs+fus ffs+fus btn_wid uv],...
                   'callback',@doOK);

cancel_btn = uicontrol('style','pushbutton',...
                       'string',cancelstring,...
                       'position',[ffs+2*fus+btn_wid ffs+fus btn_wid uv],...
                       'callback',@doCancel);


set([fig, ok_btn, cancel_btn], 'keypressfcn', @doKeypress);

set(fig,'position',getnicedialoglocation(fp, get(fig,'Units')));

setdefaultbutton(fig, ok_btn); % make ok_btn the default button

% make sure we are on screen
movegui(fig)
set(fig, 'visible','on'); drawnow;

%%% not working in matlab8.1 on linux
% % a hack to make table updated on losing focus
% % http://www.mathworks.com/matlabcentral/newsreader/view_thread/284958
% if ~verLessThan('matlab', '7.6')
%     jscroll = findjobj(ad.htb);
%     jtable = jscroll.getViewport.getComponent(0);
%     hjtable = handle(jtable,'CallbackProperties');
%     set(hjtable, 'FocusLostCallback', @tblFocusLostCallback);
% end

% default parameters
ad.value = 0;
ad.action = 0;
ad.codesub = codesubdef;
setappdata(0,'dlgGridSlashParamAppData__',ad);

try
    % Give default focus to the OK button *after* the figure is made visible
    uicontrol(ok_btn);
    uiwait(fig);
catch
    if ishandle(fig)
        delete(fig)
    end
end

if isappdata(0,'dlgGridSlashParamAppData__')
    ad = getappdata(0,'dlgGridSlashParamAppData__');
    ok = ad.value;
    action = ad.action;
    codesub = ad.codesub;
    rmappdata(0,'dlgGridSlashParamAppData__')
else
    % figure was deleted
    ok = 0;
    action = [];
    codesub = [];
end


% figure, OK and Cancel KeyPressFcn
function doKeypress(src, evd) %#ok<INUSL>

switch evd.Key
    case 'escape'
        doCancel([],[]);
end


% OK callback
function doOK(src, evd) %#ok<INUSD>

if isappdata(0,'dlgGridSlashParamAppData__')
    ad = getappdata(0,'dlgGridSlashParamAppData__');
else
    error('No required appdata found') %%%%%%%%%%
end
ad.value = 1;
% if ad.action == 3
%     cs = getappdata(ad.htb,'data'); % fileexchange uitable function
    
    if verLessThan('matlab', '7.6')
        cs = cell(get(ad.htb,'Data')); % old stock uitable function
    else
        cs = get(ad.htb,'Data'); % new stock uitable function
%         cs(all(cellfun(@isempty,cs),2), :) = []; % remove empty rows
%         if any(cellfun(@isempty,cs(:))) || any(cellfun(@isnan,cs(:)))
%             errordlg('Code substitution table can contain only numerical values in pairs','Error')
%             return % return to dialogue, do not exit
%         end
%         cs = cell2mat(cs);
    end
    cs(all(cellfun(@isempty,cs),2), :) = []; % remove empty rows
    cs = cellfun(@str2double,cs); % convert cell array of strings to numerical array
    % check if all values are numerical
    if any(isnan(cs(:))) % NaN remain in the array if non-numeric values have been entered
        errordlg('Code substitution table can contain only numerical values in pairs','Error')
        return % return to dialogue, do not exit
    end
    
    
    ad.codesub = cs;
% else
%     ad.codesub = [];
% end
setappdata(0,'dlgGridSlashParamAppData__',ad);
delete(gcbf);


% Cancel callback
function doCancel(cancel_btn, evd) %#ok<INUSD>
ad.value = 0;
ad.action = [];
ad.codesub = [];
setappdata(0,'dlgGridSlashParamAppData__',ad)
delete(gcbf);


function selectionChangeCallback(hObject,eventdata)
ad = getappdata(0,'dlgGridSlashParamAppData__');
ad.action = str2double(get(eventdata.NewValue,'Tag'));
setappdata(0,'dlgGridSlashParamAppData__',ad)
if ad.action==3
    if verLessThan('matlab', '7.6')
        set(ad.htbtitle,'Visible','on')
        set(ad.htb,'Visible',true)
    else
        set(ad.htbtitle,'Enable','on')
        set(ad.htb,'Enable','on')
    end
else
    if verLessThan('matlab', '7.6')
        set(ad.htbtitle,'Visible','off')
        set(ad.htb,'Visible',false)
    else
        set(ad.htbtitle,'Enable','off')
        set(ad.htb,'Enable','off')
    end
end


function tblFocusLostCallback(jtable,eventdata)

component = jtable.getEditorComponent;
if ~isempty(component)
    event = javax.swing.event.ChangeEvent(component);
    jtable.editingStopped(event);
end

% function htbCallback(src,evd)
% ad = getappdata(0,'dlgGridSlashParamAppData__');
% cs = cell(get(ad.htb,'Data'));
% ''


function cellEditCallback(src,evd)
evd;


function wbd(src,evd) %#ok<INUSD>

% if isappdata(0,'dlgGridSlashParamAppData__')
%     ad = getappdata(0,'dlgGridSlashParamAppData__');
% else
%     return
% end
% cp = get(src,'CurrentPoint');
% x = cp(1);
% y = cp(2);
% 
% % enable greyed-out check boxes on click
% if strcmp(get(ad.badlen_chkbx,'Enable'),'off')
%     bp = ad.badlen_chkbx_pos;
%     if x>=bp(1) && x<=bp(1)+bp(3) && y>=bp(2) && y<=bp(2)+bp(4)
%         set(ad.badlen_chkbx,'Enable','on')
%     end
% end