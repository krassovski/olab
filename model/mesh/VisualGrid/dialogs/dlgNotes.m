function [ok,txt] = dlgNotes(txt,figname,okstring,cancelstring)
%dlgNotes dialog to display and edit work notes.
%   Creates a modal dialog box with text edit field.
% 
%   Input:
%   txt     notes at the time of dialog call
%
%   Output:
%   ok      is 1 if you push the Save button, or 0 if you push the Cancel
%           button or close the figure
%   txt     updated notes
%
% M.Krassovski, 2011
% Parts of code are based on the standard LISTDLG function.


% figname = 'File notes';
% okstring = 'Save';
% cancelstring = 'Cancel';

fus = 8;  % frame/uicontrol spacing, in pixels
ffs = 8;  % frame/figure spacing, in pixels
uh = 120; % uicontrol button horizontal dimension (width), in pixels
uv = 22;  % uicontrol button vertical dimension (height), in pixels

nv = 6; % number of ui elements (e.g. buttons or edit fields) in the vertical
nh = 4; % -- in the horizontal

fp = get(0,'defaultfigureposition');
h = 2*ffs + (nh-1)*fus + nh*uh;
v = 2*ffs + nv*fus + nv*uv;
fp = [fp(1) fp(2)+fp(4)-v h v];  % keep upper left corner fixed

fig_props = { ...
    'name'                   figname ...
    'color'                  get(0,'defaultUicontrolBackgroundColor') ...
    'resize'                 'on' ...
    'numbertitle'            'off' ...
    'menubar'                'none' ...
    'windowstyle'            'modal' ...
    'visible'                'off' ...
    'createfcn'              ''    ...
    'position'               fp   ...
    'closerequestfcn'        'delete(gcbf)' ...
%     'WindowButtonDownFcn'    @wbd
            };

fig = figure(fig_props{:});

btn_wid = (fp(3)-2*(ffs+fus)-fus)/6;

% 1st row from top
%---------------------------

ad.txt_edit = uicontrol('style','edit',...
                   'Max',2,'Min',0,...
                   'string',deblank(cellstr(txt)),...
                   'BackgroundColor','w',...
                   'HorizontalAlignment','left',...
                   'Units','normalized',...
                   'position',[0.03 0.23 0.94 0.72],...
                   'callback',@doEdit);


% bottom row
%-----------
ok_btn = uicontrol('style','pushbutton',...
                   'string',okstring,...
                   'position',[ffs+fus ffs+fus btn_wid uv],...
                   'callback',@doOK);

cancel_btn = uicontrol('style','pushbutton',...
                       'string',cancelstring,...
                       'position',[ffs+2*fus+btn_wid ffs+fus btn_wid uv],...
                       'callback',@doCancel);


set([fig, ok_btn, cancel_btn], 'keypressfcn', @doKeypress);

set(fig,'position',getnicedialoglocation(fp, get(fig,'Units')));

setdefaultbutton(fig, ok_btn); % make ok_btn the default button

% make sure we are on screen
movegui(fig)
set(fig, 'visible','on'); drawnow;

% default values
ad.value = 1;
ad.txt = txt;
setappdata(0,'dlgNotesAppData__',ad);

try
    % Give default focus to the OK button *after* the figure is made visible
    uicontrol(ok_btn);
    uiwait(fig);
catch
    if ishandle(fig)
        delete(fig)
    end
end

if isappdata(0,'dlgNotesAppData__')
    ad = getappdata(0,'dlgNotesAppData__');
    ok = ad.value;
    txt = ad.txt;
    rmappdata(0,'dlgNotesAppData__')
else
    % figure was deleted
    ok = 0;
    txt = [];
end


% figure, OK and Cancel KeyPressFcn
function doKeypress(src, evd) %#ok<INUSL>
switch evd.Key
 case 'escape'
  doCancel([],[]);
end


% OK callback
function doOK(src, evd) %#ok<INUSD>

if isappdata(0,'dlgNotesAppData__')
    ad = getappdata(0,'dlgNotesAppData__');
else
    error('No required appdata found') %%%%%%%%%%
end
ad.value = 1;
ad.txt = get(ad.txt_edit,'String');
setappdata(0,'dlgNotesAppData__',ad);
delete(gcbf);


% Cancel callback
function doCancel(cancel_btn, evd) %#ok<INUSD>

if isappdata(0,'dlgNotesAppData__')
    ad = getappdata(0,'dlgNotesAppData__');
else
    error('No required appdata found') %%%%%%%%%%
end
ad.value = 0;
ad.txt = [];
setappdata(0,'dlgNotesAppData__',ad)
delete(gcbf);


function doEdit(src,evd) %#ok<INUSD>
if isappdata(0,'dlgNotesAppData__')
    ad = getappdata(0,'dlgNotesAppData__');
end
ad.txt = get(src,'String');
setappdata(0,'dlgNotesAppData__',ad)