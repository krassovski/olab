function [ok,pp] = dlgPlotParam(ppdef,fname)
%dlgPlotParam dialog to set plotting parameters for objects in VisualGrid.
%   Creates a modal dialog box which allows setting all plotting parameters.
% 
%   Input:
%   PPDEF is a structure containing default plotting parameters.  
%   FNAME (optional) file name for displaying in the window title
%
%   Output:
%   OK is 1 if you push the OK button, or 0 if you push the Cancel button or
%   close the figure.
%   Returns plotting parameters set by user in PP structure with same fields as
%   PPDEF. This will be [] when OK is 0.
%
% M.Krassovski, 2011
% Parts of code are based on the standard LISTDLG function.

if exist('fname','var') && ~isempty(fname)
    figname = ['Plotting Parameters for file ' fname];
else
    figname = 'Plotting Parameters for Selected Object(s)';
end
colorstring = 'Set color';
okstring = 'OK';
cancelstring = 'Cancel';

fus = 8;  % frame/uicontrol spacing, in pixels
ffs = 8;  % frame/figure spacing, in pixels
uh = 150; % uicontrol button horizontal dimension (width), in pixels
uv = 22;  % uicontrol button vertical dimension (height), in pixels

nv = 13; % number of ui elements (e.g. buttons or edit fields) in the vertical
nh = 2; % -- in the horizontal

fp = get(0,'defaultfigureposition');
h = 2*ffs + (nh-1)*fus + nh*uh;
v = 2*ffs + nv*fus + nv*uv;
fp = [fp(1) fp(2)+fp(4)-v h v];  % keep upper left corner fixed

fig_props = { ...
    'name'                   figname ...
    'color'                  get(0,'defaultUicontrolBackgroundColor') ...
    'resize'                 'off' ...
    'numbertitle'            'off' ...
    'menubar'                'none' ...
    'windowstyle'            'modal' ...
    'visible'                'off' ...
    'createfcn'              ''    ...
    'position'               fp   ...
    'closerequestfcn'        'delete(gcbf)' ...
    'WindowButtonDownFcn'    @wbd
            };

fig = figure(fig_props{:});

btn_wid = (fp(3)-2*(ffs+fus)-fus)/2;
% btn_wid = uh; %%%%%%%%%%%

% default plotting parameters for objects
ad.value = 0;
ad.pp = ppdef;
setappdata(0,'dlgPlotParamAppData__',ad);

irow = 1; % 1st row from top
%---------------------------
ah = axes('Units','pixels','Position',[ffs+fus ffs+fus+(fus+uv)*(nv-irow) btn_wid uv]);
hp = plot([0.1 0.5 0.9],[0 0 0],'Color',ppdef.color);
hold on
hp1st  = plot(0.1,0,'Color',ppdef.color,'Marker','o','LineStyle','none');
hplast = plot(0.9,0,'Color',ppdef.color,'Marker','+','LineStyle','none');
set(ah,'XTick',[],'YTick',[],'XLim',[0 1])


% uicontrol('Style','text','String','Color:','HorizontalAlignment','left',...
%     'position',[ffs+fus ffs+fus+(fus+uv)*4.5 btn_wid uh])
color_btn = uicontrol('style','pushbutton',...
                   'string',colorstring,...
                   'position',[ffs+2*fus+btn_wid ffs+fus+(fus+uv)*(nv-irow) btn_wid uv],...
                   'callback',{@doSetColor,hp});

irow = 2; % 2nd row from top
%---------------------------
% uicontrol('Style','text','String','Line width:','HorizontalAlignment','left',...
%     'position',[ffs+fus+btn_wid ffs+fus+(fus+uv)*4.5 btn_wid uh])
line_text = uicontrol('style','text',...
                   'string','Line: ','HorizontalAlignment','left',...
                   'position',[ffs+  fus               ffs+fus+(fus+uv)*(nv-irow) btn_wid*2/3 uv]);
stylestring = {'-',':','--','-.','none'};
style_drop = uicontrol('style','popupmenu',...
                   'string',stylestring,...
                   'value',find(strcmp(stylestring, ppdef.style)),...
                   'position',[ffs+2*fus+  btn_wid*2/3 ffs+fus+(fus+uv)*(nv-irow) btn_wid*2/3 uv],...
                   'callback',{@doSetStyle,hp});
width_edit = uicontrol('style','edit',...
                   'string',num2str(ppdef.width),...
                   'position',[ffs+3*fus+2*btn_wid*2/3 ffs+fus+(fus+uv)*(nv-irow) btn_wid*2/3 uv],...
                   'callback',{@doSetWidth,hp});

irow = 3; % 3rd row from top
%---------------------------
marker_text = uicontrol('style','text',...
                   'string','Marker: ','HorizontalAlignment','left',...
                   'position',[ffs+  fus               ffs+fus+(fus+uv)*(nv-irow) btn_wid*2/3 uv]);               
stylestring = {'none','.','o','d','s','*','^','v'};
marker_drop = uicontrol('style','popupmenu',...
                   'string',stylestring,...
                   'value',find(strcmp(stylestring, ppdef.marker)),...
                   'position',[ffs+2*fus+  btn_wid*2/3 ffs+fus+(fus+uv)*(nv-irow) btn_wid*2/3 uv],...
                   'callback',{@doSetMarker,hp});
size_edit = uicontrol('style','edit',...
                   'string',num2str(ppdef.markersize),...
                   'position',[ffs+3*fus+2*btn_wid*2/3 ffs+fus+(fus+uv)*(nv-irow) btn_wid*2/3 uv],...
                   'callback',{@doSetSize,hp});

irow = 4; % 4th row from top
%---------------------------
if isempty(ppdef.endmarkerflag)
    val = 1;
    enable = 'off';
else
    val = ppdef.endmarkerflag;
    enable = 'on';
end
ad.endmarker_chkbx_pos = [ffs+fus ffs+fus+(fus+uv)*(nv-irow) btn_wid*3/2 uv];
ad.endmarker_chkbx = uicontrol('style','checkbox',...
                   'string','Show end markers (polylines only)',...
                   'value',val,...
                   'enable',enable,...
                   'position',ad.endmarker_chkbx_pos,...
                   'callback',{@doSetEndmarkerFlag,hp1st,hplast});
               
irow = 5; % 5th row from top
%---------------------------
if isempty(ppdef.badlenflag)
    val = 1;
    enable = 'off';
else
    val = ppdef.badlenflag;
    enable = 'on';
end
ad.badlen_chkbx_pos = [ffs+fus ffs+fus+(fus+uv)*(nv-irow) btn_wid*3/2 uv];
ad.badlen_chkbx = uicontrol('style','checkbox',...
                   'string','Mark short edges',...
                   'value',val,...
                   'enable',enable,...
                   'position',ad.badlen_chkbx_pos,...
                   'callback',@doSetBadlenFlag);
badlen_edit = uicontrol('style','edit',...
                   'string',num2str(ppdef.badlenthres),...
                   'position',[ffs+3*fus+2*btn_wid*2/3 ffs+fus+(fus+uv)*(nv-irow) btn_wid*2/3 uv],...
                   'callback',@doSetBadlenThres);
% [ffs+2*fus+btn_wid ffs+fus+(fus+uv)*(nv-irow) btn_wid uv]

irow = 6; % 6th row from top
%---------------------------
if isempty(ppdef.badangflag)
    val = 1;
    enable = 'off';
else
    val = ppdef.badangflag;
    enable = 'on';
end
ad.badang_chkbx_pos = [ffs+fus ffs+fus+(fus+uv)*(nv-irow) btn_wid*3/2 uv];
ad.badang_chkbx = uicontrol('style','checkbox',...
                   'string','Mark small angles (polylines only)',...
                   'value',val,...
                   'enable',enable,...
                   'position',ad.badang_chkbx_pos,...
                   'callback',@doSetBadangFlag);
badang_edit = uicontrol('style','edit',...
                   'string',num2str(ppdef.badangthres*180/pi),...
                   'position',[ffs+3*fus+2*btn_wid*2/3 ffs+fus+(fus+uv)*(nv-irow) btn_wid*2/3 uv],...
                   'callback',@doSetBadangThres);
               
irow = 7; % 7th row from top
%---------------------------
if isempty(ppdef.badratflag)
    val = 1;
    enable = 'off';
else
    val = ppdef.badratflag;
    enable = 'on';
end
ad.badrat_chkbx_pos = [ffs+fus ffs+fus+(fus+uv)*(nv-irow) btn_wid*3/2 uv];
ad.badrat_chkbx = uicontrol('style','checkbox',...
                   'string','Mark length ratia above (polylines only)',...
                   'value',val,...
                   'enable',enable,...
                   'position',ad.badrat_chkbx_pos,...
                   'callback',@doSetBadratFlag);
badrat_edit = uicontrol('style','edit',...
                   'string',num2str(ppdef.badratthres),...
                   'position',[ffs+3*fus+2*btn_wid*2/3 ffs+fus+(fus+uv)*(nv-irow) btn_wid*2/3 uv],...
                   'callback',@doSetBadratThres);
               
irow = 8; % 8th row from top
%---------------------------
if isempty(ppdef.eqlflag)
    val = 1;
    enable = 'off';
else
    val = ppdef.eqlflag;
    enable = 'on';
end
ad.eql_chkbx_pos = [ffs+fus ffs+fus+(fus+uv)*(nv-irow) btn_wid*3/2 uv];
ad.eql_chkbx = uicontrol('style','checkbox',...
                   'string','EQL test (grids only)',...
                   'value',val,...
                   'enable',enable,...
                   'tooltip','Mark "low-quality" triangles',...
                   'position',ad.eql_chkbx_pos,...
                   'callback',@doSetEqlFlag);
eql_edit = uicontrol('style','edit',...
                   'string',num2str(ppdef.eqlthres),...
                   'tooltip','Trigrid defaults: 1.2 - blue; 1.4 - yellow; 2.0 - red',...
                   'position',[ffs+3*fus+2*btn_wid*2/3 ffs+fus+(fus+uv)*(nv-irow) btn_wid*2/3 uv],...
                   'callback',@doSetEqlThres);

irow = 9; % 9th row from top
%---------------------------
if isempty(ppdef.dhhflag)
    val = 1;
    enable = 'off';
else
    val = ppdef.dhhflag;
    enable = 'on';
end
ad.dhh_chkbx_pos = [ffs+fus ffs+fus+(fus+uv)*(nv-irow) btn_wid*3/2 uv];
ad.dhh_chkbx = uicontrol('style','checkbox',...
                   'string','dh/h (grids only)',...
                   'value',val,...
                   'enable',enable,...
                   'tooltip','Mark triangles with large dh/h',...
                   'position',ad.dhh_chkbx_pos,...
                   'callback',@doSetDHHFlag);
dhh_edit = uicontrol('style','edit',...
                   'string',num2str(ppdef.dhhthres),...
                   'tooltip','Hannah/Greenberg default: 0.3',...
                   'position',[ffs+3*fus+2*btn_wid*2/3 ffs+fus+(fus+uv)*(nv-irow) btn_wid*2/3 uv],...
                   'callback',@doSetDHHThres);

               
irow = 10; % 10th row from top
%---------------------------
if isempty(ppdef.bridgemarkerflag)
    val = 1;
    enable = 'off';
else
    val = ppdef.bridgemarkerflag;
    enable = 'on';
end
ad.bridgemarker_chkbx_pos = [ffs+fus ffs+fus+(fus+uv)*(nv-irow) btn_wid*3/2 uv];
ad.bridgemarker_chkbx = uicontrol('style','checkbox',...
                   'string','Mark "bridges" (grids only)',...
                   'value',val,...
                   'enable',enable,...
                   'tooltip','Mark edges connecting two non-consequitive boundary nodes',...
                   'position',ad.bridgemarker_chkbx_pos,...
                   'callback',@doSetBridgeMarkerFlag);

irow = 11; % 11th row from top
%---------------------------
if isempty(ppdef.codemarkerflag)
    val = 1;
    enable = 'off';
else
    val = ppdef.codemarkerflag;
    enable = 'on';
end
ad.codemarker_chkbx_pos = [ffs+fus ffs+fus+(fus+uv)*(nv-irow) btn_wid uv];
ad.codemarker_chkbx = uicontrol('style','checkbox',...
                   'string','Show code markers',...
                   'value',val,...
                   'enable',enable,...
                   'tooltip','Colored node code markers',...
                   'position',ad.codemarker_chkbx_pos,...
                   'callback',@doSetCodeMarkerFlag);
               
irow = 12; % 12th row from top
%---------------------------
if isempty(ppdef.bathycolorflag)
    val = 1;
    enable = 'off';
else
    val = ppdef.bathycolorflag;
    enable = 'on';
end
ad.bathycolor_chkbx_pos = [ffs+fus ffs+fus+(fus+uv)*(nv-irow) btn_wid*2 uv];
ad.bathycolor_chkbx = uicontrol('style','checkbox',...
                   'string','Show bathymetry with color',...
                   'value',val,...
                   'enable',enable,...
                   'tooltip','Colored node code markers',...
                   'position',ad.bathycolor_chkbx_pos,...
                   'callback',@doSetBathyColorFlag);
               
               
% bottom row
%-----------
ok_btn = uicontrol('style','pushbutton',...
                   'string',okstring,...
                   'position',[ffs+fus ffs+fus btn_wid uv],...
                   'callback',@doOK);

cancel_btn = uicontrol('style','pushbutton',...
                       'string',cancelstring,...
                       'position',[ffs+2*fus+btn_wid ffs+fus btn_wid uv],...
                       'callback',@doCancel);


set([fig, ok_btn, cancel_btn], 'keypressfcn', {@doKeypress, width_edit});

set(fig,'position',getnicedialoglocation(fp, get(fig,'Units')));

setdefaultbutton(fig, ok_btn); % make ok_btn the default button

% make sure we are on screen
movegui(fig)
set(fig, 'visible','on'); drawnow;

% default plotting parameters for objects
ad.value = 0;
ad.pp = ppdef;
setappdata(0,'dlgPlotParamAppData__',ad);

try
    % Give default focus to the OK button *after* the figure is made visible
    uicontrol(ok_btn);
    uiwait(fig);
catch
    if ishandle(fig)
        delete(fig)
    end
end

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
    ok = ad.value;
    pp = ad.pp;
    rmappdata(0,'dlgPlotParamAppData__')
else
    % figure was deleted
    ok = 0;
    pp = [];
end


% figure, OK and Cancel KeyPressFcn
function doKeypress(src, evd) %#ok<INUSL>
% if get(editwidth,'Selected','on') %%% this is not a solution
%     try %#ok<TRYNC>
%         width = str2double(get(src,'String'));
%         set(hp,'LineWidth',width)
%     end
% else
    switch evd.Key
     case 'escape'
      doCancel([],[]);
    end
% end


% OK callback
function doOK(src, evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
else
    error('No required appdata found') %%%%%%%%%%
end
ad.value = 1;
setappdata(0,'dlgPlotParamAppData__',ad);
delete(gcbf);


% Cancel callback
function doCancel(cancel_btn, evd) %#ok<INUSD>
ad.value = 0;
ad.pp = [];
setappdata(0,'dlgPlotParamAppData__',ad)
delete(gcbf);


function doSetColor(src,evd,hp) %#ok<INUSL>

color = uisetcolor(get(hp,'Color'),'Set color of the new object');
if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.color = color;
setappdata(0,'dlgPlotParamAppData__',ad)

set(hp,'Color',ad.pp.color)


function doSetWidth(src,evd,hp) %#ok<INUSL>

set(gcbf,'KeyPressFcn',{@widthkeypress,src})
if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.width = str2double(get(src,'String'));
setappdata(0,'dlgPlotParamAppData__',ad)
set(hp,'LineWidth',ad.pp.width)


function doSetStyle(src,evd,hp) %#ok<INUSL>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
stylestring = get(src,'String');
ad.pp.style = stylestring{get(src,'Value')};
setappdata(0,'dlgPlotParamAppData__',ad)

set(hp,'LineStyle',ad.pp.style)


function doSetMarker(src,evd,hp) %#ok<INUSL>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
stylestring = get(src,'String');
ad.pp.marker = stylestring{get(src,'Value')};
setappdata(0,'dlgPlotParamAppData__',ad)

set(hp,'Marker',ad.pp.marker)


function doSetSize(src,evd,hp) %#ok<INUSL>

set(gcbf,'KeyPressFcn',{@widthkeypress,src})
if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.markersize = str2double(get(src,'String'));
setappdata(0,'dlgPlotParamAppData__',ad)
set(hp,'LineWidth',ad.pp.markersize)


function doSetEndmarkerFlag(src,evd,h1st,hlast) %#ok<INUSL>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.endmarkerflag = get(src,'Value');
setappdata(0,'dlgPlotParamAppData__',ad)

if ad.pp.endmarkerflag
    set(h1st,'Visible','on')
    set(hlast,'Visible','on')
else
    set(h1st,'Visible','off')
    set(hlast,'Visible','off')
end


function doSetBadlenFlag(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.badlenflag = get(src,'Value');
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetBadlenThres(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.badlenthres = str2double(get(src,'String'));
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetBadangFlag(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.badangflag = get(src,'Value');
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetBadangThres(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.badangthres = str2double(get(src,'String'))*pi/180; % to rad
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetBadratFlag(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.badratflag = get(src,'Value');
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetBadratThres(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.badratthres = str2double(get(src,'String'));
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetEqlFlag(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.eqlflag = get(src,'Value');
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetEqlThres(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.eqlthres = str2double(get(src,'String'));
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetDHHFlag(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.dhhflag = get(src,'Value');
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetDHHThres(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.dhhthres = str2double(get(src,'String'));
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetBridgeMarkerFlag(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.bridgemarkerflag = get(src,'Value');
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetCodeMarkerFlag(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.codemarkerflag = get(src,'Value');
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetBathyColorFlag(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.bathycolorflag = get(src,'Value');
setappdata(0,'dlgPlotParamAppData__',ad)


function wbd(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
else
    return
end
cp = get(src,'CurrentPoint');
x = cp(1);
y = cp(2);

% enable greyed-out check boxes on click
if strcmp(get(ad.badlen_chkbx,'Enable'),'off')
    bp = ad.badlen_chkbx_pos;
    if x>=bp(1) && x<=bp(1)+bp(3) && y>=bp(2) && y<=bp(2)+bp(4)
        set(ad.badlen_chkbx,'Enable','on')
    end
end
if strcmp(get(ad.badang_chkbx,'Enable'),'off')
    bp = ad.badang_chkbx_pos;
    if x>=bp(1) && x<=bp(1)+bp(3) && y>=bp(2) && y<=bp(2)+bp(4)
        set(ad.badang_chkbx,'Enable','on')
    end
end
if strcmp(get(ad.badrat_chkbx,'Enable'),'off')
    bp = ad.badrat_chkbx_pos;
    if x>=bp(1) && x<=bp(1)+bp(3) && y>=bp(2) && y<=bp(2)+bp(4)
        set(ad.badrat_chkbx,'Enable','on')
    end
end
if strcmp(get(ad.eql_chkbx,'Enable'),'off')
    bp = ad.eql_chkbx_pos;
    if x>=bp(1) && x<=bp(1)+bp(3) && y>=bp(2) && y<=bp(2)+bp(4)
        set(ad.eql_chkbx,'Enable','on')
    end
end
if strcmp(get(ad.codemarker_chkbx,'Enable'),'off')
    bp = ad.codemarker_chkbx_pos;
    if x>=bp(1) && x<=bp(1)+bp(3) && y>=bp(2) && y<=bp(2)+bp(4)
        set(ad.codemarker_chkbx,'Enable','on')
    end
end
if strcmp(get(ad.bathycolor_chkbx,'Enable'),'off')
    bp = ad.bathycolor_chkbx_pos;
    if x>=bp(1) && x<=bp(1)+bp(3) && y>=bp(2) && y<=bp(2)+bp(4)
        set(ad.bathycolor_chkbx,'Enable','on')
    end
end