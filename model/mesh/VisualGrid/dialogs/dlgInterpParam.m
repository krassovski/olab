function [ok,zlw,zhw,orphLWinc,orphHWinc] = dlgInterpParam(zlw,zhw)
%dlgInterpParam dialog to set parameters for in terpolating (morphing)
%between polygons in VisualGrid.
%   Creates a modal dialog box which allows setting the parameters.
% 
%   Input:
%   zlw,zhw     default elevations for two polygon sets.  
%
%   Output:
%   OK is 1 if you push the OK button, or 0 if you push the Cancel button or
%   close the figure.
%   Returns zlw,zhw and flags orphLWinc,orphHWinc set by user. All set to [] if OK is 0.
%
% M.Krassovski, 2011
% Parts of code are based on the standard LISTDLG function.


figname = 'Polygon Morphing Parameters';
okstring = 'OK';
cancelstring = 'Cancel';

% default values for the flags
orphLWdef = 1;
orphHWdef = 1;

fus = 8;  % frame/uicontrol spacing, in pixels
ffs = 8;  % frame/figure spacing, in pixels
uh = 120; % uicontrol button horizontal dimension (width), in pixels
uv = 22;  % uicontrol button vertical dimension (height), in pixels

nv = 6; % number of ui elements (e.g. buttons or edit fields) in the vertical
nh = 2; % -- in the horizontal

fp = get(0,'defaultfigureposition');
h = 2*ffs + (nh-1)*fus + nh*uh;
v = 2*ffs + nv*fus + nv*uv;
fp = [fp(1) fp(2)+fp(4)-v h v];  % keep upper left corner fixed

fig_props = { ...
    'name'                   figname ...
    'color'                  get(0,'defaultUicontrolBackgroundColor') ...
    'resize'                 'off' ...
    'numbertitle'            'off' ...
    'menubar'                'none' ...
    'windowstyle'            'modal' ...
    'visible'                'off' ...
    'createfcn'              ''    ...
    'position'               fp   ...
    'closerequestfcn'        'delete(gcbf)' ...
%     'WindowButtonDownFcn'    @wbd
            };

fig = figure(fig_props{:});

btn_wid = (fp(3)-2*(ffs+fus)-fus)/2;

% % default plotting parameters for objects
% ad.value = 0;
% ad.zlw = [];
% ad.zhw = [];
% ad.orphLWinc = [];
% ad.orphHWinc = [];
% setappdata(0,'dlgPlotParamAppData__',ad);

irow = 1; % 1st row from top
%---------------------------
uicontrol('Style','text','String','Z of the LW contour:','HorizontalAlignment','left',...
    'position',[ffs+fus ffs+fus+(fus+uv)*(nv-irow) btn_wid*1.5 uv])

zlw_edit = uicontrol('style','edit',...
                   'string',num2str(zlw),...
                   'position',[ffs+2*fus+btn_wid*1.5 ffs+fus+(fus+uv)*(nv-irow) btn_wid*0.5 uv],...
                   'callback',@doSetZLW);

irow = 2; % 2nd row from top
%---------------------------
uicontrol('Style','text','String','Z of the HW contour:','HorizontalAlignment','left',...
    'position',[ffs+fus ffs+fus+(fus+uv)*(nv-irow) btn_wid*1.5 uv])

zhw_edit = uicontrol('style','edit',...
                   'string',num2str(zhw),...
                   'position',[ffs+2*fus+btn_wid*1.5 ffs+fus+(fus+uv)*(nv-irow) btn_wid*0.5 uv],...
                   'callback',@doSetZHW);


irow = 3; % 3rd row from top
%---------------------------
lwinc_chkbx_pos = [ffs+fus ffs+fus+(fus+uv)*(nv-irow) btn_wid*2 uv];
lwinc_chkbx = uicontrol('style','checkbox',...
                   'string','Include "orphan" LW contours',...
                   'value',orphLWdef,...
                   'position',lwinc_chkbx_pos,...
                   'callback',@doSetLWincFlag);

irow = 4; % 4th row from top
%---------------------------
hwinc_chkbx_pos = [ffs+fus ffs+fus+(fus+uv)*(nv-irow) btn_wid*2 uv];
hwinc_chkbx = uicontrol('style','checkbox',...
                   'string','Include "orphan" HW contours',...
                   'value',orphHWdef,...
                   'position',hwinc_chkbx_pos,...
                   'callback',@doSetHWincFlag);

% bottom row
%-----------
ok_btn = uicontrol('style','pushbutton',...
                   'string',okstring,...
                   'position',[ffs+fus ffs+fus btn_wid uv],...
                   'callback',@doOK);

cancel_btn = uicontrol('style','pushbutton',...
                       'string',cancelstring,...
                       'position',[ffs+2*fus+btn_wid ffs+fus btn_wid uv],...
                       'callback',@doCancel);


set([fig, ok_btn, cancel_btn], 'keypressfcn', @doKeypress);

set(fig,'position',getnicedialoglocation(fp, get(fig,'Units')));

setdefaultbutton(fig, ok_btn); % make ok_btn the default button

% make sure we are on screen
movegui(fig)
set(fig, 'visible','on'); drawnow;

% default plotting parameters for objects
ad.value = 0;
ad.zlw = [];
ad.zhw = [];
ad.orphLWinc = orphLWdef;
ad.orphHWinc = orphHWdef;
setappdata(0,'dlgPlotParamAppData__',ad);

try
    % Give default focus to the OK button *after* the figure is made visible
    uicontrol(ok_btn);
    uiwait(fig);
catch
    if ishandle(fig)
        delete(fig)
    end
end

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
    ok = ad.value;
    zlw = ad.zlw;
    zhw = ad.zhw;
    orphLWinc = ad.orphLWinc;
    orphHWinc = ad.orphHWinc;
    rmappdata(0,'dlgPlotParamAppData__')
else
    % figure was deleted
    ok = 0;
    zlw = [];
    zhw = [];
    orphLWinc = [];
    orphHWinc = [];
end


% figure, OK and Cancel KeyPressFcn
function doKeypress(src, evd) %#ok<INUSL>
switch evd.Key
 case 'escape'
  doCancel([],[]);
end


% OK callback
function doOK(src, evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
else
    error('No required appdata found') %%%%%%%%%%
end
ad.value = 1;
setappdata(0,'dlgPlotParamAppData__',ad);
delete(gcbf);


% Cancel callback
function doCancel(cancel_btn, evd) %#ok<INUSD>
ad.value = 0;
ad.zlw = [];
ad.zhw = [];
ad.orphLWinc = [];
ad.orphHWinc = [];
setappdata(0,'dlgPlotParamAppData__',ad)
delete(gcbf);


function doSetZLW(src,evd) %#ok<INUSD>
if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.zlw = str2double(get(src,'String'));
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetZHW(src,evd) %#ok<INUSD>
if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.zhw = str2double(get(src,'String'));
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetLWincFlag(src,evd) %#ok<INUSD>
if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.orphLWinc = get(src,'Value');
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetHWincFlag(src,evd) %#ok<INUSD>
if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.orphHWinc = get(src,'Value');
setappdata(0,'dlgPlotParamAppData__',ad)