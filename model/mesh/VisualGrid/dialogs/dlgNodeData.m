function [ok,pp] = dlgNodeData(selstr,def)
%dlgNodeData dialog to view and set data for selected nodes in VisualGrid.
%   Creates a modal dialog box which allows setting node data.
% 
%   Input:
%   selstr   cell array of strings representing selected objects and nodes.
%   def      is a structure containing default data, e.g. common for all nodes.
%
%   Output:
%   OK is 1 if you push the OK button, or 0 if you push the Cancel button or
%   close the figure.
%   Returns node data possibly updated by userp in PP structure with same
%   fields as DEF. This will be [] when OK is 0.
%
% M.Krassovski, 2011
% Parts of code are based on the standard LISTDLG function.

figname = 'Data for Selected Node(s)';
okstring = 'OK';
cancelstring = 'Cancel';

fus = 8;  % frame/uicontrol spacing, in pixels
ffs = 8;  % frame/figure spacing, in pixels
lv = 100;  % list vertical dimension (height), in pixels
uh = 160; % uicontrol button horizontal dimension (width), in pixels
uv = 22;  % uicontrol button vertical dimension (height), in pixels

nv = 6; % number of ui elements (e.g. buttons or edit fields) in the vertical (excluding the list)
nh = 2; % -- in the horizontal

fp = get(0,'defaultfigureposition');
h = 2*ffs + (nh-1)*fus + nh*uh;
v = 2*ffs + (nv-1+1)*fus + nv*uv + lv;
fp = [fp(1) fp(2)+fp(4)-v h v];  % keep upper left corner fixed

fig_props = { ...
    'name'                   figname ...
    'color'                  get(0,'defaultUicontrolBackgroundColor') ...
    'resize'                 'off' ...
    'numbertitle'            'off' ...
    'menubar'                'none' ...
    'windowstyle'            'modal' ...
    'visible'                'off' ...
    'createfcn'              ''    ...
    'position'               fp   ...
    'closerequestfcn'        'delete(gcbf)' ...
            };
%             'WindowButtonDownFcn'    @wbd

fig = figure(fig_props{:});

btn_wid = (fp(3)-2*(ffs+fus)-fus)/nh;
% btn_wid = uh; %%%%%%%%%%%

% default plotting parameters for objects
ad.value = 0;
ad.pp = def;
setappdata(0,'dlgPlotParamAppData__',ad);

%---------------------------
irow = 1; % 1st row from top
uicontrol('Style','text','String','Selection list:','HorizontalAlignment','Left',...
    'position',[ffs*3 ffs+fus+(fus+uv)*(nv-irow)+lv btn_wid*2 uv])
selecton_list = uicontrol('style','edit',...
                   'string',selstr,...
                   'position',[ffs*3 ffs+fus+(fus+uv)*(nv-irow) btn_wid*2 lv],...
                   'max',3,'min',1,... % values do not mater, max-min must be > 1
                   'Enable','inactive');

%---------------------------
irow = 2; % 2nd row from top
uicontrol('Style','text','String','x:','HorizontalAlignment','right',...
    'position',[ffs ffs+fus+(fus+uv)*(nv-irow) btn_wid/2 uv])
x_edit = uicontrol('style','edit',...
                   'string',num2str(def.x),...
                   'position',[ffs+fus+btn_wid/2 ffs+fus+(fus+uv)*(nv-irow) btn_wid*1.5 uv],...
                   'callback',@doSetX);

%---------------------------
irow = 3; % 3rd row from top
uicontrol('Style','text','String','y:','HorizontalAlignment','right',...
    'position',[ffs ffs+fus+(fus+uv)*(nv-irow) btn_wid/2 uv])
y_edit = uicontrol('style','edit',...
                   'string',num2str(def.y),...
                   'position',[ffs+fus+btn_wid/2 ffs+fus+(fus+uv)*(nv-irow) btn_wid*1.5 uv],...
                   'callback',@doSetY);

%---------------------------
irow = 4; % 4th row from top
uicontrol('Style','text','String','z:','HorizontalAlignment','right',...
    'position',[ffs ffs+fus+(fus+uv)*(nv-irow) btn_wid/2 uv])
z_edit = uicontrol('style','edit',...
                   'string',num2str(def.z),...
                   'position',[ffs+fus+btn_wid/2 ffs+fus+(fus+uv)*(nv-irow) btn_wid*1.5 uv],...
                   'callback',@doSetZ);

%---------------------------
irow = 5; % 5th row from top
uicontrol('Style','text','String','code:','HorizontalAlignment','right',...
    'position',[ffs ffs+fus+(fus+uv)*(nv-irow) btn_wid/2 uv])
code_edit = uicontrol('style','edit',...
                   'string',num2str(def.code),...
                   'position',[ffs+fus+btn_wid/2 ffs+fus+(fus+uv)*(nv-irow) btn_wid*1.5 uv],...
                   'callback',@doSetCode);               


% bottom row
%-----------
ok_btn = uicontrol('style','pushbutton',...
                   'string',okstring,...
                   'position',[ffs+fus ffs+fus btn_wid uv],...
                   'callback',@doOK);

cancel_btn = uicontrol('style','pushbutton',...
                       'string',cancelstring,...
                       'position',[ffs+2*fus+btn_wid ffs+fus btn_wid uv],...
                       'callback',@doCancel);


set([fig, ok_btn, cancel_btn], 'keypressfcn', @doKeypress);

set(fig,'position',getnicedialoglocation(fp, get(fig,'Units')));

setdefaultbutton(fig, ok_btn); % make ok_btn the default button

% make sure we are on screen
movegui(fig)
set(fig, 'visible','on'); drawnow;

% default plotting parameters for objects
ad.value = 0;
ad.pp = def;
setappdata(0,'dlgPlotParamAppData__',ad);

try
    % Give default focus to the OK button *after* the figure is made visible
    uicontrol(ok_btn);
    uiwait(fig);
catch
    if ishandle(fig)
        delete(fig)
    end
end

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
    ok = ad.value;
    pp = ad.pp;
    rmappdata(0,'dlgPlotParamAppData__')
else
    % figure was deleted
    ok = 0;
    pp = [];
end


% figure, OK and Cancel KeyPressFcn
function doKeypress(src, evd) %#ok<INUSL>
% if get(editwidth,'Selected','on') %%% this is not a solution
%     try %#ok<TRYNC>
%         width = str2double(get(src,'String'));
%         set(hp,'LineWidth',width)
%     end
% else
    switch evd.Key
     case 'escape'
      doCancel([],[]);
    end
% end


% OK callback
function doOK(src, evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
else
    error('No required appdata found') %%%%%%%%%%
end
ad.value = 1;
setappdata(0,'dlgPlotParamAppData__',ad);
delete(gcbf);


% Cancel callback
function doCancel(cancel_btn, evd) %#ok<INUSD>
ad.value = 0;
ad.pp = [];
setappdata(0,'dlgPlotParamAppData__',ad)
delete(gcbf);


function doSetX(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.x = str2num(get(src,'String')); %#ok<ST2NM> % use str2num - it gives empty on empty string ''
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetY(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.y = str2num(get(src,'String')); %#ok<ST2NM> % use str2num - it gives empty on empty string ''
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetZ(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.z = str2num(get(src,'String')); %#ok<ST2NM> % use str2num - it gives empty on empty string ''
setappdata(0,'dlgPlotParamAppData__',ad)


function doSetCode(src,evd) %#ok<INUSD>

if isappdata(0,'dlgPlotParamAppData__')
    ad = getappdata(0,'dlgPlotParamAppData__');
end
ad.pp.code = str2num(get(src,'String')); %#ok<ST2NM> % use str2num - it gives empty on empty string ''
setappdata(0,'dlgPlotParamAppData__',ad)