***************
VisualGrid docs
***************

VisualGrid is a GUI tool to work with bathymetry and coastlines, as well as to
generate and edit triangular grids.

Disclaimer
==========

This is a highly experimental software that was tested only for certain 
cases in the course of work on particular projects. 
It is undoubtedly full of bugs and has a lot of ad-hoc features.
Use at your own risk.

Acknowledgements
==================

VisualGrid was inspired by TRIGRID written by Roy Walters and Falconer Henry.


Objects supported
=================

The following types of objects are supported:

1.   set of stand alone nodes, e.g. bathymetry soundings;

2.   polyline (nodes in consecutive order with each continuous segment
     terminated with NaN), polygon being a special case with identical
     1st and last nodes;

3.   triangular grid;

4.   image.


Functionality
=============

To edit objects make sure the selection tool (the arrow) is selected. 
If it's not working click within the main axes.

Mouse actions are similar to popular vector-graphics editing software, e.g. Inkscape, CorelDraw.
You can select a node; drag and drop it; select multiple nodes with rubber-box 
selection or by holding Shift key.

Zoom using Shift-scroll. Select the zoom tool and double click within the axes to zoom to all objects.

Scroll using mouse scroll in the vertical and Control-scroll in the horizontal.

Z-scroll changes z-value (depth) of the node in focus.

Save/open work in the native vg format (mat file containing specific structure).

Import data in various formats (shp, Trigrid nod and ngh/nei, xyz ASCII files).

Import/export from Matlab base workspace.

Import Google Maps satellite images based on the specified UTM zone and image resolution.
See Edit -> Google Map Settings.

Import custom images, e.g. nav charts; georeference them (see below); digitize bathymetry 
soundings using "freenode" tool.

Generate FEM grids with area-specific custom size functions. 
Slightly modified Mesh2d code is used as a grid-generation engine.

Undo is implement for many (but not all!) actions. There is no redo, however.

Explore menu and context menu for more functionality.


Image georeferencing based on the available coastline
=====================================================

Georeferencing is based on matching three reference points of the coastline with
the corresponding three points in the image.

1. Import image and coastline.

2. Create two 3-point polylines:

   a. On top of the image, connecting three distinct points,

   b. On top of the coastline, connecting the corresponding three points in the same order.

3. Make sure the "coast" 3-point polyline goes before the "image" 3-point polyline in the 
   object list. Use Transform -> Bring to front/back menus to order the objects in the list.

4. Select both 3-point polylines.

5. Go to Transform -> Define Transform menu; save the transform info in a file.

6. Select the image only.

7. Go to Transform -> Apply Transform menu. The image should now be transformed to 
   match the coastline (at least at the reference points).
