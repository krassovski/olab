function h = gns_depth_refdist(xi,yi,b,h2z,hmin,hmax,xr,yr,h2dist,dmax)
% Grid node spacing as linear function of z and distance from a reference
% point, whichever is greater. A service routine for mesh2d.
% xi,yi     target location
% b         spatial function (e.g. bathymetry) structure with fields
%           x,y,z,tri; tri is optional, if no tri supplied, bathymetry is
%           triangulated by delaunay.m
% h2z       node spacing to z ratio
% hmin,hmax lower and upper limits for the resulting node spacing
% xr,yr     reference location
% h2z2dist  dependence of h2z from distance to distance from xr,yr ratio
% dmax      upper distance limit for distance dependence

x = b.x;
y = b.y;
z = b.z;
if ~isfield(b,'tri')
    tri = delaunay(x,y);
else
    tri = b.tri;
end

% z-dependence
i = tsearch(x,y,tri,xi,yi);
zi = tinterp([x y],tri,z,[xi yi],i);
h = zi*h2z;

% distance-dependence
if ~isempty(xr)
    d = sqrt((xr-xi).^2+(yr-yi).^2); % dist from ref location
    hdepthdep = d*h2dist;
    if ~isempty(dmax)
        hdepthdep(d>dmax) = hmax;
    end
    h(h<hdepthdep) = hdepthdep(h<hdepthdep);
end

h(h<hmin) = hmin;
h(h>hmax) = hmax;