function h = gns_depth_refpoly(xi,yi,b,a)
% Grid node spacing as linear function of z. Individual parameters
% specified in struct array prm, 1st element representing entire area and
% each subsequent a selected polygonal region. For overlapping polygons the
% lesser value of h is taken.
% A service routine for mesh2d.
%
% xi,yi     target location
% b         spatial function (e.g. bathymetry) structure with fields
%           x,y,z,tri; tri is optional, if no tri supplied, bathymetry is
%           triangulated by delaunay.m
% a         parameters for each individual area:
%   poly      n�2 polygon defining the area, possibly multiply-connected (NaN-separated)
%   h2z       node spacing to z ratio (optional)
%   hmin,hmax lower and upper limits for node spacing

warning('off','MATLAB:tsearch:DeprecatedFunction')
warning('off','MATLAB:dsearch:DeprecatedFunction')
c = onCleanup(@cleanup_this);

if isfield(a,'h2z')
    x = b.x;
    y = b.y;
    z = b.z;
    if ~isfield(b,'tri')
        tri = delaunay(x,y);
    else
        tri = b.tri;
    end
end

h = Inf(size(xi));
if numel(h)==0
    return
end

for ia = 1:length(a)
    if ia==1
        in = (1:length(xi))'; % 1st area includes all nodes, poly is disregarded
    else 
        [node,edge] = prepnodes_poly(a(ia).poly);
        in = inpoly([xi(:) yi(:)], node, edge); % works for multiply-connected polygons
%         in = inpolygon(xi, yi, a(ia).poly(:,1), a(ia).poly(:,2)); % takes simple polygons only
    end
    
    if isfield(a,'h2z')
        % z-dependence
        it = tsearch(x,y,tri,xi(in),yi(in));
        zi = tinterp([x y],tri,z,[xi(in) yi(in)],it);
        hi = zi*a(ia).h2z;
    else
        % no z-dependence
        hi = Inf(size(in)); % no constraint
    end

    % min-max constraints
    hi(hi<a(ia).hmin) = a(ia).hmin;
    hi(hi>a(ia).hmax) = a(ia).hmax;
    
    % update h values which are lower then previous
    h(in) = min(h(in),hi);
end


function cleanup_this()
warning('on','MATLAB:tsearch:DeprecatedFunction')
warning('on','MATLAB:dsearch:DeprecatedFunction')