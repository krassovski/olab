function h = gns_sqrtdepth_refpoly(xi,yi,b,a)
% Grid node spacing proportional to sqrt(z):   
% h = h0 + h2z * sqrt(z), where h0 is spacing at z=0.
% Individual parameters specified in struct array a, 1st element 
% representing entire area and each subsequent a selected polygonal region.
% For overlapping polygons the lesser value of h is taken.
% A service routine for mesh2d.
%
% xi,yi     target location
% b         spatial function (e.g. bathymetry) structure with fields
%           x,y,z,tri; tri is optional, if no tri supplied, bathymetry is
%           triangulated by delaunay.m
% a         parameters for each individual area:
%   poly      [n x 2] polygon defining the area, possibly multiply-connected (NaN-separated)
%   h0        node spacing for z=0
%   h2z       "relaxation" coefficient
%   dzthres   dz/z threshold, the parameter for smooth bathymetry 
%             (e.g. the one which is supplied to
%             depth_smooth_hannah2_greenberg.f);
%             if this parameter is used the bathymetry distortion after
%             smoothing is reduced.
%   dzthres_zmin    do not apply dzthres for depths less than this
%   hmin      lower limit for node spacing
%   hmax      upper limit for node spacing

warning('off','MATLAB:tsearch:DeprecatedFunction')
warning('off','MATLAB:dsearch:DeprecatedFunction')
c = onCleanup(@cleanup_this);

if isfield(a,'h2z') || isfield(a,'dzthres') && ~isempty(b)
    x = b.x;
    y = b.y;
    z = b.z;
    z(z<0) = 0; %mk 2017-02-10: to avoid complex size function, h, at z<0
    if ~isfield(b,'tri')
        tri = delaunay(x,y);
    else
        tri = b.tri;
    end
end

h = Inf(size(xi));
if numel(h)==0
    return
end

for ia = 1:length(a)
    if ia==1
%         in = (1:length(xi))'; % 1st area includes all nodes, poly is disregarded
        in = true(size(xi)); % 1st area includes all nodes, poly is disregarded
    else 
        [node,edge] = prepnodes_poly(a(ia).poly);
        in = inpoly([xi(:) yi(:)], node, edge); % works for multiply-connected polygons
%         in = inpolygon(xi, yi, a(ia).poly(:,1), a(ia).poly(:,2)); % takes simple polygons only
    end
    ni = sum(in);
    if ni==0
        continue
    end
    
    xiin = xi(in);
    yiin = yi(in);
    if isfield(a,'h2z') || isfield(a,'dzthres') && ~isempty(b)
        it = tsearch(x,y,tri,xiin,yiin); %#ok<DGEO4>
        zi = tinterp([x y],tri,z,[xiin yiin],it);
    end
    
    % z-dependence
    if isfield(a,'h2z') && ~isempty(b)
%         hi = zi*a(ia).h2z;
%         hi = sqrt((a(ia).h2z^2)*zi + a(ia).hmin^2); % was before 16-Oct-2015
        hi = a(ia).h0 + a(ia).h2z * zi.^.5;   % changed to this on 16-Oct-2015
        %mk if depth is negative this will be a complex number
    else
        % no z-dependence
        hi = Inf(ni,1); % no constraint
    end
    
    % maximum h constraint
    hi(hi>a(ia).hmax) = a(ia).hmax;
    
    % apply dz/z constraint (smoothing): decrease size function at steep
    % relative bathymetry gradients
    if isfield(a,'dzthres') && ~isempty(b)
        rz = zeros(ni,1);
%         for k = 1:ni
        for k = find(~(zi < a(ia).dzthres_zmin))' % for locations with z>dzthres_zmin, NaNs work as -Inf
            d = ((xiin(k)-x).^2 + (yiin(k)-y).^2).^.5; % dist from each test location to all bathy points
            iclose = d < hi(k)/2; % within hi/2 radius
            if any(iclose)
%                 rz(k) = range(z(iclose)); % maximum depth change within the radius
                rz(k) = max(z(iclose))-min(z(iclose)); % maximum depth change within the radius
            end
        end
        dzc = a(ia).dzthres .* zi ./ rz; % reduction coefficient
        dzc(dzc>1 | ~isfinite(dzc)) = 1; % do not increase size function; and no change if zi==0, rz==0
        hi = hi .* dzc;
    end

    % minimum h constraint
    hi(hi<a(ia).hmin) = a(ia).hmin;
    
    % update h values which are lower than previous
    h(in) = min(h(in),hi);
end


function cleanup_this()
warning('on','MATLAB:tsearch:DeprecatedFunction')
warning('on','MATLAB:dsearch:DeprecatedFunction')