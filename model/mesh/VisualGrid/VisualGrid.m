function VisualGrid()
% Coastline, bathymetry, and triangular grid editing GUI.

% Object types supported:
%     1   set of stand alone nodes, e.g. bathymetry soundings
%     2   polyline (nodes in consequitive order with each continous segment
%         terminated with NaN), polygon being a special case with identical
%         1st and last nodes
%     3   triangular grid
%     4   image

VG_NAME = 'VisualGrid';
VG_VERSION = []; %3.08
VG_AUTHOR = 'Maxim Krassovski';
VG_YEAR = '2010-2018';
VG_AFFILIATION = 'DFO-IOS';

dev_mode = false;

if ~dev_mode
    warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame')
end

olab_addpath('development')
olab_addpath('geodesy')
olab_addpath('geometry')
olab_addpath('geometry/geom2d')
olab_addpath('geometry/Simplipoly')
olab_addpath('geometry/triangulation')
olab_addpath('image')
olab_addpath('io')
olab_addpath('system')
olab_addpath('model/mesh')
olab_addpath('model/mesh/Mesh2d v24')

olab_addpath('model/mesh/VisualGrid/colormaps')
olab_addpath('model/mesh/VisualGrid/dialogs')
olab_addpath('model/mesh/VisualGrid/docs')
olab_addpath('model/mesh/VisualGrid/icons')
olab_addpath('model/mesh/VisualGrid/size_functions')

% install GUILayout toolbox:
if verLessThan('matlab','8.4') % R2014b
    olab_addpath('development/GUILayout-v1p17')
    olab_addpath('development/GUILayout-v1p17/layout')
    olab_addpath('development/GUILayout-v1p17/Patch')
    olab_addpath('development/GUILayout-v1p17/layoutHelp')
else
    % This version is for MATLAB release R2014b onwards
    olab_addpath('development/GUILayout-v2p31/layout')
end


% environment parameters
%-----------------------

% "about" info
H.vg_name = VG_NAME;
H.vg_version = VG_VERSION;
H.vg_author = VG_AUTHOR;
H.vg_year = VG_YEAR;
H.vg_affiliation = VG_AFFILIATION;

% program path
H.vgpath = fileparts(mfilename('fullpath')); % path to this file

% default plotting parameters for polylines
H.ppdef = obj_default_pp;

H.pthres = 0.005; % threshold (relative to main axes size) to determine nodes in focus
H.hselparam.color = [.5 .5 .5]; % color for selection markers (for selected nodes)
H.hselparam.size = 8; % size for selection markers
H.colornodes_n = 500;    % show at most n nodes in color code mode
H.colornodes_size = 24;  % marker size for color code mode
H.codesub = [0 5; 1 6]; % default parameter for dlgGridSlashParam
H.undolim = 20;
H.zpredefstr = '[-5:0.1:10.9 11:400]';
H.zpredef = eval(H.zpredefstr);

H.keypressed.ctrl = false; %%%%%%%%%%%%
H.keypressed.shift = false; %%%%%%%%%%%%
H.keypressed.alt = false; %%%%%%%%%%%%
H.keypressed.z = false; %%%%%%%%%%%%
% getkeynow('init'); % doesn`t work

% google maps parameters
H.gmap.utmzone = []; % 0 for geographic coordinates
H.gmap.zoomlevel = []; % 1 to 20

% window layout
% margins in pixels
% main axes
H.layout.mal = 50; % left
H.layout.mar = 140; % right with space for colorbar (account for window frame ~50 pixels)
H.layout.mab = 60; % bottom
H.layout.mat = 100; % top (account for window frame ~50 pixels)
% status string
H.layout.msb = 10; % bottom
H.layout.msh = 12; % height


% GUI layout
%-----------

% place the main figure
[H.hfm,H.hleft,H.ah,H.hsel,H.hfoc,H.tbl,H.statstr,H.colortri] = ...
    place_main_window(H.layout,H.hselparam);

% change menu
H.menu = place_menu(H.hfm);

% lay out the toolbar
[H.tools,H.toolgroupex] = place_toolbar(H.hfm);

% define context menue
[H.cmnode,H.cmnodeitem] = place_context_menu();


% initialization
%---------------
[H.obj,H.grp,H.sel,H.undo,H.currentfile,H.notes] = workNew;

% save handles
guidata(H.hfm,H)

set(H.hleft,'ResizeFcn',@cLeftResize)


function [obj,grp,sel,undo,currentfile,notes] = workNew(hsel,h,htbl)
% Initialization of a new work.

if exist('hsel','var')
    clear_selection(hsel,zeros(0,2),[],htbl);
end

if exist('h','var')
    clearhandlestruct(h)
end

if exist('htbl','var') && ishandle(htbl)
    set(htbl,'Data',{})
end

% initialize editable objects
obj = obj_struct(0);

% groups structure
grp = grp_struct(0);

% selection array
sel = zeros(0,2); % 1st col - objn, 2nd col - noden

% initialize undo structure
undo = undo_struct;

currentfile = '';

notes = '';


function ifree = obj_isfree(htbl)
% Indices of objects for which editing is enabled.

d = get(htbl,'Data');
ifree = find(cell2mat(d(:,1)));


function pp = obj_default_pp()
% Default plotting parameters for objects.

pp.color = [0 0 1]; % blue
pp.width = 0.5; % pts
pp.style = '-'; % solid line
pp.marker = '.';
pp.markersize = 12;
pp.colornodes_marker = 'o';
pp.colornodes_size = 24;
pp.endmarkerflag = true;
pp.badangflag = false;
pp.badangthres = 120*pi/180; % rad
pp.badratflag = false;
pp.badratthres = 1.2;
pp.badlenflag = false;
pp.badlenthres = 10; % axes units, e.g. m
pp.eqlflag = false; % "equilateralness" of triangles ==1/quality (quality.m)
pp.eqlthres = 1.4; % [1.2 1.4 2.0 Inf]; % default Trigrid "equilateralness" thresholds
pp.eqlcolor = [1 0 0];  % [0 0 1; 1 1 0; 1 0 0]; % {'b','y','r'} default Trigrid colors
pp.dhhflag = false; % dh/h in triangles
pp.dhhthres = 0.3; % default Hannah/Greenberg threshold
pp.dhhcolor = [1 0 0];
pp.linesmoothing = 'off';
% colors for node codes 4-column array: [code rgb]
% pp.nodecolormap = [0 0 1 1; 1 .8 .4 0; 2 1 .5 0; 5 0 .35 .75; 6 .5 0 0];
pp.bridgemarkerflag = false;
pp.codemarkerflag = false;
pp.bathycolorflag = false;


function [hfm,hLeft,ah,hsel,hfoc,htbl,statstr,colortri] = ...
    place_main_window(lo,hselparam)
% Place the main figure.

mpos = get(0,'MonitorPosition');
mpos = mpos(1,:); % 1st monitor in case of multiple monitors
wpos = mpos+[2 43 -5 -122];
hfm = figure('Name','VisualGrid','NumberTitle','off',...
    'Position',wpos,... %[459 35 1218 948],... % [560 133 1218 858],...
    'WindowScrollWheelFcn',@wswcb,...
    'KeyPressFcn',{@wkpcb,''},'KeyReleaseFcn',@wkrcb,...
    'CloseRequestFcn',@wclosereq); 
    %'Interruptible','off' -- can't do this because rbbox won't work
    % 'BusyAction','cancel' - doesn't seem to speed up the things
    % we need 'KeyPressFcn' and 'KeyReleaseFcn' to keep track of state
    % of Ctrl key; alternatively, implement this check via underlying
    % Java object

% [6 85 min(mpos(3)-508, 1412) min(mpos(4)-161, 919)]

%     'WindowButtonUpFcn',@wbucb); %,... % WindowKeyPressFcn
%     'CloseRequestFcn',@mainClose); 
%     ,'Interruptible','off'

% initially wbmcb callback only tracks close-by objects
% callback routines can be changed later in the program depending on the program state

% use package Tools\Development\GUILayout-v1p10
hboxflex = uiextras.HBoxFlex( 'Parent', hfm );
hLeft = uipanel( 'Parent', hboxflex);
hRight = uipanel( 'Parent', hboxflex);
set( hboxflex, 'Sizes', [-5 -1.4], 'Spacing', 3 ); % relative panel sizes

% object list table
dat = {};
columnname =   {'Editable','Visible',' '};
columnformat = {[],[],'char'}; 
columneditable =  [true true false]; 
columnwidth = {50,50,200};

if ~verLessThan('matlab', '7.6')
%     jt = javax.swing.JTable(2,5);
%     jt.setFocusable(false); %%% not working % so the figure doesn't loose the focus
%     jt.setCellSelectionEnabled(false);
    
    htbl = uitable('Parent',hRight,...
            'Units','normalized','Position',[.05 .05 .9 .9],...
            'Data', dat,... 
            'ColumnName', columnname,...
            'ColumnFormat', columnformat,...
            'ColumnEditable', columneditable,...
            'ColumnWidth',columnwidth,...
            'CellEditCallback',@cbTableEdit);
        
    jscroll = findjobj(htbl);
    jtable = jscroll.getViewport.getComponent(0);
    jtable.setFocusable(false); %%% not working % so the figure doesn't loose the focus
    jtable.setCellSelectionEnabled(false);
%mk     jtable.setSurrendersFocusOnKeystroke(true)
    % hjtable = handle(jtable,'CallbackProperties');
    % set(hjtable, 'FocusLostCallback', @myCallbackFunc);
    % function myCallbackFunc(jtable,eventdata)
    %   component = jtable.getEditorComponent;
    %   if ~isempty(component)
    %      event = javax.swing.event.ChangeEvent(component);
    %      jtable.editingStopped(event);
    %   end
    % end

else
    htbl = NaN;
%     set(hRight,'Units','pixels')
%     panpos = get(hRight,'Position');
%     dat = {true,true,' '};
% %     dat(1,:) = [];
%     htbl = uitable('Parent',hRight,...
%             'Position',[10 10 panpos(3)-20 panpos(4)-20],...
%             'NumColumns',3,...
%             'NumRows',1,...
%             'Data', dat,...
%             'ColumnNames', columnname,...
%             'ColumnWidth',70);
%         
% %     htbl.addTableChangeListener(@cbTableEdit)
%     set(htbl,'DataChangedCallback',@cbTableEdit_7_5)
%         
% %         ,...
% %             'Data', cell(1,3),... 
% %             'Editable',[true true false]
% % 'Position',[.05 .05 .9 .9],...
% %             'DataChangedCallback',@cbTableEdit);
% 
% %     set(htbl.getUIContainer,'Parent',hRight)
% 
%     dat(1,:) = [];
%     set(htbl,'Data',dat)
end


% place main axes
wpos = getpixelposition(hLeft);
ah = axes('Parent',hLeft,...
    'Units','pixel','Position',[lo.mal lo.mab wpos(3)-lo.mar wpos(4)-lo.mat],...
    'HitTest','off'); %,'DataAspectRatio',[1 1 1],'Layer','top'
if verLessThan('matlab','8.4') % R2014b
    set(ah,'DrawMode','fast')
else
%     set(ah,'SortMethod','depth') % use ZData to display overlapping objects (slower)
                                  % incorrectly puts nodes under an image
    set(ah,'SortMethod','childorder') % 'childorder'  faster, but may render objects in incorrect order
end
hold(ah,'on')
grid on
axis equal

% create handles for selection markers
hsel = plot(ah,NaN,NaN,'s','Color',hselparam.color,'MarkerSize',hselparam.size,'HitTest','off');
% .regular = plot(ah,NaN,NaN,'s','Color',hsel.color,'MarkerSize',hsel.size,'HitTest','off');
% hsel.first   = plot(ah,NaN,NaN,'o','Color',hsel.color,'MarkerSize',hsel.size,'HitTest','off');
% hsel.last    = plot(ah,NaN,NaN,'+','Color',hsel.color,'MarkerSize',hsel.size,'HitTest','off');
hfoc(1) = plot(ah,NaN,NaN,'s','Color',hselparam.color,...
            'MarkerSize',max(4,hselparam.size-3),'HitTest','off');
hfoc(2) = text(NaN,NaN,'','Parent',ah,....
            'BackgroundColor',[.7 .9 .7],'HitTest','off');
        
% coloured triangulation for "colornodes" tool
colortri = patch(NaN,NaN,NaN,'EdgeColor','none','FaceColor','interp','Visible','off','FaceAlpha',.8);

% status string
statstr = uicontrol(hLeft,'Style','text',...
    'Position',[lo.mal lo.msb wpos(3)-lo.mar lo.msh],'String','',...
    'HorizontalAlignment','left');
% 'Units','normalized','Position',[.05 .015 .7 .015]


function h = place_menu(hfm)
% Main figure menu.

% change original figure menu
mh = findall(hfm,'Type','uimenu');

% grab some items from the standard Matlab figure menu
mhfigexport = findall(mh,'Label','Expo&rt Setup...');
mhfigprintpreview = findall(mh,'Label','Print Pre&view...');
mhfigprint = findall(mh,'Label','&Print...');
mhcopyfigure = findall(mh,'Label','Copy &Figure');
mhcopyoptions = findall(mh,'Label','Copy &Options...');
mheditcolormap = findall(mh,'Label','Color&map...');
% mhcolorbar = findall(mh,'Label','&Colorbar');
% mhdock = findall(mh,'Label','Dock VisualGrid'); %%%%% works only after 1st Desktop menu call

% upper level menu items
h.file = uimenu(hfm,'Label','&File','Callback',@cbFile);
% h.view = uimenu(hfm,'Label','&View');
h.edit = uimenu(hfm,'Label','&Edit','Callback',@cbEdit);
h.transform = uimenu(hfm,'Label','&Transform','Callback',@cbTransform);
h.nodeset = uimenu(hfm,'Label','&Node Set','Callback',@cbNodeSet);
h.polyline = uimenu(hfm,'Label','&Polyline','Callback',@cbPolyline);
h.grid = uimenu(hfm,'Label','&Grid','Callback',@cbGrid);
h.help = uimenu(hfm,'Label','&Help');

% File menu items
h.filei.new = uimenu(h.file,'Label','&New','Callback',@cbFileNew,'Accelerator','N');
h.filei.open = uimenu(h.file,'Label','&Open...','Callback',@cbFileOpen,'Accelerator','O');
h.filei.save = uimenu(h.file,'Label','&Save','Callback',@cbFileSave,'Accelerator','S',...
    'Separator','on','Enable','off');
h.filei.saveas = uimenu(h.file,'Label','Save &As...','Callback',@cbFileSaveAs);
h.filei.import = uimenu(h.file,'Label','&Import File...','Callback',@cbFileImport,...
    'Separator','on');
% h.menui.filei.loadvar = uimenu(h.file,'Label','Load polyline from the base workspace...','Callback',@cbLoadVar);
% uimenu(h.file,'Label','Load bathymetry from the base workspace...','Callback',@cbLoadBat);
% uimenu(h.file,'Label','Load triangular grid from the base workspace...','Callback',@cbLoadGrid);
%%%%% combine the above three menu items and deal with various types of inputs in the callback
h.filei.loadvar = uimenu(h.file,'Label','Import From Base Workspace...',...
    'Callback',@cbImportVariables);
% h.filei.importgooglemap = uimenu(h.file,'Label','Import Google Map...',...
%     'Callback',@cbImportGoogleMap);

h.filei.export = uimenu(h.file,'Label','&Export','Callback',@cbFileExport);
h.filei.exporti.saveinfile = uimenu(h.filei.export, 'Label', 'Selected Objects To MAT File...', ...
    'Callback', {@cbSaveInFile,true});
h.filei.exporti.saveallinfile = uimenu(h.filei.export, 'Label', 'All Objects To MAT File...', ...
    'Callback', {@cbSaveInFile,false});
h.filei.exporti.tobase = uimenu(h.filei.export, 'Label', 'Selected Objects To Base Workspace...', ...
    'Callback', @cbSaveInBase);
% h.filei.exporti.saveallinbase = uimenu(h.filei.export, 'Label', 'All Polylines To Base Workspace...', ...
%     'Callback', @cbSaveAllInBase);
h.filei.exporti.savenod = uimenu(h.filei.export, 'Label', 'Selection To NOD File...', ...
    'Callback', @cbSaveNOD);
h.filei.exporti.savexyz = uimenu(h.filei.export, 'Label', 'Selection To XYZ File...', ...
    'Callback', @cbSaveXYZ);
h.filei.exporti.savengh = uimenu(h.filei.export, 'Label', 'Selected Grid To NGH File...', ...
    'Callback', @cbSaveNGH);
h.filei.exporti.saveind = uimenu(h.filei.export, 'Label', 'Selected Node Indices...', ...
    'Callback', @cbSaveIndices);

if ~isempty(mhfigexport)
    cb = get(mhfigexport,'Callback');
    h.filei.figexport = uimenu(h.file,'Label','Figure Expo&rt Setup...',...
        'Callback',cb{1},'Separator','on');
end
if ~isempty(mhfigprintpreview)
    cb = get(mhfigprintpreview,'Callback');
    h.filei.figprintpreview = uimenu(h.file,'Label','Figure Print Pre&view...',...
        'Callback',cb{1});
end
if ~isempty(mhfigprint)
    cb = get(mhfigprint,'Callback');
    h.filei.figprint = uimenu(h.file,'Label','Figure &Print...',...
        'Callback',cb{1});
end
h.filei.exit = uimenu(h.file,'Label','Exit','Callback',@cbExit,...
    'Separator','on');

% View menu items
% if ~isempty(mhdock)
%     cb = get(mhdock,'Callback');
%     h.viewi.dock = uimenu(h.view,'Label','Dock VisualGrid',...
%         'Callback',cb{1});
% end
% if ~isempty(mhcolorbar)
%     cb = get(mhcolorbar,'Callback');
%     h.viewi.colorbar = uimenu(h.view,'Label','&Colorbar',...
%         'Callback',cb{1});
% end

% Edit menu items
h.editi.selectallnodes = uimenu(h.edit, 'Label','Select All Nodes for the Current Object',...
    'Callback', @cbSelectAllNodes);
h.editi.selectduplicatenodes = uimenu(h.edit, 'Label','Select Duplicate Nodes',...
    'Callback', @cbSelectDuplicateNodes); % ,'Tooltip','Last in each duplicate list is NOT selected'
h.editi.selectinpoly = uimenu(h.edit, 'Label','Select Nodes In Polygon',...
    'Callback', @cbSelectInPoly);
h.editi.selectcoincident = uimenu(h.edit, 'Label','Select Nodes Coincident With...',...
    'Callback', @cbSelectCoincident);
h.editi.editnode = uimenu(h.edit, 'Label','Edit Selected Nodes...',...
    'Callback', @cbEditNode);
h.editi.deletenode = uimenu(h.edit, 'Label','Delete Node   [Del]',...
    'Callback', @cbDeleteNode);
h.editi.deleteobj = uimenu(h.edit, 'Label','Delete Object(s)    [Ctrl+Del]',...
    'Callback', @cbDeleteObject);
h.editi.combine = uimenu(h.edit, 'Label','Combine Node Sets or Polylines',...
    'Callback', @cbCombine);
h.editi.duplicate = uimenu(h.edit, 'Label','Duplicate',...
    'Callback', @cbDuplicate);
h.editi.convert2nodes = uimenu(h.edit, 'Label','Convert to Node Set',...
    'Callback', @cbConvert2Nodes);
h.editi.plotparam = uimenu(h.edit, 'Label', 'Object Appearance...',...
    'Callback', @cbSetPlotParam, 'Separator','on');
h.editi.gmap = uimenu(h.edit, 'Label', 'Google Map Settings',...
    'Separator','on');
h.editi.gmaputmzone = uimenu(h.editi.gmap, 'Label', 'UTM Zone...',...
    'Callback', @cbSetUTMZone);
%     'tooltip','UTM zone to calculate position for Google Map retrieval (zero for geographic coordinates)');
h.editi.gmapzoom = uimenu(h.editi.gmap, 'Label', 'Zoom Level...',...
    'Callback', @cbSetZoomLevel); % ,'tooltip','1 to 19 (higher value for more detail)'
if ~isempty(mheditcolormap)
    cb = get(mheditcolormap,'Callback');
    h.editi.editcolormap = uimenu(h.edit,'Label','Color&map...',...
        'Callback',cb{1},'Separator','on');
end
if ~isempty(mhcopyfigure)
    cb = get(mhcopyfigure,'Callback');
    h.editi.figexport = uimenu(h.edit,'Label','Copy &Figure',...
        'Callback',cb{1},'Separator','on');
end
if ~isempty(mhcopyoptions)
    cb = get(mhcopyoptions,'Callback');
    h.editi.figprintpreview = uimenu(h.edit,'Label','Copy &Options...',...
        'Callback',cb{1});
end

% Transform menu items
h.transformi.coordoffset = uimenu(h.transform,...
    'Label','Offset Coordinates','Callback', @cbCoordOffset);
h.transformi.coordscale = uimenu(h.transform,...
    'Label','Scale Coordinates','Callback', @cbCoordScale);
h.transformi.transformdefine = uimenu(h.transform,...
    'Label','Define Transform','Callback', @cbTransformDefine);
h.transformi.transformapply = uimenu(h.transform,...
    'Label','Apply Transform...','Callback', @cbTransformApply);
h.transformi.redepth = uimenu(h.transform, 'Label','Redepth object',...
    'Callback', @cbRedepth);
h.transformi.setmindepth = uimenu(h.transform,'Label','Set minimum depth',...
    'Callback',@cbSetMinDepth);
h.transformi.imgrotate = uimenu(h.transform,...
    'Label','Rotate Image','Callback', @cbImgRotate);
h.transformi.imgdigitize = uimenu(h.transform,...
    'Label','Grayscale-Digitize Image...','Callback', @cbImgDigitize);
h.transformi.coord2utm = uimenu(h.transform,...
    'Label','Convert to UTM Coordinates','Callback', @cbCoord2UTM);
h.transformi.coord2geo = uimenu(h.transform,...
    'Label','Convert to Geographic Coordinates','Callback', @cbCoord2Geo);
h.transformi.fathom2m = uimenu(h.transform, 'Label','Fathoms.Feet to Metres...',...
    'Callback', @cbFathom2M);
h.transformi.m2fathom = uimenu(h.transform, 'Label','Metres to Fathoms.Feet...',...
    'Callback', @cbM2Fathom);
h.transformi.tofront = uimenu(h.transform,...
    'Label','Bring To Front','Callback', @cbToFront,'Separator','on');
h.transformi.toback = uimenu(h.transform,...
    'Label','Bring To Back','Callback', @cbToBack);
h.transformi.tofrontone = uimenu(h.transform,...
    'Label','Bring To Front One Level','Callback', @cToFrontOnce);
h.transformi.tobackone = uimenu(h.transform,...
    'Label','Bring To Back One Level','Callback', @cToBackOnce);

% Node Set menu items
h.nodeseti.bathynew = uimenu(h.nodeset, 'Label','New...',...
    'Callback', @cbBathyNew);
h.nodeseti.predefz = uimenu(h.nodeset,'Label','Set Predefined &Z Values...',...
    'Callback',@cbPredefZ);
h.nodeseti.manedit = uimenu(h.nodeset,'Label','Set Manual Editing Mode...',...
    'Callback',@cbManEdit);
% 'Tooltip','Sets to move the edited nodes in a separate object',...
h.nodeseti.bathydec = uimenu(h.nodeset, 'Label','Decimate...',...
    'Callback', @cbBathyDecimate);
h.nodeseti.bathytrim = uimenu(h.nodeset, 'Label','Trim by polygon...',...
    'Callback', {@cbBathyTrim,false});
h.nodeseti.deleteinpolygon = uimenu(h.nodeset, 'Label','Delete within polygon...',...
    'Callback', {@cbBathyTrim,true});
h.nodeseti.extract = uimenu(h.nodeset, 'Label','Extract Selected Nodes Into A Separate Object',...
    'Callback', @cbSegmentExtract);
h.nodeseti.bathydom = uimenu(h.nodeset, 'Label','Delete Competing Samplings...',...
    'Callback', @cbBathyDominate);
h.nodeseti.cdt = uimenu(h.nodeset, 'Label','Triangulate With Boundary Constraint...',...
    'Callback', @cbBathyCDT);
h.nodeseti.convert2poly = uimenu(h.nodeset, 'Label','Convert to polyline',...
    'Callback', @cbConvert2Poly);

% Polyline menu items
h.polylinei.break = uimenu(h.polyline, 'Label','Break Segment',...
    'Callback', @cbBreakSegment); % @cbSplitContour
h.polylinei.connect = uimenu(h.polyline, 'Label','Connect Segments',...
    'Callback', @cbConnectSegments,'Enable','off'); % @cbConnectContours
h.polylinei.joinadjacent = uimenu(h.polyline, 'Label','Join Adjacent Segments...',...
    'Callback', @cbJoinAdjacent);
h.polylinei.close = uimenu(h.polyline, 'Label','Close Segment',...
    'Callback', @cbCloseSegment,'Enable','off');
h.polylinei.selectopen = uimenu(h.polyline, 'Label','Select Open Segments',...
    'Callback', @cbSelectOpenSegments);
h.polylinei.extract = uimenu(h.polyline, 'Label','Extract Segment(s) Into A Separate Object',...
    'Callback', @cbSegmentExtract);
h.polylinei.deleteseg = uimenu(h.polyline, 'Label','Delete Segment(s)   [Shift+Del]',...
    'Callback', @cbDeleteSegment,'Enable','off');
h.polylinei.polyclip = uimenu(h.polyline, 'Label','Clip by polygon',...
    'Callback', @cbPolyClip);
h.polylinei.polyinterp = uimenu(h.polyline, 'Label','Interpolate Between Contours...',...
    'Callback', @cbPolyInterp,'Separator','on');
h.polylinei.simplify = uimenu(h.polyline, 'Label','Simplify...',...
    'Callback', @cbPolySimplify);
h.polylinei.upsample = uimenu(h.polyline, 'Label','Up-sample...',...
    'Callback', @cbPolyUpsample);
h.polylinei.polydom = uimenu(h.polyline, 'Label','Delete Colose-by Samplings...',...
    'Callback', @cbPolyDominate);
h.polylinei.selectsegment = uimenu(h.polyline, 'Label','Select segment',...
    'Callback', @cbPolySelSeg,'Separator','on');
h.polylinei.orientallccw = uimenu(h.polyline, 'Label','Orient all segments CCW',...
    'Callback', @cbPolyOrientAllCCW);
h.polylinei.orientsegcw = uimenu(h.polyline, 'Label','Orient segment CW',...
    'Callback', @cbPolyOrientSegCW);

% Grid menu items
h.gridi.gridgenerate = uimenu(h.grid,'Label','Generate Grid...',...
    'Callback',@cbGridGenerate);
h.gridi.griddeleteandfix = uimenu(h.grid, 'Label','Delete Node And Fill Hole   [Alt+Del]',...
    'Callback', @cbGridDeleteAndFix); % ,'Enable','off'
h.gridi.griddeletetriangle = uimenu(h.grid, 'Label','Delete Triangle',...
    'Callback', @cbGridDeleteTriangle);
h.gridi.gridcollapseedge = uimenu(h.grid, 'Label','Collapse Edge',...
    'Callback', @cbGridCollapseEdge);
h.gridi.gridsplitedge = uimenu(h.grid, 'Label','Split Edge',...
    'Callback', @cbGridSplitEdge);
h.gridi.gridcleave = uimenu(h.grid, 'Label','Cleave Node',...
    'Callback', @cbCleaveNode);
h.gridi.gridcleaveauto = uimenu(h.grid, 'Label','Cleave auto...',...
    'Callback', @cbCleaveAuto);
h.gridi.griddekite = uimenu(h.grid, 'Label','De-kite',...
    'Callback', @cbDekiteNode);
h.gridi.griddekiteauto = uimenu(h.grid, 'Label','De-kite auto',...
    'Callback', {@cbDekiteAuto,false});
h.gridi.griddekiteautonb = uimenu(h.grid, 'Label','De-kite auto (no "bridges")',...
    'Callback', {@cbDekiteAuto,true});
h.gridi.gridexedge = uimenu(h.grid, 'Label','Exchange Edge',...
    'Callback', @cbExchangeEdge);
h.gridi.gridexedgeauto = uimenu(h.grid, 'Label','Exchange Edge auto',...
    'Callback', @cbExchangeEdgeAuto); %,...
%     'ToolTip','Exchange edge for non-EQL triangles at the boundary according to the criterion in "object appearance"');
h.gridi.gridboundaryselect = uimenu(h.grid, 'Label','Select Boundary Nodes',...
    'Callback', @cbGridBoundarySelect);
h.gridi.gridboundaryselectloop = uimenu(h.grid, 'Label','Select Boundary Loop',...
    'Callback', @cbGridBoundarySelectLoop);
h.gridi.gridselectcode = uimenu(h.grid, 'Label','Select Nodes By Code...',...
    'Callback', @cbGridSelectCode);
h.gridi.gridselectnneighbours = uimenu(h.grid, 'Label','Select Nodes By Number of Neighbours...',...
    'Callback', @cbGridSelectNNeighbours);
h.gridi.gridselectle = uimenu(h.grid, 'Label','Select Local Extrema Nodes');
    h.gridi.gridselectle_bexc = uimenu(h.gridi.gridselectle, 'Label','Excluding Boundary Nodes',...
        'Callback', {@cbGridSelectLocalExtrema,true});
    h.gridi.gridselectle_binc = uimenu(h.gridi.gridselectle, 'Label','Including Boundary Nodes',...
        'Callback', {@cbGridSelectLocalExtrema,false});
h.gridi.gridextractboundary = uimenu(h.grid, 'Label','Extract Boundary',...
    'Callback', @cbExtractBoundary);
h.gridi.gridextractboundaryloop = uimenu(h.grid, 'Label','Extract Boundary Loop',...
    'Callback', @cbExtractBoundaryLoop);
h.gridi.gridextract = uimenu(h.grid, 'Label','Extract Selected Part',...
    'Callback', @cbExtractGridPart);
h.gridi.gridslash = uimenu(h.grid, 'Label','Slash Grid by Polygon',...
    'Callback', @cbGridSlash);
h.gridi.gridclean = uimenu(h.grid, 'Label','"Clean" Boundary',...
    'Callback', @cbGridClean);
h.gridi.gridmerge = uimenu(h.grid, 'Label','Merge with another grid',...
    'Callback', @cbGridMerge);
h.gridi.gridrefine = uimenu(h.grid, 'Label','Refine Entire Grid',...
    'Callback', @cbGridRefine);
h.gridi.gridrefinesel = uimenu(h.grid, 'Label','Refine Selected Triangles',...
    'Callback', @cbGridRefineSel);
h.gridi.gridsmooth = uimenu(h.grid, 'Label','Smooth Grid',...
    'Callback', {@cbGridSmooth,false});
h.gridi.gridsmoothfixobn = uimenu(h.grid, 'Label','Smooth Grid (fix open boundary neighbours)',...
    'Callback', {@cbGridSmooth,true});
h.gridi.gridfix = uimenu(h.grid, 'Label','Fix Grid',...
    'Callback', @cbGridFix);
h.gridi.gridinfo = uimenu(h.grid, 'Label','Get Info',...
    'Callback', @cbGridInfo);

% Help menu items
h.helpi.helpdoc = uimenu(h.help,'Label','&Documentation...',...
    'Callback',@cbHelpDoc);
h.helpi.helpabout = uimenu(h.help,'Label','&About...',...
    'Callback',@cbHelpAbout);

% remove initial figure menues
delete(mh);

%-------------------------
% cmnodeitem.saveinbase = uimenu(cmnode, 'Label', 'Save Selected Polylines In Base Workspace...', ...
%     'Callback', @cbSaveInBase, 'Separator','on');
% cmnodeitem.saveallinbase = uimenu(cmnode, 'Label', 'Save All Polylines In Base Workspace...', ...
%     'Callback', @cbSaveAllInBase);
% 
% cmnodeitem.saveinfile = uimenu(cmnode, 'Label', 'Save Selected Polylines In File...', ...
%     'Callback', {@cbSaveInFile,true});
% cmnodeitem.saveallinfile = uimenu(cmnode, 'Label', 'Save All Polylines In File...', ...
%     'Callback', {@cbSaveInFile,false});
% %------
% cmnodeitem.savengh = uimenu(cmnode, 'Label', 'Save Selected Grid in NGH file...', ...
%     'Callback', @cbSaveNGH, 'Separator','on');
% cmnodeitem.savenod = uimenu(cmnode, 'Label', 'Save Selection in NOD file...', ...
%     'Callback', @cbSaveNOD, 'Separator','on');



function [cmnode,cmnodeitem] = place_context_menu()
% Create context menu for right-clicking on a node.

% % Define the context menu for click into empty space (no object)
% H.cmempty = uicontextmenu;
% % H.cmemptyitem.createpolylinetool = uimenu(H.cmempty, 'Label','Create New Polyline','Callback',@cmeCreateNewPolyline);
% % H.cmemptyitem.stopdrawingtool = uimenu(H.cmempty, 'Label','Stop Drawing','Callback',@cmeStopDrawing);

% Define the context menu for nodes (click on a coastline object)
cmnode = uicontextmenu;

cmnodeitem.plotparam = uimenu(cmnode, 'Label', 'Appearance Parameters...',...
    'Callback', @cbSetPlotParam);

cmnodeitem.selectallnodes = uimenu(cmnode, 'Label','Select All Nodes for the Current Object',...
    'Callback', @cbSelectAllNodes,'Separator','on');
cmnodeitem.deletenode = uimenu(cmnode, 'Label','Delete Node       [Del]',...
    'Callback', @cbDeleteNode);
cmnodeitem.inserttool = uimenu(cmnode, 'Label','Insert Node',...
    'Callback',[],'Enable','off');
cmnodeitem.breaktool = uimenu(cmnode, 'Label','Break Segment',...
    'Callback', @cbBreakSegment,'Visible','off'); % @cbSplitContour
% cmnodeitem.connecttool = uimenu(cmnode, 'Label','Connect Segments',...
%     'Callback', @cbConnectSegments,'Enable','off','Visible','off'); % @cbConnectContours
cmnodeitem.closetool = uimenu(cmnode, 'Label','Close Segment',...
    'Callback', @cbCloseSegment,'Enable','off','Visible','off');
cmnodeitem.segextract = uimenu(cmnode, 'Label','Extract Segment(s) Into A Separate Object',...
    'Callback', @cbSegmentExtract,'Visible','off');
cmnodeitem.segcombine = uimenu(cmnode, 'Label','Combine Node Sets or Polylines',...
    'Callback', @cbCombine,'Visible','off');
cmnodeitem.deletesegtool = uimenu(cmnode, 'Label','Delete Segment(s)   (Shift+Del)',...
    'Callback', @cbDeleteSegment,'Enable','off');
cmnodeitem.deleteobjtool = uimenu(cmnode, 'Label','Delete Object(s)    (Ctrl+Del)',...
    'Callback', @cbDeleteObject);

cmnodeitem.polyinterp = uimenu(cmnode, 'Label','Interpolate between contours...',...
    'Callback', @cbPolyInterp,'Visible','off');
cmnodeitem.simplify = uimenu(cmnode, 'Label','Simplify polyline...',...
    'Callback', @cbPolySimplify,'Visible','off');
% cmnodeitem.simplifyinout = uimenu(cmnode, 'Label','Simplify polyline in/out of polygon...','Callback', @cbPolySimplifyInOut);
% cmnodeitem.coordoffset = uimenu(cmnode, 'Label','Offset coordinates','Callback', @cbCoordOffset);
% cmnodeitem.coord2utm = uimenu(cmnode, 'Label','Convert to UTM coordinates','Callback', @cbCoord2UTM);
% cmnodeitem.coord2geo = uimenu(cmnode, 'Label','Convert to geographic coordinates','Callback', @cbCoord2Geo);

cmnodeitem.bathydec = uimenu(cmnode, 'Label','Decimate bathymetry...',...
    'Callback', @cbBathyDecimate,'Separator','on','Visible','off');
cmnodeitem.bathydom = uimenu(cmnode, 'Label','Delete competing bathymetry samplings...',...
    'Callback', @cbBathyDominate,'Visible','off');

% cmnodeitem.gridgenerate = uimenu(cmnode, 'Label','Generate Grid','Callback', @cbGridGenerate,'Separator','on');
% cmnodeitem.gridgenerate = uimenu(cmnode, 'Label','Generate Grid','Separator','on');
% cmnodeitem.gridgen_simple = uimenu(cmnodeitem.gridgenerate, 'Label','Simple','Callback', @cbGridGenSimple);
% cmnodeitem.gridgen_depthdep = uimenu(cmnodeitem.gridgenerate, 'Label','Depth-dependent','Callback', @cbGridGenDepthdep,'Enable','off');
% cmnodeitem.gridgen_depthpoly = uimenu(cmnodeitem.gridgenerate, 'Label','Polygon-specific','Callback', @cbGridGenDepthPoly);

cmnodeitem.griddeleteandfix = uimenu(cmnode, 'Label','Delete Node And Fill Hole    (Alt+Del)',...
    'Callback', @cbGridDeleteAndFix,'Visible','off'); % ,'Enable','off'
cmnodeitem.gridcleave = uimenu(cmnode, 'Label','Cleave node','Callback', @cbCleaveNode,'Visible','off');
cmnodeitem.gridcleaveauto = uimenu(cmnode, 'Label','Cleave auto...','Callback', @cbCleaveAuto,'Visible','off');
cmnodeitem.griddekite = uimenu(cmnode, 'Label','De-kite','Callback', @cbDekiteNode,'Visible','off');
cmnodeitem.griddekiteauto = uimenu(cmnode, 'Label','De-kite auto','Callback', {@cbDekiteAuto,false},'Visible','off');
cmnodeitem.griddekiteautonb = uimenu(cmnode, 'Label','De-kite auto (no "bridges")','Callback', {@cbDekiteAuto,true},'Visible','off');
cmnodeitem.gridexedge = uimenu(cmnode, 'Label','Exchange edge','Callback', @cbExchangeEdge,'Visible','off');
cmnodeitem.gridboundaryselect = uimenu(cmnode, 'Label','Select Boundary Nodes',...
    'Callback', @cbGridBoundarySelect,'Visible','off');
cmnodeitem.gridboundaryselectloop = uimenu(cmnode, 'Label','Select Boundary Loop',...
    'Callback', @cbGridBoundarySelectLoop,'Visible','off');
%%%%%%%%%%%%%%
cmnodeitem.gridselectcode = uimenu(cmnode, 'Label','Select Nodes By Code...',...
    'Callback', @cbGridSelectCode,'Visible','off');
cmnodeitem.gridextractboundary = uimenu(cmnode, 'Label','Extract boundary','Callback', @cbExtractBoundary,'Visible','off');
cmnodeitem.gridextractboundaryloop = uimenu(cmnode,...
    'Label','Extract boundary loop','Callback', @cbExtractBoundaryLoop,'Visible','off');
%%%%%%%%%%%%%%
cmnodeitem.gridextract = uimenu(cmnode, 'Label','Extract selected part','Callback', @cbExtractGridPart,'Visible','off');
cmnodeitem.gridslash = uimenu(cmnode, 'Label','Slash grid by polygon','Callback', @cbGridSlash,'Visible','off');
cmnodeitem.gridclean = uimenu(cmnode, 'Label','"Clean" Boundary','Callback', @cbGridClean,'Visible','off');
cmnodeitem.gridmerge = uimenu(cmnode, 'Label','Merge with another grid','Callback', @cbGridMerge,'Visible','off');
cmnodeitem.gridsmooth = uimenu(cmnode, 'Label','Smooth grid','Callback', {@cbGridSmooth,false},'Visible','off');
cmnodeitem.gridsmoothfixobn = uimenu(cmnode, 'Label','Smooth grid (fix open boundary neighbours)','Callback', {@cbGridSmooth,true},'Visible','off');
cmnodeitem.gridredepth = uimenu(cmnode, 'Label','Redepth grid','Callback', @cbRedepth,'Visible','off');

% cmnodeitem.imgrotate = uimenu(cmnode, 'Label','Rotate image','Callback', @cbImgRotate,'Separator','on');

cmnodeitem.tofrontone = uimenu(cmnode, 'Label','Bring To Front One Level','Callback', @cToFrontOnce,'Separator','on');
cmnodeitem.tobackone = uimenu(cmnode, 'Label','Bring To Back One Level','Callback', @cToBackOnce);
cmnodeitem.tofront = uimenu(cmnode, 'Label','Bring To Front','Callback', @cbToFront);
cmnodeitem.toback = uimenu(cmnode, 'Label','Bring To Back','Callback', @cbToBack);
% cmnodeitem.setcolortool = uimenu(cmnode, 'Label','Change Color...','Callback', @cbSetColor,'Separator','on');
%------

% cmnodeitem.qmarker = uimenu(cmnode, 'Label', 'Quality markers', 'Separator','on');
% cmnodeitem.qmarkeritem.markangle = uimenu(cmnodeitem.qmarker,...
%     'Label', 'Mark sharp angles','Callback',@cbqMarkAngle,'Checked','off');
% if ppdef.badangflag
%     set(cmnodeitem.qmarkeritem.markangle,'Checked','on')
% end
% cmnodeitem.qmarkeritem.setangle = uimenu(cmnodeitem.qmarker,...
%     'Label', 'Set angle threshold...','Callback',@cbqSetAngle);
% cmnodeitem.qmarkeritem.markedge = uimenu(cmnodeitem.qmarker,...
%     'Label', 'Mark short edges','Callback',@cbqMarkEdge,'Checked','off');
% if ppdef.badlenflag
%     set(cmnodeitem.qmarkeritem.markedge,'Checked','on')
% end
% cmnodeitem.qmarkeritem.setedge = uimenu(cmnodeitem.qmarker,...
%     'Label', 'Set edge length threshold...','Callback',@cbqSetEdge);
%------
% cmitem.undo = uimenu(cmenu, 'Label', 'Undo', 'Callback', @cmUndo,
% 'Separator','on');
%------


% set(hfm,'UIContextMenu',cmenu); % this makes cmenu jump anywhere on figure except axes


function [h,toolgroupex] = place_toolbar(hfm)
% Lay out the toolbar based on the initial Matlab's figure toolbar.

set(hfm,'Toolbar','figure'); % place a standard Matlab figure toolbar
htb = findall(hfm,'Type','uitoolbar'); % add tools to the standard toolbar
% htb = uitoolbar(fh); % create another toolbar under the standard one

% delete unwanted tools
delete(findall(htb,'TooltipString','Rotate 3D'))
delete(findall(htb,'TooltipString','Hide Plot Tools'))
delete(findall(htb,'TooltipString','Show Plot Tools and Dock Figure'))

% hide unwanted tools
% set(findall(htb,'TooltipString','Rotate 3D'),'Visible','off')
% set(findall(htb,'TooltipString','Insert Colorbar'),'Visible','off')
% set(findall(htb,'TooltipString','Insert Legend'),'Visible','off')
% set(findall(htb,'TooltipString','Hide Plot Tools'),'Visible','off')
% set(findall(htb,'TooltipString','Show Plot Tools and Dock Figure'),'Visible','off')

h.filenew = findall(htb,'TooltipString','New Figure');
set(h.filenew,'ClickedCallback',@cbFileNew,'TooltipString','New File') % 'Tag',''

h.fileopen = findall(htb,'TooltipString','Open File');
set(h.fileopen,'ClickedCallback',@cbFileOpen,'TooltipString','Open File') % 'Tag',''

h.filesave = findall(htb,'TooltipString','Save Figure');
set(h.filesave,'ClickedCallback',@cbFileSave,'TooltipString','Save File','Enable','off') % 'Tag',''

h.edit = findall(htb,'TooltipString','Edit Plot');
set(h.edit,'ClickedCallback',@toolEdit,'TooltipString','Edit Objects') % 'Tag',''

%%%%% the following may be done more elegantly using modeManager (check undocumentedmatlab.com)
h.zoom = findall(htb,'TooltipString','Zoom In');
set(h.zoom,'OnCallback',@zoomOnCallback,'OffCallback',@zoomOffCallback,'ClickedCallback','')
% zoomicon = fullfile(matlabroot,'/toolbox/matlab/icons/zoomplus.mat');

% h.zoomout = findall(htb,'TooltipString','Zoom Out');
% set(h.zoomout,'OnCallback',@zoomOnCallback,'OffCallback',@zoomOffCallback)
delete(findall(htb,'TooltipString','Zoom Out'))

h.pan = findall(htb,'TooltipString','Pan');
set(h.pan,'OnCallback',@zoomOnCallback,'OffCallback',@zoomOffCallback)


% add base workspace import tool to the toolbar
% img2 = rand(16,16,3);
imwsimport = imread('wsimport.bmp');
h.wsimport = uipushtool(htb,'CData',imwsimport,'Separator','on',...
'TooltipString','Import variable(s) from the base workspace',...
'ClickedCallback',@cbImportVariables,'HandleVisibility','off'); % cbLoadVar

% add undo tool to the toolbar
h.undo = uipushtool(htb,'CData',imgReadTransparent('undo.bmp'),'Separator','on',...
'TooltipString','Undo','ClickedCallback',@undofcn,...
'HandleVisibility','off','Enable','off');

% add a freehand tool to the toolbar
h.freehand = uitoggletool(htb,'CData',imgReadTransparent('freehand.bmp'),...
    'TooltipString','Create Polyline','ClickedCallback',@toolFreehand,...
    'HandleVisibility','off');

% add a freenode tool to the toolbar
h.freenode = uitoggletool(htb,'CData',imgReadTransparent('freenode.bmp'),...
    'TooltipString','Create Node Set','ClickedCallback',@toolFreenode,...
    'HandleVisibility','off');

% add a "color nodes" tool to the toolbar
h.colornodes = uitoggletool(htb,'CData',imgReadTransparent('colornodes.bmp'),...
    'TooltipString','Show Bathymetry for Close-by Nodes','ClickedCallback',@toolColorNodes,...
    'HandleVisibility','off');

% add a "Google Maps" tool to the toolbar
h.googlemaps = uipushtool(htb,'CData',imgReadTransparent('googlemaps.bmp'),'Separator','on',...
    'TooltipString','Load Google Map image','ClickedCallback',@toolGoogleMaps,...
    'HandleVisibility','off');

% add Notes tool to the toolbar
h.notes = uipushtool(htb,'CData',imgReadTransparent('notes.bmp'),'Separator','on',...
'TooltipString','Work notes','ClickedCallback',@toolNotes,...
'HandleVisibility','off','Enable','on');

% add Cleave tool to the toolbar
h.cleave = uitoggletool(htb,'CData',imgReadTransparent('cleave.bmp'),'Separator','on',...
'TooltipString','Cleave node (grids only)','ClickedCallback',@toolCleave,...
'HandleVisibility','off','Enable','on');

% register mutually exclusive tools
toolgroupex = findall(htb,'TooltipString','Zoom In');
% toolgroupex(end+1) = findall(htb,'TooltipString','Zoom Out');
toolgroupex(end+1) = findall(htb,'TooltipString','Pan');
toolgroupex(end+1) = findall(htb,'TooltipString','Edit Objects');
toolgroupex(end+1) = findall(htb,'TooltipString','Create Polyline');
toolgroupex(end+1) = findall(htb,'TooltipString','Create Node Set');
toolgroupex(end+1) = findall(htb,'TooltipString','Cleave node (grids only)');
% toolgroupex(end+1) = findall(htb,'TooltipString','View Nodes Color Coded');

% % from http://undocumentedmatlab.com/blog/category/undocumented-function/
% hToolbar = findall(gcf,'tag','FigureToolBar');
% hUndo=uisplittool('parent',hToolbar);       % uisplittool
% hRedo=uitogglesplittool('parent',hToolbar); % uitogglesplittool
% 
% % Load the Redo icon
% icon = fullfile(matlabroot,'/toolbox/matlab/icons/greenarrowicon.gif');
% [cdata,map] = imread(icon);
%  
% % Convert white pixels into a transparent background
% map(find(map(:,1)+map(:,2)+map(:,3)==3)) = NaN;
%  
% % Convert into 3D RGB-space
% cdataRedo = ind2rgb(cdata,map);
% cdataUndo = cdataRedo(:,[16:-1:1],:);
%  
% % Add the icon (and its mirror image = undo) to latest toolbar
% set(hUndo, 'cdata',cdataUndo, 'tooltip','undo','Separator','on', ...
%            'ClickedCallback','uiundo(gcbf,''execUndo'')');
% set(hRedo, 'cdata',cdataRedo, 'tooltip','redo', ...
%            'ClickedCallback','uiundo(gcbf,''execRedo'')');

% Re-arranging the toolbar controls placement
% Let us now re-arrange our toolbar buttons. Unfortunately, a bug causes
% uisplittools and uitogglesplittools to always be placed flush-left when the
% toolbar�s children are re-arranged (anyone at TMW reading this in time for the
% R2011a bug-parade selection?).
% 
% So, we can�t re-arrange the buttons at the HG-children level. Luckily, we can
% re-arrange directly at the Java level (note that until now, the entire
% discussion of uisplittool and uitogglesplittool was purely Matlab-based):
% 
% jToolbar = get(get(htb,'JavaContainer'),'ComponentPeer');
% jButtons = jToolbar.getComponents;
% for buttonId = length(jButtons)-3 : -1 : 7  % end-to-front
%    jToolbar.setComponentZOrder(jButtons(buttonId), buttonId+1);
% end
% jToolbar.setComponentZOrder(jButtons(end-2), 5);   % Separator
% jToolbar.setComponentZOrder(jButtons(end-1), 6);   % Undo
% jToolbar.setComponentZOrder(jButtons(end), 7);     % Redo
% jToolbar.revalidate;

jToolbar = get(get(htb,'JavaContainer'),'ComponentPeer');
jButtons = jToolbar.getComponents;
for buttonId = length(jButtons)-2 : -1 : 6  % end-to-front
    jToolbar.setComponentZOrder(jButtons(buttonId), buttonId+1);
end
jToolbar.setComponentZOrder(jButtons(end-1), 5);   % Separator
jToolbar.setComponentZOrder(jButtons(end), 6);   % Undo
jToolbar.revalidate;


function img = imgReadTransparent(fname)
% Read image from a file and make background transparent.

img = double(imread(fname))/255;

% Convert pixels of the same color as img(1,1,:) into a transparent background 
itr = img(:,:,1)==img(1,1,1) & img(:,:,2)==img(1,1,2) & img(:,:,3)==img(1,1,3);
trmask = img(:,:,1); 
trmask(itr) = NaN; 
img(:,:,1) = trmask; 


% Object types:
%     1   set of stand alone nodes (no NaN termination)
%     2   coastline (each element is NaN-terminated)
%     3   triangular grid (no NaN termination)
%     4   image
function obj = obj_struct(n)
% Generate object structure array of length n with empty fields.
% n is 1 by default. n can be 0.

obj = struct('type',[],'objn',[],'noden',[],'x',[],'y',[],'z',[],'code',[],'tri',[],...
    'h',[],'pp',[],'comment',[],'meta',[],'imx',[],'imy',[],'cdata',[],'cmap',[]);
if exist('n','var') && isnumeric(n); obj = repmat(obj,1,n); end


function grp = grp_struct(n)
% Generate group structure array of length n with empty fields.
% n is 1 by default. n can be 0.

grp = struct('grpn',[],'objlist',[],'subgrplist',[],'comment',[]);
if exist('n','var') && isnumeric(n); grp = repmat(grp,1,n); end


function undo = undo_struct()
% Initialize undo structure.

undo = struct('action',[],'label',[],'r',[],'filesaved',[]);
undo(1) = []; % make it empty


function [obj,undo] = obj_load(obj, objnew, ah, pp, htbl)
% Draw objects objnew and insert them in the object array obj according to their objn.

if ~exist('pp','var')
    pp = [];
%     pp = obj_default_pp;
end

% make sure z and code are double
for io = 1:length(objnew)
    objnew(io).x = double(objnew(io).x);
    objnew(io).y = double(objnew(io).y);
    objnew(io).z = double(objnew(io).z);
    objnew(io).code = double(objnew(io).code);
end

% make object structures consistent (in case objects created with older
% versions of VG are being loaded)
objnew = obj_matchfields(obj,objnew);

objnew = obj_draw(ah, objnew, pp);
[obj,undo] = oInsert(obj,objnew,[],htbl);


function objnew = obj_matchfields(obj,objnew)
% Make fields in objnew match fields in obj.

flds = fieldnames(obj);
fnew = fieldnames(objnew);

% add missing fields
fdif = setdiff(flds,fnew);
for ifl = 1:length(fdif)
    objnew(1).(fdif{ifl}) = []; % sets the new field in all objnew to []
end

objnew = rmfield(objnew, setdiff(fnew,flds)); % remove redundant fields

objnew = orderfields(objnew,obj); % order fields as in obj


function obj = oCreate(r,objn,otype)
% Create a set of objects extracting all relevant fields from a struct array r.
% objn      length(r) sets object number for each new object
% otype     length(1) or length(r) forces each object to be of type otype; if
%           not specified or empty then r must contain a field "type"

nr = length(r);

if ~exist('objn','var') || isempty(objn)
    objn = 1:nr;
end
if ~exist('otype','var') || isempty(otype)
    otype = NaN(1,nr);
elseif length(otype) == 1 % copy for all objects
    otype = repmat(otype,1,nr);
end

obj = obj_struct(nr);
for ir = 1:nr
    obj(ir) = oCreate1(r(ir),objn(ir),otype(ir));
end


function [obj,err] = oCreate1(r,objn,otype)
% Create one object extracting all relevant fields from a struct r.
% objn      object number for the new object
% otype     if is not NaN then forces the object to be of type otype

err = 0;
obj = obj_struct;
if ~exist('otype','var') || isnan(otype)
    otype = r.type;
end
if otype == 4 && ~(isfield(r,'imx') && isfield(r,'imy') && isfield(r,'cdata') && isfield(r,'cmap')) % image
% if otype == 4 && ~(isfield(r,'imx') && isfield(r,'imy') && isfield(r,'rgb')) % image
    err = 'oCreate: No required fields in the passed structure.';
end
if isfield(r,'x')
    nx = length(r.x);
    obj.x = r.x(:)'; % ensure row vectors
elseif otype==4
    % for an image, fill in x,y fields with r.x(1),r.y(1)
    nx = 1;
    obj.x = r.imx(1);
else%if ismember(otype,[1 2 3])
    err = 'oCreate: No required fields in the passed structure or inconsistent fileds.';
end
if isfield(r,'y') && length(r.y)==nx
    obj.y = r.y(:)';
elseif otype==4
    obj.y = r.imy(1);
else%if ismember(otype,[1 2 3])
    err = 'oCreate: No required fields in the passed structure or inconsistent fileds.';
end
if isfield(r,'z') && length(r.z)==nx
    obj.z = r.z(:)';
else
    obj.z = zeros(1,nx);
end
if isfield(r,'code') && length(r.code)==nx
    obj.code = r.code(:)';
else
    if otype == 2
        obj.code = ones(1,nx); % default for boundary
    else
        obj.code = zeros(1,nx); % default interior
    end
end
if isfield(r,'meta') && isfield(r.meta,'featurelist')
    obj.meta.featurelist = r.meta.featurelist;
end
if otype == 3
    if isfield(r,'tri') && max(r.tri(:)) <= nx
                        %%%%% check consistency of tri and the number of nodes
        obj.tri = r.tri;
    else
        err = 'oCreate: No required fields in the passed structure or inconsistent fileds.';
    end
end
if otype == 4 % image
    if isfield(r,'imx') && isfield(r,'imy') && isfield(r,'cdata') && isfield(r,'cmap') % && isfield(r,'rgb')
        obj.imx = r.imx;
        obj.imy = r.imy;
        obj.cdata = r.cdata;
        obj.cmap = r.cmap;
%         obj.rgb = r.rgb;
    else
        err = 'oCreate: No required fields in the passed structure.';
    end
end
obj.type = otype;
obj.objn = repmat(objn,1,nx);
obj.noden = 1:nx;
if isfield(r,'comment')
    obj.comment = r.comment;
end



function obj = oCreatePolyline(x,y,objn,otype,z,code,tri)

obj = obj_struct;
obj.type = otype;
nn = length(x);
obj.objn = ones(1,nn)*objn;
obj.noden = 1:nn;
obj.x = x;
obj.y = y;
if exist('z','var') && ~isempty(z)
    obj.z = z;
else
    obj.z = zeros(size(x));
end
if exist('code','var') && ~isempty(code)
    obj.code = code;
else
    obj.code = ones(size(x));
end
if otype == 3
    obj.tri = tri;
end



function [obj,err] = obj_update_data(obj,htbl,x,y,objn,z,code,tri,imx,imy,cdata,cmap)
% Update object with new x,y,z,code,objn values and adjust graphical object
% accordingly. If x,y,z,code,objn are empty they are left unchanged.

err = false;
if ~exist('objn','var') || isempty(objn)
    objn = obj.objn(1);
end

if ~exist('x','var') || isempty(x)
    x = obj.x;
end
if ~exist('y','var') || isempty(y)
    y = obj.y;
end
if ~exist('imx','var') || isempty(imx)
    imx = obj.imx;
end
if ~exist('imy','var') || isempty(imy)
    imy = obj.imy;
end
if ~exist('cdata','var') || isempty(cdata)
    cdata = obj.cdata;
end
if ~exist('cmap','var') || isempty(cmap)
    cmap = obj.cmap;
end
% if ~exist('rgb','var') || isempty(rgb)
%     rgb = obj.rgb;
% end

difn = length(obj.x)~=length(x); % true if number of nodes has changed
if ~exist('z','var') || isempty(z)
    if difn % if number of nodes has changed
        z = repmat(obj.z(1),size(x)); %%%%% is there a better way?
%         z = interp1(obj.x,obj.z,x); %%%%% this will not work for non-distinct x values
    else
        z = obj.z;
    end
end

if ~exist('code','var') || isempty(code)
    if difn % if number of nodes has changed
        code = repmat(obj.code(1),size(x));
    else
        code = obj.code;
    end
end


if ~exist('tri','var') || isempty(tri)
    if obj.type==3 && difn
        % if number of nodes has changed for a grid and no tri supplied
        err = true;
        return
    else
        % for grids tri has not changed, for non-grids we don't care, leave tri as before
        tri = obj.tri;
    end
end

% update object data
obj.x = x(:)';
obj.y = y(:)';
obj.z = z(:)';
obj.code = code(:)';
obj.tri = tri;
obj.imx = imx;
obj.imy = imy;
obj.cdata = cdata;
obj.cmap = cmap;
% obj.rgb = rgb;

nn = length(obj.x);
obj.objn = ones(1,nn)*objn;
setlayer(obj,objn) % make it visually in the correct layer
obj.noden = 1:nn;

% update the object list table
if ishandle(htbl)
    if verLessThan('matlab', '7.6')
        dat = cell(get(htbl,'Data')); % old stock uitable function
    else
        dat = get(htbl,'Data');
    end
    dat{objn,3} = obj_string(obj);
    set(htbl,'Data',dat);
end

% update graphical representation
if obj.type == 3 % for triangular grids
        
%     set(obj.h.c,'Vertices',[x' y'],'Faces',tri); %%%%%%%
%     set(obj.h.c,'XData',x(tri'),'YData',y(tri'),'ZData',-ones(size(tri'))*objn);
    [ebnd,eint] = grid_edge(tri);
    e = [ebnd; eint];
    xe = x(e);
    ye = y(e);
    xe = [xe NaN(size(xe,1),1)]';
    ye = [ye NaN(size(ye,1),1)]';
    xe = xe(:);
    ye = ye(:);
    
    set(obj.h.c,'XData',xe,'YData',ye,'ZData',-ones(size(xe))*objn);
    
    % triangle quality markers
    if strcmp(get(obj.h.badlen,'Visible'),'on')
        [xe,ye] = grid_quality_edgelen(x,y,tri,obj.pp.badlenthres);
        set(obj.h.badlen,'XData',xe,'YData',ye,'ZData',repmat(-objn,size(xe))); % short edge markers
    end
    if strcmp(get(obj.h.eql,'Visible'),'on')
        eql = 1./quality([obj.x' obj.y'], tri); % "equlateralness"
        lmark = eql>obj.pp.eqlthres; % logical indices
        xc = sum(x(tri),2)/3.0; % coordinates of triangle centroids
        yc = sum(y(tri),2)/3.0;
        set(obj.h.eql,'XData',xc(lmark),'YData',yc(lmark),...
            'ZData',-ones(size(xc(lmark)))*objn); % adjust markers
    end
    if strcmp(get(obj.h.bridge,'Visible'),'on') % bridge markers
        ebr = grid_bridge(tri);
        xb = mean(x(ebr),2);
        yb = mean(y(ebr),2);
        set(obj.h.bridge,'XData',xb,'YData',yb,'ZData',repmat(-objn,size(xb)));
    end
    if strcmp(get(obj.h.bathycolor,'Visible'),'on')
        el = [tri tri(:,1)]'; % closed triangle patches
        set(obj.h.bathycolor,'XData',x(el),'YData',y(el),'CData',z(el),...
            'ZData',repmat(obj.objn(1),size(el)));
    end
    
elseif obj.type == 4 % for images
    set(obj.h.c,'XData',imx(1),'YData',imy(1)); %%%%% 'ZData',repmat(-objn,size(x))
    set(obj.h.img,'XData',imx,'YData',imy,'CData',cdata); %%%%% 'ZData',repmat(-objn,size(x))
%     get(obj.h.img,'Parent');
    set(gcbf,'Colormap',cmap);
    
else % for node sets and polylines
    set(obj.h.c,'XData',x,'YData',y,'ZData',repmat(-objn,size(x)));
    
    if obj.type == 2 % polyline
        [xs,ys,xe,ye] = polyline_1standlast(x,y);
        vlevel = -ones(size(xs))*objn;
        set(obj.h.c1st,'XData',xs,'YData',ys,'ZData',vlevel);
        set(obj.h.clast,'XData',xe,'YData',ye,'ZData',vlevel);

        % polyline quality markers
        if strcmp(get(obj.h.badang,'Visible'),'on')
            [xa,ya] = polyline_quality_angle(x,y,obj.pp.badangthres);
            set(obj.h.badang,'XData',xa,'YData',ya,'ZData',repmat(-objn,size(xa))); % bad angle markers
        end
        if strcmp(get(obj.h.badrat,'Visible'),'on')
            [xr,yr] = polyline_quality_ratio(x,y,obj.pp.badratthres);
            set(obj.h.badrat,'XData',xr,'YData',yr,'ZData',repmat(-objn,size(xr))); % bad ratio markers
        end
        if strcmp(get(obj.h.badlen,'Visible'),'on')
            [xe,ye] = polyline_quality_edgelen(x,y,obj.pp.badlenthres);
            set(obj.h.badlen,'XData',xe,'YData',ye,'ZData',repmat(-objn,size(xe))); % short edge markers
        end
    else % for node sets only
        if strcmp(get(obj.h.bathycolor,'Visible'),'on')
            tri = delaunay(x,y);
            el = [tri tri(:,1)]'; % closed triangle patches
            set(obj.h.bathycolor,'XData',x(el),'YData',y(el),'CData',z(el));
        end
    end
end

% colors for node codes 4-column array: [code rgb]
%%%%% only these codes are allowed so far: 0 1 2 5 6
%%%%% include nodecolormap in pp structure
if obj.type ~= 4 % except image objects
    nodecolormap = [0 0 1 1; 1 .8 .4 0; 2 1 .5 0; 5 0 .35 .75; 6 .5 0 0];
    for ic = 1:size(nodecolormap,1)
        in = code==nodecolormap(ic,1);
        set(obj.h.codemarker(ic),'XData',x(in),'YData',y(in),'ZData',repmat(-objn,size(x(in))));
    end
end

% resize handles (small squares on the bounds of the object)
%%%%% images?
x1 = min(x,[],'omitnan'); x3 = max(x,[],'omitnan'); x2 = mean([x1 x3]);
y1 = min(y,[],'omitnan'); y3 = max(y,[],'omitnan'); y2 = mean([y1 y3]);
xh = [x1 x1 x1 x2 x3 x3 x3 x2];
yh = [y1 y2 y3 y3 y3 y2 y1 y1];
set(obj.h.rsz,'XData',xh,'YData',yh);


function [xs,ys,xe,ye,is,ie] = polyline_1standlast(x,y)
% Coordinates of start and end nodes of a multiply-connected polyline.
ib = find(isnan(x)); % terminating NaNs
is = [1 ib(1:end-1)+1]; % start of each segment
ie = ib-1; % end of each segment
xs = x(is);
ys = y(is);
xe = x(ie);
ye = y(ie);


function [xa,ya] = polyline_quality_angle(x,y,canglethres)
% bad angles
% Checks only positive angles, i.e. only bays but not capes. 
% For this to work properly, the external boundary must be oriented CW, and
% the  internal boundaries (islands) CCW.

% % side  -1, 0, or 1  determines the sign of the angles to test
% % For codes==1,5 (main boundary) check only positive angles;
% % For codes==2,6 (islands) check only negative angles;
% % For all other codes (unknown boundary) check only positive angles

v = [x(:) y(:)];
ang = line2line(v(1:end-2,:),v(2:end-1,:),v(3:end,:),v(2:end-1,:)); % angles between conseq edges
% if side<0
%     % determine small negative angles
%     abad = [false; ang>-canglethres; false]; % logical index for each node
% elseif side>0
%     % determine small positive angles
%     abad = [false; ang<canglethres; false]; % logical index for each node
% else % side==0
%     % check both sides
%     abad = [false; abs(ang)<canglethres; false]; % logical index for each node
% end

% determine small positive angles
abad = [false; ang>=0 & ang<canglethres; false]; % logical index for each node
xa = x(abad);
ya = y(abad);


function [xa,ya] = polyline_quality_ratio(x,y,rthres)
% bad ratio between succesive edge lengths

%%%% this should be done for each continuous segment separately
e = (diff(x).^2 + diff(y).^2).^0.5;
r = e(1:end-1)./e(2:end); % ratio between conseq edge lengths
abad = [false (r<1./rthres | r>rthres) false]; % logical index for each node
xa = x(abad);
ya = y(abad);


function [xm,ym] = polyline_quality_edgelen(x,y,cedgelenthres)
% short edges
ebad = ((diff(x).^2 + diff(y).^2).^0.5) < cedgelenthres;
xm = x(1:end-1)+diff(x)/2;
ym = y(1:end-1)+diff(y)/2;
xm = xm(ebad);
ym = ym(ebad);


function [xm,ym] = grid_quality_edgelen(x,y,tri,cedgelenthres)
% short edges for a triangular grid

e = sort(reshape(tri(:,[1 2 2 3 3 1])',2,[]))'; % each edge is a sorted row
e = unique(e,'rows'); % unique edges
el = sqrt(diff(x(e),1,2).^2 + diff(y(e),1,2).^2); % edge lengths
ebad = el < cedgelenthres;

% edge mid-points
xm = mean(x(e),2);
ym = mean(y(e),2);

% mid-points for short edges
xm = xm(ebad);
ym = ym(ebad);



% Callbacks
%----------

function cLeftResize(src,~) % ,ha,hastat
% Called on left panel resize.

H = guidata(gcbf);
lo = H.layout;

hch = get(src,'Children');
ha = hch(end);
hastat = hch(end-1);

wpos = getpixelposition(src);
opos = get(ha,'Position'); % original position
tpos = [lo.mal lo.mab wpos(3)-lo.mar wpos(4)-lo.mat]; % target position

% do not shrink beyond the axes size limit
alim = 200; % axes size limit in both width and height
boundflag = false;
if tpos(3) < alim
    tpos(3) = alim;
    wpos(3) = tpos(3) + lo.mar;
    boundflag = true;
end
if tpos(4) < alim
    tpos(4) = alim;
    wpos(4) = tpos(4) + lo.mat;
    boundflag = true;
end
if boundflag
    setpixelposition(src,wpos)
end

% resize axes such that the center remains in the center of the axes
rw = 1 - tpos(3)/opos(3);
rh = 1 - tpos(4)/opos(4);
xlim = get(ha,'Xlim');
ylim = get(ha,'Ylim');
xlim = xlim + [1 -1]*(xlim(2)-xlim(1))*rw/2;
ylim = ylim + [1 -1]*(ylim(2)-ylim(1))*rh/2;
setAxisLim(ha,xlim,ylim); % ,H.menu.view_geogrid,H.geogrid,H.utmzone
%%%% take geogrid functionality from visualbox
set(ha,'Position',tpos);
set(hastat,'Position',[lo.mal lo.msb wpos(3)-lo.mar lo.msh]);

guidata(src,H)


function setAxisLim(ha,xlim,ylim) % ,viewgeogrid,hgeogrid,utmzone

set(ha,'XLim',xlim,'YLim',ylim)

%%%% take geogrid functionality from visualbox
% % resetting geogrid takes long, skip it if it is not displayed
% if strcmp(get(viewgeogrid,'Checked'),'on')
%     geogridSet(ha,xlim,ylim,hgeogrid,utmzone);
% end


function wclosereq(src,evnt) %#ok<INUSD>
% Close request function for the main window.

H = guidata(src);
if ~fileIsUptodate(H)
    while true % until the window is closed or return to work selected
        % display a question dialog box
        selection = questdlg('The work is not saved...',...
          'Closing window...',...
          'Save and quit','Continue working','Quit without saving','Continue working');
        switch selection
            case 'Save and quit'
                [ok,err] = fileSaveVG(H, H.currentfile); % saves a VG file, returns true on success
                if ~ok
                    % the only case for the next iteration
                    errordlg({['Error writing file: ' H.currentfile],err.message});
                else
                    delete(gcbf)
                    return
                end
            case 'Continue working'
                return
            case 'Quit without saving'
                delete(gcbf)
                return
        end
    end
else
    delete(gcbf)
    return
end


function wbdcb(src,evnt) %#ok<INUSD>

H = guidata(src);

% cursor position
cp = get(H.ah,'CurrentPoint');  
xc = cp(1,1);
yc = cp(1,2);

% don't do anything if mouse is outside the main axes, e.g. resizing panels
xlim = get(H.ah,'XLim');
if xc<xlim(1) || xc>xlim(2)
    return
else
    ylim = get(H.ah,'YLim');
    if yc<ylim(1) || yc>ylim(2)
        return
    end
end

% work on unlocked objects only
obj = H.obj(obj_isfree(H.tbl));

% nodes' coordinates
x = [obj.x];
y = [obj.y];
ifoc = ffocus1(H.ah,x,y,xc,yc,H.pthres); % only one ifoc returned

% clean up the focus markers
cleanfoc(H.hfoc)

seltype = get(src,'SelectionType');

if ~isempty(ifoc)
    otype = [H.obj.type];
    objn = [obj.objn];
    noden = [obj.noden];
    
    % make matching end node of a closed polyline segment selected too
    [nodepos,matchend] = node1storlast(ifoc,otype(objn(ifoc)),find(diff(objn)),find(isnan(x)),length(x));
    if nodepos && x(ifoc)==x(matchend) && y(ifoc)==y(matchend)
        ifoc(end+1) = matchend;
    end
    
    objf = objn(ifoc)'; % object and node in focus
    nodef = noden(ifoc)';
    
    if all(otype(objf) == 4) && ~isempty(H.obj(objf).cmap) % for indexed images
        set(H.hfm,'Colormap',H.obj(objf).cmap)
    end

    % nodes in focus which are currently selected
    isselected = ismember([objf nodef],H.sel,'rows');
    
    % clear previous selections if necessary
    if (strcmp(seltype,'alt') || strcmp(seltype,'normal')) && any(~isselected)
        % click on an unselected node 
        H.sel = clear_selection(H.hsel);
        % no table de-selection until 'set to drag'
    end
    
    if any(~isselected)
        % no table selection until 'set to drag'
        H.sel = oNodeSelect(H.hsel,H.sel,x(ifoc),y(ifoc),objf,nodef);
%         [~] = axes(H.ah); % pass focus back to axes
%         % or set(H.hfm,'CurrentAxes',H.ah)
    elseif strcmp(seltype,'extend') % shift-click on a selected node
        ids = ismember(H.sel,[objf nodef],'rows');
        % clear selection only for the clicked node
        [H.sel,tblobj2clear] = clear_selection(H.hsel,H.sel,ids);
    end
    
    if strcmp(seltype,'alt') && ~H.keypressed.ctrl % right-click but not Ctrl-click
        set(H.cmnodeitem.deletesegtool,'Enable','on');
        isel = find(ismember([objn' noden'],H.sel,'rows'));
        if all(otype(objn(isel))==2) % for the coastlines
            % determine if 1st or last node of a coastline is in focus
            [nodepos,~,isegm] = node1storlast(isel,otype(objn(isel)),find(diff(objn)),find(isnan(x)),length(x));
%             if length(nodepos)==2 && all(nodepos) && isegm(1) ~= isegm(2) % && objn(isel(1)) ~= objn(isel(2))
%                 set(H.cmnodeitem.connecttool,'Enable','on') %%%%% move this to the menu invocation callback
%             end
            if length(nodepos)==2 && all(nodepos) && isegm(1) == isegm(2)
                set(H.cmnodeitem.closetool,'Enable','on') %%%%% move this to the menu invocation callback
            end
            
%             % determine state of quality check flags
%             pp = getPlotParam(H.obj(unique(H.sel(:,2))));
%             if all([pp.badangflag])
%                 set(H.cmnodeitem.qmarkeritem.markangle,'Checked','on')
%             elseif all(~[pp.badangflag])
%                 set(H.cmnodeitem.qmarkeritem.markangle,'Checked','off')
%             end
%             if all([pp.badlenflag])
%                 set(H.cmnodeitem.qmarkeritem.markedge,'Checked','on')
%             elseif all(~[pp.badlenflag])
%                 set(H.cmnodeitem.qmarkeritem.markedge,'Checked','off')
%             end
        end
        
        % invoke node context menu
        set(H.cmnode,'Position',get(H.hfm,'CurrentPoint'),'Visible','on');
    elseif strcmp(seltype,'open') % double-click on a selected node(s)
        % edit properties for selected nodes
        [H.obj,undo] = oNodeEdit(H.obj,H.sel);
        if ~isempty(undo)
            H.undo = undo_register(H,undo);
        end
    else % simple click
        % set to drag the selected nodes
        set(src,'WindowButtonMotionFcn',{@wbmcb_nodedrag,xc,yc})
        set(src,'WindowButtonUpFcn',@wbucb_nodedrag)
        H.tmp.obj = H.obj(unique(H.sel(:,1))); % reserve a copy of selected objects at the start of drag
        ud.action = 'oNodeMove';
        ud.label = 'node move';
        ud.r.objn = H.sel(:,1);
        ud.r.noden = H.sel(:,2);
        ud.r.dx = 0;
        ud.r.dy = 0;
        z = [H.obj.z];
        [~,inode,isel] = intersect([objn' noden'],H.sel,'rows');
        ud.r.zold = NaN(size(H.sel,1),1);
        ud.r.zold(isel) = z(inode);
        H.tmp.ud = ud; % temporary undo structure
    %     set(src,'KeyPressFcn',@wkpcb_nodedrag,objn)
    end
    
    guidata(src,H)
    
    
    % now clear/select objects in the table
    %%%% has to be after 'set to drag' for correct functionality, to deal
    %%%% with 'axes lost focus' situation
    
    %%%% 'tabledone' introduced to deal with table bug after 'confirm' in manual edit mode
%     if isfield(H,'tabledone') && H.tabledone
%         H.tabledone = false;
%         guidata(src,H)
%     else
    if length(objf)~=1 || obj_manedit(H.obj(objf))~=1

        % clear previous selections if necessary
        if (strcmp(seltype,'alt') || strcmp(seltype,'normal')) && any(~isselected)
            % click on an unselected node
            tableDeselect(H.tbl) 
        end

        if any(~isselected)
            tableSelect(H.tbl,objf)
        elseif strcmp(seltype,'extend') % shift-click on a selected node
            % clear selection only for the clicked node if the entire object
            % has become de-selected
            tableDeselect(H.tbl,tblobj2clear);
        end
    end
    
else % no nodes in focus
    % check for click on an edge of a polyline
    if ~strcmp(seltype,'extend') % if click not on a node and no shift pressed
        H.sel = clear_selection(H.hsel,[],[],H.tbl);
        set(H.hfm,'KeyPressFcn',{@wkpcb,''})
        [iobj,objpos,projp] = ffocus2(src,xc,yc); % this picks only polyline objects
        if ~isempty(iobj) % click on an edge of a polyline
            H.sel = oNodeSelect(H.hsel,H.sel,projp(1),projp(2),iobj,objpos,H.tbl);
            if strcmp(seltype,'alt') % right-click or Ctrl-click
                set(H.cmnodeitem.inserttool,'Callback',{@cbInsertNode,iobj,objpos,projp},'Enable','on')

                % invoke node context menu
                set(H.cmnode,'Position',get(H.hfm,'CurrentPoint'),'Visible','on');
            end
            guidata(src,H)
            return
        end
    end
    
    % click on empty space
    set(src,'WindowButtonMotionFcn','')
    rbbox; % finalRect = rbbox; % return figure units
    set(src,'WindowButtonMotionFcn',@wbmcb)
    point2 = get(gca,'CurrentPoint');    % button up detected
    point1 = [xc yc];                    % extract x and y
    point2 = point2(1,1:2);
    p1 = min(point1,point2);             % calculate locations
    offset = abs(point1-point2);         % and dimensions
    xbox = [p1(1) p1(1)+offset(1) p1(1)+offset(1) p1(1) p1(1)];
    ybox = [p1(2) p1(2) p1(2)+offset(2) p1(2)+offset(2) p1(2)];
%     axis manual
    in = inpolygon(x,y,xbox,ybox);          % in dataspace units
    
    if all(~in) % shortcut
        guidata(src,H)
        return
    end
    
    objn = [obj.objn]; % only editable objects (not locked)
    noden = [obj.noden];
    objf = objn(in); % objects and nodes in focus
    nodef = noden(in);
    xin = x(in);
    yin = y(in);
    if ~strcmp(seltype,'extend') % if no shift pressed
        H.sel = clear_selection(H.hsel,[],[],H.tbl);
        H.sel = oNodeSelect(H.hsel,H.sel,xin,yin,objf,nodef,H.tbl);
    else
        iadd = ~ismember([objf' nodef'],H.sel,'rows'); % are not among previously selected
%         irem = find(ismember(H.sel,[objf' nodef'],'rows'));
%         % clear selection only for the previously selected captured nodes
%         H.sel = clear_selection(H.hsel,H.sel,irem,H.tbl);
        H.sel = oNodeSelect(H.hsel,H.sel,xin(iadd),yin(iadd),objf(iadd),nodef(iadd),H.tbl);
    end
    
    guidata(src,H)
end
% "Alt" key has no effect on this

% guidata(src,H)


function row = tableGetSelected(htbl)
% Get indices of selected rows in the table.

jUIScrollPane = findjobj(htbl); 
jUITable = jUIScrollPane.getViewport.getView;

row = jUITable.getSelectedRows + 1; % Java indexes start at 0


function tableSelect(htbl,objf)
% Select the object(s) with index (indices) objf in the Object List table.

OBJNAMECOL = 3; % object names are in the 3rd column of the table

jUIScrollPane = findjobj(htbl); 
jUITable = jUIScrollPane.getViewport.getView;

row = jUITable.getSelectedRows + 1; % Java indexes start at 0
% col = jUITable.getSelectedColumns + 1;

% http://www.mathworks.com/matlabcentral/newsreader/view_thread/165066
trsel = setdiff(objf,row); % rows not selected
if ~isempty(trsel) % if it is not yet selected in the table
    for it = 1:length(trsel)
        jUITable.changeSelection(trsel(it)-1, OBJNAMECOL-1, true, false);
    end
end


function tableDeselect(htbl,objf)
% De-select the object(s) with index (indices) objf in the Object List table.
% If objf is not specified clear all selections.
%
% See
% http://www.jidesoft.com/javadoc/com/jidesoft/grid/JideTable.html
% http://www.mathworks.com/matlabcentral/newsreader/view_thread/165066

jUIScrollPane = findjobj(htbl); 
jUITable = jUIScrollPane.getViewport.getView;

if ~exist('objf','var') || (~isempty(objf) && length(objf)==1 && isinf(objf))
    % clear all selections
    jUITable.clearSelection;
else % including empty objf
    % clear only specified selections
    
    OBJNAMECOL = 3; % object names are in the 3rd column of the table
    
    row = jUITable.getSelectedRows + 1; % Java indexes start at 0
    
    trsel = intersect(objf,row); % rows selected
    if ~isempty(trsel) % if it is selected in the table
        for it = 1:length(trsel)
            % toggle selection
            jUITable.changeSelection(trsel(it)-1, OBJNAMECOL-1, true, false);
        end
    end
end


function wbmcb(src,~,wbm_old)

if exist('wbm_old','var') % supplied if wbmcb is called from toolEdit callback
%     disp('calling wbm_old') %%%%
    wbm_old; %#ok<VUNUS> % call the old WindowButtonMotion callback
end

H = guidata(src);

% cursor position
cp = get(H.ah,'CurrentPoint');  
xc = cp(1,1);
yc = cp(1,2);

% work on unlocked objects only
obj = H.obj(obj_isfree(H.tbl));

% nodes' coordinates for all objects
x = [obj.x];
y = [obj.y];

xlim = get(H.ah,'XLim');
ylim = get(H.ah,'YLim');

d = ((x-xc).^2+(y-yc).^2).^0.5; % distance from cursor position to each node

if ~isempty(x) && strcmp(get(H.tools.colornodes,'State'),'on') % toolColorNode state is on
    z = [obj.z];
    
    inv = isnan(z);
    z(inv) = [];
    din = d(~inv);
    xin = x(~inv);
    yin = y(~inv);
    
%     d = ((x-xc).^2+(y-yc).^2).^0.5; % distance from cursor position to each node
    [~,in] = sort(din);
    in = in(1:min(length(in),H.colornodes_n)); % take at most n closest nodes
    xin = xin(in);
    yin = yin(in);
    z = z(in);

%     %-------
%     % coloured scatterplot
%     % take only nodes within current limits of the main axes
%     in = xin>xlim(1) & xin<xlim(2) & yin>ylim(1) & yin<ylim(2);
%     xin = xin(in);
%     yin = yin(in);
%     z = z(in);
%     objn = objn(in);
%
%     ou = unique(objn); % in-range objects
%     ox = setdiff(1:length(obj),ou); % out-of-range objects
%     
%     % remove markers for out-of-range objects
%     for io = ox
%         if H.obj(io).type~=4 % except for images
%             set(H.obj(io).h.colornodes,'XData',[],'YData',[],'CData',[]);
%         end
%     end
%     
%     % adjust markers for in-range objects
%     if ~isempty(ou)
%         for io = ou
%             if H.obj(io).type~=4 % except for images
%                 no = objn==io & ~isnan(xin);
%                 set(H.obj(io).h.colornodes,'XData',xin(no),'YData',yin(no),'CData',z(no));
%             end
%         end
%     end
    
    %--------
    % coloured triangulation of n-closest nodes
    warning('off','MATLAB:delaunay:DupPtsDelaunayWarnId')
    tri = delaunay(xin,yin);
    warning('on','MATLAB:delaunay:DupPtsDelaunayWarnId')
    el = [tri tri(:,1)]'; % closed triangle patches
    set(H.colortri,'XData',xin(el),'YData',yin(el),'CData',z(el));
    set(H.ah,'CLim',[min(z,[],'omitnan') max(z,[],'omitnan')])
end

% ifoc = ffocus1(H.ah,x,y,xc,yc,H.pthres); %%%%% avoid function calls if possible
dthres = (diff(xlim).^2 + diff(ylim).^2).^0.5 * H.pthres;

% % fast-eliminate out-of-range nodes
% in = find(x<xc+dthres & x>xc-dthres & y<yc+dthres & y>yc-dthres);
% x = x(in);
% y = y(in);
% d = ((x-xc).^2+(y-yc).^2).^0.5; % distance from cursor position to each node

ifoc = find(d==min(d) & d<dthres, 1, 'first');

% % clean up the focus markers
% if isfield(H,'hfoc') && all(ishandle(H.hfoc)); delete(H.hfoc); H.hfoc = []; end

if ~isempty(ifoc)
%     % determine if 1st or last node of a coastline is in focus
%     otype = [obj.type];
%     objn = [obj.objn];
%     noden = [obj.noden];
%     nn = length(x);
%     if otype(objn(ifoc)) == 2 && noden(ifoc) == 1 % 1st node of a coastline
%         H.hfoc = plot(H.ah,x(ifoc),y(ifoc),'o','Color',H.selcolor,'MarkerSize',10,'HitTest','off');
%     elseif otype(objn(ifoc)) == 2 && (ifoc==nn-1 || objn(ifoc+2)~=objn(ifoc)) 
%         % last node of a coastline
%         % last node in the overall list or next non-NaN nodes' object nuber is different
%         % from the one in focus
%         H.hfoc = plot(H.ah,x(ifoc),y(ifoc),'+','Color',H.selcolor,'MarkerSize',10,'HitTest','off');
%     else
%         set(H.hfoc,'XData',x(ifoc),'YData',y(ifoc));
%     end

    % make sure focus markers are on top
    hc = get(H.ah,'Children');
    set(H.ah,'Children',[ H.hfoc(:); hc(~ismember(hc,H.hfoc))])
    
    z = [obj.z];
    code = [obj.code];
    noden = [obj.noden];
%     if H.keypressed.z
        fstr = {['id: ' num2str(noden(ifoc))];
                ['x: ' num2str(x(ifoc))];
                ['y: ' num2str(y(ifoc))];
                ['z: ' num2str(z(ifoc))];
                ['code: ' num2str(code(ifoc))]};
        set(H.hfoc(2),'Position',[x(ifoc)+diff(xlim)*0.02 y(ifoc)],'String',fstr); % show z value
%     else
        set(H.hfoc(1),'XData',x(ifoc),'YData',y(ifoc)); % just a square marker
%     end
else
%     disp('   ffocus2')
%     tic
%     [iobj,objpos,projp] = ffocus2(src,xc,yc);
%     toc
%     if ~isempty(iobj)
%         H.hfoc = plot(H.ah,projp(1),projp(2),'^g','MarkerSize',6,'HitTest','off');
%     end
    cleanfoc(H.hfoc)
end

guidata(src,H)



% function nodepos = node1storlast(ifoc,otype,nodef,objnthis,objnnext,nn)
% % determine if 1st or last node of a coastline is in focus
% ifoc = ifoc(:)';
% nodepos = zeros(size(ifoc)); % default value is 0
% nodepos(otype == 2 & (ifoc==nn-1 | objnnext~=objnthis)) = 2; % last node of a coastline
% nodepos(otype == 2 & nodef == 1) = 1; % 1st node of a coastline, also for a one-node coastline

function [nodepos,matchend,iseg] = node1storlast(ifoc,otype,ob,ib,ntot)
% Determine if 1st or last node of a coastline is in focus.
% ifoc  indices into the combined array of nodes in focus
% otype (optional) size(ifoc) object types for nodes in focus
% ob    object breaks in the combined arrays (where object number changes) 
% ib    polyline segment breaks in the combined arrays (indices of NaNs)
% ntot  total number of nodes in all objects
% matchend  indices of matching nodes
% iseg  segment number in the entire list
% Call: nodepos = node1storlast(ifoc,otype,find(diff(objn)),find(isnan(x)),length(x));
% where objn,x are combined arrays of object IDs and x-coordinate


ifoc = ifoc(:)'; % ensure row vector
if isempty(otype)
    otype = ones(size(ifoc))*2; % consider them all polylines
else
    otype = otype(:)'; % ensure row vector
end
u = union(ob+1, ib+1); %R2013a need to ensure u is a row
is = [1 u(:)']; % all 1st nodes (sorted)
is(is>ntot) = [];
ie = union(setdiff([ob ntot],ib),ib-1); % all last nodes (sorted): 
    % setdiff([ob ntot],ib) are last nodes for non-polyline objects
    % ib-1 are last nodes for NaN-terminated segments
nodepos = zeros(size(ifoc)); % default value is 0
ilast = otype == 2 & ismember(ifoc,ie);
i1st = otype == 2 & ismember(ifoc,is);
nodepos(ilast) = 2; % last node of a coastline
nodepos(i1st) = 1; % 1st node of a coastline, also for a one-node coastline

if nargout>1 % matchend requested
    matchend = zeros(size(ifoc));
    iseg = zeros(size(ifoc)); % segment numbers in the entire list
    [~,iifoc,iie] = intersect(ifoc,ie);
    [~,iunsort] = sort(iifoc);
    iseg(ilast) = iie(iunsort); % segment number for end nodes
    matchend(ilast) = is(iie(iunsort)); % matching start nodes for end nodes
    [~,iifoc,iis] = intersect(ifoc,is);
    [~,iunsort] = sort(iifoc);
    iseg(i1st) = iis(iunsort); % segment number for start nodes
    matchend(i1st) = ie(iis(iunsort)); % matching end nodes for start nodes
end


% functions for node placement callbacks
%----------------------------------
% behaviour:
% specify depth;
% click to place a nodes with specified depth;
% press Enter to specify another depth; etc.
% Scrolling still works both for window pan and for adjusting z values.

function wbdcb_freenode(src,evnt) %#ok<INUSD>
H = guidata(src);

if strcmp(get(src,'SelectionType'),'alt') % right-click
    [ok,z] = dlgFreeNodeZ_2(H.freenode_z); % get Z for subsequently created nodes
    if ~ok
        [H.obj,H.undo] = freenode_finalize(H);
    else
        H.freenode_z = z;
    end
else % left-click: placing a new node
    cp = get(H.ah,'CurrentPoint'); % cursor position
    xc = cp(1,1);
    yc = cp(1,2);

    if isnan(H.obj(1).x(end)) % 1st node
        H.obj(1).x(1) = xc; % replace dummy NaN with a valid value
        H.obj(1).y(1) = yc;
        H.obj(1).z(1) = H.freenode_z;
        H.obj(1).code(1) = 0;
    else % 2nd and subsequent nodes
        H.obj(1).x(end+1) = xc;
        H.obj(1).y(end+1) = yc;
        H.obj(1).z(end+1) = H.freenode_z;
        H.obj(1).code(end+1) = 0;
    end

    H.obj(1) = obj_update_data(H.obj(1),H.tbl);

    fstr = {['x: ' num2str(xc)];
            ['y: ' num2str(yc)];
            ['z: ' num2str(H.freenode_z)];
            ['code: ' num2str(H.obj(1).code(end))]};
    set(H.hfoc(2),'Position',[xc+diff(get(H.ah,'XLim'))*0.02 yc],'String',fstr); % show z value
    set(H.hfoc(1),'XData',xc,'YData',yc); % and a square marker
end

guidata(src,H)


function [obj,undostack] = freenode_finalize(H)
% Finalize freenode operation.

set(H.tools.freenode,'State','off') % stop freenode operation
set(H.tools.edit,'State','on')
toolEdit(H.tools.edit,[]) % go back to Edit state

obj = H.obj;

if length(H.obj(1).x)==1 && isnan(H.obj(1).x) % empty object
    obj = oDelete(H.obj,1,H.tbl); % delete without undo
    undostack = undo_unregister(H); % remove the last undo record
else % not empty
    undostack = H.undo; % keep the last undo record
    
    duration = (now - get(H.statstr,'UserData'))*24; % hours
    speed = length(H.obj(1).x)/duration; % samplings per hour
    sstr = ['Time elapsed: ' num2str(duration) ' hrs; Speed: ' num2str(round(speed)) ' samplings/hour.'];
    set(H.statstr,'String',sstr,'UserData',[])
end



% functions for polyline drawing callbacks
%----------------------------------
% behaviour: 
% click once to place the starting node;
% move mouse pointer to the next location;
% click once to place an interim node and move to continue with the next edge;
% double-click to place the finishing node.
% moving close to the start node snaps to it -- double click will produce a
% closed polyline, i.e. polygon.
% pressing Shift snaps to vertical or horizontal from the last node

function wbdcb_draw_idle(src,evd) %#ok<INUSD>
H = guidata(src);
if strcmp(get(src,'SelectionType'),'alt') % right-click
%     set(H.cmempty,'Position',get(H.hfm,'CurrentPoint'),'Visible','on');
else
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
%     H.sel %%%%%%%%%%% diagnostic output
    
    cp = get(H.ah,'CurrentPoint'); % cursor position
    x = cp(1,1);
    y = cp(1,2);

    objnew = oCreatePolyline([x x NaN],[y y NaN],1,2);
        % duplicate the last node - it will be replaced when mouse is moved
        
    %%%%% ad hoc
    pp = H.ppdef;
%     pp.color = [122 16 228]/255; % purple
    pp.color = [0 1 0]; % green
    objnew = obj_draw(H.ah, objnew, pp);
    H.obj = oInsert(H.obj, objnew, 1, H.tbl); % make it 1st, the uppermost object

    set(H.hfm,'WindowButtonDownFcn',@wbdcb_draw,...
              'WindowButtonMotionFcn',@wbmcb_draw,...
              'KeyPressFcn',{@wkpcb,'draw'});
    
    guidata(src,H)
end


function wbmcb_draw(src,evnt) %#ok<INUSD>
H = guidata(src);

cp = get(H.ah,'CurrentPoint'); % cursor position
xc = cp(1,1);
yc = cp(1,2);

x = H.obj(1).x(1:end-2);
y = H.obj(1).y(1:end-2);

% snap to 1st node if close
d = ((x(1)-xc)^2+(y(1)-yc)^2)^0.5; % distance from cursor position to 1st node
dthres = (diff(get(H.ah,'XLim')).^2 + diff(get(H.ah,'YLim')).^2).^0.5 * H.pthres;
if d<dthres
    xc = x(1);
    yc = y(1);
    
%     % move pointer to the exact 1st node location
% %     cp(:,1) = xc; cp(:,2) = yc; set(H.ah,'CurrentPoint',cp) % can't do this -- read only property
%     fpos = get(H.hfm,'Position'); % in pixels on the screen
%     origunits = get(H.ah,'Units');
%     apos = get(H.ah,'Position');  % relative to the figure window
%     set(H.ah,'Units',origunits);
%     xlim = get(H.ah,'XLim');
%     ylim = get(H.ah,'YLim');
%     
%     %%%%% this doen't give the correct location
%     xpix = (xc-xlim(1))*apos(3)/diff(xlim) + apos(1) + fpos(1);
%     ypix = (yc-ylim(1))*apos(4)/diff(ylim) + apos(2) + fpos(2);
%     set(0,'PointerLocation',[xpix ypix])
end

H.obj(1) = obj_update_data(H.obj(1),H.tbl,[x xc NaN],[y yc NaN],[]);

guidata(src,H)


function wbdcb_draw(src,evnt) %#ok<INUSD>
H = guidata(src);

cp = get(H.ah,'CurrentPoint'); % cursor position
xc = cp(1,1);
yc = cp(1,2);

x = H.obj(1).x(1:end-2);
y = H.obj(1).y(1:end-2);

% snap to 1st node if close
d = ((x(1)-xc)^2+(y(1)-yc)^2)^0.5; % distance from cursor position to 1st node
dthres = (diff(get(H.ah,'XLim')).^2 + diff(get(H.ah,'YLim')).^2).^0.5 * H.pthres;
if d<dthres
    xc = x(1);
    yc = y(1);
end

if strcmp(get(src,'SelectionType'),'open') % double-click
    % finalize the object
    x = [x NaN];
    y = [y NaN];
    
%     set(H.hfm,'WindowButtonDownFcn',@wbdcb_draw_idle,...
%               'WindowButtonMotionFcn',[],...
%               'KeyPressFcn',{@wkpcb,''});
else
    % fix the node
    % duplicate the last node - it will be replaced when mouse is moved
    x = [x xc xc NaN]; 
    y = [y yc yc NaN];
end
H.obj(1) = obj_update_data(H.obj(1),H.tbl,x,y,[]);

guidata(src,H)

if strcmp(get(src,'SelectionType'),'open') % double-click
    set(H.tools.edit,'State','on')
    toolEdit(H.tools.edit,[])
end


% functions for node drag and drop callbacks
%----------------------------------
function wbmcb_nodedrag(src,evnt,xc,yc) %#ok<INUSL>
% xc,yc         position of the cursor at the start of the drag

H = guidata(src);

objn = H.sel(:,1); % object and node indices in focus
noden = H.sel(:,2);
objnu = unique(objn);

cp = get(H.ah,'CurrentPoint'); % cursor position
xp = cp(1,1);
yp = cp(1,2);

if H.keypressed.ctrl
    
    if length(objn) == 1 && H.obj(objn).type==2 % && ...
%        (noden == 1 || noden == length(H.obj(objn).x)-1) % 1st or last node (for polylines only)
        [nodestatus,imatchend] = node1storlast(noden,2,[],find(isnan(H.obj(objn).x)),length(H.obj(objn).x));
        % if 1st or last node of a "non-island" polyline is being dragged,
        % snap to the other end node if dragged close
%         if noden == 1
%             x1 = H.tmp.obj.x(end-1);
%             y1 = H.tmp.obj.y(end-1);
%         else
%             x1 = H.tmp.obj.x(1);
%             y1 = H.tmp.obj.y(1);
%         end
        if nodestatus % 1 or 2
            x1 = H.tmp.obj.x(imatchend);
            y1 = H.tmp.obj.y(imatchend);
        
            d = ((x1-xp)^2+(y1-yp)^2)^0.5; % distance from cursor position to 1st node
            dthres = (diff(get(H.ah,'XLim')).^2 + diff(get(H.ah,'YLim')).^2).^0.5 * H.pthres;
            if d<dthres
                xp = x1;
                yp = y1;
                % xc is not exactly equal to H.tmp.obj.x(noden) for some reason; correst this discrepancy
                xc = H.tmp.obj.x(noden);
                yc = H.tmp.obj.y(noden);
            end
        end
    elseif length(objn) == 2 && length(objnu) == 1 && H.obj(objnu).type==2 % && ...
%        (noden(1) == 1 && noden(2) == length(H.obj(objnu).x)-1) % 1st AND last node (for polylines only)
        [nodestatus,~,iseg] = node1storlast(noden,[2 2],[],find(isnan(H.obj(objnu).x)),length(H.obj(objnu).x));
        % if the 1st and last node of an "island" polyline is being
        % dragged, disconnect them by dragging only the last node
        if all(nodestatus) && iseg(1)==iseg(2) % && all(ismember(imatchend,ifoc))
            % leave only last node in focus
            H.sel = clear_selection(H.hsel,H.sel,1); %,H.tbl

            objn(1) = [];
            noden(1) = [];
            
            H.tmp.ud.r.objn(1) = [];
            H.tmp.ud.r.noden(1) = [];
            H.tmp.ud.r.zold(1) = [];
        end
    end
end

dx = xp-xc;
dy = yp-yc;
if dx == 0 && dy == 0
    return
end

% control for a node in a grid to stay within its neighbouring triangles' area
if H.keypressed.ctrl
    otype = [H.tmp.obj.type];
    if any(otype==3)
        for io = 1:length(objnu) % objnu(:)'
            objc = H.tmp.obj(io); % extract a copy of object with coordinates at the start of drag
            if objc.type==3 % grid
                inode = noden(objn==objnu(io));
                x = objc.x;  x(inode) = x(inode) + dx;
                y = objc.y;  y(inode) = y(inode) + dy;
                if any(~grid_validmove([x' y'],objc.tri,inode))
                    return
                end
            end
        end
    end
end

xm = NaN(1,length(objn)); % coordinates to move selection markers
ym = xm;
nmove = true(size(xm));
for io = 1:length(objnu) % objnu(:)'
    inode = noden(objn==objnu(io)); % nodes belonging to this object
    objc = H.tmp.obj(io); % extract a copy of object with coordinates at the start of drag
    x = objc.x;  x(inode) = x(inode) + dx;
    y = objc.y;  y(inode) = y(inode) + dy;
    z = objc.z;
    imx = objc.imx + dx;
    imy = objc.imy + dy;
    
    %%%% No automatic redepth to speed up the operation: make it an option in the menu
%     if objc.type==3 && length(objc.x)~=length(inode) % grid with not all nodes selected
%         % interpolate depths
%         tri = double(objc.tri);
%         ii = ~ismember(inode,grid_boundarynodes(tri)); % interior nodes
%         if any(ii)
%             xo = objc.x'; % original coordinates
%             yo = objc.y';
%             inodei = inode(ii); % interior nodes in focus
%             xi = x(inodei)'; % new nodes requiring interpolation
%             yi = y(inodei)';
%             tri = tri( any(ismember(tri,inodei),2), :); % triangles containing interior nodes in focus
%             
%             k = tsearch(x,y,tri,xi,yi);
% %             k = tsearch_mex(x,y,tri,xi,yi);
%             z(inodei) = tinterp([xo yo], tri, z', [xi yi], k);
%         end
%     end

    H.obj(objnu(io)) = obj_update_data(H.obj(objnu(io)),[],x,y,[],z,[],[],imx,imy); % change the actual object
    
    xm(objn==objnu(io)) = x(inode);
    ym(objn==objnu(io)) = y(inode);
end

% move selection markers
xdata = get(H.hsel,'XData');
ydata = get(H.hsel,'YData');
xdata(nmove) = xm(nmove);
ydata(nmove) = ym(nmove);
set(H.hsel,'XData',xdata,'YData',ydata)

set(src,'KeyPressFcn',{@wkpcb,'nodedrag'},'KeyReleaseFcn',@wkrcb)

H.tmp.ud.r.dx = dx;
H.tmp.ud.r.dy = dy;
set(H.statstr,'String',...
        ['Moving:   dx = ' num2str(dx) '   dy = ' num2str(dy)])

guidata(src,H)


function wbucb_nodedrag(src,evnt) %#ok<INUSD>
% xc,yc         position of the cursor at the start of the drag

H = guidata(src);

% if we created an "island" make the other node selected too
objn = H.sel(:,1); % object and node indices in focus
noden = H.sel(:,2);
if length(objn) == 1 && H.obj(objn).type==2 % && ... % a single node of a polyline is in focus
%    (noden == 1 || noden == length(H.obj(objn).x)-1) && ... % 1st or last node in focus
%    H.obj(objn).x(1) == H.obj(objn).x(end-1) && H.obj(objn).y(1) == H.obj(objn).y(end-1) % it is an island now
    [nodestatus,imatchend] = node1storlast(noden,2,[],find(isnan(H.obj(objn).x)),length(H.obj(objn).x));
    if nodestatus &&... % 1st or last node was moved
       H.obj(objn).x(noden) == H.obj(objn).x(imatchend) &&...
       H.obj(objn).y(noden) == H.obj(objn).y(imatchend) % it is a polygon now

        x = H.obj(objn).x(imatchend);
        y = H.obj(objn).y(imatchend);

        H.sel = oNodeSelect(H.hsel,H.sel,x,y,objn,imatchend);
    end
    
    % this is to keep the objects in table selected
    if nodestatus
        tableSelect(H.tbl,unique(objn))
    end
end

% switch back to regular callbacks
set(src,'WindowButtonMotionFcn',@wbmcb,'WindowButtonUpFcn',[])
set(src,'KeyPressFcn',{@wkpcb,'nodesel'},'KeyReleaseFcn',@wkrcb) % treats Delete key press and Ctrl-Z


% process various cases
% the node was moved
ismoved = ~isempty(H.tmp.ud) && (H.tmp.ud.r.dx~=0 || H.tmp.ud.r.dy~=0);
% % the object is in the manual edit mode
% isman = length(objn) == 1 && obj_manedit(H.obj(objn)) == 1;

% simple click for objects in manual edit mode confirms the node without
% any change
% if isman
isman = false;
if length(objn) == 1
    if obj_manedit(H.obj(objn)) == 1
        isman = true;
%         H.tabledone = true;
        % "original" object in manual edit mode
        cn = H.obj(objn).meta.obj_confirmed; % corrsponding "confirmed" object number
        % move the node to the "confirmed" object
        [H.obj(objn),H.obj(cn),undothis] = oConfirm(H.obj(objn),H.obj(cn),noden,H.tbl);
        
        H.sel = clear_selection(H.hsel); % de-select all nodes; and de-select in table
        H.sel = oNodeSelect(H.hsel,H.sel,H.obj(cn).x(noden),H.obj(cn).y(noden),cn,noden);
%     elseif obj_manedit(H.obj(objn)) == 2
%         isman = true;
%         H.tabledone = true;
%         % "confirmed" object in manual edit mode
%         cn = H.obj(objn).meta.obj_orig; % corrsponding "original" object number
%         % move the node to the "confirmed" object
%         [H.obj(objn),H.obj(cn),undothis] = oConfirm(H.obj(objn),H.obj(cn),noden,[]);
%         undothis.label = 'set "un-confirmed"';
%         
%         clear_selection(H.hsel); % de-select all nodes; and de-select in table
%         H.sel = oNodeSelect(H.hsel,H.sel,H.obj(cn).x(noden),H.obj(cn).y(noden),cn,noden);
    end
end

% process objects in manual edit mode and save undo information
if ismoved
%     if isman %%%% do not set as confirmed after moving
%         % "original" object in manual edit mode
%         undothis.r.edit = H.tmp.ud; % record the edit action
%     else
        undothis = H.tmp.ud;
%     end
    set(H.statstr,'String',...
        ['Moved:   dx = ' num2str(H.tmp.ud.r.dx) '   dy = ' num2str(H.tmp.ud.r.dy)])
end

if ismoved || isman
    H.undo = undo_register(H,undothis);
end


H.tmp = [];

guidata(src,H)

% update selection
if isman
    tableDeselect(H.tbl);
    tableSelect(H.tbl,cn)
end


function [objo,objc,ud] = oConfirm(objo,objc,noden,htbl)
% Move a node from "original" to "confirmed" object.

% delete one node

% save undo information
ud.action = 'oConfirm';
ud.label = 'set "confirmed"';
ud.r.objno = objo.objn(1);
ud.r.objnc = objc.objn(1);
ud.r.noden = noden;

% copy to "confirmed"
objc.x(noden) = objo.x(noden);
objc.y(noden) = objo.y(noden);
objc.z(noden) = objo.z(noden);

% invalidate in "original"
objo.x(noden) = NaN;
objo.y(noden) = NaN;
objo.z(noden) = NaN;

% update graphics
objo = obj_update_data(objo,htbl);
objc = obj_update_data(objc,htbl);


function wkrcb(src,evnt)
if strcmp(evnt.Key,'control')
    H = guidata(src);
    H.keypressed.ctrl = false;
    guidata(src,H)
end
if strcmp(evnt.Key,'shift')
    H = guidata(src);
    H.keypressed.shift = false;
    guidata(src,H)
end
if strcmp(evnt.Key,'alt')
    H = guidata(src);
    H.keypressed.alt = false;
    guidata(src,H)
end
if strcmp(evnt.Key,'z') && (isempty(evnt.Modifier) || ~strcmp(evnt.Modifier{1},'control'))
    H = guidata(src);
    H.keypressed.z = false;
    
%     % change string square marker for in-focus nodes
%     if strcmpi(get(H.hfoc,'Type'),'text')
%         pfoc = get(H.hfoc,'Position');
%         zfoc = get(H.hfoc,'UserData');
% 
%         delete(H.hfoc)
%         H.hfoc = plot(H.ah,pfoc(1),pfoc(1),'s','Color',H.hselparam.color,'MarkerSize',max(4,H.hselparam.size-3),'HitTest','off');
%         set(H.hfoc,'UserData',zfoc)
%         
%         'marker' %%%%
%     end
    
    guidata(src,H)
end


function wkpcb(src,evnt,state)

if strcmp(evnt.Key,'control')
    H = guidata(src);
    H.keypressed.ctrl = true;
    guidata(src,H)
end
if strcmp(evnt.Key,'shift')
    H = guidata(src);
    H.keypressed.shift = true;
    guidata(src,H)
end
if strcmp(evnt.Key,'alt')
    H = guidata(src);
    H.keypressed.alt = true;
    guidata(src,H)
end
if strcmp(evnt.Key,'z') && (isempty(evnt.Modifier) || ~strcmp(evnt.Modifier{1},'control'))
    H = guidata(src);
    H.keypressed.z = true;
    
%     % change square marker to string for in-focus nodes
%     if strcmpi(get(H.hfoc,'Type'),'lineseries')
%         xfoc = get(H.hfoc,'XData');
%         yfoc = get(H.hfoc,'YData');
%         zfoc = get(H.hfoc,'UserData');
% 
%         delete(H.hfoc)
%         H.hfoc = text(xfoc,yfoc,num2str(zfoc),'Parent',H.ah,....
%             'Color','r','HorizontalAlignment','center','HitTest','off');
%         set(H.hfoc,'UserData',zfoc)
%         
%         'string' %%%%
%     end
    
    guidata(src,H)
end

switch state
    case ''
        if strcmp(evnt.Key,'z') && ~isempty(evnt.Modifier) && strcmp(evnt.Modifier{1},'control') % undo command
            undofcn(src);
        end
    case 'nodesel'
        if strcmp(evnt.Key,'delete')
            if ~isempty(evnt.Modifier) && strcmp(evnt.Modifier{1},'shift')
                % Shift-Delete
                cbDeleteSegment(src,evnt)
            elseif ~isempty(evnt.Modifier) && strcmp(evnt.Modifier{1},'control')
                % Ctrl+Delete
                cbDeleteObject(src,evnt)
            elseif ~isempty(evnt.Modifier) && strcmp(evnt.Modifier{1},'alt')
                % Alt+Delete
                cbGridDeleteAndFix(src,evnt)
            else
                % just Delete
                cbDeleteNode(src,evnt)
            end
            
        elseif ~isempty(evnt.Modifier) && strcmp(evnt.Modifier{1},'control')
            switch evnt.Key
                case 'z' % undo command
                    undofcn(src);
                case 'c'
                    H = guidata(src);
                    H.buffer = H.obj(unique(H.sel(:,1)));
                    guidata(src,H)
                case 'v'
                    H = guidata(src);
                    objnew = (H.buffer);
                    % insert them on top of the old objects
                    for io = 1:length(objnew)
                        objnew(io).objn = repmat(io,size(objnew(io).x));
                    end
                    [H.obj,undothis] = obj_load(H.obj, objnew, H.ah, [], H.tbl);
                    H.undo = undo_register(H,undothis);
                    guidata(src,H)
            end
        end
    case 'nodedrag'
        if strcmp(evnt.Key,'escape')
            % restore object plots
            H = guidata(src);
            sel = H.sel;
            objn = sel(:,1);
            noden = sel(:,2);
            objnu = unique(objn);
            for io = 1:length(objnu)
                H.obj(objnu(io)) = obj_update_data(H.obj(objnu(io)),H.tbl, ...
                    H.tmp.obj(io).x, H.tmp.obj(io).y,[],H.tmp.obj(io).z,[],[],...
                    H.tmp.obj(io).imx, H.tmp.obj(io).imy);
%                 (obj,x,y,objn,z,code,tri,imx,imy,cdata,cmap)
            end

            % restore selection markers
            xdata = get(H.hsel,'XData');
            ydata = get(H.hsel,'YData');
            for is = 1:length(noden) %%%%%%%%% avoid the loop?
                xdata(is) = H.obj(objn(is)).x(noden(is));
                ydata(is) = H.obj(objn(is)).y(noden(is));
            end
            set(H.hsel,'XData',xdata,'YData',ydata)

            set(H.hfm,'WindowButtonMotionFcn',@wbmcb,'WindowButtonUpFcn',[],'KeyPressFcn',{@wkpcb,''})
            H.tmp = [];
            guidata(src,H)
        end
    case 'draw'
        if strcmp(evnt.Key,'escape') % remove last placed node
            H = guidata(src);
            x = H.obj(1).x(1:end-3);
            if isempty(x) % the 1st node is removed, cancel drawing and go to idle mode
                H.obj = oDelete(H.obj,1);
                set(H.hfm,'WindowButtonDownFcn',@wbdcb_draw_idle,...
                      'WindowButtonMotionFcn',[],...
                      'KeyPressFcn',{@wkpcb,''});
            else
                y = H.obj(1).y(1:end-3);
                cp = get(H.ah,'CurrentPoint'); % cursor position
                xc = cp(1,1);
                yc = cp(1,2);
                H.obj(1) = obj_update_data(H.obj(1),H.tbl,[x xc NaN],[y yc NaN],[]);
            end
            guidata(src,H)
        end
    case 'freenode'
        H = guidata(src);
        if strcmp(evnt.Key,'return') || strcmp(evnt.Key,'space')
            [ok,z] = dlgFreeNodeZ_2(H.freenode_z); % get Z for subsequently created nodes
            if ~ok
                [H.obj,H.undo] = freenode_finalize(H);
%                 set(H.tools.freenode,'State','off') % stop freenode operation
%                 set(H.tools.edit,'State','on')
%                 toolEdit(H.tools.edit,[]) % go back to Edit state
%                 return
            else
                H.freenode_z = z;
            end
        elseif strcmp(evnt.Key,'escape') % remove last placed node
            if length(H.obj(1).x) == 1 % the 1st node is being removed, fill in with dummy NaN
                x = NaN;
                y = NaN;
                z = NaN;
            else
                x = H.obj(1).x(1:end-1);
                y = H.obj(1).y(1:end-1);
                z = H.obj(1).z(1:end-1);
            end
            H.obj(1) = obj_update_data(H.obj(1),H.tbl,x,y,[],z);
        end
        guidata(src,H)
end


function ifoc = ffocus1(ah,x,y,xp,yp,pthres,allflag)
% Find closest point in x,y to cusor pointer xp,yp; 
% if allflag is supplied and true return all points within the range.

dthres = (diff(get(ah,'XLim')).^2 + diff(get(ah,'YLim')).^2).^0.5 * pthres;

% % fast-eliminate out-of-range nodes
% in = find(x<xp+dthres & x>xp-dthres & y<yp+dthres & y>yp-dthres);
% x = x(in);
% y = y(in);

d = ((x-xp).^2+(y-yp).^2).^0.5; % distance from cursor position to each node

if ~exist('allflag','var') || isempty(allflag) || ~allflag
    % return only one point - closest (first in the list if several points have
    % the smallest distance)
    ifoc = find(d==min(d) & d<dthres, 1, 'first');
else
    % all points within the range
    ifoc = find(d<dthres);
end
% ifoc = in(ifoc);


function [iobj,objpos,projp] = ffocus2(src,xp,yp)
% Check what coastline is in focus.
H = guidata(src);
obj = H.obj(obj_isfree(H.tbl));
obj = obj([obj.type]==2); % take only polylines
if isempty(obj)
    iobj = []; objpos = []; projp = [];
    return
end
poly = [[obj.x]' [obj.y]'];
[pos,dist,projp] = projPointOnPolyline([xp yp], poly);
if dist > (diff(get(H.ah,'XLim')).^2 + diff(get(H.ah,'YLim')).^2).^0.5 * H.pthres
    iobj = []; objpos = []; projp = [];
else
    objn = [obj.objn];
    noden = [obj.noden];
    iobj = objn(ceil(pos));
    objpos = noden(floor(pos+1)) + pos-floor(pos); % e.g. for a point midway between node#1 and node#2, objpos=1.5;
end


%------------------------------------------------------
% menu callbacks

function cbFile(src,evd) %#ok<INUSD>


function cbFileExport(src,evd) %#ok<INUSD>

H = guidata(src);
objf = unique(H.sel(:,1)); % selected objects
h = H.menu.filei.exporti;

if ~isempty(objf) % && all([H.obj(objf).type]==2)
    set(h.tobase,'Enable','on')
    set(h.saveinfile,'Enable','on')
else
    set(h.tobase,'Enable','off')
    set(h.saveinfile,'Enable','off')
end

if any([H.obj.type]==2)
%     set(h.saveallinbase,'Enable','on')
    set(h.saveallinfile,'Enable','on')
else
%     set(h.saveallinbase,'Enable','off')
    set(h.saveallinfile,'Enable','off')
end

if ~isempty(objf) && all(ismember([H.obj(objf).type],[1 2]))
    set(h.savenod,'Enable','on')
else
    set(h.savenod,'Enable','off')
end

if ~isempty(objf) && all(ismember([H.obj(objf).type],[1 2 3]))
    set(h.savexyz,'Enable','on')
else
    set(h.savexyz,'Enable','off')
end

if length(objf)==1 && H.obj(objf).type==3
    set(h.savengh,'Enable','on')
else
    set(h.savengh,'Enable','off')
end


function cbEdit(src,evd) %#ok<INUSD>
H = guidata(src);
h = H.menu.editi;
objf = unique(H.sel(:,1)); % selected objects
if isempty(objf)
    set(h.selectallnodes,'Enable','off')
    set(h.selectduplicatenodes,'Enable','off')
    set(h.deletenode,'Enable','off')
    set(h.deleteobj,'Enable','off')
    set(h.combine,'Enable','off')
    set(h.plotparam,'Enable','off')
else
    set(h.selectallnodes,'Enable','on')
    set(h.deletenode,'Enable','on')
    set(h.deleteobj,'Enable','on')
    if all([H.obj(objf).type]==1) || all([H.obj(objf).type]==2)
        set(h.combine,'Enable','on')
    else
        set(h.combine,'Enable','off')
    end
    if all([H.obj(objf).type]==2) || all([H.obj(objf).type]==3)
        set(h.convert2nodes,'Enable','on')
    else
        set(h.convert2nodes,'Enable','off')
    end
    if all(ismember([H.obj(objf).type],[1 2 3])) % only node-based objects
        set(h.selectduplicatenodes,'Enable','on')
    end
    set(h.plotparam,'Enable','on')
end



function cbTransform(src,evd) %#ok<INUSD>
% Enable/disable menu items according to program state.

H = guidata(src);
h = H.menu.transformi;
objf = unique(H.sel(:,1)); % selected objects

if ~isempty(objf)
    obj = H.obj(objf);
    set(h.coordoffset,'Enable','on')
    set(h.coordscale,'Enable','on')
    set(h.transformapply,'Enable','on')
    
    if all(ismember([obj.type],[1 2 3]))
         if length(objf)==2 && ...
             length(obj(1).x(~isnan(obj(1).x)))==3 && ...
             length(obj(2).x(~isnan(obj(2).x)))==3
            set(h.transformdefine,'Enable','on')
        end
        set(h.redepth,'Enable','on')
        set(h.setmindepth,'Enable','on')
        set(h.coord2utm,'Enable','on')
        set(h.coord2geo,'Enable','on')
        set(h.fathom2m,'Enable','on')
        set(h.m2fathom,'Enable','on')
    else
        set(h.transformdefine,'Enable','off')
        set(h.redepth,'Enable','off')
        set(h.setmindepth,'Enable','off')
        set(h.coord2utm,'Enable','off')
        set(h.coord2geo,'Enable','off')
        set(h.fathom2m,'Enable','off')
        set(h.m2fathom,'Enable','off')
    end
    if all([H.obj(objf).type] == 4)
        set(h.imgrotate,'Enable','on')
        set(h.imgdigitize,'Enable','on')
    else
        set(h.imgrotate,'Enable','off')
        set(h.imgdigitize,'Enable','off')
    end
    set(h.tofront,'Enable','on')
    set(h.toback,'Enable','on')
    set(h.tofrontone,'Enable','on')
    set(h.tobackone,'Enable','on')
else
    tf = fields(h);
    for ifl = 1:length(tf)
        set(h.(tf{ifl}),'Enable','off')
    end
end


function cbNodeSet(src,evd) %#ok<INUSD>
H = guidata(src);
h = H.menu.nodeseti;

if strcmp(get(H.tools.freenode,'State'),'off')
    set(h.bathynew,'Label','&New')
else
    set(h.bathynew,'Label','&Finalize Object')
    set(h.manedit,'Enable','off')
    set(h.bathydec,'Enable','off')
    set(h.bathytrim,'Enable','off')
    set(h.bathydom,'Enable','off')
    set(h.deleteinpolygon,'Enable','off')
    set(h.extract,'Enable','off')
    set(h.cdt,'Enable','off')
    set(h.convert2poly,'Enable','off')
end

% enable/disable items based on the selection
objn = unique(H.sel(:,1))';
if isempty(objn)
    set(h.manedit,'Enable','off')
    set(h.bathydec,'Enable','off')
    set(h.bathytrim,'Enable','off')
    set(h.bathydom,'Enable','off')
    set(h.deleteinpolygon,'Enable','off')
    set(h.extract,'Enable','off')
    set(h.cdt,'Enable','off')
    set(h.convert2poly,'Enable','off')
else
    obj = H.obj(objn);
    if any([obj.type]~=1)
        set(h.manedit,'Enable','off')
        set(h.bathydec,'Enable','off')
        set(h.bathytrim,'Enable','off')
        set(h.bathydom,'Enable','off')
        set(h.deleteinpolygon,'Enable','off')
        set(h.extract,'Enable','off')
        set(h.convert2poly,'Enable','off')
    else
        % allow editing only node sets
        set(h.bathytrim,'Enable','on')
        
        % check objects in manual edit mode
        isman = obj_manedit(H.obj(objn));
        if all(isman)
            set(h.manedit,'Label','Stop manual edit mode')
            %'Tooltip','Breaks connection between objects. Objects become regular.'
            set(h.manedit,'Enable','on')
        elseif all(~isman)
            set(h.manedit,'Label','Set Manual Editing Mode...')
            %'Tooltip','Sets to move the edited nodes in a separate object'
            set(h.manedit,'Enable','on')
        else
            set(h.manedit,'Enable','off')
        end
        
        if length(objn)==1 % allow editing only one node set at a time
            set(h.bathydec,'Enable','on')
            set(h.bathydom,'Enable','on')
            set(h.deleteinpolygon,'Enable','on')
            set(h.extract,'Enable','on')
            set(h.convert2poly,'Enable','on')
        end
    end

    if all(ismember([obj.type],[1 2 3])) % selection of any number of node-based objects
        set(h.cdt,'Enable','on')
    else
        set(h.cdt,'Enable','off')
    end
end


function cbPolyline(src,evd) %#ok<INUSD>
H = guidata(src);
h = H.menu.polylinei;

% h.break  @cbBreakSegment
% h.connect  @cbConnectSegments
% h.close  @cbCloseSegment
% h.extract  @cbSegmentExtract
% selectopen
% h.deleteseg  @cbDeleteSegment
% h.polyinterp % always on if there are at least two polylines
% h.simplify % all(otype == 2)

% menu items to be disabled if any of the selected objects is not a polyline
polylineonly = [h.break
            h.connect
            h.joinadjacent
            h.close
            h.selectopen
            h.extract
            h.deleteseg
            h.polyclip
            h.simplify
            h.upsample
            h.polydom
            h.selectsegment
            h.orientallccw
            h.orientsegcw];
        
% menu items to be enabled only when one object is selected
singleobj = [
    h.break
    h.connect
    h.joinadjacent
    h.close
    h.selectopen
    h.extract
    h.deleteseg
    h.polyclip
    h.upsample
    h.polydom
    h.selectsegment
    h.orientallccw
    h.orientsegcw
    ];

% enable/disable items based on the selection
objn = unique(H.sel(:,1))';
obj = H.obj(objn);
otype = [obj.type];
if ~isempty(otype) && all(otype==2)
    set(polylineonly,'Enable','on')
    if length(objn)~=1 % allow editing only one polyline at a time
        set(singleobj,'Enable','off')
    else
%         if strcmp(get(H.cmnodeitem.connecttool,'Enable'),'off')
%             set(h.connect,'Enable','off')
%         end        
    end
else % some selected objects are not polylines
    set(polylineonly,'Enable','off')
end
if length(find([H.obj.type]==2))<2 % not enough polylines
    set(h.polyinterp,'Enable','off')
end


function cbGrid(src,evd) %#ok<INUSD>
H = guidata(src);
h = H.menu.gridi;

% h.gridgenerate % always on if there is al least one polyline (to specify the grid boundary)
% h.griddeleteandfix @cbGridDeleteAndFix
% h.gridcleave @cbCleaveNode
% h.gridcleaveauto @cbCleaveAuto
% h.griddekite @cbDekiteNode
% h.griddekiteauto {@cbDekiteAuto,false}
% h.griddekiteautonb {@cbDekiteAuto,true}
% h.gridexedge @cbExchangeEdge
% h.gridexedgeauto @cbExchangeEdgeAuto
% h.gridboundaryselect @cbGridBoundarySelect
% h.gridselectcode @cbGridSelectCode
% h.gridextractboundary @cbExtractBoundary
% h.gridextract @cbExtractGridPart
% h.gridslash @cbGridSlash
% h.gridclean @cbGridClean
% h.gridmerge @cbGridMerge
% h.gridsmooth @cbGridSmooth

% menu items to be disabled if any of the selected objects is not a grid
typeonly = [
    h.griddeleteandfix
    h.griddeletetriangle
    h.gridcollapseedge
    h.gridsplitedge
    h.gridcleave
    h.gridcleaveauto
    h.griddekite
    h.griddekiteauto
    h.griddekiteautonb
    h.gridexedge
    h.gridexedgeauto
    h.gridboundaryselect
    h.gridboundaryselectloop
    h.gridselectcode
    h.gridselectnneighbours
    h.gridselectle
    h.gridextractboundary
    h.gridextractboundaryloop
    h.gridextract
    h.gridslash
    h.gridclean
    h.gridmerge
    h.gridrefine
    h.gridrefinesel
    h.gridsmooth
    h.gridsmoothfixobn
    h.gridfix
    h.gridinfo
    ];
        
% menu items to be enabled only when one object is selected
singleobj = [
    h.griddeletetriangle
    h.gridcollapseedge
    h.gridsplitedge
    h.gridcleave
    h.gridcleaveauto
    h.griddekiteauto
    h.griddekiteautonb
    h.gridexedge
    h.gridexedgeauto
    h.gridboundaryselect
    h.gridboundaryselectloop
    h.gridselectcode
    h.gridselectnneighbours
    h.gridselectle
    h.gridextractboundary
    h.gridextractboundaryloop
    h.gridextract
    h.gridslash
    h.gridclean
    h.gridmerge
    h.gridrefine
    h.gridrefinesel
    h.gridsmooth
    h.gridsmoothfixobn
    h.gridfix
    h.gridinfo
    ];

% menu items to be enabled only when one node is selected
singlenode = [
    h.griddeleteandfix
    h.griddekite
    ];

% menu items to be enabled only when two nodes are selected
doublenode = [
    h.gridcollapseedge
    h.gridsplitedge
    h.gridexedge
    ];

% menu items to be enabled only when selected nodes are on the boundary and
% on the same boundary loop
looponly = [
    h.gridboundaryselectloop
    h.gridextractboundaryloop
    ];

% jsetuiprop(looponly,'ToolTipText','');

% enable/disable items based on the selection
objn = unique(H.sel(:,1))';
noden = unique(H.sel(:,2))';
obj = H.obj(objn);
otype = [obj.type];
if ~isempty(otype) && all(otype==3)
    set(typeonly,'Enable','on')
    if length(objn)~=1 % allow editing only one object at a time
        set(singleobj,'Enable','off')
    else
        [ebnd,eint] = grid_edge(obj.tri);
        
        % fast boundary node check
        bn = unique(ebnd);
        if any(~ismember(noden,bn)) % some of the selected nodes are not boundary nodes
            jsetuiprop(looponly,'ToolTipText',...
                'Nodes must be on the boundary and on the same boundary loop');
            set(looponly,'Enable','off')
            % same-loop check is too slow, do it if the routine is called
%             dosameloopcheck = false;
%         else
%             dosameloopcheck = true;
        end
        
        if length(noden)~=1
            set(singlenode,'Enable','off')
%             % same-loop check is too slow, do it if the routine is called
%             if dosameloopcheck
%                 % slow same-loop node check
%                 [loop,errn] = edge_extract_loop(ebnd,noden(1));
%                 if errn
%                     jsetuiprop(looponly,'ToolTipText','Does not work for branching boundaries')
%                     set(looponly,'Enable','off')
%                 end
%                 if any(~ismember(noden,loop)) % some of the selected nodes are not part of this loop
%                     jsetuiprop(looponly,'ToolTipText',...
%                         'Nodes must be on the same boundary loop');
%                     set(looponly,'Enable','off')
%                 end
%             end
        end
        
        if length(noden)~=2
            set(doublenode,'Enable','off')
        else % exactly 2 nodes selected
            e = [ebnd; eint];
            if ~ismember(noden,e,'rows')
                set(h.gridcollapseedge,'Enable','off')
                set(h.gridsplitedge,'Enable','off')
            end
            if ~ismember(noden,eint,'rows')
                set(h.gridexedge,'Enable','off')
            end
        end
        if length(noden)<3 || all(sum(ismember(obj.tri,noden),2)~=3)
            set(h.griddeletetriangle,'Enable','off')
            set(h.gridrefinesel,'Enable','off')
        end
    end
else % some selected objects are not grids
    set(typeonly,'Enable','off')
end
if isempty(find([H.obj.type]==2, 1)) % no polylines -> can not generate grid
    set(h.gridgenerate,'Enable','off')
else
    set(h.gridgenerate,'Enable','on')
end


function cbSelectAllNodes(src,evd) %#ok<INUSD>
H = guidata(src);
objnu = unique(H.sel(:,1));
x = [H.obj.x];
y = [H.obj.y];
objn = [H.obj.objn];
noden = [H.obj.noden];
is = ismember(objn,objnu);
H.sel = clear_selection(H.hsel); % clear all, no table de-select
H.sel = oNodeSelect(H.hsel,H.sel,x(is),y(is),objn(is),noden(is)); % make selection
% no need to update the table, since the object has been selected before
guidata(src,H)


function cbDeleteNode(src,evd) %#ok<INUSD>
H = guidata(src);

selnew = clear_selection(H.hsel,[],[],H.tbl);
[H.obj,undothis] = oNodeDelete(H.obj,H.sel(:,1),H.sel(:,2),H.tbl);
H.undo = undo_register(H,undothis);
H.sel = selnew;

set(H.hfm,'WindowButtonMotionFcn',@wbmcb,'WindowButtonUpFcn',[],'KeyPressFcn',{@wkpcb,''})
guidata(src,H)


function cbInsertNode(src,evd,iobj,pos,pins) %#ok<INUSL>
if pos==round(pos) % make sure we are not duplicating a node
    return
end
H = guidata(src);

obj1 = H.obj(iobj);
x = pins(:,1);
y = pins(:,2);
noden = floor(pos);
xn = obj1.x(noden:noden+1); % neigbouring nodes
yn = obj1.y(noden:noden+1);
zn = obj1.z(noden:noden+1);
z = zn(1) + (zn(2)-zn(1)) * ((x-xn(1))^2 + (y-yn(1))^2)^0.5 / ((xn(2)-xn(1))^2 + (yn(2)-yn(1))^2)^0.5;
code = obj1.code(noden);

[H.obj,undothis] = oNodeInsert(H.obj,iobj,noden+1,x,y,z,code,H.tbl);
H.undo = undo_register(H,undothis);

guidata(src,H)


function cbBreakSegment(src,evd) %#ok<INUSD>
H = guidata(src);

selnew = clear_selection(H.hsel,[],[],H.tbl);
[H.obj,undothis] = oBreak(H.obj,H.sel,H.tbl);
H.undo = undo_register(H,undothis);
H.sel = selnew;

guidata(src,H)


function cbConnectSegments(src,evd) %#ok<INUSD>
H = guidata(src);

selnew = clear_selection(H.hsel,[],[],H.tbl);
[H.obj,undothis] = oConnect(H.obj,H.sel,H.tbl);
if isstruct(undothis)
    H.undo = undo_register(H,undothis);
else
    errordlg(undothis,'Error connecting segments')
end
H.sel = selnew;

guidata(src,H)


function cbJoinAdjacent(src,evd) %#ok<INUSD>
H = guidata(src);

prompt = {'Sequence of thresholds to apply:'};
dlg_title = 'Diftance threshold';
rd = inputdlg(prompt,dlg_title,1,{'10:10:50'});
if isempty(rd) % if Cancel was pressed
    return
end
rd = eval(rd{1});

objn = unique(H.sel(:,1));
obj = H.obj(objn);

% [H.obj,undothis] = oConnect(H.obj,H.sel,H.tbl);
if oIsGeo(obj)
    coordtype = 'geo';
else
    coordtype = 'xy';
end

% break into segments for input to coast_joinseg
x = [obj.x];
y = [obj.y];
inan = [0 find(isnan(x))];
nr = length(inan)-1;
r(1:nr) = struct('x',[],'y',[]);
for ir = 1:nr
    inr = inan(ir)+1:inan(ir+1);
    r(ir).x = x(inr);
    r(ir).y = y(inr);
end

% join
[rland,risland,rdiscarded] = coast_joinseg(r,coordtype,rd);
xc = [[rland.x] [risland.x]];
yc = [[rland.y] [risland.y]];
if ~isempty(rdiscarded)
    ndis = sum(~isnan([rdiscarded.x]));
    warndlg(['Number of discarded nodes: ' num2str(ndis)],'Warning')
end

undo.action = 'cedit'; % generic object edit operation
undo.label = 'join adjacent segments';
undo.r.objn = objn;
undo.r.obj = obj;

% replace old object with the inside part of the polyline
H.obj(objn) = obj_update_data(obj,H.tbl,xc,yc);
%%%% old values of z(1),code(1),objn(1) are expanded to all nodes

% if isstruct(undo)
    H.undo = undo_register(H,undo);
% else
%     errordlg(undo,'Error joining segments')
% end

H.sel = clear_selection(H.hsel,[],[],H.tbl);

guidata(src,H)


function tf = oIsGeo(obj)
% Attempt to determine object coordinate type.
x = [obj.x];
y = [obj.y];
x = x(isnan(x));
y = y(isnan(y));
tf = all(x>-180 & x<360 & y>=-90 & y<=90);


function cbCloseSegment(src,evd) %#ok<INUSD>
H = guidata(src);

% % allow editing only one polyline object at a time
% if length(objn)~=1 || obj.type~=2
%     warndlg('Invalid node selection for closing a segment.','Warning')
%     return
% end

selnew = clear_selection(H.hsel,[],[],H.tbl);

objn = H.sel(1,1);
nodef = H.sel(:,2);
noden = H.sel(nodef==max(nodef),2);
nodeni = H.sel(nodef==min(nodef),2);
obj = H.obj(objn);
xi = obj.x(noden);
yi = obj.y(noden);
zi = obj.z(noden);
codei = obj.code(noden);
[H.obj,undothis] = oNodeInsert(H.obj,objn,nodeni,xi,yi,zi,codei,H.tbl);

if isstruct(undothis)
    H.undo = undo_register(H,undothis);
else
    errordlg(undothis,'Error closing the segment')
end
H.sel = selnew;

set(H.cmnodeitem.closetool,'Enable','off')

guidata(src,H)


function cbSegmentExtract(src,evd) %#ok<INUSD>
H = guidata(src);

selnew = clear_selection(H.hsel,[],[],H.tbl);
[H.obj,undothis] = oExtract(H.obj,H.sel,H.ah,H.tbl);
if isstruct(undothis)
    H.undo = undo_register(H,undothis);
else
    errordlg(undothis,'Error extracting segment')
end
H.sel = selnew;

guidata(src,H)


function cbCombine(src,evd) %#ok<INUSD>
H = guidata(src);

selnew = clear_selection(H.hsel,[],[],H.tbl);
[H.obj,undothis] = oCombine(H.obj,H.sel,H.tbl);
if isstruct(undothis)
    H.undo = undo_register(H,undothis);
else
    errordlg(undothis,'Error combining segments')
end
H.sel = selnew;

guidata(src,H)


function cbDuplicate(src,evd) %#ok<INUSD>
H = guidata(src);

selnew = clear_selection(H.hsel,[],[],H.tbl);
objn = H.sel(:,1);
objdup = H.obj(unique(objn));

% assign new object numbers beginnig at 1 to place new objects on top
% maxn = nanmax([H.obj.objn]);
for id = 1:length(objdup)
    objdup(id).objn = repmat(id,size(objdup(id).objn));
end

[H.obj,undothis] = obj_load(H.obj,objdup,H.ah,[],H.tbl);

undothis.label = 'duplicate object';
H.undo = undo_register(H,undothis);

H.sel = selnew;

if length(objdup)==1
    set(H.statstr,'String',['Duplicated object created: ' obj_string(objdup)])
else
    set(H.statstr,'String',[num2str(length(objdup)) ' objects duplicated'])
end

guidata(src,H)


function cbManEdit(src,~)
H = guidata(src);

objn = unique(H.sel(:,1));
[isman,conn] = obj_manedit(H.obj(objn));
if all(~isman) % all not in manual editing mode
    n = length(objn);
    if n==1
        statstr = ['Object in manual edit mode: ' obj_string(H.obj(objn))];
    else
        statstr = [num2str(n) ' objects are set to manual edit mode'];
    end
    
    [H.obj,undothis] = obj_manedit_set(H.obj,objn,H.ah,H.tbl);
    
    H.undo = undo_register(H,undothis);
    guidata(src,H)
    
    set(H.statstr,'String',statstr)

elseif all(isman) % all in manual editing mode
    % include connected objects
    add = ~ismember(conn,objn);
    objn = [objn; conn(add)];
    isman = [isman 3-isman(add)]; 
    % for isman==1 connected isman->2
    % for isman==2 connected isman->1
    
    [H.obj,undothis] = obj_manedit_stop(H.obj,objn,isman,H.tbl);
    H.undo = undo_register(H,undothis);
    guidata(src,H)
    
    set(H.statstr,'String',[num2str(length(objn)) ' object(s) are set to normal edit mode'])
end


function [obj,ud] = obj_manedit_set(obj,objn,ah,htbl)

n = length(objn);
objdup = obj(objn);

% "confirmed" objects folow the "original" ones in the object list
for id = 1:n
    objdup(id).x(:,:) = NaN;
    objdup(id).y(:,:) = NaN;
    objdup(id).z(:,:) = NaN;
    objdup(id).objn(:,:) = objn(id)+1;
    c = objdup(id).pp.color;
    objdup(id).pp.color = c + ([1 1 1] - c)/2; % just fade the original color
    objdup(id).meta.obj_orig = objn(id);
    obj(objn(id)).meta.obj_confirmed = objn(id)+1;
    obj_update_data(obj(objn(id)),htbl); % update table
end

[obj,undoload] = obj_load(obj,objdup,ah,[],htbl);

ud.action = 'obj_manedit_set';
ud.label = 'set manual edit mode';
ud.r.objn = objn;
ud.r.undoload = undoload;


function [obj,ud] = obj_manedit_stop(obj,objn,isman,htbl)

n = length(objn);

% set to normal editing mode by removing special field in meta
conn = NaN(n,1); % record original state for undo
for k = 1:n
    if isman(k)==2 % "confirmed" object
        conn(k) = obj(objn(k)).meta.obj_orig;
        obj(objn(k)).meta = rmfield(obj(objn(k)).meta,'obj_orig');
    elseif isman(k)==1 % "original" object
        conn(k) = obj(objn(k)).meta.obj_confirmed;
        obj(objn(k)).meta = rmfield(obj(objn(k)).meta,'obj_confirmed');
    end
    obj_update_data(obj(objn(k)),htbl); % update table
end

% remove NaNs
objs = obj(objn);
inv = isnan([objs.x]) | isnan([objs.z]);
objnn = [objs.objn];
noden = [objs.noden];
[obj,undonan] = oNodeDelete(obj,objnn(inv),noden(inv),htbl);

ud.action = 'obj_manedit_stop';
ud.label = 'stop manual edit mode';
ud.r.objn = objn;
ud.r.isman = isman;
ud.r.conn = conn;
ud.r.undonan = undonan;


function cbConvert2Nodes(src,evd) %#ok<INUSD>
H = guidata(src);

selnew = clear_selection(H.hsel,[],[],H.tbl);
objn = H.sel(:,1);
objnu = unique(objn);
objdup = H.obj(objnu);

% assign new object numbers beginnig at 1 to place new objects on top
for id = 1:length(objdup)
%     objdup(id).objn = repmat(id,size(objdup(id).objn));
    if objdup(id).type == 2 % polylines
        o = objdup(id);
        
        lrem = isnan(o.x); % terminating NaNs
        
        % remove duplicate nodes
        node = [o.x' o.y'];
        [~, m] = unique(node,'rows');
        nn = size(node,1);
        idup = setdiff(1:nn, m); % duplicate nodes
        lrem(idup) = true;
        
        o.x(lrem) = [];
        o.y(lrem) = [];
        o.z(lrem) = [];
        o.code(lrem) = [];
        o.objn(lrem) = [];

        o.noden = (1:length(o.x)); % re-number nodes
        
        objdup(id) = o;
        
    else % objdup(id).type == 3
        objdup(id).tri = [];
    end
    objdup(id).type = 1;
end

%%%TODO Would be better to replace the object than to place a new one on top
[H.obj,undodelete] = oDelete(H.obj,objnu,H.tbl);
[H.obj,undoload] = obj_load(H.obj,objdup,H.ah,[],H.tbl);
% H.obj(objnu) = obj_update_data(objdup);

undo.action = 'replace';
undo.label = 'convert to node set';
undo.r.undodelete = undodelete;
undo.r.undoload = undoload;
H.undo = undo_register(H,undo);

H.sel = selnew;

if length(objdup)==1
    set(H.statstr,'String',['Converted object: ' obj_string(objdup)])
else
    set(H.statstr,'String',[num2str(length(objdup)) ' objects converted to node sets'])
end

guidata(src,H)


function cbConvert2Poly(src,evd) %#ok<INUSD>
H = guidata(src);

selnew = clear_selection(H.hsel,[],[],H.tbl);
objn = H.sel(:,1);
objnu = unique(objn);
o = H.obj(objnu);

% allow editing only one node set object at a time
if length(objnu)~=1 || o.type~=1
    warndlg('Operation is allowed for only one node set object at a time.','Warning')
    return
end

% assign new object the number 1 to place it on top
%     objdup(id).objn = repmat(id,size(o.objn));

%%%TODO Let the user pick the start node
nodesel = H.sel(:,2);
istart = nodesel(1);
[o.x,o.y,isort] = node2poly(o.x,o.y,istart);
o.z = o.z(isort);
o.code = o.code(isort);
o.type = 2; % polyline


% add the terminating NaN
o.x(end+1) = NaN;
o.y(end+1) = NaN;
o.z(end+1) = NaN;
o.code(end+1) = NaN;
o.objn(end+1) = o.objn(1);
o.noden = (1:length(o.x)); % re-number nodes

%%%TODO Would be better to replace the object than to place a new one on top
[H.obj,undodelete] = oDelete(H.obj,objnu,H.tbl);
[H.obj,undoload] = obj_load(H.obj,o,H.ah,[],H.tbl);
% H.obj(objnu) = obj_update_data(objdup);

undo.action = 'replace';
undo.label = 'convert node set to polyline';
undo.r.undodelete = undodelete;
undo.r.undoload = undoload;
H.undo = undo_register(H,undo);

H.sel = selnew;

set(H.statstr,'String',['Converted object: ' obj_string(o)])

guidata(src,H)


function cbDeleteSegment(src,evd) %#ok<INUSD>
H = guidata(src);

selnew = clear_selection(H.hsel,[],[],H.tbl);
[H.obj,undothis] = oDeleteSegment(H.obj,H.sel,H.tbl);
if isstruct(undothis)
    H.undo = undo_register(H,undothis);
else
    errordlg(undothis,'Error deleting segment')
end
H.sel = selnew;

guidata(src,H)


function cbPolyClip(src,evd) %#ok<INUSD>
H = guidata(src);

objn_slash = pickobj(H.hfm,2); % pick polyline (Type 2 object) to clip with
if isempty(objn_slash)
    warndlg('No ploygon was selected, cancelling operation.','')
    return
end
oslash = H.obj(objn_slash);

objn = unique(H.sel(:,1));
obj = H.obj(objn);
[xc,yc] = poly_clip(obj.x, obj.y, oslash.x, oslash.y);

undo.action = 'cedit'; % generic object edit operation
undo.label = 'clip by polygon';
undo.r.objn = objn;
undo.r.obj = obj;

% replace old object with the inside part of the polyline
H.obj(objn) = obj_update_data(obj,H.tbl,xc',yc');
%%%% old values of z(1),code(1),objn(1) are expanded to all nodes

% if isstruct(undo)
    H.undo = undo_register(H,undo);
% else
%     errordlg(undo,'Error clipping by polygon')
% end
H.sel = clear_selection(H.hsel,[],[],H.tbl);

guidata(src,H)


function cbDeleteObject(src,evd) %#ok<INUSD>
H = guidata(src);

selnew = clear_selection(H.hsel,[],[],H.tbl);

objnu = unique(H.sel(:,1))'; % numbers of objects selected, sorted

[H.obj,undothis] = oDelete(H.obj,objnu,H.tbl);
H.undo = undo_register(H,undothis);

H.sel = selnew;

set(H.cmnodeitem.deletesegtool,'Enable','off');

guidata(src,H)


function cbBathyNew(src,evd) %#ok<INUSD>
H = guidata(src);

if strcmp(get(H.tools.freenode,'State'),'off')
    % turn on the freenode tool
    set(H.tools.freenode,'State','on')
    toolFreenode(H.tools.freenode,[])
else
    [H.obj,H.undo] = freenode_finalize(H);
end



function cbBathyDecimate(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow editing only one bathymetry object at a time
if length(objn)~=1 || obj.type~=1
    warndlg('Decimating is allowed for only one bathymetry object at a time.','Warning')
    return
end

% % get threshold from user
% prompt = {'Radius of decimation:'};
% dlg_title = 'Setting the parameter';
% rd = inputdlg(prompt,dlg_title,1,{'10'});
% if isempty(rd) % if Cancel was pressed
%     return
% end
% rd = str2double(rd{1});
% 
% tic %%%%
% 
% % pause(0.001) % make interruption in Matlab command window possible
% hwait = waitbar(0,'Decimating bathymetry. 0% done. ETA: ','CreateCancelBtn','delete(gcbf)');
% 
% % decimate nodes
% ndel = bathy_decimate(obj.x, obj.y, rd, hwait);
% toc %%%%
% 
% if ishandle(hwait) % if Cancel wasn't pressed
%     close(hwait)
% else
%     return
% end
% 
% odel = repmat(objn,size(ndel));
% [H.obj,ud] = oNodeDelete(H.obj,odel,ndel,H.tbl);
% H.undo = undo_register(H,ud);


% % Version with filtering
% % For decimation only, faster than bathy_decimate by about 30%.
% %-----------------------
% % get parameters from user
% prompt = {'Median filtering radius:','Radius of decimation:'};
% dlg_title = 'Setting the parameter';
% ui = inputdlg(prompt,dlg_title,1,{'0','10'});
% if isempty(ui) % if Cancel was pressed
%     return
% end
% rf = str2double(ui{1});
% rd = str2double(ui{2});
% utmzone = []; %%% no conversion
% 
% % tic %%%%
% %NOTE: FIR filtering is not implemented correctly, use median filtering.
% [obj.x, obj.y, obj.z, ndel] = bathy_filter(obj.x, obj.y, obj.z, rd,rf, utmzone);
% % toc %%%%


% Version with median filtering
% get parameters from user
prompt = {'Radius of decimation:','Apply median filtering (1-yes, 0-no):'};
dlg_title = 'Decimation parameters';
ui = inputdlg(prompt,dlg_title,1,{'10','0'});
if isempty(ui) % if Cancel was pressed
    return
end
rd = str2double(ui{1});
filterflag = logical(str2double(ui{2}));
[obj.x, obj.y, obj.z, ndel] = bathy_decimate_median(obj.x, obj.y, obj.z, rd, filterflag);


if isnan(ndel) % if Cancel was pressed
    return
end

obj.code(ndel) = [];
H.obj(objn) = obj_update_data(obj,H.tbl);
undo.action = 'cedit'; % generic object edit operation
undo.label = 'filter and decimate';
undo.r.objn = objn;
undo.r.obj = obj;
H.undo = undo_register(H,undo);

H.sel = clear_selection(H.hsel,[],[],H.tbl);

guidata(src,H)


function cbBathyTrim(src,~,deletein)
H = guidata(src);

objn_slash = pickobj(H.hfm,2); % pick polyline (Type 2 object) for slashing the bathymetry
if isempty(objn_slash)
    warndlg('No ploygon was selected, cancelling operation.','')
    return
end
oslash = H.obj(objn_slash);
[node,cnect] = prepnodes_poly([[oslash.x]' [oslash.y]']);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);
inv = inpoly([[obj.x]' [obj.y]'],node,cnect);
if ~deletein
    inv = ~inv;
end

odel = [obj.objn];
ndel = [obj.noden];
odel = odel(inv);
ndel = ndel(inv);

[H.obj,ud] = oNodeDelete(H.obj,odel,ndel,H.tbl);
H.undo = undo_register(H,ud);

guidata(src,H)


function cbBathyDominate(src,evd) %#ok<INUSD>
H = guidata(src);

odom = unique(H.sel(:,1))';
objdom = H.obj(odom);

% allow editing only one bathymetry object at a time
if length(odom)~=1 || objdom.type~=1
    warndlg('Operation is allowed for only one bathymetry object at a time.','Warning')
    return
end

% get threshold from user
prompt = {'Suppression radius:'};
dlg_title = 'Setting the parameter';
rd = inputdlg(prompt,dlg_title,1,{'10'});
if isempty(rd) % if Cancel was pressed
    return
end
rd = str2double(rd{1});

% suppress unlocked objects only
objnfree = intersect(find([H.obj.type]==1), obj_isfree(H.tbl)); % unlocked node sets

% suppress competing nodes
osup = setdiff(objnfree,odom); % indices of bathymetry objects to suppress
[odel,ndel] = bathy_dominate(objdom, H.obj(osup), rd);

if ~isnan(odel) % Cancel wasn't pressed
    [H.obj,ud] = oNodeDelete(H.obj,odel,ndel,H.tbl);
    H.undo = undo_register(H,ud);
end

guidata(src,H)


function cbPolyDominate(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
objdom = H.obj(objn);

% % allow editing only one bathymetry object at a time
% if length(objn)~=1 || objdom.type~=1
%     warndlg('Operation is allowed for only one bathymetry object at a time.','Warning')
%     return
% end

% get threshold from user
prompt = {'Suppression radius:'};
dlg_title = 'Setting the parameter';
rd = inputdlg(prompt,dlg_title,1,{'10'});
if isempty(rd) % if Cancel was pressed
    return
end
rd = str2double(rd{1});

% hwait = waitbar(0,{'Removing competing bathymetry nodes.','0% done. ETA: '},'CreateCancelBtn','delete(gcbf)');

% decimate nodes
osup = setdiff(find([H.obj.type]==1),objn); % indices of bathymetry objects to suppress

obj = H.obj(osup);
p = [[obj.x]' [obj.y]']; % nodes to test
[node,edge] = prepnodes_poly([[objdom.x]' [objdom.y]']);
edgexy = [node(edge(:,1),:), node(edge(:,2),:)]; % polygon edges [x1 y1 x2 y2]
% edgexy = [[objdom.x]' [objdom.y]']; % for use by distance2curve

np = size(p,1); % number of nodes to test
ldel = false(p,1); % preallocate

chunklen = 1000; % process in chunks of this size
nchunk = ceil(np/chunklen);

hwait = waitbar(0,{'Removing competing bathymetry nodes.','0% done. ETA: '},'CreateCancelBtn','delete(gcbf)');
tst = now;
for ic = 1:nchunk
    k = (1:chunklen) + (ic-1)*chunklen; % indices for the current chunk
    k(k>np) = []; % clip the last chunk indices
    d = dist2poly(p(k,:),edgexy,rd); % way faster than distance2curve
%     [~,d] = distance2curve(edgexy,p(k,:));
    ldel(k) = d<rd;
    
    if ~ishandle(hwait) % waitbar Cancel pressed
        return
    end
    pdone = ic/nchunk;
    eta = tst + (now-tst)/pdone;
    ustr = {'Removing competing bathymetry nodes.',...
        [num2str(pdone*100) '% done. ETA: ' datestr(eta)]};
    waitbar(pdone,hwait,ustr)
end

if ~ishandle(hwait) % waitbar Cancel was pressed
    return
end
close(hwait)

odel = [obj.objn];
ndel = [obj.noden];
odel = odel(ldel);
ndel = ndel(ldel);

[H.obj,ud] = oNodeDelete(H.obj,odel,ndel,H.tbl);
H.undo = undo_register(H,ud);

guidata(src,H)


function cbFathom2M(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';

[obj,err] = obj_fathom2m(H.obj(objn));

if ~isempty(err) % check for valid range and number of decimal places
    
    % make erroneous nodes selected
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
    H.sel = oNodeSelect(H.hsel,H.sel,err(:,3),err(:,4),err(:,1),err(:,2),H.tbl);

    errordlg('Some z-values are not consistent with fathom-feet notation (see the selected nodes).',...
        'Erroneous z-values')
else
    for io = 1:length(obj)
        [H.obj(objn(io)),err] = obj_update_data(obj(io),[]);
        if err
            disp('cbFathom2M: Problem updating an object.') %%% output to Command Window for now
        end
    end
    undo.action = 'fathom2m';
    undo.label = 'fathoms to metres';
    undo.r.objn = objn;
    H.undo = undo_register(H,undo);
end

guidata(src,H)


function [obj,err] = obj_fathom2m(obj)
% Convert z-values of objects from fathoms and feet to metres.
% Whole part is considered fathoms and decimal part feet.
% Feet should not be > 5 and should be whole numbers, i.e. one decimal place.

err = [];
for io = 1:length(obj)
    z = obj(io).z;
    
    ftm = fix(z); % round toward zero
    ft = (z-ftm)*10;
    
    noderr = find(ft>5 | abs(ft-round(ft))>eps*1e7);
    
    if ~isempty(noderr) % check for valid range and number of decimal places
        % append erroneous nodes to err array
        err = [err; obj.objn(noderr)' noderr' obj.x(noderr)' obj.y(noderr)']; %#ok<AGROW>
        continue
    end
    
    % conversion
    ftm2m = 1.8288;
    z = ftm*ftm2m + ft*ftm2m/6; % 6 feet per fathom
    
    obj(io).z = z;
end


function cbM2Fathom(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';

obj = obj_m2fathom(H.obj(objn));

for io = 1:length(obj)
    [H.obj(objn(io)),err] = obj_update_data(obj(io),[]);
    if err
        disp('cbM2Fathom: Problem updating an object.') %%% output to Command Window for now
    end
end
undo.action = 'm2fathom';
undo.label = 'metres to fathoms';
undo.r.objn = objn;
H.undo = undo_register(H,undo);

guidata(src,H)


function obj = obj_m2fathom(obj)
% Convert z-values of objects from metres to fathoms.
% Fathom notation: Whole part is fathoms and decimal partis feet.
% Feet are rounded to the neares whole number (1 to 5).

for io = 1:length(obj)
    z = obj(io).z;
    
    % conversion
    ftm2m = 1.8288;
    z = z/ftm2m; % decimal fathoms
    
    ftm = fix(z); % round toward zero
    ft = round((z-ftm)*6); % whole feet; 6 feet per fathom
    
    % wrap 6 ft to the next fathom
    iwrap = ft==6;
    ftm(iwrap) = ftm(iwrap) + 1;
    ft(iwrap) = 0;
    
    z = ftm + ft/10; % make feet the decimal part
    
    obj(io).z = z;
end


function cbCleaveNode(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow editing only one grid object at a time (multiple nodes are allowed)
if length(objn)~=1 || obj.type~=3
    warndlg('Cleave is allowed for only one triangular grid object at a time.','Warning')
    return
end

noden = H.sel(:,2);

[p,tri,z,code,perr] = grid_cleave_n([obj.x' obj.y'],obj.tri,noden,obj.z,obj.code);

if size(perr,1) ~= length(noden) % if changes have been made
    undothis.action = 'gridedit';
    undothis.label = 'cleave node';
    undothis.r = obj; %%%%% saving the entire grid object - this can inflate undo structure considerably;
                           %%%%% if cleaving only one node at a time is allowed,
                           %%%%% then maybe we can save only changes to the grid???
    H.undo = undo_register(H,undothis);
    
    obj = obj_update_data(obj,H.tbl,p(:,1)',p(:,2)',objn,z,code,tri);
    H.obj(objn) = obj;
    
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
    if ~isempty(perr)
        warndlg('The nodes which remain selected can not be cleaved.','Warning');

        % make unsuccessfull nodes selected
%         noderr = intersect(p,perr,'rows');
%         for in = 1:length(noderr)
%             h = plot(H.ah,obj.x(noderr(in)),obj.y(noderr(in)),'s','Color',H.selcolor,'MarkerSize',8,'HitTest','off');
%             H.sel(in,1:3) = [h objn noderr(in)];
%         end
        noderr = find(ismember(p,perr,'rows'));
        H.sel = oNodeSelect(H.hsel,H.sel,obj.x(noderr),obj.y(noderr),repmat(objn,size(noderr)),noderr,H.tbl);
    end
else
    warndlg('The selected nodes can not be cleaved.','Warning');
end

guidata(src,H)


function cbCleaveAuto(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow editing only one grid object at a time
if length(objn)~=1 || obj.type~=3
    warndlg('Automatic cleave is allowed for only one triangular grid object at a time.','Warning')
    return
end

% get threshold from user
prompt = {'Cleave nodes with number of neigbours more than or equal to:'};
dlg_title = 'Automatic cleave threshold';
nthres = inputdlg(prompt,dlg_title,1,{'8'}); % default threshold is 8 neighbours
if isempty(nthres) % if Cancel was pressed
    return
end
nthres = str2double(nthres{1});

[p,tri,z,code,perr] = grid_cleave_nthres([obj.x' obj.y'],obj.tri,nthres,obj.z,obj.code);

if size(p,1)~=length(obj.x) % if changes have been made (cleaving always increases the number of nodes)
    undothis.action = 'gridedit';
    undothis.label = 'automatic cleave';
    undothis.r = obj; %%%%% saving the entire grid object - this can inflate undo structure considerably
    H.undo = undo_register(H,undothis);
    
    obj = obj_update_data(obj,H.tbl,p(:,1)',p(:,2)',objn,z,code,tri);
    H.obj(objn) = obj;
    
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
    if ~isempty(perr)
        warndlg('The nodes which remain selected can not be cleaved.','Warning');

        % make unsuccessfull nodes selected
%         noderr = intersect(p,perr,'rows');
%         for in = 1:length(noderr)
%             h = plot(H.ah,obj.x(noderr(in)),obj.y(noderr(in)),'s','Color',H.selcolor,'MarkerSize',8,'HitTest','off');
%             H.sel(in,1:3) = [h objn noderr(in)];
%         end
        noderr = find(ismember(p,perr,'rows'));
        H.sel = oNodeSelect(H.hsel,H.sel,obj.x(noderr),obj.y(noderr),repmat(objn,size(noderr)),noderr,H.tbl);
    end
else
    warndlg('No nodes to cleave.','Warning');
end

guidata(src,H)


function cbGridRefine(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow editing only one grid object at a time (multiple nodes are allowed)
if length(objn)~=1 || obj.type~=3
    warndlg('Refine is allowed for only one object at a time.','Warning')
    return
end

try
    tri = obj.tri;
    [p,trir,z,code] = grid_refine([obj.x' obj.y'],tri,[],obj.z',obj.code');
    H.obj(objn) = obj_update_data(obj,H.tbl,p(:,1),p(:,2),objn,z,code,trir);
    
    undothis.action = 'gridedit';
    undothis.label = 'refine grid';
    undothis.r = obj; %%%%% saving the entire grid object
    undothis.r.ro = [];
    H.undo = undo_register(H,undothis);
    
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
    
    guidata(src,H)
catch err
    errordlg({'refine.m failed.',err.message});
end


function cbGridRefineSel(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);
noden = H.sel(:,2);

% allow editing only one grid object at a time (multiple nodes are allowed)
if length(objn)~=1 || obj.type~=3
    warndlg('Refine is allowed for only one object at a time.','Warning')
    return
end

try
    tri = obj.tri;
    lt = all(ismember(tri,noden),2); % triangles with ALL nodes selected
    [p,trir,z,code] = grid_refine([obj.x' obj.y'],tri,lt,obj.z',obj.code');
    H.obj(objn) = obj_update_data(obj,H.tbl,p(:,1),p(:,2),objn,z,code,trir);
    
    undothis.action = 'gridedit';
    undothis.label = 'refine triangles';
    undothis.r = obj; %%%%% saving the entire grid object
    undothis.r.ro = [];
    H.undo = undo_register(H,undothis);
    
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
    
    guidata(src,H)
catch err
    errordlg({'refine.m failed.',err.message});
end


function cbGridDeleteAndFix(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);
noden = H.sel(:,2);

% allow editing only one grid object at a time (multiple nodes are allowed)
if length(objn)~=1 || length(noden)~=1 || obj.type~=3
    warndlg('Delete-and-fix is allowed for only one node at a time.','Warning')
    return
end

try
    [x,y,tri,z,code] = grid_removenodefixhole(obj.x,obj.y,obj.tri,noden,obj.z,obj.code);
    
    H.obj(objn) = obj_update_data(obj,H.tbl,x,y,objn,z,code,tri);
    
    undothis.action = 'gridedit';
    undothis.label = 'delete node and fix hole';
    undothis.r = obj; %%%%% saving the entire grid object
    undothis.r.ro = [];
    H.undo = undo_register(H,undothis);
    
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
    
    guidata(src,H)
catch err
    warndlg({'cbGridDeleteAndFix failed.',err.message});
end


function cbGridDeleteTriangle(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);
noden = H.sel(:,2);

% allow editing only one grid object at a time (multiple nodes are allowed)
if length(objn)~=1 || length(noden)<3 || obj.type~=3
    warndlg('Deleting triangle is allowed for only one object at a time.','Warning')
    return
end

x = obj.x(:);
y = obj.y(:);
try
    po = [x y]; % original set of nodes
    [p,tri,z,code,err] = grid_removetriangle(noden,po,obj.tri,obj.z,obj.code);
    
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
    
    if err
        H.sel = oNodeSelect(H.hsel,H.sel,x(err),y(err),...
            repmat(objn,size(err)),err); % make branching boundary nodes selected
        warndlg({'Boundary became branching at the following nodes (selected):',num2str(err(:)')});
    end
%         warndlg({'Operation cancelled.','Boundary became branching at the selected nodes.'});
%     else
        undothis.action = 'gridedit';
        undothis.label = 'delete triangle';
        undothis.r = obj; %%%%% saving the entire grid object
        if isempty(p)
            [H.obj,undothis.r.ro] = oDelete(H.obj,objn,H.htbl); % delete empty objects
        else
            H.obj(objn) = obj_update_data(obj,H.tbl,p(:,1)',p(:,2)',objn,z',code',tri);
            undothis.r.ro = [];
        end
        H.undo = undo_register(H,undothis);
%     end

    guidata(src,H)
catch err2
    warndlg({'cbGridDeleteTriangle failed.',err2});
end


function cbDekiteNode(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);
noden = H.sel(:,2);

% allow editing only one grid object at a time (multiple nodes are allowed)
if length(objn)~=1 || length(noden)~=1 || obj.type~=3
    warndlg('De-kite is allowed for only one node at a time. You may use De-kite auto for automatic de-kiting.','Warning') % triangular grid object
    return
end

bnd = unique(grid_edge(obj.tri)); % boundary nodes
if ismember(noden,bnd)
    warndlg('De-kite is allowed for interior nodes only.','Warning')
    return
end

[p,tri,err] = grid_dekite_one([obj.x' obj.y'],obj.tri,noden,bnd);

if ~err % if successful
    undothis.action = 'gridedit';
    undothis.label = 'de-kite node';
    undothis.r = obj; %%%%% saving the entire grid object - this can inflate undo structure considerably
                  %%%%% can we save only the changes???
    undothis.r.ro = [];
    H.undo = undo_register(H,undothis);

    % update z and code arrays
    z = obj.z;
    code = obj.code;
    z(noden) = [];
    code(noden) = [];

    H.obj(objn) = obj_update_data(obj,H.tbl,p(:,1)',p(:,2)',objn,z,code,tri);
    
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
else
    warndlg(err);
end

guidata(src,H)


function cbDekiteAuto(src,evd,nobridgesflag) %#ok<INUSL>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow editing only one grid object at a time (multiple nodes are allowed)
if length(objn)~=1 || obj.type~=3
    warndlg('De-kite is allowed for only one triangular grid object at a time.','Warning')
    return
end

po = [obj.x' obj.y']; % keep a copy of original nodes as a reference for z,code adjustment
[p,tri,perr] = grid_dekite_tri(po, obj.tri, nobridgesflag);

if size(p,1) ~= length(obj.x) % de-kite always reduces the number of nodes
    
    % update z,code arrays
    [~,irem] = setdiff(po,p,'rows');
    z = obj.z;
    code = obj.code;
    z(irem) = [];
    code(irem) = [];
    if length(z)~=size(p,1)
        warndlg('Inconsistency in field lengths after automatic de-kite operation. No changes have been made.','Warning')
        return
    end
    
    undothis.action = 'gridedit';
    undothis.label = 'automatic de-kite';
    undothis.r = obj; %%%%% saving the entire grid object - this can inflate undo structure considerably
    H.undo = undo_register(H,undothis);

    obj = obj_update_data(obj,H.tbl,p(:,1)',p(:,2)',objn,z,code,tri);
    H.obj(objn) = obj;
    
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
    if ~isempty(perr)
        warndlg('De-kite failed for the nodes which remain selected.','Warning');

        % make unsuccessfull nodes selected
%         noderr = intersect(p,perr,'rows');
%         for in = 1:length(noderr)
%             h = plot(H.ah,obj.x(noderr(in)),obj.y(noderr(in)),'s','Color',H.selcolor,'MarkerSize',8,'HitTest','off');
%             H.sel(in,1:3) = [h objn noderr(in)];
%         end
        noderr = find(ismember(p,perr,'rows'));
        H.sel = oNodeSelect(H.hsel,H.sel,obj.x(noderr),obj.y(noderr),repmat(objn,size(noderr)),noderr,H.tbl);
    end
else
    warndlg('The selected nodes can not be de-kited.','Warning');
end

guidata(src,H)


function cbGridCollapseEdge(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);
noden = sort(H.sel(:,2)); % must be sorted

% allow editing only one edge at a time (two nodes must be selected) 
%%%%% implement with edge selection for grids
if length(objn)~=1 || length(noden)~=2 || obj.type~=3 % triangular grid object
    warndlg('Edge collapse is allowed for only one edge at a time.','Warning')
    return
end

[ebnd,eint] = grid_edge(obj.tri); % internal edges
e = [ebnd; eint];
if ~ismember(noden',e,'rows')
    warndlg({'Operation cancelled:',...
        'Edge must be selected as a pair of nodes belonging to the same edge.'},'Warning')
    return
end

[obj.x,obj.y,obj.tri,obj.z,obj.code] = ...
    grid_collapseedge(obj.x,obj.y,obj.tri,noden,obj.z,obj.code);

% if ~err % if successful
    undothis.action = 'gridedit';
    undothis.label = 'collapse edge';
    undothis.r = obj; %%%%% saving the entire grid object - this can inflate undo structure considerably;
                       %%%%% if cleaving only one node at a time is allowed,
                       %%%%% then maybe we can save only changes to the
                       %%%%% grid???
    undothis.r.ro = [];
    H.undo = undo_register(H,undothis);

    H.obj(objn) = obj_update_data(obj,H.tbl);
    
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
% else
%     warndlg(err,'Warning');
% end

guidata(src,H)


function cbGridSplitEdge(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);
noden = sort(H.sel(:,2)); % must be sorted

% allow editing only one edge at a time (two nodes must be selected) 
%%%%% implement with edge selection for grids
if length(objn)~=1 || length(noden)~=2 || obj.type~=3 % triangular grid object
    warndlg('Edge split is allowed for only one edge at a time.','Warning')
    return
end

[p,tri,z,code] = grid_edge_split([obj.x' obj.y'],obj.tri,noden,obj.z',obj.code');

% if ~warn % if successful
    H.sel = clear_selection(H.hsel,[],[],H.tbl);

    undothis.action = 'gridedit';
    undothis.label = 'split edge';
    undothis.r = obj; %%%%% saving the entire grid object - this can inflate undo structure considerably;
                       %%%%% if cleaving only one node at a time is allowed,
                       %%%%% then maybe we can save only changes to the
                       %%%%% grid???
    undothis.r.ro = [];
    H.undo = undo_register(H,undothis);

    H.obj(objn) = obj_update_data(obj,H.tbl,p(:,1),p(:,2),objn,z,code,tri);
% else
%     warndlg(warn,'Error');
% end

guidata(src,H)


function cbExchangeEdge(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);
noden = sort(H.sel(:,2)); % must be sorted

% allow editing only one edge at a time (two nodes must be selected) 
%%%%% implement with edge selection for grids
if length(objn)~=1 || length(noden)~=2 || obj.type~=3 % triangular grid object
    warndlg('Edge exchange is allowed for only one edge at a time.','Warning')
    return
end

[~,eint] = grid_edge(obj.tri); % internal edges
if ~ismember(noden',eint,'rows')
    warndlg('Edge exchange is allowed for interior edges only. No changes have been made.','Warning')
    return
end

[tri,err,lt,tex] = grid_edge_exchange(obj.tri, noden);

if ~err % if successful
    undothis.action = 'gridedgeexchange';
    undothis.label = 'exchange edge';
    undothis.r.objn = objn;
    undothis.r.lt = lt;
    undothis.r.tex = tex;
    H.undo = undo_register(H,undothis);

    H.obj(objn) = obj_update_data(obj,[],[],[],[],[],[],tri);
    
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
else
    warndlg(err,'Warning');
end

guidata(src,H)


function cbExchangeEdgeAuto(src,evd) %#ok<INUSD>
% Exchange edge for non-EQL triangles according to set EQL criterion in
% "object appearance".

H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);
eql = obj.pp.eqlthres;

[tri,itex,torig] = grid_edge_exchange_auto(obj.tri,obj.x,obj.y, eql);

undothis.action = 'gridtrisub'; % substitution of triangles
undothis.label = 'exchange edge auto';
undothis.r.objn = objn;
undothis.r.itex = itex;
undothis.r.torig = torig;
H.undo = undo_register(H,undothis);

H.obj(objn) = obj_update_data(obj,[],[],[],[],[],[],tri);
% H.sel = clear_selection(H.hsel,[],[],H.tbl);

guidata(src,H)


function cbGridBoundarySelect(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow working with only one grid object at a time
if length(objn)~=1 || obj.type~=3 % triangular grid object
    warndlg('Extracting boundary is allowed for only one object at a time.','Warning')
    return
end

H.sel = clear_selection(H.hsel,[],[],H.tbl);

bn = grid_boundarynodes(obj.tri);

% make boundary nodes selected
H.sel = oNodeSelect(H.hsel,H.sel,obj.x(bn),obj.y(bn),repmat(objn,size(bn)),bn,H.tbl);
        
guidata(src,H)


function cbGridBoundarySelectLoop(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);
noden = H.sel(:,2); % first selected node

% allow working with only one grid object at a time
if length(objn)~=1 || obj.type~=3 % triangular grid object
    warndlg('Extracting boundary is allowed for only one object at a time.','Warning')
    return
end

set(H.hfm,'Pointer','watch');drawnow
ebnd = grid_edge(obj.tri);
[loop,errn,wrn] = edge_extract_loop(ebnd,noden(1));
set(H.hfm,'Pointer','arrow')
if errn
    errordlg({'Operation failed:','Boundary branches (see the selected node).'},'Error');
    % make branching node selected
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
    H.sel = oNodeSelect(H.hsel,H.sel,obj.x(errn),obj.y(errn),repmat(objn,size(errn)),errn,H.tbl);
elseif any(~ismember(noden,loop)) % some of the selected nodes are not part of this loop
    errordlg({'Operation failed:','The selected nodes belong to different loops.'},'Error');
else    
    % make loop nodes selected
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
    H.sel = oNodeSelect(H.hsel,H.sel,obj.x(loop),obj.y(loop),repmat(objn,size(loop)),loop,H.tbl);
    if wrn
        warndlg(wrn,'Warning')
    end
end
guidata(src,H)


function cbGridSelectCode(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow working with only one grid object at a time
if length(objn)~=1 || obj.type~=3 % triangular grid object
    warndlg('The operation is allowed for only one object at a time.','Warning')
    return
end

codeu = unique(obj.code);
[ok,sel] = dlgCodeSelect(codeu);
if ~ok || isempty(sel)
    return
end

nsel = find(ismember(obj.code,codeu(sel)));

H.sel = clear_selection(H.hsel,[],[],H.tbl);

% make boundary nodes selected
H.sel = oNodeSelect(H.hsel,H.sel,obj.x(nsel),obj.y(nsel),repmat(objn,size(nsel)),nsel,H.tbl);
        
guidata(src,H)


function cbGridSelectNNeighbours(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow working with only one grid object at a time
if length(objn)~=1 || obj.type~=3 % triangular grid object
    warndlg('The operation is allowed for only one object at a time.','Warning')
    return
end

%%%% create a custom input dialog with checkmark to include/exclude
%%%% boundary nodes
dlg_title = 'Selection criteria';
prompt = {'Number of neigbours (the selection will exclude boundary nodes):'};
num_lines = 1;
def = {'4'};

response = inputdlg(prompt,dlg_title,num_lines,def);
if isempty(response) % if Cancel was pressed
    return
end
n = str2double(response{1});

[ebnd,eint] = grid_edge(obj.tri);
b = unique(ebnd); % boundary nodes
eu = [ebnd; eint];
nodenum = unique(eu); % unique node numbers in t
nn = histc(eu(:),nodenum); % number of neighbours (number of edges) for each node
nsel = nodenum(nn==n); % nodes to remove
nsel = setdiff(nsel,b); % exclude boundary nodes %%%%

H.sel = clear_selection(H.hsel,[],[],H.tbl);

% make boundary nodes selected
H.sel = oNodeSelect(H.hsel,H.sel,obj.x(nsel),obj.y(nsel),repmat(objn,size(nsel)),nsel,H.tbl);
        
guidata(src,H)


function cbGridSelectLocalExtrema(src,evd,excludeboundary) %#ok<INUSL>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow working with only one grid object at a time
if length(objn)~=1 || obj.type~=3 % triangular grid object
    warndlg('The operation is allowed for only one object at a time.','Warning')
    return
end

x = obj.x';
y = obj.y';
[ipl,ipu] = grid_localextrema([x y], obj.tri, obj.z, excludeboundary);
nsel = unique([find(ipl); find(ipu)]);

H.sel = clear_selection(H.hsel,[],[],H.tbl);

% make boundary nodes selected
H.sel = oNodeSelect(H.hsel,H.sel,x(nsel),y(nsel),repmat(objn,size(nsel)),nsel,H.tbl);
        
guidata(src,H)


function cbSelectInPoly(src,evd) %#ok<INUSD>
H = guidata(src);

objn_slash = pickobj(H.hfm,2); % pick polyline (Type 2 object) for slashing the bathymetry
if isempty(objn_slash)
    warndlg('No ploygon was selected, cancelling operation.','')
    return
end
oslash = H.obj(objn_slash);
[node,cnect] = prepnodes_poly([[oslash.x]' [oslash.y]']);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);
nsel = find(inpoly([[obj.x]' [obj.y]'],node,cnect));

H.sel = clear_selection(H.hsel,[],[],H.tbl);

% make in-polygon nodes selected
H.sel = oNodeSelect(H.hsel,H.sel,obj.x(nsel),obj.y(nsel),repmat(objn,size(nsel)),nsel,H.tbl);

set(H.statstr,'String',[num2str(length(nsel)) ' nodes selected.'])

guidata(src,H)


function cbSelectCoincident(src,evd) %#ok<INUSD>
H = guidata(src);

roff = 1e-7; % do comparison within this roundoff error

objn = unique(H.sel(:,1))';

% % allow working with only one grid object at a time
% if length(objn)~=1 % any object
%     warndlg('The operation is allowed for only one object at a time.','Warning')
%     return
% end

objn2 = pickobj(H.hfm,[1 2 3]); % pick a node-based object
if objn2<1 % Escape, Enter or Space pressed
    warndlg('No object was specified for matching, cancelling operation.','')
    return
end
if ismember(objn2,objn) % one of the selected objects is picked
    warndlg('The object specified for matching cannot be one of the selected objects, cancelling operation.','')
    return
end
obj2 = H.obj(objn2);
obj2nodes = [obj2.x' obj2.y'];
obj2nodes = round(obj2nodes/roff)*roff;

H.sel = clear_selection(H.hsel,[],[],H.tbl);

for io = objn
    obj = H.obj(io);
    objinodes = [obj.x' obj.y'];
    objinodes = round(objinodes/roff)*roff;
    nsel = find(ismember(objinodes,obj2nodes,'rows'));
    
    
    % make coincident nodes selected
    H.sel = oNodeSelect(H.hsel,H.sel,obj.x(nsel),obj.y(nsel),repmat(io,size(nsel)),nsel,H.tbl);
end

guidata(src,H)


function cbSelectDuplicateNodes(src,evd) %#ok<INUSD>
H = guidata(src);

% can work with many objects
objn = unique(H.sel(:,1))';

% % allow working with only one grid object at a time
% if length(objn)~=1 % any object
%     warndlg('The operation is allowed for only one object at a time.','Warning')
%     return
% end

objn = objn(ismember([H.obj(objn).type],[1 2 3])); % only node-based objects

obj = H.obj(objn);
node = [obj.x' obj.y'];
nid = [obj.objn' obj.noden']; % node IDs
[~, m] = unique(node,'rows');
nn = size(node,1);
idup = setdiff(1:nn, m);

% 1st and last nodes of segments
otype = [obj.type];
nodepos = node1storlast(1:nn,otype(objn)',find(diff(nid(:,1)))',find(isnan(node(:,1)))',nn);
isegend = find(nodepos);
idup = setdiff(idup,isegend);
%%%%todo: remove end nodes of polyline segments from the list ONLY if they do not
%%%% coincide with any other nodes

H.sel = clear_selection(H.hsel,[],[],H.tbl);

% make duplicates selected
H.sel = oNodeSelect(H.hsel,H.sel,node(idup,1)',node(idup,2)',nid(idup,1)',nid(idup,2)',H.tbl);

set(H.statstr,'String',[num2str(length(idup)) ' nodes selected (last node in each duplicate list is NOT selected).'])

guidata(src,H)


function cbSelectOpenSegments(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))'; % selected objects
objn = objn([H.obj(objn).type]==2); % polylines only

% allow working with only one polyline at a time
if length(objn)~=1 % any object
    warndlg('The operation is allowed for only one polyline at a time.','Warning')
    return
end

obj = H.obj(objn);
x = obj.x;
y = obj.y;
[xs,ys,xe,ye,is,ie] = polyline_1standlast(x,y); % 1st and last nodes of segments

io = find(xs~=xe | ys~=ye); % open segments
no = length(io);

lo = false(size(x)); % logical indices of nodes in open segments
for iio = io
    lo(is(iio):ie(iio)) = true;
end

H.sel = clear_selection(H.hsel,[],[],H.tbl);

% make open-segment nodes selected
H.sel = oNodeSelect(H.hsel,H.sel,x(lo),y(lo),obj.objn(lo),obj.noden(lo),H.tbl);

set(H.statstr,'String',[num2str(sum(lo)) ' nodes selected in ' num2str(no) ' open segments.'])

guidata(src,H)


function cbPolySelSeg(src,evd) %#ok<INUSD>
% Select all nodes for the picked segments.
H = guidata(src);

objn = unique(H.sel(:,1))'; % selected objects
objn = objn([H.obj(objn).type]==2); % polylines only

% allow working with only one polyline at a time
if length(objn)~=1 % any object
    warndlg('The operation is allowed for only one polyline at a time.','Warning')
    return
end

obj = H.obj(objn);
x = obj.x;
y = obj.y;
[~,~,~,~,is,ie] = polyline_1standlast(x,y); % 1st and last nodes of segments

lo = false(size(x));
lo(H.sel(:,2)) = true; % logical indices of selected nodes
no = 0;
for iio = 1:length(is)
    ic = is(iio):ie(iio);
    if any(lo(ic))
        lo(ic) = true;
        no = no + 1;
    end
end

H.sel = clear_selection(H.hsel,[],[],H.tbl);

% make all nodes for picked segments selected
H.sel = oNodeSelect(H.hsel,H.sel,x(lo),y(lo),obj.objn(lo),obj.noden(lo),H.tbl);

set(H.statstr,'String',[num2str(sum(lo)) ' nodes selected in ' num2str(no) ' segment(s).'])


% function cbPolyOrientAllCCW(src,evd) %#ok<INUSD>
% % Orient all segments CCW.
% 
% H = guidata(src);
% 
% % selnew = clear_selection(H.hsel,[],[],H.tbl);
% [H.obj,undothis] = oPolyOrientAllCCW(H.obj,H.sel,H.tbl);
% if isstruct(undothis)
%     H.undo = undo_register(H,undothis);
% else
%     errordlg(undothis,'Error orienting polyline')
% end
% % H.sel = selnew;
% 
% guidata(src,H)
% 
% 
% function [obj,undo] = oPolyOrientAllCCW(obj,sel,htbl)
% % Orient all segments CCW.
% % sel   two-column [objn noden]
% 
% objf = sel(:,1);
% objfu = unique(objf);
% % objn = objn([H.obj(objn).type]==2); % polylines only
% if any([obj(objfu).type] ~= 2)
%     undo = 'Segments can be oriented only in polylines.';
%     return
% end
% % allow working with only one polyline at a time
% if length(objfu)~=1 % any object
%     undo = 'The operation is allowed for only one polyline at a time.';
%     return
% end
% nodef = sel(:,2);
% 
% %%%%%%%%%%%%5
% objnrem = [];
% nodenrem = [];
% for io = objfu(:)'
%     ic = objf==io;
%     obj1 = obj(io);
%     cnoden = obj1_segment_get(obj1,nodef(ic)); % cell array of node indices
%     
%     % record nodes to delete
%     cnoden = cell2mat(cnoden);
%     objnrem = [objnrem; repmat(io,size(cnoden))];  %#ok<AGROW>
%     nodenrem = [nodenrem; cnoden]; %#ok<AGROW>
% end
% obj = H.obj(objn);
% x = obj.x;
% y = obj.y;
% [xr,yr] = poly2ccw(x,y);
% [tf, loc] = ismember(A, S, 'rows')
% [obj,undo] = oNodeReorder(obj,objnrem,nodenrem,htbl);

%%%%%%%%%
function cbPolyOrientAllCCW(src,evd) %#ok<INUSD>
% Orient all segments CCW.

H = guidata(src);

objn = unique(H.sel(:,1))'; % selected objects
objn = objn([H.obj(objn).type]==2); % polylines only

% allow working with only one polyline at a time
if length(objn)~=1 % any object
    warndlg('The operation is allowed for only one polyline at a time.','Warning')
    return
end

obj = H.obj(objn);
x = obj.x;
y = obj.y;
z = obj.z;
code = obj.code;

% [xr,yr] = poly2ccw(x,y);
% xr(end+1) = NaN; % terminate last segment
% yr(end+1) = NaN;
pccw = poly_orient([x(:) y(:)],'multiface')';
xr = pccw(1,:);
yr = pccw(2,:);

[~, loc] = ismember(pccw', [x' y'], 'rows');
loc(loc==0) = [];
zr = NaN(size(z));
coder = NaN(size(code));
ival = ~isnan(x);
zr(ival) = z(loc);
coder(ival) = code(loc);

H.obj(objn) = obj_update_data(obj,[],xr,yr,[],zr,coder);
%%%%%%%%%%%%TODO undo

guidata(src,H)


function cbPolyOrientSegCW(src,evd) %#ok<INUSD>
% Orient selected segment CW.
H = guidata(src);

objn = unique(H.sel(:,1))'; % selected objects
objn = objn([H.obj(objn).type]==2); % polylines only

% allow working with only one polyline at a time
if length(objn)~=1 % any object
    warndlg('The operation is allowed for only one polyline at a time.','Warning')
    return
end

obj = H.obj(objn);
x = obj.x;
y = obj.y;
z = obj.z;
code = obj.code;

% [~,~,~,~,is,ie] = polyline_1standlast(x,y); % 1st and last nodes of segments
% 
% lo = false(size(x));
% lo(H.sel(:,2)) = true; % logical indices of selected nodes
% no = 0;
% for iio = 1:length(is)
%     ic = is(iio):ie(iio);
%     if any(lo(ic))
%         lo(ic) = true;
%         no = no + 1;
%     end
% end

lo = obj1_segment_get(obj,H.sel(:,2)); % cell array of node indices
lo = cell2mat(lo);

% [xr,yr] = poly2cw(x(lo),y(lo));
% xr(end+1) = NaN; % terminate last segment
% yr(end+1) = NaN;
pcw = poly_orient([x(lo)' y(lo)'],-1,'multiface')';
xr = pcw(1,:);
yr = pcw(2,:);

[~, loc] = ismember([xr' yr'], [x' y'], 'rows');
loc(loc==0) = [];
zr = NaN(size(xr));
coder = NaN(size(xr));
ival = ~isnan(xr);
zr(ival) = z(loc);
coder(ival) = code(loc);

x(lo) = xr;
y(lo) = yr;
z(lo) = zr;
code(lo) = coder;

H.obj(objn) = obj_update_data(obj,[],x,y,[],z,code);

%%%%%%%%%%%%TODO undo

guidata(src,H)


function cbEditNode(src,evd) %#ok<INUSD>
H = guidata(src);
[H.obj,undo] = oNodeEdit(H.obj,H.sel); % edit properties for selected nodes
H.undo = undo_register(H,undo);
guidata(src,H)


function cbExtractBoundary(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow working with only one grid object at a time
if length(objn)~=1 || obj.type~=3 % triangular grid object
    warndlg('Extracting boundary is allowed for only one object at a time.','Warning')
    return
end

try
    [cb,errn,wrn] = tri_boundary(obj.tri);
    if errn
        errordlg({'Operation failed:','Boundary branches (see the selected node).'},'Error');
        H.sel = clear_selection(H.hsel,[],[],H.tbl);
        
        % make branching node selected
        H.sel = oNodeSelect(H.hsel,H.sel,obj.x(errn),obj.y(errn),repmat(objn,size(errn)),errn,H.tbl);
        
        guidata(src,H)
        return
    end
    if wrn
        warndlg(wrn,'Warning');
    end
catch err
    warndlg({'Boundary extraction failed. Possible cause: tri_boundary.m',err.message},'Warning');
    return
end

[okpressed,pp] = dlgPlotParam(H.ppdef);
if ~okpressed % Cancel was pressed
    return
end

% poly = NaN(length(cb),2);
% ival = ~isnan(cb);
% poly(ival,:) = [obj.x(cb(ival))' obj.y(cb(ival))'];

r.x = NaN(size(cb));
r.y = NaN(size(cb));
r.z = NaN(size(cb));
r.code = NaN(size(cb));
ival = ~isnan(cb);
r.x(ival) = obj.x(cb(ival));
r.y(ival) = obj.y(cb(ival));
r.z(ival) = obj.z(cb(ival));
r.code(ival) = obj.code(cb(ival));

% if ~isempty(poly)
%     r.x = poly(:,1); % all in one object
%     r.y = poly(:,2);
    otype = 2;
    onew = oCreate(r,[],otype);
    [H.obj,undothis] = obj_load(H.obj,onew,H.ah,pp,H.tbl);
    undothis.label = 'extract boundary';
    H.undo = undo_register(H,undothis);
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
% end

guidata(src,H)


function cbExtractBoundaryLoop(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow working with only one grid object at a time
if length(objn)~=1 || obj.type~=3 % triangular grid object
    warndlg('Extracting boundary is allowed for only one object at a time.','Warning')
    return
end

try
    set(H.hfm,'Pointer','watch');drawnow
    noden = H.sel(:,2);
    ebnd = grid_edge(obj.tri);
    [cb,errn,wrn] = edge_extract_loop(ebnd,noden(1)); % first selected node
    set(H.hfm,'Pointer','arrow')
    
    if errn
        errordlg({'Operation failed:','Boundary branches (see the selected node).'},'Error');
        % make branching node selected
        H.sel = clear_selection(H.hsel,[],[],H.tbl);
        H.sel = oNodeSelect(H.hsel,H.sel,obj.x(errn),obj.y(errn),repmat(objn,size(errn)),errn,H.tbl);
    elseif any(~ismember(noden,cb)) % some of the selected nodes are not part of this loop
        errordlg({'Operation failed:','The selected nodes belong to different loops.'},'Error');
    else
        if wrn
            warndlg(wrn,'Warning')
        end

        [okpressed,pp] = dlgPlotParam(H.ppdef);
        if ~okpressed % Cancel was pressed
            return
        end

        H.sel = clear_selection(H.hsel,[],[],H.tbl);
        
        r.x = [obj.x(cb) NaN]; % no NaNs in cb
        r.y = [obj.y(cb) NaN];
        r.z = [obj.z(cb) NaN];
        r.code = [obj.code(cb) NaN];
        otype = 2;
        onew = oCreate(r,[],otype);
        [H.obj,undothis] = obj_load(H.obj,onew,H.ah,pp,H.tbl);
        undothis.label = 'extract loop';
        H.undo = undo_register(H,undothis);
    end
    guidata(src,H)
catch err
    errordlg({'Boundary extraction failed.',err.message},'Error');
end


function cbExtractGridPart(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow working with only one grid object at a time
if length(objn)~=1 || obj.type~=3 % triangular grid object
    warndlg('Extracting grid part is allowed for only one object at a time.','Warning')
    return
end

noden = H.sel(:,2); % must be sorted

[p,r.tri,r.z,r.code] = grid_extract([obj.x' obj.y'],obj.tri,obj.z,obj.code,noden);
r.x = p(:,1);
r.y = p(:,2);
otype = 3;
onew = oCreate(r,[],otype);
[H.obj,undothis] = obj_load(H.obj,onew,H.ah,obj.pp,H.tbl);
H.undo = undo_register(H,undothis);

H.sel = clear_selection(H.hsel,[],[],H.tbl);

guidata(src,H)


function cbBathyCDT(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1));
obj = H.obj(objn);

% collect nodes
x = [obj.x];
y = [obj.y];
z = [obj.z];
code = [obj.code];

% remove terminating NaNs
lnan = isnan(x);
x(lnan) = [];
y(lnan) = [];
z(lnan) = [];
code(lnan) = [];

% get boundary to constrain the triangulation
bo = obj([obj.type]==2);
if isempty(bo) % no boundary specified
    % triangulate
    try
        tri = delaunay(x(:),y(:));
    catch err
        errordlg(err.message,'Error triangulating nodes')
    end
else
    [node,cnect] = prepnodes_poly([[bo.x]' [bo.y]']);
    try
        tri = cdt_mk([x(:) y(:)],node,cnect);
    catch err
        errordlg(err.message,'Error triangulating nodes')
    end
end

[p,tri,z,code] = grid_fix([x(:) y(:)],tri,z,code);

% load the resulting grid as a new objects
r.x = p(:,1);
r.y = p(:,2);
r.z = z;
r.code = code;
r.tri = tri;
% ebnd = grid_edge(tri);
% r.code(unique(ebnd(:))) = 2; %%%%% all boundary is islands for now

otype = 3;
onew = oCreate(r,[],otype);
[H.obj,undo] = obj_load(H.obj,onew,H.ah,H.ppdef,H.tbl);
undo.label = 'triangulate nodes';
H.undo = undo_register(H,undo);

guidata(src,H)


function cbGridGenerate(src,evd) %#ok<INUSD>
H = guidata(src);

% get boundary from user
hmsg = msgbox('Step 1: Pick polylines representing boundary of the model grid. When done press Enter to go to Step 2. Esc to cancel grid generation.','Generating Grid: Step 1','modal');
uiwait(hmsg)
% uiresume(hmsg)
allowableobjects = 2; % pick polylines only (Type 2 object)
obound = [];
while true
    objn = pickobj(H.hfm,allowableobjects);
    if objn==0 % Escape pressed
        return
    elseif objn<0 % Enter or Space pressed
        break
    end
    obound(end+1) = objn; %#ok<AGROW>
end

% get bathymetry from user
title = 'Generating Grid: Step 2';
qstring = 'Step 2: Specify data to control model grid spacing, e.g. bathymetry. The specified data can be a single triangulated dataset or multiple objects consisting of nodes, including node sets, polylines, and triangulated data. In the second case the entire dataset will be triangulated internally. Cancel to generate grid without node spacing control.';
button1 = 'Single triangulated dataset';
button2 = 'Node sets';
button3 = 'No depth-dependence';
response =  questdlg(qstring,title,button1,button2,button3,button1);
switch response
    case button1
        allowablebathy = 3; % grids only
        hmsg = msgbox('Step 2: Pick a triangular grid object. Esc to skip Step 2.','Generating Grid: Step 2','modal');
        uiwait(hmsg)
    case button2
        allowablebathy = [1 2 3]; % all node objects
        hmsg = msgbox('Step 2: Pick objects. When done press Enter to start grid generation routine. Esc to skip Step 2.','Generating Grid: Step 2','modal');
        uiwait(hmsg)
    case button3
        allowablebathy = 0;
end
obathy = [];
if any(allowablebathy)
    while true
        objn = pickobj(H.hfm,allowablebathy); % pick bathymetry objects
        if objn<1 % Escape, Enter or Space pressed
            break
        end
        obathy(end+1) = objn; %#ok<AGROW>
        if length(allowablebathy)==1 && allowablebathy==3
            break % only one object required
        end
    end
end

% Step 3: get grid generation parametrs for the entire grid and specific regions
% if any(allowablebathy) % if depth-dependent node spacing requested
a = dlgRegionParameters('Entire Area Parameters',[],any(allowablebathy)); % for the entire grid
if isempty(a) % Cancel pressed
%     allowablebathy = 0; % second chance for the user to skip depth-dependence of node spacing
    areaflag = false;
else
    areaflag = true;
    
    a(1).poly = []; % 1st element represents the enitre region, poly is ignored by grid-generation routine

    title = 'Generating Grid: Step 3';
    qstring = {'Step 3: Pick polygons for specific regions and specify individual grid-generation parameters for them.',...
        'Polygons should be defined starting from the outermost and go from low to high target resolution.',...
        'When done press Enter to proceed. Press Skip now or Esc during polygon selection process to skip area specification.'};
    button1 = 'Start area specification';
    button2 = 'Skip area specification';
    response =  questdlg(qstring,title,button1,button2,button1);

    oregion = [];
    if strcmp(response,button1)
        if isfield(a,'dzthres')
            dzthres = a.dzthres;
        else
            dzthres = NaN;
        end
        if isfield(a,'dzthres_zmin')
            dzthres_zmin = a.dzthres_zmin;
        else
            dzthres_zmin = NaN;
        end
        if any(allowablebathy) % if depth-dependent node spacing requested
            def = [a.h0 a.h2z dzthres dzthres_zmin a.hmin a.hmax];
        else
            def = [dzthres a.hmin a.hmax];
        end
        allowableobjects = 2; % polylines only
        while true
            objn = pickobj(H.hfm,allowableobjects); % pick polyline (Type 2 object)
            if objn==0 % Escape pressed
                oregion = [];
            elseif objn<0 % Enter or Space pressed
                break
            end
            oregion(end+1) = objn; %#ok<AGROW>
            ac = dlgRegionParameters('Area-Specific Parameters',def,any(allowablebathy));
            if isempty(ac)
                continue % on Cancel press, disregard the last polygon, keep asking for more polygons
            else
                % assign polygon
                obj = H.obj(oregion(end));
                poly = [obj.x' obj.y']; % can be multiply-connected
                cpoly = poly2cell(poly);
                ac.poly = [cpoly{1}; NaN NaN]; % 1st simple polygon goes to the current element
                for ip = 2:length(cpoly) % for multiply-connected polygons
                    % append 2nd and all subsequent simple polygons to the end of the array
                    ac(ip) = ac(1); % preserve the parameters
                    ac(ip).poly = [cpoly{ip}; NaN NaN];
                end

                a = [a ac]; %#ok<AGROW>
            end
        end
    end
end
% end

% Step 4: prepare user input for mesh2d routine
options = dlgGridOptions;
% options.output = false;
options.output = true;

obj = H.obj(obound);
[node,cnect] = prepnodes_poly([[obj.x]' [obj.y]']);

% %%%%distmesh test
% a(1).poly = [[obj.x]' [obj.y]'];

% depthfun = @gns_depth_refpoly; % linear dependence of h on z
depthfun = @gns_sqrtdepth_refpoly; % h proportional to sqrt(z); with dz/z threshold
% depthfun = @gns_sqrtdepth_smooth_refpoly; % h proportional to sqrt(z) and
% limited by dh/h threshold %%%%%%%
    
if any(allowablebathy) % if depth-dependent node spacing requested
    objb = H.obj(obathy);
    b.x = [objb.x]';
    b.y = [objb.y]';
    b.z = [objb.z]';
    inv = isnan(b.x);
    b.x(inv) = [];
    b.y(inv) = [];
    b.z(inv) = [];
    if length(allowablebathy)==1 && allowablebathy==3
        b.tri = objb.tri;
    else
        b.tri = delaunay(b.x,b.y); %%%%%%%%%%%%%%
    end

    hdata.args = {b,a};
    hdata.fun = depthfun; % dependence of h on z
elseif areaflag % if area-specific constraints specified (except depth-dependence)
    hdata.args = {[],a}; % no bathymetry
    hdata.fun = depthfun; % dependence of h on z
else % no area-specific constraints
    hdata = [];
%     [p,tri,stats,err,popen] = mesh2d(node,cnect,[],options); % grid generation
end

%%%% to use mesh2d with poly_lfs ( where LFS is measured only across the interiror)
hdata.polylfs = [[obj.x]' [obj.y]']; % used in quadtree_mk to estimate LFS

% Step 5: call the routine
[p,tri,~,err,popen] = mesh2d(node,cnect,hdata,options); % grid generation

if ~isempty(err)
    H.sel = oNodeSelect(H.hsel,H.sel,popen(:,1)',popen(:,2)',ones(size(popen,1),1),ones(size(popen,1),1)); %%%%% hack: not actual object and node numbers
    errordlg({err,'See the selected nodes.'},'Error generating the grid')
else
    % load the resulting grid as a new objects
    r.x = p(:,1)';
    r.y = p(:,2)';
    r.z = zeros(size(r.x));
    r.tri = tri;

    r.code = zeros(size(r.x)); % all internal nodes are coded 0
    
%     ebnd = grid_edge(tri);
%     r.code(unique(ebnd(:))) = 2; %%%%% all boundary is islands for now
    b = tri_boundary(tri,r.x==min(r.x)); % x==min(x) ensures 1st boundary loop is the outer boundary
    ib = isnan(b);
    ib1 = find(ib,1,'first');
    b(ib) = [];
    b1 = b(1:ib1-1); % 1st loop -- outer boundary
    b = b(ib1:end); % the rest of loops -- islands
    r.code(unique(b1)) = 1; %%%%% all outer boundary is Code 1 for now
    %%% The user needs to adjust Codes for open boundary.
    r.code(unique(b)) = 2; % all islands are Code 2

    otype = 3;
    onew = oCreate(r,[],otype);
    [H.obj,undo] = obj_load(H.obj,onew,H.ah,H.ppdef,H.tbl);
    H.undo = undo_register(H,undo);
end

guidata(src,H)


function a = dlgGridOptions()
% Get user-specified options for input to mesh2d.
%   h2z       node spacing to z ratio
%   hmin,hmax lower and upper limits for node spacing

dlg_title = 'Generating Grid: Step 4: Options for Grid Generation';
prompt = {'Maximum allowable (relative) gradient in the size function (lower value gives higher "quality" of triangles):',...
          'Maximum allowable number of iterations (if convergence not reached):'};
num_lines = 1;
def = {'0.2','20'};

response = inputdlg(prompt,dlg_title,num_lines,def);
if isempty(response) % if Cancel was pressed
    a = [];
    return
end

a.dhmax = eval(response{1});
a.maxit = str2double(response{2});



function a = dlgRegionParameters(dlg_title,def,h2z_flag)
% Get area-specific parametrs from user.
%   h2z       node spacing to z ratio
%   hmin,hmax lower and upper limits for node spacing

num_lines = 1;

if h2z_flag
    prompt = {'h0:  Node spacing at z=0:',...
              'h/sqrt(z): Ratio of node spacing to sqrt(z) of the specified node datasets:',...
              'dz/z: Maximum relative depth change threshold:',...
              'zmin for dz/z: Minimum depth to apply dz/z threshold:',...
              'hmin: Lower limit for node spacing (axes units):'...
              'hmax: Upper limit for node spacing (axes units):'};

    if exist('def','var') && ~isempty(def)
        def = {num2str(def(1)),rats(def(2)),num2str(def(3)),num2str(def(4)),num2str(def(5)),num2str(def(6))};
    else
%         def = {'500','250','0.3','500','Inf'};
        def = {'2000','150','0.3','200','500','5000'};
    end
else
    prompt = {'dz/z: Maximum relative depth change threshold:',...
              'hmin: Lower limit for node spacing (axes units):'...
              'hmax: Upper limit for node spacing (axes units):'};

    if exist('def','var') && ~isempty(def)
        def = {num2str(def(1)),num2str(def(2))};
    else
        def = {'0.3','100','1000'};
    end
end
response = inputdlg(prompt,dlg_title,num_lines,def);
if isempty(response) % if Cancel was pressed
    a = [];
    return
end

if h2z_flag
    a.h0 = eval(response{1});
    a.h2z = eval(response{2});
    dzthres = str2double(response{3});
    if isfinite(dzthres)
        a.dzthres = dzthres;
    end
    a.dzthres_zmin = str2double(response{4}); % empty string gives NaN
    a.hmin = str2double(response{5});
    a.hmax = str2double(response{6});
else
    dzthres = str2double(response{1});
    if isfinite(dzthres)
        a.dzthres = dzthres;
    end
    a.hmin = str2double(response{2});
    a.hmax = str2double(response{3});
end




function cbGridSlash(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow working with only one grid object at a time
if length(objn)~=1 || obj.type~=3 % triangular grid object
    warndlg('Extracting grid part is allowed for only one object at a time.','Warning')
    return
end

objn_slash = pickobj(H.hfm,2); % pick polyline (Type 2 object) for slashing the grid
if isempty(objn_slash)
    warndlg('No ploygon was selected, cancelling operation.','')
    return
end
oslash = H.obj(objn_slash);
poly = [oslash.x(1:end-1)' oslash.y(1:end-1)'];

[ok,boundaryaction,H.codesub] = dlgGridSlashParam(H.codesub);
if ~ok
    return
end

[p,tri,z,code, p2,tri2,z2,code2] = ...
    grid_slash([obj.x' obj.y'],obj.tri,obj.z,obj.code,poly,boundaryaction,H.codesub);
r.x = p2(:,1)';
r.y = p2(:,2)';
r.tri = tri2;
r.z = z2(:)'; % ensure row vectors
r.code = code2(:)';

% replace old object with the inside grid
H.obj(objn) = obj_update_data(obj,H.tbl,p(:,1)',p(:,2)',objn,z(:)',code(:)',tri);

% insert outside grid as a new object
otype = 3;
onew = oCreate(r,objn+1,otype);
H.obj = obj_load(H.obj,onew,H.ah,obj.pp,H.tbl);

ud.action = 'gridsplit';
ud.label = 'slash grid';
r.objn = objn;
r.x = obj.x;
r.y = obj.y;
r.z = obj.z;
r.code = obj.code;
r.tri = obj.tri;
ud.r = r;
H.undo = undo_register(H,ud);

H.sel = clear_selection(H.hsel,[],[],H.tbl);

guidata(src,H)


function cbGridClean(src,evd) %#ok<INUSD>
% Callback for grid_cleanboundary operation: Remove grid nodes on the edges of
% a polyline. The operation is performed on a (refined) grid before merging it with
% another (original) grid when open boundary has been discretized in the process of
% grid generation/refinement.

H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow working with only one grid object at a time
if length(objn)~=1 || obj.type~=3 % triangular grid object
    warndlg('Cleaning grid boundary is allowed for only one object at a time.','Warning')
    return
end

% request polyline to match
objn_poly = pickobj(H.hfm,2); % pick polyline (Type 2 object) for cleaning the grid
if isempty(objn_poly)
    warndlg('No ploygon was selected, cancelling operation.','')
    return
end
opoly = H.obj(objn_poly);
poly = [opoly.x(1:end-1)' opoly.y(1:end-1)'];

gridnode = [obj.x' obj.y'];
tri = obj.tri;
z = obj.z;
code = obj.code;

% call the routine
[gridnode,tri,z,code] = grid_cleanboundary(gridnode,tri,poly,z,code);

% update the grid object
H.obj(objn) = obj_update_data(obj,H.tbl,gridnode(:,1)',gridnode(:,2)',objn,z(:)',code(:)',tri);

% save the undo information
undothis.action = 'gridedit';
undothis.label = '"clean" boundary';
undothis.r = obj; %%%%% saving the entire grid object
H.undo = undo_register(H,undothis);

guidata(src,H)


function cbGridMerge(src,evd) %#ok<INUSD>
H = guidata(src);

objn1 = unique(H.sel(:,1))';
obj1 = H.obj(objn1);

% allow working with only one grid object at a time
if length(objn1)~=1 || obj1.type~=3 % triangular grid object
    warndlg('A single grid must be selected to start the merge operation.','Warning')
    return
end

objn2 = pickobj(H.hfm,3); % pick another grid (Type 3 object) for merging
if ~objn2
    return
end
obj2 = H.obj(objn2);

x1 = obj1.x';
y1 = obj1.y';
tri1 = obj1.tri;
x2 = obj2.x';
y2 = obj2.y';
tri2 = obj2.tri;


%%%% This is done in grid_merge now.
% % adjust positions for very close nodes
% [ebnd,eint] = grid_edge(tri2);
% e = [ebnd; eint];
% % set tolerance at 10% of the minimum edge length in Grid 2
% tol = (min(((x2(e(:,1))-x2(e(:,2))).^2 + (y2(e(:,1))-y2(e(:,2))).^2).^0.5)) * 0.10; 
% [x2,y2,wrn] = moveclose(x2,y2,x1,y1,tol);
% 
% % adjust positions for very close boundary nodes
% e1 = grid_edge(tri1); % boundary edges
% e2 = grid_edge(tri2);
% % set tolerance at 2% of the minimum edge length in Grid 2
% tol = (min(((x2(e2(:,1))-x2(e2(:,2))).^2 + (y2(e2(:,1))-y2(e2(:,2))).^2).^0.5)) * 0.02; 
% bn1 = unique(e1(:)); % boundary nodes
% bn2 = unique(e2(:)); % boundary nodes
% [x2(bn2),y2(bn2),wrn] = moveclose(x2(bn2),y2(bn2),x1(bn1),y1(bn1),tol);
% if wrn
%     warndlg('Close node pairs found in the first grid','Warning');
% end

% % grid overlap test %%%%% tsearch works only for convex shapes, use inpoly
% [inp, onp] = inpoly([x1 y1], [x2 y2], ebnd); % ebnd for Grid 2
% in = find(inp & ~onp);
% if ~isempty(in)
%     H.sel = clear_selection(H.hsel);
%     H.sel = oNodeSelect(H.hsel,H.sel,x1(in)',y1(in)',repmat(objn1,size(in)),in);
%     errordlg('Selected grids overlap. Grid 1 nodes within Grid 2 are selected.','Overlapping grids');
%     return
% end
% 
% ebnd = grid_edge(tri1); % ebnd for Grid 1
% [inp, onp] = inpoly([x2 y2], [x1 y1], ebnd);
% in = find(inp & ~onp);
% if ~isempty(in)
%     H.sel = clear_selection(H.hsel);
%     H.sel = oNodeSelect(H.hsel,H.sel,x2(in)',y2(in)',repmat(objn2,size(in)),in);
%     errordlg('Selected grids overlap. Grid 2 nodes within Grid 1 are selected.','Overlapping grids');
%     return
% end

% merge operation
[p,tri,z,code,in1,in2,wrn] = grid_merge([x1 y1], tri1, [x2 y2], tri2,...
                          obj1.z,obj2.z,  obj1.code,obj2.code);

if wrn
    warndlg('Close node pairs found in the first grid','Warning');
end
if ~isempty(in1) || ~isempty(in2)
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
    H.sel = oNodeSelect(H.hsel,H.sel,x1(in1)',y1(in1)',repmat(objn1,size(in1)),in1,H.tbl);
    H.sel = oNodeSelect(H.hsel,H.sel,x2(in2)',y2(in2)',repmat(objn2,size(in2)),in2,H.tbl);
    errordlg('Selected grids overlap. Grid nodes within another grid are selected.',...
        'Overlapping grids');
    return
end
    
% replace Grid 1 object with the merged grid
H.obj(objn1) = obj_update_data(obj1,H.tbl,p(:,1)',p(:,2)',objn1,z',code',tri);

% delete Grid 2 object
H.obj = oDelete(H.obj,objn2,H.tbl);

ud.action = 'gridmerge';
ud.label = 'merge grids';
r.objn = objn1;
r.x = x1;
r.y = y1;
r.z = obj1.z;
r.code = obj1.code;
r.tri = tri1;
r.objins = obj2;
ud.r = r;
H.undo = undo_register(H,ud);

H.sel = clear_selection(H.hsel,[],[],H.tbl);

% check for hanging nodes
ihang = tri_hangingnode(tri,size(p,1));
if ~isempty(ihang)
    if objn2<objn1 %#ok<BDSCI>
        objn1 = objn1-1; % adjust obj1 number if it's changed after deleting obj2
    end
    H.sel = oNodeSelect(H.hsel,H.sel,p(:,1)',p(:,2)',repmat(objn1,size(ihang)),ihang);
    warndlg('Hanging nodes found in the resulting grid. Hanging nodes are selected.','Warning');
end

guidata(src,H)


function cbGridSmooth(src,~,keepobn)
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow working with only one grid object at a time
if length(objn)~=1 || obj.type~=3 % triangular grid object
    warndlg('Smoothing is allowed for only one grid object at a time.','Warning')
    return
end

x = obj.x;
y = obj.y;
z = obj.z;
code = obj.code;
tri = obj.tri;

if keepobn
    [p,tris,pfun] = grid_smooth([x' y'],tri,[],[],[z' code'],[],code'==5);
else
    [p,tris,pfun] = grid_smooth([x' y'],tri,[],[],[z' code']);
end
xs = p(:,1)';
ys = p(:,2)';
zs = pfun(:,1)';
code = pfun(:,2)';

% % make sure z is consistent with xs,ys (they may have been changed in smoothmesh by fixmesh)
% if length(xs)~=length(x)
%     [is,is,io] = intersect(p,[x' y'],'rows');
%     zs(is) = z(io);
%     code(is) = code(io);
% else
%     zs = z;
% end

%%%% no automatic redepth to speed up the operation
%%%% make it an option in the menu
% % redepth against its original configuration
% % zr = grid_redepth(xs,ys,x,y,z,tri);
% zr = tri_interp(xs,ys,x,y,z,tri);
% ival = ~isnan(zr);
% zs(ival) = zr(ival);

H.obj(objn) = obj_update_data(obj,H.tbl,xs,ys,[],zs,code,tris);

undo.action = 'gridedit';
undo.label = 'smooth grid';
undo.r.objn = objn;
undo.r = obj;
H.undo = undo_register(H,undo);

guidata(src,H)


function cbGridFix(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow working with only one grid object at a time
if length(objn)~=1 || obj.type~=3 % triangular grid object
    warndlg('Fixing grid is allowed for only one grid object at a time.','Warning')
    return
end

x = obj.x;
y = obj.y;
z = obj.z;
code = obj.code;
tri = obj.tri;

[p,tri,pfun] = fixmesh([x' y'],tri,[z' code']); % part of mesh2d toolbox
x = p(:,1)';
y = p(:,2)';
z = pfun(:,1)';
code = pfun(:,2)';

H.obj(objn) = obj_update_data(obj,H.tbl,x,y,[],z,code,tri);

undo.action = 'gridedit';
undo.label = 'fix grid';
undo.r.objn = objn;
undo.r = obj;
H.undo = undo_register(H,undo);

guidata(src,H)


function cbGridInfo(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow working with only one grid object at a time
if length(objn)~=1 || obj.type~=3 % triangular grid object
    warndlg('Smoothing is allowed for only one grid object at a time.','Warning')
    return
end

x = obj.x;
y = obj.y;
z = obj.z;
code = obj.code;
tri = obj.tri;

[~,txt] = grid_info(x,y,tri,z,code,obj.pp);

msgbox(char(txt));


function cbRedepth(src,evd) %#ok<INUSD>
H = guidata(src);

objn1 = unique(H.sel(:,1))';
obj1 = H.obj(objn1);

% allow working with only one node-based object at a time
if length(objn1)~=1 || ~ismember(obj1.type,[1 2 3])  % any node-based object
    warndlg('Redepthing is allowed for only one node-based object at a time.','Warning')
    return
end

objn2 = pickobj(H.hfm,[1 3]); % pick a reference node set or grid (Type 1 or 3) for redepthing
if ~objn2
    return
end
obj2 = H.obj(objn2);

% ask for extrapolation threshold
prompt = {'Extrapolation threshold (m); 0 for no extrapolation, Inf for no-threshold extrapolation:'};
extrap_dist = inputdlg(prompt,'Extrapolation',1,{'0'});
if isempty(extrap_dist) % if Cancel was pressed
    warndlg('Operation cancelled')
    return
end
[extrap_dist, status] = str2num(extrap_dist{1});   %#ok<ST2NM>
if ~status
    warndlg('Invalid value supplied; operation cancelled')
    return
end

% wait cursor
set(H.hfm,'Pointer','watch'); drawnow

x1 = obj1.x;
y1 = obj1.y;
z1 = obj1.z;

x2 = obj2.x;
y2 = obj2.y;
z2 = obj2.z;
if obj2.type==3
    tri2 = obj2.tri;
else
    tri2 = delaunay(x2,y2);
end

% z = grid_redepth(x1,y1,x2,y2,z2,tri2);
z = tri_interp(x1,y1,x2,y2,z2,tri2,extrap_dist);
ival = ~isnan(z);

undo.action = 'redepth';
undo.label = 'redepth';
undo.r.objn = objn1;
undo.r.noden = ival;
undo.r.z = z1(ival);

z1(ival) = z(ival); % change only re-depthed nodes

H.obj(objn1) = obj_update_data(obj1,H.tbl,[],[],[],z1);

H.undo = undo_register(H,undo);

set(H.statstr,'String',['Redepthed ' num2str(sum(ival)) ' of ' num2str(length(x1)) ' nodes.'])

% restore cursor
set(H.hfm,'Pointer','arrow'); drawnow

guidata(src,H)


function cbSetMinDepth(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';
obj = H.obj(objn);

% allow working with only one grid object at a time
if length(objn)~=1 || ~ismember(obj.type,[1 2 3])  % any node-based object
    warndlg('The operation is allowed for only one node-based object at a time.','Warning')
    return
end

prompt = 'Minimum depth:';
title = 'Minimum depth';
lines = 1;
def = {'5'};
zmin = inputdlg(prompt,title,lines,def);
if isempty(zmin) % if Cancel was pressed
    return
end
zmin = str2double(zmin{1});

z = obj.z;
ival = z<zmin;

if all(~ival) % no depths to correct
    return
end

undo.action = 'redepth';
undo.label = 'set minimum depth';
undo.r.objn = objn;
undo.r.noden = ival;
undo.r.z = z(ival);

z(ival) = zmin;

H.obj(objn) = obj_update_data(obj,H.tbl,[],[],[],z);

H.undo = undo_register(H,undo);

guidata(src,H)


function cbSetUTMZone(src,evd) %#ok<INUSD>
H = guidata(src);

if ~isempty(H.gmap.utmzone)
    def = {num2str(H.gmap.utmzone)};
else
    % default UTM zone for dialog is the one common to all zones recorded for
    % all objects
    obj = H.obj;
    no = length(obj);
    defzone = NaN(no,1);
    for io = 1:no
        if isfield(obj(io).meta,'utmzone') && isnumeric(obj(io).meta.utmzone)
            defzone(io) = unique(obj(io).meta.utmzone);
        end
    end
    defzone(isnan(defzone)) = [];
    defzone = unique(defzone);
    if isempty(defzone)
        def = {'0'};
    elseif length(defzone)>1 % more than one unique zone encountered
        def = {'10'}; % BC west coast
    else
        def = {num2str(defzone)};
    end
end

% prompt = 'UTM zone:';
prompt = ['UTM zone ',...
        '(1 to 60, negative for southern hemisphere ',...
        'or zero for geographic coordinates):'];
title = 'Set UTM Zone';
lines = 1;
zone = inputdlg(prompt,title,lines,def);
if isempty(zone) % if Cancel was pressed
    return
end
zone = str2double(zone{1});
if zone < -60 || zone > 60
    errstr = {'UTM zone must be 1 to 60',...
        '(negative for southern hemisphere)',...
        'or zero for geographic coordinates.'};
    errordlg(errstr,'Error')
    return
end
H.gmap.utmzone = zone;

guidata(src,H)


function cbSetZoomLevel(src,evd) %#ok<INUSD>
H = guidata(src);

prompt = 'Zoom level for Google Maps (1 to 19):';
title = 'Google Maps Zoom Level';
lines = 1;
if ~isempty(H.gmap.zoomlevel)
    def = {num2str(H.gmap.zoomlevel)};
else
    def = {'12'};
end
zl = inputdlg(prompt,title,lines,def);
if isempty(zl) % if Cancel was pressed
    return
end
zl = round(str2double(zl{1}));
if zl < 1 || zl > 19
    errstr = 'Zoom level must be between 1 and 19';
    errordlg(errstr,'Error')
    return
end
H.gmap.zoomlevel = zl;

guidata(src,H)


function cbPredefZ(src,evd) %#ok<INUSD>
H = guidata(src);



prompt = 'Predefined z values (Matlab expression):';
title = 'Set predefined z values';
lines = 1;
str = inputdlg(prompt,title,lines,{H.zpredefstr});
if isempty(str) % if Cancel was pressed
    return
end
H.zpredef = eval(str{1});
H.zpredefstr = str{1};

guidata(src,H)


function cbImgRotate(src,evd) %#ok<INUSD>
H = guidata(src);

prompt = 'Rotation angle (positive is counter-clockwise):';
title = 'Rotation angle';
lines = 1;
def = {'0'};
ang = inputdlg(prompt,title,lines,def);
if isempty(ang) % if Cancel was pressed
    return
end
ang = str2double(ang{1});

objn = unique(H.sel(:,1))';

[H.obj(objn),undo] = img_rotate(H.obj(objn),ang);
undo.r.objn = objn;
H.undo = undo_register(H,undo);

guidata(src,H)


function cbImgDigitize(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1))';

% calculate the monochrome luminance by combining the RGB values according
% to the NTSC standard, which applies coefficients related to the eye's
% sensitivity to RGB colors.
% imgr is an intensity image with integer values ranging from a minimum of
% zero to a maximum of 255.
imrgb = H.obj(objn).cdata;
imgr = .2989*imrgb(:,:,1) +.5870*imrgb(:,:,2) +.1140*imrgb(:,:,3);

lines = 1;
def = {'0'};


% define scaling 

hmsg = msgbox('Pick the lower threshold pixel within the image...','Image Scaling Thresholds','modal');
uiwait(hmsg)
[ix,iy] = pickobj(H.hfm,4,objn);
if ix<1
    return
end
imlow = imgr(iy,ix);

prompt = 'Lowest Z-value:';
title = 'Lower value threshold';
zmin = inputdlg(prompt,title,lines,def);
if isempty(zmin) % if Cancel was pressed
    return
end
zmin = str2double(zmin{1});


hmsg = msgbox('Pick the higher threshold pixel within the image...','Image Scaling Thresholds','modal');
uiwait(hmsg)
[ix,iy] = pickobj(H.hfm,4,objn);
if ix<1
    return
end
imhigh = imgr(iy,ix);

prompt = 'Highest Z-value:';
title = 'Higher value threshold';
zmax = inputdlg(prompt,title,lines,def);
if isempty(zmax) % if Cancel was pressed
    return
end
zmax = str2double(zmax{1});


objnew = img_digitize(H.obj(objn).imx,H.obj(objn).imy,imgr,imlow,imhigh,zmin,zmax);

[H.obj,undo] = obj_load(H.obj, objnew, H.ah, H.ppdef, H.tbl);
H.undo = undo_register(H,undo);

guidata(src,H)


function objnew = img_digitize(imx,imy,imgray,imlow,imhigh,zmin,zmax)
% Convert a grayscale or indexed image defined by imx,imy,imgray to a node
% set with z defined by linear transformation specified by correspondence
% of imlow,imhigh to zmin,zmax.

c = [1 double(imlow); 1 double(imhigh)]\[zmin; zmax]; % find offset and scale
imgray = double(imgray(:));
z = [ones(size(imgray)) imgray]*c; % apply offset and scale
z(z<zmin) = zmin; % apply thresholds
z(z>zmax) = zmax;
[x,y] = meshgrid(imx,imy);

r.type = 1; % node set
r.x = x(:);
r.y = y(:);
r.z = z;
objnew = oCreate(r); % form a new node set object


function [obj,undo] = img_rotate(obj,ang)
% if ~exist('imrotate','file')
%     warndlg('Image processing toolbox is required for image rotate.','Warning')
%     return
% end

undo.action = 'cedit';
undo.label = 'rotate image';
undo.r.obj = obj;

for io = 1:length(obj)
%     J = imrotate(obj.cdata,ang,'bilinear'); % ,'crop'
    [imr,cpr] = mk_imrotate(obj(io).cdata, ang/180*pi, [1; 1]); %%%%% exessive expansion in mk_imrotate
    
    sz = size(obj(io).cdata);
    x2i = (obj(io).imx(end)-obj(io).imx(1))/sz(2);
    y2i = (obj(io).imy(end)-obj(io).imy(1))/sz(1);
    
    joffset = (cpr(1)-1);
    ioffset = (cpr(2)-1);
    
    sz = size(imr);
    imx = obj(io).imx(1) + ((0:sz(2)-1)-joffset)*x2i;
    imy = obj(io).imy(1) + ((0:sz(1)-1)+ioffset)*y2i;

    obj(io).x = imx(1);
    obj(io).y = imy(1);
    obj(io).imx = imx;
    obj(io).imy = imy;
    obj(io).cdata = imr;
    
    [obj(io),err] = obj_update_data(obj(io),[]);
    if err
        disp('img_rotate: Problem updating an object.') %%% output to Command Window for now
    end
end


function cbCoordOffset(src,evd) %#ok<INUSD>
H = guidata(src);

prompt = {'X offset:','Y offset:','Z offset:'};
title = 'Specify offset values for horizontal axes and z';
lines = 1;
def = {'0','0','0'};
offset = inputdlg(prompt,title,lines,def);
if isempty(offset) % if Cancel was pressed
    return
end
xoffset = str2double(offset{1});
yoffset = str2double(offset{2});
zoffset = str2double(offset{3});

objn = unique(H.sel(:,1))';

[H.obj(objn),undo] = obj_offset(H.obj(objn),xoffset,yoffset,zoffset);
undo.r.objn = objn;
H.undo = undo_register(H,undo);

H.sel = clear_selection(H.hsel,[],[],H.tbl); % or adjust x,y of the selection markers

guidata(src,H)


function cbCoordScale(src,evd) %#ok<INUSD>
H = guidata(src);

prompt = {'X scale:','Y scale:','Z scale:'};
title = 'Specify scaling factor for horizontal axes and z';
lines = 1;
def = {'1','1','1'};
scale = inputdlg(prompt,title,lines,def);
if isempty(scale) % if Cancel was pressed
    return
end
xscale = str2double(scale{1});
yscale = str2double(scale{2});
zscale = str2double(scale{3});

objn = unique(H.sel(:,1))';

[H.obj(objn),undo] = obj_scale(H.obj(objn),xscale,yscale,zscale);
undo.r.objn = objn;
H.undo = undo_register(H,undo);

H.sel = clear_selection(H.hsel,[],[],H.tbl); % or adjust x,y of the selection markers

guidata(src,H)


function [obj,undo] = obj_offset(obj,xoffset,yoffset,zoffset)
for io = 1:length(obj)
    obj(io).x = obj(io).x + xoffset;
    obj(io).y = obj(io).y + yoffset;
    obj(io).z = obj(io).z + zoffset;
    obj(io).imx = obj(io).imx + xoffset; % empty stays empty
    obj(io).imy = obj(io).imy + yoffset;
    [obj(io),err] = obj_update_data(obj(io),[]);
    if err
        disp('obj_offset: Problem updating an object.') %%% output to Command Window for now
    end
end
undo.action = 'coordoffset';
undo.label = 'offset coordinates';
undo.r.xoffset = xoffset;
undo.r.yoffset = yoffset;
undo.r.zoffset = zoffset;


function [obj,undo] = obj_scale(obj,xscale,yscale,zscale)
for io = 1:length(obj)
    obj(io).x = obj(io).x * xscale;
    obj(io).y = obj(io).y * yscale;
    obj(io).z = obj(io).z * zscale;
    obj(io).imx = obj(io).imx * xscale; % empty stays empty
    obj(io).imy = obj(io).imy * yscale;
    [obj(io),err] = obj_update_data(obj(io),[]);
    if err
        disp('obj_scale: Problem updating an object.') %%% output to Command Window for now
    end
end
undo.action = 'coordscale';
undo.label = 'scale coordinates';
undo.r.xscale = xscale;
undo.r.yscale = yscale;
undo.r.zscale = zscale;


function cbTransformDefine(src,evd) %#ok<INUSD>
% Transform is defined by two 3-point objects.
% Select the two objetcs and call menu Transform->Define Transform.
% 1st 3-point object (lower in the object table) contains 3 points in the
% object to be transformed, e.g. 3 characteristic coastline features in the map image.
% 2nd 3-point object (upper in the object table) contains corresponding 3 points in the
% object to match, e.g. same 3 characteristic coastline features in the polyline.

H = guidata(src);

objn = unique(H.sel(:,1))'; % selected objects
r = H.obj(objn);

x1 = r(1).x';
y1 = r(1).y';
x2 = r(2).x';
y2 = r(2).y';

x1 = x1(~isnan(x1));
y1 = y1(~isnan(y1));
x2 = x2(~isnan(x2));
y2 = y2(~isnan(y2));

% transformation matrix is obtained from two 3-point sets or 3-point polylines
m = inv([x1 y1 ones(3,1)]) * [x2 y2 ones(3,1)]; %#ok<NASGU>

% save transformation matrix in MAT file
FilterSpec = '*.*';
DialogTitle = 'Save transformation matrix in MAT file';
DefaultName = envget('lastexport');
[FileName,PathName] = uiputfile(FilterSpec,DialogTitle,DefaultName);
if FileName
    [~,fname,fext] = fileparts(FileName);
    if strcmp(fext,'.*')
        FileName = fname;
    end
    save([PathName FileName],'m');
    envset('lastexport',PathName)
end


function cbTransformApply(src,evd) %#ok<INUSD>
% Apply earlier defined transformation to selected objects.

% read matrix from earler saved mat file
filterspec = ...
  {'*.mat',      'MAT-files (*.mat)'; ...
   '*.*',        'All Files / XYZ Text Files (*.*)'};
dialogtitle = 'Select file with transformation matrix';
DefaultName = envget('lastexport');
[filename,pathname] = uigetfile(filterspec,dialogtitle,...
    DefaultName,'MultiSelect','off');
if length(filename)==1 && ~filename
    return
end
fullfile = [pathname filename];
m = load(fullfile);
if ~isfield(m,'m') || ~all(size(m.m)==3)
    errordlg('File must contain 3x3 matrix named m.')
end
m = m.m;
        
H = guidata(src);
objn = unique(H.sel(:,1))';
[H.obj(objn),undo] = obj_transform(H.obj(objn),m,H.tbl);
undo.r.objn = objn;
H.undo = undo_register(H,undo);
H.sel = clear_selection(H.hsel,[],[],H.tbl); % or adjust x,y of the selection markers
guidata(src,H)


function [obj,undo] = obj_transform(obj,m,tbl)
undo.action = 'coordtransform';
undo.label = 'transform';
for io = 1:length(obj)
    [obj(io),r.obj(io)] = obj_transform1(obj(io),m,tbl);
end
r.remsizexdata = [];
undo.r = r;


function [obj,robj] = obj_transform1(obj,m,tbl)
robj = struct('type',[],'x',[],'y',[],'imx',[],'imy',[],'cdata',[]);
robj.type = obj.type;
robj.x = obj.x;
robj.y = obj.y;
if obj.type==4 % image
    robj.imx = obj.imx;
    robj.imy = obj.imy;
    robj.cdata = obj.cdata;
    [obj.imx,obj.imy,obj.cdata] = imtransform(obj.imx, obj.imy, obj.cdata, m);
    obj.x = obj.imx(1);
    obj.y = obj.imy(1);
else % node-containing object
    ival = ~isnan(obj.x);
    xy = inv(m)'*[obj.x(ival); obj.y(ival); ones(1,length(obj.x(ival)))];
    obj.x(ival) = xy(1,:);
    obj.y(ival) = xy(2,:);
end
obj = obj_update_data(obj,tbl);


function cbCoord2UTM(src,evd) %#ok<INUSD>
H = guidata(src);
objn = unique(H.sel(:,1))';

% default UTM zone for dialog is the one common to all zones recorded for
% all selected objects
defzone = NaN(length(objn),1);
for io = 1:length(objn)
    if isfield(H.obj(objn(io)).meta,'utmzone')
        defzone(io) = H.obj(objn(io)).meta.utmzone;
    end
end
defzone = unique(defzone);
if length(defzone)>1 % more than one unique zone encountered
    defzone = [];
end

utmzone = dlgUTMzone(defzone);
if ~utmzone
    return
end

[H.obj(objn),undo] = obj_2UTM(H.obj(objn),utmzone);
undo.r.objn = objn;
H.undo = undo_register(H,undo);
H.sel = clear_selection(H.hsel,[],[],H.tbl); % or adjust x,y of the selection markers
guidata(src,H)


function [obj,undo] = obj_2UTM(obj,utmzone)
undo.action = 'coordtransform';
undo.label = 'convert to UTM';
for io = 1:length(obj)
    [obj(io),r.obj(io)] = obj1_2UTM(obj(io),utmzone);
end
r.remsizexdata = 'utmzone';
undo.r = r;


function [obj,robj] = obj1_2UTM(obj,utmzone)
robj = struct('type',[],'x',[],'y',[],'imx',[],'imy',[],'cdata',[]);
robj.type = obj.type;
robj.x = obj.x;
robj.y = obj.y;
if obj.type==4 % image
    robj.imx = obj.imx;
    robj.imy = obj.imy;
    [obj.imx, ~, utmzone] = geo2utm(repmat(obj.y,size(obj.imx)), obj.imx,utmzone);
    % use zone output by geo2utm (important if default utmzone was specified):
    [~, obj.imy, utmzone] = geo2utm(obj.imy, repmat(obj.x,size(obj.imy)),utmzone);
    obj.x = obj.imx(1);
    obj.y = obj.imy(1);
%     utmzone = utmzone(1,:);
else % node-containing object
    ival = ~isnan(obj.x);
    [obj.x(ival), obj.y(ival), utmzone] = geo2utm(obj.y(ival), obj.x(ival), utmzone);
    % use zone output by geo2utm (important if default utmzone was specified)
end
obj.meta.utmzone = utmzone;
obj = obj_update_data(obj,[]);


function [obj,redo] = obj_undocoordtransform(obj,robj,remsizexdata)
redo.action = 'coordtransform';
redo.label = 'coordinate transform';
for io = 1:length(obj)
    obj(io).x = robj(io).x;
    obj(io).y = robj(io).y;
    if obj(io).type==4 % image
        obj(io).imx = robj(io).imx;
        obj(io).imy = robj(io).imy;
        if ~isempty(robj(io).cdata)
            obj(io).cdata = robj(io).cdata;
        end
    end
    obj(io) = obj_update_data(obj(io),[]);
    if ~isempty(remsizexdata) && isfield(obj(io).meta, remsizexdata)
%         r.obj(io).remsizexdata = obj(io).meta.(remsizexdata);
        obj(io).meta = rmfield(obj(io).meta, remsizexdata);
    end
end


function cbCoord2Geo(src,evd) %#ok<INUSD>
H = guidata(src);
objn = unique(H.sel(:,1))';
[H.obj(objn),undo] = obj_2geo(H.obj(objn));
undo.r.objn = objn;
H.undo = undo_register(H,undo);
H.sel = clear_selection(H.hsel,[],[],H.tbl); % or adjust x,y of the selection marers
guidata(src,H)


function [obj,undo] = obj_2geo(obj)
undo.action = 'coordtransform';
undo.label = 'convert to geographic coordinates';
for io = 1:length(obj)
    [obj(io),r.obj(io)] = obj1_2geo(obj(io));
end
r.remsizexdata = [];
undo.r = r;


function [obj,robj] = obj1_2geo(obj)
   
% for all objects (node-containing objects and images)
ival = ~isnan(obj.x);
nval = length(find(ival));
if isfield(obj.meta,'utmzone')
    utmzone = obj.meta.utmzone;
    if size(utmzone,1)~=1 && size(utmzone,1)~=nval
        utmzone = false;
    end
else
    utmzone = false;
end
if ~utmzone
    prompt = {['Provide UTM zone for conversion of the ' obj_string(obj) ', e.g. ''09 U''']};
    title = 'Provide UTM zone';
    lines = 1;
    def = {''};
    utmzone = inputdlg(prompt,title,lines,def);
    if ~isempty(utmzone) % if OK was pressed
        utmzone = utmzone{1};
        if all((utmzone>='0' & utmzone<='9') | utmzone=='.')
            utmzone = str2double(utmzone);
        end
    else
        robj = obj; % no change for undo
        return
    end
end

robj = struct('type',[],'x',[],'y',[],'imx',[],'imy',[],'cdata',[]);
robj.type = obj.type;
robj.x = obj.x;
robj.y = obj.y;
if obj.type==4 % image
    robj.imx = obj.imx;
    robj.imy = obj.imy;
    [~, obj.imx] = utm2geo(obj.imx, repmat(obj.y,size(obj.imx)), utmzone);
    obj.imy      = utm2geo(repmat(obj.x,size(obj.imy)), obj.imy, utmzone);
    obj.x = obj.imx(1);
    obj.y = obj.imy(1);
else % node-containing object
    ival = ~isnan(obj.x);
%     [obj.y(ival), obj.x(ival)] = utm2deg(obj.x(ival), obj.y(ival), utmzone);
    [obj.y(ival), obj.x(ival)] = utm2geo(obj.x(ival), obj.y(ival), utmzone);
end
obj = obj_update_data(obj,[]);


function c = sel_string(obj,sel)
% Cell array of strings representing selected objects and nodes.
ns = size(sel,1);
c = cell(ns,1);
for is = 1:ns
    ostring = obj_string(obj(sel(is,1)));
    c{is} = ['Object ' num2str(sel(is,1)) ': ' ostring '  Node ' num2str(sel(is,2))];
end


function ostring = obj_string(obj1)
switch obj1.type
    case 1
        ostring = 'Set of nodes';
        szstr = [num2str(length(obj1.x(~isnan(obj1.x)))) ' nodes'];
        if obj_manedit(obj1)==1
            % for "original" node sets (manual edit mode)
            ostring = [' ^ ' ostring];
        elseif obj_manedit(obj1)==2
            % for "confirmed" node sets (manual edit mode)
            ostring = [' * ' ostring];
        end
    case 2
        ns = length(find(isnan(obj1.x))); % number of continous segments
        if ns==1 && obj1.x(1)==obj1.x(end-1) && obj1.y(1)==obj1.y(end-1)
            ostring = 'Polygon';
            szstr = [num2str(length(obj1.x)-2) ' nodes']; % ignore terminating NaN and consider 1st and last node the same
        else
            ostring = 'Polyline';
            if ns==1
                szstr = [num2str(length(obj1.x)-1) ' nodes']; % ignore trminating NaN
            else
                szstr = [num2str(ns) ' segments, ' num2str(length(obj1.x)-ns) ' nodes']; % ignore terminating NaNs
            end
        end
    case 3
        ostring = 'Grid';
        szstr = [num2str(length(obj1.x)) ' nodes'];
    case 4
        ostring = 'Image';
        sz = size(obj1.cdata);
        szstr = [num2str(sz(1)) 'x' num2str(sz(2)) ' pixels'];
    otherwise
        ostring = 'Object';
        szstr = '';
end
ostring = [ostring ' (' szstr ')'];
if ~isempty(obj1.comment)
    ostring = [ostring ': ' obj1.comment];
end


function [out,con] = obj_manedit(obj)
% Determine if objects are in manual edit mode.

n = length(obj);
out = zeros(1,n);
con = NaN(n,1);
for k = 1:n
    [out(k),con(k)] = obj_manedit1(obj(k));
end

function [out,con] = obj_manedit1(obj1)

out = 0; % not in the manual edit mode
con = NaN;
if isstruct(obj1.meta)
    if isfield(obj1.meta,'obj_orig')
        out = 2; % "confirmed" object
        con = obj1.meta.obj_orig;
    elseif isfield(obj1.meta,'obj_confirmed')
        out = 1; % "original" object
        con = obj1.meta.obj_confirmed;
    end
end


function [objn,iy] = pickobj(fig,allowabletypes,objn)
% allowabletypes    array of object types that can be picked
% if allowabletypes == 4 and objn supplied picks a pixel from an image

% neutralize callbacks - necessary for Matlab 2007b (Version 7.5), 
% but not for 2008b (Version 7.7)
WindowButtonDownFcn=get(fig,'WindowButtonDownFcn');
WindowButtonMotionFcn=get(fig,'WindowButtonMotionFcn');
WindowButtonUpFcn=get(fig,'WindowButtonUpFcn');
set(fig,'WindowButtonDownFcn','');
set(fig,'WindowButtonMotionFcn','');
set(fig,'WindowButtonUpFcn','');

% change cursor
% if length(allowabletypes)==1 && allowabletypes==2
%     p = imread('pointer_arrow_pick_poly.bmp');
% else
    p = imread('pointer_arrow_pick.bmp'); % 1 - white, 0 - black
% end

%       1's where you want the pixel black
%       2's where you want the pixel white
%       NaNs where you want the pixel transparent
p = double(~p); % now 1 is black
p(~p) = NaN; % make 0-pixels transparent
set(fig,'Pointer','custom','PointerShapeCData',p,'PointerShapeHotSpot',[9 1])

H = guidata(fig);

if length(allowabletypes)==1 && allowabletypes==4 && exist('objn','var')
    pickpix = true;
    obj = H.obj(objn);
else
    pickpix = false;
end

figure(fig) % focus on main window
while true
    w = waitforbuttonpress;
    if w == 0 % mouse click
        cp = get(H.ah,'CurrentPoint'); % cursor position
        if pickpix
            % distances from the centres of the pixels
            dx = abs(obj.imx-cp(1,1));
            dy = abs(obj.imy-cp(1,2));
            dxmin = min(dx);
            dymin = min(dy);
            
            % if click is on the image
            if dxmin<=mean(diff(obj.imx))/2 && dymin<=mean(diff(obj.imy))/2
                ix = find(dx==dxmin,1,'first');
                iy = find(dy==dymin,1,'first');
                objn = ix;
                break
            end
        else
%             % old code (can pick locked objects as well)
%             ifoc = ffocus1(H.ah,[H.obj.x],[H.obj.y],cp(1,1),cp(1,2),H.pthres,true); % all within the range
%             objn = [H.obj.objn];
            % work on unlocked objects only
            obj = H.obj(obj_isfree(H.tbl));
            % nodes' coordinates for all unlockled objects
            x = [obj.x];
            y = [obj.y];
            ifoc = ffocus1(H.ah,x,y,cp(1,1),cp(1,2),H.pthres,true); % all within the range
            objn = [obj.objn];
            
            objn = objn(ifoc); % all objects with nodes within the sensitivity range of the mouse pointer
            objn = objn(ismember([H.obj(objn).type],allowabletypes)); % only allowable object types
            if length(objn)>1
                objn = objn(1); % take 1st object in the list in case there are several objects picked
            end
            if isempty(objn) && any(allowabletypes==2) % for polylines only
                % if no node within the range and we are looking for polylines,
                % check if any edge is within the range
                objn = ffocus2(fig,cp(1,1),cp(1,2)); % this picks only unlocked objects
            end
            break
        end
    else % key press
        key = double(get(fig,'CurrentCharacter'));
        if key==27 % 'escape'
            objn = 0;
            break
        elseif key==13 || key==32 % 'space' or 'enter'
            objn = -1;
            break
        end
    end
end

% restore cursor
set(fig,'Pointer','arrow')

% restore callbacks
set(gcf,'WindowButtonDownFcn',WindowButtonDownFcn);
set(gcf,'WindowButtonMotionFcn',WindowButtonMotionFcn);
set(gcf,'WindowButtonUpFcn',WindowButtonUpFcn);



% Object rearrange routines
%--------------------------

function cToFrontOnce(src,evd) %#ok<INUSD>
H = guidata(src);

objnu = unique(H.sel(:,1))'; % numbers of objects selected, sorted

[H.obj,undothis] = oToFrontOnce(H.obj,objnu,H.tbl);
H.undo = undo_register(H,undothis);

guidata(src,H)


function cToBackOnce(src,evd) %#ok<INUSD>
H = guidata(src);

objnu = unique(H.sel(:,1))'; % numbers of objects selected, sorted

[H.obj,undothis] = oToBackOnce(H.obj,objnu,H.tbl);
H.undo = undo_register(H,undothis);

guidata(src,H)


function cbToFront(src,evd) %#ok<INUSD>
H = guidata(src);

objnu = unique(H.sel(:,1))'; % numbers of objects selected, sorted

[H.obj,undothis] = oToFront(H.obj,objnu,H.tbl);
H.undo = undo_register(H,undothis);

guidata(src,H)


function cbToBack(src,evd) %#ok<INUSD>
H = guidata(src);

objnu = unique(H.sel(:,1))'; % numbers of objects selected, sorted

[H.obj,undothis] = oToBack(H.obj,objnu,H.tbl);
H.undo = undo_register(H,undothis);

guidata(src,H)


function cbSaveInBase(src,evd) %#ok<INUSD>
% Save selected objects in a base workspace variable.

%%%%%%%%%%%%%%%% use these standard functions
% [hdialog,ok_pressed] = export2wsdlg() % export to workspace
% uisave(variables,filename) % save in file

H = guidata(src);
objn = unique(H.sel(:,1))'; % numbers of objects selected, sorted

prompt = {'Name to assign to the variable:'};
title = 'Save objects in the base workspace';
lines = 1;
def = {''};
answer = inputdlg(prompt,title,lines,def);
if ~isempty(answer) % if OK was pressed
    assignin('base',answer{1},H.obj(objn));
end


function cbSaveInFile(src,evd,selectionflag) %#ok<INUSL>
% Save selected polyline objects in a mat file.

FilterSpec = '*.*';
DialogTitle = 'Save objects in MAT file';
% DefaultName = ['VisualGrid_export_' datestr(now,'yyyy_mmm_dd_HH_MM')];
DefaultName = envget('lastexport');

[FileName,PathName] = uiputfile(FilterSpec,DialogTitle,DefaultName);

if FileName
    H = guidata(src);
    if selectionflag
        isave = unique(H.sel(:,1))';
    else
        isave = 1:length(H.obj);
    end
    r = H.obj(isave); %#ok<NASGU> % objects selected
    [~,fname,fext] = fileparts(FileName);
    if strcmp(fext,'.*')
        FileName = fname;
    end
    save([PathName FileName],'r');
    
    envset('lastexport',PathName)
end


function cbSaveNGH(src,evd) %#ok<INUSD>
% Save selected triangular grid in an NGH file.

FilterSpec = '*.ngh';
DialogTitle = 'Save grid';
% DefaultName = 'grid.ngh'; % ['grid_' datestr(now,'yyyy_mmm_dd_HH_MM')];

DefaultName = envget('lastexport');

H = guidata(src);

% strip of the extension, it must be one from the filterspec list
[~,fname] = fileparts(H.currentfile);
DefaultName = [DefaultName filesep fname];

[FileName,PathName] = uiputfile(FilterSpec,DialogTitle,DefaultName);
if FileName
    H = guidata(src);
    objn = unique(H.sel(:,1))';
    
    %%%%% enable menue item only when the condition is true
    if length(objn)~=1
        warndlg('Only one grid at a time can be saved','Warning')
        return
    end
    
%     %%%%% make sure extension is ngh
%     [fdir,fname,fext] = fileparts(FileName);
%     if strcmp(fext,'.*')
%         FileName = fname;
%     end
    
    obj = H.obj(objn);
    x = obj.x;
    y = obj.y;
    tri = double(obj.tri);
    con = tri2con(tri,length(x)); % my vectorized version
    offset_scale = [0 0 1 1]; % x-off,y-off,x-scale,y-scale
    writengh([PathName FileName],x,y,obj.z,obj.code,1:length(x),con,offset_scale,obj.comment)
    dlmwrite([PathName FileName(1:end-3) 'tri'], [(1:size(tri,1))' tri], 'precision','%7d','delimiter','')
    
    envset('lastexport',PathName)
end


function cbSaveNOD(src,evd) %#ok<INUSD>
% Save selected polylines and sets of nodes in a NOD file.

FilterSpec = '*.nod';
DialogTitle = 'Save NOD file';
% DefaultName = 'nodes.nod'; % ['nodes_' datestr(now,'yyyy_mmm_dd_HH_MM')];

DefaultName = envget('lastexport');

H = guidata(src);

% strip of the extension, it must be one from the filterspec list
[~,fname] = fileparts(H.currentfile);
DefaultName = [DefaultName filesep fname];

[FileName,PathName] = uiputfile(FilterSpec,DialogTitle,DefaultName);
if FileName
    H = guidata(src);
    objn = unique(H.sel(:,1))';

%     %%%%% make sure extension is ngh
%     [fdir,fname,fext] = fileparts(FileName);
%     if strcmp(fext,'.*')
%         FileName = fname;
%     end
    
    obj = H.obj(objn);
    obj([obj.type]==3) = []; % no grids allowed
    
    objp = obj([obj.type]==2); % polylines
    polyz = [[objp.x]' [objp.y]' [objp.z]'];
    
    objp = obj([obj.type]==1); % sets of nodes
    xyz = [[objp.x]' [objp.y]' [objp.z]'];
    
    writenod([PathName FileName],polyz,xyz)
    
    envset('lastexport',PathName)
end


function cbSaveXYZ(src,evd) %#ok<INUSD>
% Save selected node-based objects in an XYZ file.

FilterSpec = '*.txt';
DialogTitle = 'Save XYZ file';
% DefaultName = 'nodes.txt'; % ['nodes_' datestr(now,'yyyy_mmm_dd_HH_MM')];

DefaultName = envget('lastexport');

H = guidata(src);

% strip of the extension, it must be one from the filterspec list
[~,fname] = fileparts(H.currentfile);
DefaultName = [DefaultName filesep fname];

[FileName,PathName] = uiputfile(FilterSpec,DialogTitle,DefaultName);
if FileName
    objn = unique(H.sel(:,1))';
    
    obj = H.obj(objn);
    otype = [obj.type];
    
    objp = obj(otype==2); % polylines
    polyz = [[objp.x]' [objp.y]' [objp.z]'];
%     polyz(isnan([objp.x]),:) = []; % remove terminating NaNs
    
    objp = obj(otype==1 | otype==3); % sets of nodes or grids
    xyz = [[objp.x]' [objp.y]' [objp.z]'];
    
    dlmwrite([PathName FileName],[polyz; xyz],'precision','%f','newline','pc')
    
    envset('lastexport',PathName)
end


function cbSaveIndices(src,evd) %#ok<INUSD>
% Save indices for selected nodes to a one-column ascii file.

FilterSpec = '*.txt';
DialogTitle = 'Save XYZ file';
% DefaultName = 'nodes.txt'; % ['nodes_' datestr(now,'yyyy_mmm_dd_HH_MM')];

DefaultName = envget('lastexport');

H = guidata(src);

% strip of the extension, it must be one from the filterspec list
[~,fname] = fileparts(H.currentfile);
DefaultName = [DefaultName filesep fname];

[FileName,PathName] = uiputfile(FilterSpec,DialogTitle,DefaultName);
if FileName
    noden = H.sel(:,2);
    
    dlmwrite([PathName FileName],noden,'precision','%d','newline','pc')
    
    envset('lastexport',PathName)
end


function val = envget(envfield)
% Get a field from VisualGrid environment file.

up = userpath; % location of env file
if up(end)==';' ||  up(end)==':'
    up(end) = [];
end
envname = [up filesep 'VisualGrig_env.mat'];
val = '';
if exist(envname,'file')
    env = load(envname);
    env = env.env;
    if isfield(env,envfield)
        val = env.(envfield);
    end
end


function envset(envfield,val)
% Set a field in VisualGrid environment file.

up = userpath; % location of env file
if up(end)==';' ||  up(end)==':'
    up(end) = [];
end
envname = [up filesep 'VisualGrig_env.mat'];
if exist(envname,'file')
    env = load(envname);
    env = env.env;
end
if val(end)==filesep
    val(end) = [];
end
env.(envfield) = val;
save(envname,'env');


function cbImportVariables(src,evd) %#ok<INUSD>
% Import object(s) from the base workspace.

varlist = evalin('base','who');
if isempty(varlist)
    warndlg('No variables to import');
    return
end

[selection,okpressed] = listdlg('PromptString','Select a variable:',...
                                'SelectionMode','multiple',...
                                'ListString',varlist);
%             'SelectionMode','single' or 'multiple'

H = guidata(src);
% [selection,okpressed,pp] = dlgLoadVar('PromptString','Select a variable:',...
%                 'SelectionMode','multiple',...
%                 'ListString',varlist,'PlotParam',H.ppdef);

if ~okpressed % Cancel was pressed or the dialogue was closed
    return
end

for is = 1:length(selection) % for each selected variable in the base workspace
    r = evalin('base',varlist{selection(is)});
    
    [r,otype,err] = parseStruct(r);
    if otype == 0
        warndlg(err,['Skipping variable ' varlist{selection(is)}])
        continue % skip this variable
    end
    
    onew = oCreate(r,[],otype);
    
    pp = H.ppdef;
    
    %TODO generate new color for each new object
    
    if onew.type==3 % no markers for grids
        pp.marker = 'none';
    end
    
    [okpressed,pp] = dlgPlotParam(pp);
    if okpressed % OK was pressed
        [H.obj,undothis] = obj_load(H.obj,onew,H.ah,pp,H.tbl);
        H.undo = undo_register(H,undothis);
        guidata(src,H)
    else
        warndlg('Import cancelled.')
        return
    end    
end


function [r,otype,err] = parseStruct(r)
% Determine type of object and pack data to r for import/open command.

err = '';

% determine the type of object
if isstruct(r) && isfield(r,'type')
    otype = r.type;
else
    otype = 0;
    err = 'Unrecognized object type';
    if isstruct(r) && isfield(r,'x') && isfield(r,'y')
        
        % remove empty elements
        r(arrayfun(@(x) isempty(x.x), r)) = [];
        %TODO issue message if any elements were removed as empty

        % check all elements of the structure array for type consistency
        for ir = 1:length(r)

            % field length consistency
            if length(r(ir).x) ~= length(r(ir).y)
                otype = 0;
                err = 'Fields of different lengths';
                break
            end

            % determine the type of the current element
            if isnan(r(ir).x(end)) && isnan(r(ir).y(end)) % NaN-terminated: polyline
                otype_ = 2; % polyline
            elseif isfield(r,'tri') % no NaN termination and triangulation array is there: grid
                otype_ = 3; % grid
            else % no NaN termination and no triangulation array: node set
                otype_ = 1; % node set
            end

            % check for type consistency
            if ir == 1
                otype = otype_;
            elseif otype ~= otype_
                otype = 0;
                err = 'Different objects in the selected structure';
                break
            end
        end
    end
end

if otype == 0
    return
end

if otype == 2 % polyline
    % load as one multiply-connected polyline
    rc.x = [r.x];
    rc.y = [r.y];
    if isfield(r,'z')
        rc.z = [r.z];
    end
    if isfield(r,'code')
        rc.code = [r.code];
    end
    r = rc;
end


function tf = fileIsUptodate(H)
% Check if current file is uptodate.

tf = strcmp(get(H.tools.filesave,'Enable'),'off');


function undostack = fileSetUptodate(H,uptodatestate)
% Set GUI's current file uptodate state to true or false depending on uptodatestate.

undostack = H.undo;
if uptodatestate 
    set(H.tools.filesave,'Enable','off');
    set(H.menu.filei.save,'Enable','off');
    if ~isempty(undostack)
        [undostack.filesaved] = deal(false); % only one undo can have filesaved==true
        undostack(end).filesaved = true;
    end
else
    set(H.tools.filesave,'Enable','on');
    set(H.menu.filei.save,'Enable','on');
end


function cbFileNew(src,evd) %#ok<INUSD>
% File New callback.

H = guidata(src);

if ~fileMakeSureSaved(H) % request for file save, returns false on Cancel press
    return
end

% clear previous work
[H.obj,H.grp,H.sel,H.undo,H.currentfile,H.notes] = workNew(H.hsel,[H.obj.h],H.tbl);

H.undo = fileSetUptodate(H,true);

set(H.hfm,'Name','VisualGrid')

guidata(src,H)
    

function cbFileOpen(src,evd) %#ok<INUSD>
% File Open callback.

H = guidata(src);

if ~fileMakeSureSaved(H) % request for file save, returns false on Cancel press
    return
end

% % clear previous work
% [H.obj,H.grp,H.sel,H.undo,H.currentfile,H.notes] = workNew(H.hsel,[H.obj.h],H.tbl);
% H.undo = fileSetUptodate(H,true);

% request for a new file and load objects in the specified file
[ok,H,~,fullfile,vgundo,vgnotes] = fileLoad(H,true);
if ~ok
    if ~isempty(H) % H is an error message if ~ok
        errordlg(H,'File load error')
    end
else
    % new undo is written to H only if vg file loaded
    if ~isempty(vgundo) %%% functionality: undo is saved in VG files
        H.undo = vgundo;
        set(H.tools.undo,'enable','on')
    end
    H.currentfile = fullfile;
    H.undo = fileSetUptodate(H,true); % force the Updated state (was changed when loading objects in fileLoad)
    if ~isempty(vgnotes)
        H.notes = vgnotes;
    end
    set(H.hfm,'Name',['VisualGrid - ' fullfile])
    guidata(src,H)
end


function cbFileSave(src,evd) 
% File Save callback.

H = guidata(src);


[~,~,ext] = fileparts(H.currentfile);
if strcmpi(ext,'vg') % protect non-VG files from being overwritten
    [ok,err] = fileSaveVG(H, H.currentfile); % saves a VG file, returns true on success
    if ~ok
        errordlg({['Error writing file: ' H.currentfile],err});
    else
        H.undo = fileSetUptodate(H,true);
        guidata(src,H)
    end
else % for non-VG files, call the "save as" function
    cbFileSaveAs(src,evd)
end


function cbFileSaveAs(src,evd) %#ok<INUSD>
% File Save As callback.

H = guidata(src);
[ok,fullfile] = fileRequestSave(H);
if ok
    H.currentfile = fullfile;
    H.undo = fileSetUptodate(H,true);
    set(H.hfm,'Name',['VisualGrid - ' fullfile])
    guidata(src,H)
elseif ~isempty(fullfile) % fullfile contains error string if ~ok
    errordlg(fullfile);
end


function cbFileImport(src,evd) %#ok<INUSD>
% File Import callback.

H = guidata(src);

% request for a new file and load objects in the specified file
[ok,H,undothis,~,~,vgnotes] = fileLoad(H,false);
if ~ok
    if ~isempty(H) % H is an error message if ~ok
        errordlg(H,'File load error')
    end
else
    H.undo = undo_register(H,undothis);
%     NotUptaded state was set when loading objects in fileLoad
%     fileSetUptodate(H,false)
    if ~isempty(vgnotes)
        [ok,vgnotes] = dlgNotes(vgnotes,'Treatment of imported file notes','Append','Discard');
        if ok
            if isempty(H.notes)
                H.notes = char(vgnotes);
            else
                H.notes = char(H.notes,char(vgnotes));
            end
        end
    end
    guidata(src,H)
end


function cbExit(src,evd) %#ok<INUSD>
close(gcbf)


function cbTableEdit(src,evd)
% File Import callback.

H = guidata(src);

objn = evd.Indices(1);
coln = evd.Indices(2);

dat = get(src,'Data');
if coln==2 % visibilty is changed
    if evd.NewData % turned on
        dat{objn,1} = true; % turn editablitiy on as well
        obj_visibility(H.obj(objn),1)
    else % turned off
        dat{objn,1} = false;  % turn editablitiy off as well
        obj_visibility(H.obj(objn),0)
    end
end
set(src,'Data',dat)

%%% doesn't work
% figure(H.hfm) % focus on main window
% axes(H.ah) %#ok<MAXES> % focus on main axes
% uicontrol(H.hleft)
% setfocus(H.hleft) % this simulates a clic with all the consequences, e.g. deselects previously selected nodes

jscroll = findjobj(src);
jtable = jscroll.getViewport.getComponent(0);
jtable.transferFocusBackward() % table loses focus but does not reset keyboard callbacks
set(H.hfm,'KeyPressFcn',{@wkpcb,''})

% guidata(src,H)


function obj_visibility(obj1,vis)

if vis
    % make visible the object nodes/lines
    % but only those quality markers flagged in obj1.pp
    obj_set_visible(obj1)
else
    % all handles - visibily off, but visibility flags for quality markers
    % are presserved in obj.pp
    h = getHandles(obj1);
    h(~ishandle(h)) = [];
    set(h,'Visible','off')
end


function obj_set_visible(obj)
% Set visibility of an object according to pp.

otype = obj.type;
pp = obj.pp;

set(obj.h.c,'Visible','on');

if otype==2 % polyline
    if ~isempty(pp.endmarkerflag) && pp.endmarkerflag
        set(obj.h.c1st, 'Visible','on');
        set(obj.h.clast,'Visible','on');
    end
end

if otype==1 ||otype==2 || otype==3 % any object containing nodes
    if ~isempty(pp.codemarkerflag) && pp.codemarkerflag
        set(obj.h.codemarker,'Visible','on');
    end
end

if otype==2 || otype==3 % polyline or grid
    if ~isempty(pp.badlenflag) && pp.badlenflag
        set(obj.h.badlen,'Visible','on');
    end
end

if otype==2 % polyline
    if ~isempty(pp.badangflag) && pp.badangflag
        set(obj.h.badang,'Visible','on');
    end
    if ~isempty(pp.badratflag) && pp.badratflag
        set(obj.h.badrat,'Visible','on');
    end
end

if otype==3 % grid
    if ~isempty(pp.eqlflag) && pp.eqlflag
        set(obj.h.eql,'Visible','on');
    end
end

if otype==4 % image
    set(obj.h.img,'Visible','on');
end



function [ok,fullfile] = fileRequestSave(H)

FilterSpec = ...
  {'*.vg',       'VisualGrid Files (*.vg)'; ...
   '*.mat',      'MAT-files (*.mat)'; ...
   '*.*',        'All Files (*.*)'};
DialogTitle = 'Save file';

% strip of the extension, it must be one from the filterspec list
[fpath,fname] = fileparts(H.currentfile);
DefaultName = [fpath filesep fname];

[FileName,PathName,FilterIndex] = uiputfile(FilterSpec,DialogTitle,DefaultName);

if FileName
    [~,~,dext] = fileparts(FilterSpec{FilterIndex,1}); % default extension
    
    % change extension to the one from the list
    if ~strcmp(dext,'.*')
        [~,fname] = fileparts(FileName);
        FileName = [fname dext];
    end
    fullfile = [PathName FileName];
    
    [ok,err] = fileSaveVG(H,fullfile);
    if ~ok
        if isstruct(err)
            err = err.message;
        end
%         fullfile = {['Error writing file\n' fullfile '\n'],err};
        fullfile = {'Error writing file',fullfile,err};
    else
        ok = true;
    end
else
    ok = false;
    fullfile = [];
end


function [ok,err] = fileSaveVG(H,fname)
% Save a VG file, return true on success.

err = '';

obj = H.obj; %#ok<NASGU>
undo = H.undo; %#ok<NASGU>
notes = H.notes; %#ok<NASGU>

% protect non-VG files from being overwritten by adding vg extension to fname
[~,~,ext] = fileparts(fname);
if ~strcmpi(ext,'.vg')
%     ok = false;
%     return
    fname = [fname '.vg'];
end

try
    save(fname,'obj','undo','notes'); % fname with no extension results in .mat 
                          % extension added to the saved file name
                          
    ok = true;
catch err
    ok = false;
    err = err.message;
end


function ok = fileMakeSureSaved(H)

ok = true;
if ~fileIsUptodate(H)
    button = questdlg('Save current file?','Current file is not saved','Yes') ;
    switch lower(button)
        case 'yes'
            [ok,fullfile] = fileRequestSave(H);
            if ok
                H.currentfile = fullfile;
            elseif ~isempty(fullfile) % fullfile contains error string if ~ok
                errordlg(fullfile);
            end
        case 'cancel'
            ok = false;
        % case 'no' - do nothing, proceed to file open
    end     
end


function [success,H,undo,fullfile,vgundo,vgnotes] = fileLoad(H,clearflag)
% Asks user for a file and adds objects from the specified file to the current
% work.

undo = [];

filterspec = ...
  {'*.vg',       'VisualGrid Files (*.vg)'; ...
   '*.nod;*.NOD','NOD Files (*.nod,*.NOD)'; ...
   '*.ngh;*.NGH','NGH Files (*.ngh,*.NGH)'; ...
   '*.shp;*.SHP','Shape Files (*.shp)'; ...
   '*.jpg;*.gif;*png;*.tif;','Image Files (*.jpg,*.gif,*png,*.tif)'; ...
   '*.mat',      'MAT-files (*.mat)'; ...
   '*.*',        'All Files / XYZ Text Files (*.*)'};
dialogtitle = 'Select file(s) to load';
if clearflag % "open"
    DefaultName = envget('lastfolder');
    MultiSelect = 'off';
else % "import"
    DefaultName = envget('lastimport');
    MultiSelect = 'on';
end
[filename,pathname,filterindex] = uigetfile(filterspec,dialogtitle,...
    DefaultName,'MultiSelect',MultiSelect);
if length(filename)==1 && ~filename
    success = false;
    H = '';
    fullfile = '';
    vgundo = [];
    vgnotes = [];
    return
end

if clearflag % "open"
    envset('lastfolder',pathname);
    
    % clear previous work
    [H.obj,H.grp,H.sel,H.undo,H.currentfile,H.notes] = ...
        workNew(H.hsel,[H.obj.h],H.tbl);
    H.undo = fileSetUptodate(H,true);
    
else % "import"
    envset('lastimport',pathname);
end

if filterindex == 7 && ~iscell(filename) % '*.*'
    [~,~,ext] = fileparts(filename);
    % try to determine file type by its extension
    switch lower(ext)
        case '.vg'
            filterindex = 1;
        case '.nod'
            filterindex = 2;
        case '.ngh'
            filterindex = 3;
        case '.shp'
            filterindex = 4;
        case '.jpg'
            filterindex = 5;
        case '.jpeg'
            filterindex = 5;
        case '.gif'
            filterindex = 5;
        case '.png'
            filterindex = 5;
        case '.tif'
            filterindex = 5;
        case '.tiff'
            filterindex = 5;
        case '.mat'
            filterindex = 6;
%         otherwise
%             filterindex = 7; % assign 7 to xyz text files
    end
end

success = 0;
vgundo = []; %%% functionality: undo is saved in VG files
vgnotes = [];

if iscell(filename) && filterindex ~= 7 % no import of multiple files except for xyz files
    return
end

if iscell(filename)
    fullfile = cellfun(@(x) [pathname x],filename,'UniformOutput',false);
else
    fullfile = [pathname filename];
end

switch filterindex
    case 1 % native VisualGrid files
        try
        % VG is a mat file with .vg extension (must contain obj and undo
        % variables according to VisualGrid data structure)
        vg = load(fullfile,'-mat');

        
        % Resolve compatibility issues
        %%% ensure backwards compatibility with versions before V2.73
        if ~isfield(vg.obj,'cdata')
            [vg.obj.cdata] = deal(false);
        end
        if ~isfield(vg.obj,'cmap')
            [vg.obj.cmap] = deal(false);
        end
        if isfield(vg.obj,'rgb')
            for io = 1:length(vg.obj)
                vg.obj(io).cmap = vg.obj(io).rgb;
            end
            vg.obj = rmfield(vg.obj,'rgb');
        end
        %%% ensure backwards compatibility with versions before V2.75
        if ~isfield(vg.obj(1).pp,'endmarkerflag')
        %%%%% if one object doesn't have it then all loaded objects do not have it    
            pptemplate = obj_default_pp;
            for io = 1:length(vg.obj)
                vg.obj(io).pp.endmarkerflag = true;
                vg.obj(io).pp = obj_matchfields(pptemplate, vg.obj(io).pp);
            end
        end
        %%% ensure backwards compatibility with versions before V2.84
        if ~isfield(vg.obj(1).pp,'bridgemarkerflag') || isempty(vg.obj(1).pp.bridgemarkerflag)
        %%%%% if one object doesn't have it then all loaded objects do not have it    
            pptemplate = obj_default_pp;
            for io = 1:length(vg.obj)
                vg.obj(io).pp.bridgemarkerflag = false;
                vg.obj(io).pp = obj_matchfields(pptemplate, vg.obj(io).pp);
            end
        end
        %%% ensure backwards compatibility with versions before V3.03
        if ~isfield(vg.obj(1).pp,'badratflag') || isempty(vg.obj(1).pp.badratflag)
        %%%%% if one object doesn't have it then all loaded objects do not have it    
            pptemplate = obj_default_pp;
            for io = 1:length(vg.obj)
                vg.obj(io).pp.badratflag = pptemplate.badratflag;
                vg.obj(io).pp.badratthres = pptemplate.badratthres;
                vg.obj(io).pp = obj_matchfields(pptemplate, vg.obj(io).pp);
            end
        end
        %%% ensure backwards compatibility with versions before 
        if ~isfield(vg.obj(1).pp,'dhhflag') || isempty(vg.obj(1).pp.dhhflag)
        %%%%% if one object doesn't have it then all loaded objects do not have it    
            pptemplate = obj_default_pp;
            for io = 1:length(vg.obj)
                vg.obj(io).pp.dhhflag = pptemplate.dhhflag;
                vg.obj(io).pp.dhhthres = pptemplate.dhhthres;
                vg.obj(io).pp.dhhcolor = pptemplate.dhhcolor;
                vg.obj(io).pp = obj_matchfields(pptemplate, vg.obj(io).pp);
            end
        end
        % files created with a bug (inconsistent code field for some node set objects)
        for io = 1:length(vg.obj)
            nx = length(vg.obj(io).x);
            if length(vg.obj(io).code) ~= nx
                vg.obj(io).code = zeros(1,nx);
            end
        end
        %%% ensure backwards compatibility with versions before V2.83
        if ~isfield(vg.undo,'label')
            [vg.undo.label] = deal('');
            vg.undo = orderfields(vg.undo,undo_struct);
        end
        %%% ensure backwards compatibility with versions before V2.85
        if ~isfield(vg.obj(1),'meta')
        %%%%% if one object doesn't have it then all loaded objects do not have it    
            for io = 1:length(vg.obj)
                % sizexdata had only one field, utmzone; transfer the data
                % to the new field
                if isfield(vg.obj(io).sizexdata,'utmzone')
                    utmzone = vg.obj(io).sizexdata.utmzone;
                    % reduce it if it is the same for all nodes
                    if length(unique(utmzone))==1
                        utmzone = utmzone(1);
                    end
                    vg.obj(io).meta.utmzone = utmzone;
                end
            end
            objtemplate = obj_struct;
            vg.obj = obj_matchfields(objtemplate,vg.obj);
        end
        
        %%% this is done in obj_load
%         % make sure z and code are double
%         for io = 1:length(vg.obj)
%             vg.obj(io).z = double(vg.obj(io).z);
%             vg.obj(io).code = double(vg.obj(io).code);
%         end
        
        [H.obj,undo] = obj_load(H.obj,vg.obj,H.ah,[],H.tbl);
        vgundo = vg.undo; %%% functionality: undo is saved in VG files
        if isfield(vg,'notes')
            vgnotes = vg.notes;
        end
        success = 1;
        catch ME
            H = {['Can not read ' fullfile],ME.message};
        end
    case 2
        % NOD files (native to Trigrid)
        try
            [poly,xyz] = readnod(fullfile);
            [okpressed,pp] = dlgPlotParam(H.ppdef);
            if okpressed % OK was pressed
                if ~isempty(poly)
                    r.x = poly(:,1); r.y = poly(:,2); r.z = poly(:,3);
                    otype = 2;
%                     onew = oCreate(poly2coast(poly),[],otype);
                    onew = oCreate(r,[],otype); % all segments in one object
                    success = 1;
                else
                    onew = obj_struct(0);
                end
                if ~isempty(xyz)
                    r.x = xyz(:,1); r.y = xyz(:,2); r.z = xyz(:,3);
                    otype = 1;
                    onew = [onew oCreate(r,length(onew)+1,otype)];
                    success = 1;
                end
                if success
                    [H.obj,undo] = obj_load(H.obj,onew,H.ah,pp,H.tbl);
                end
            end
        catch ME
            H = {['Can not read ' fullfile],ME.message};
        end
    case 3
        % NGH files (native to Trigrid)
        try
            [x,y,z,code,~,con,offset_scale,comment] = readngh(fullfile);
            [okpressed,pp] = dlgPlotParam(H.ppdef);
            if okpressed && ~isempty(x) % OK was pressed and array is not empty
                r.x = x; r.y = y; r.z = z; r.code = code;
                r.tri = con2tri(double(con));
                r.offset_scale = offset_scale;
                if iscell(comment)
                    comment = strjoin(comment,' ');
                end
                r.comment = comment;
                otype = 3;
                onew = oCreate(r,[],otype);
                [H.obj,undo] = obj_load(H.obj,onew,H.ah,pp,H.tbl);
                success = 1;
            end
        catch ME
            H = {['Can not read ' fullfile],ME.message};
        end
    case 4 % SHP files
%         [err,header,data,warn] = shpread(fullfile);
        
        [err,header,warn] = shpread_header(fullfile);
        if ~err
            if header.shapetype == 1
                % fast reading of Type 1 shp files (Point)
                [~,data.x,data.y] = shpread_type1(fullfile,true);
    %             % shpread doesn't deal with Point (Type 1) shp data, try Matlab
    %             % shaperead function
    %             try
    %                 [~,data] = shaperead(fullfile);
    %             catch err
    %                 err = {'Matlab''s shaperead failed.',err.message};
    %                 errordlg(err,'Error reading .shp file')
    %             end
                err = false;
            elseif header.shapetype == 11
                % fast reading of Type 11 shp files (Point Z)
                [~,data.x,data.y,data.z] = shpread_type11(fullfile,true);
                err = false;
            else
                [~,data,err,warn] = shpread(fullfile,true);
                
                % read feature codes from DBF file if it exists
                [~,filenameonly] = fileparts(filename);
                dbffile = [pathname filenameonly '.dbf'];
                if exist(dbffile,'file')
                    [dbf, dbffield] = dbfread(dbffile);
                    ifeature = strcmpi('feature',dbffield); % which field contains feature codes
                    if sum(ifeature)==1 % if one and only one match is found
                        feature = dbf(:,ifeature); % feature for each segment
                        featurelist = unique(feature); % list of features
                        % record feature number in the list as node codes
                        for k = 1:length(data)
                            data(k).code = repmat(find(strcmp(feature{k},featurelist)),size(data(k).x));
                        end
                    end
                end
            end
        end
        
        if err
            errordlg(err,'Error reading .shp file')
        else
            [okpressed,pp] = dlgPlotParam(H.ppdef);
                                                
            if okpressed % OK was pressed
                
                if exist('featurelist','var') % if DBF file was read
                    r.meta.featurelist = featurelist;
                end
                % combine separate elements in one array
                r.x = [data.x];
                r.y = [data.y];
                if isfield(data,'z')
                    r.z = [data.z];
                end
                if isfield(data,'code')
                    r.code = [data.code];
                end
                
                % determine the type of object to create based on shapetype
                if ismember(header.shapetype,[3 5 13]) % create a polyline
                    otype = 2;
                    
%                     % combine separate elements in one array
%                     r.x = [data.x];
%                     r.y = [data.y];
%                     if header.shapetype == 13 % PolyLineZ
%                         r.z = [data.z];
%                     end
%                                         
%                     onew = oCreate(r,[],otype); % all segments in one object
%                     success = 1;
                elseif ismember(header.shapetype,[1 11]) % Point or PointZ; create a node set
%                 elseif header.shapetype == 11 % PointZ; create a node set
                    otype = 1;
                    
%                     % combine separate elements in one array
%                     r.x = [data.x];
%                     r.y = [data.y];
%                     if header.shapetype == 11
% %                         r.x = [data.x];
% %                         r.y = [data.y];
%                         r.z = [data.z];
% %                     else % 1
% %                         r.x = [data.POINT_X];
% %                         r.y = [data.POINT_Y];
% %                         r.z = [data.Z];
%                     end
%                     
%                     onew = oCreate(r,[],otype);
%                     success = 1;
                end
                
                onew = oCreate(r,[],otype);
                success = 1;
                
                [H.obj,undo] = obj_load(H.obj,onew,H.ah,pp,H.tbl);

                if ~isempty(warn)
                    warndlg(warn,'Warning')
                end
            end
        end
    case 5
        % image files
        try
            info = imfinfo(fullfile);
            if strcmp(info.ColorType,'indexed')
                [r.cdata,r.cmap] = imread(fullfile); % indexed
                
%                 [im,cmap] = imread(fullfile); % indexed
%                 
%                 % to deal with out-of-memory error, try doing it by piece
%                 im = ind2rgb_byparts(im,cmap);
%                 if ~im
%                     H = ['Can not read ' fullfile '. Out of memory.'];
%                     return
%                 end
            else
                r.cdata = imread(fullfile); % rgb
                r.cmap = [];
%                 im = imread(fullfile); % rgb
            end
            [n,m,~] = size(r.cdata);
%             [n,m,gbg] = size(im);
            [~,~,ext] = fileparts(filename);
            switch lower(ext)
                case '.jpg'
                    georefext = '.jgw';
                case '.jpeg'
                    georefext = '.jgw';
%                 case '.gif'
%                     georefext = '.ggw'; %%%%%
                case '.png'
                    georefext = '.pgw';
                case '.tif'
                    georefext = '.tfw';
                case '.tiff'
                    georefext = '.tfw';
            end
            [~,filenameonly] = fileparts(filename);
            georeffile = [pathname filenameonly georefext];
            if exist(georeffile,'file')
                grf = load(georeffile);
                x = grf(5)+(0:m-1)*grf(1);
                y = grf(6)+(0:n-1)*grf(4);
            else
%                 xlim = get(H.ah,'XLim');
%                 ylim = get(H.ah,'YLim');
%                 x = xlim(1) : diff(xlim)/(m-1) : xlim(2); % fit to axes
%                 y = ylim(1) : diff(ylim)/(n-1) : ylim(2);
                x = 1:m; % maintain aspect ratio
                y = 1:n;
                r.cdata = r.cdata(n:-1:1,:,:); % flipud for 3D array %%%%% hack for Milne image
            end
            
            r.imx = x; r.imy = y; % r.rgb = im;
            otype = 4;
            onew = oCreate(r,[],otype);
            [H.obj,undo] = obj_load(H.obj,onew,H.ah,[],H.tbl);
            success = 1;
        catch ME
            H = {['Can not read ' fullfile],ME.message};
        end
    case 6
%         H = 'This file type is not supported in the current version';
        try
            r = load(fullfile,'-mat');
            if isfield(r,'r') && length(fields(r))==1
                r = r.r;
            end

            [r,otype,err] = parseStruct(r);
            if otype == 0
                errordlg(err,['Can not parse data from ' filename])
            end
            
            [okpressed,pp] = dlgPlotParam(H.ppdef);                                    
            if okpressed % OK was pressed
                onew = oCreate(r,[],otype);
                [H.obj,undo] = obj_load(H.obj,onew,H.ah,pp,H.tbl);
                success = 1;
            end
        catch ME
            H = {['Can not load ' filename '. Parsing the mat file failed.'],...
            	 ME.message};
        end
    case 7
        if ~iscell(fullfile)
            fullfile = {fullfile};
            filename = {filename};
        end
        
        for k = 1:length(fullfile)
            try
                xyz = load(fullfile{k});
                [okpressed,pp] = dlgPlotParam(H.ppdef, filename{k});

                if okpressed && ~isempty(xyz) % OK was pressed and array is not empty
                    r.x = xyz(:,1); r.y = xyz(:,2); r.z = xyz(:,3);
                    otype = 1;
                    onew = oCreate(r,[],otype);
                    [H.obj,undo] = obj_load(H.obj,onew,H.ah,pp,H.tbl);
                    success = 1;
                end
            catch ME
                H = {['Can not load ' filename{k} '. File must be a 3-column text file'],...
                     ME.message};
            end
        end
end

if success
    H.zoomprev = zoom_all(H); % zoom to all objects
%     set(H.tools.lock,'State','off')
    
    set(H.tools.edit,'State','on') % switch to Edit Tool
    toolEdit(H.tools.edit,[])
end


function cbSetPlotParam(src,evd) %#ok<INUSD>
% Set appearance parameters for objects.

H = guidata(src);
on = unique(H.sel(:,1)); % selected object indices
os = H.obj(on); % selected objects
ppcombined = ppCombine(os); % pick parameters common for all selected objects
[okpressed,pp] = dlgPlotParam(ppcombined); % get new params via the dialogue
if okpressed % OK was pressed
    H.obj(on) = obj_set_pp(os,pp); % set
end

guidata(src,H)


function obj = obj_set_pp(obj,pp)
% Set plotting parameters for objects obj according to pp.

for io = 1:length(obj)
    obj(io) = obj_set_pp_one(obj(io),pp);
end


function obj = obj_set_pp_one(obj,pp)
% Set plotting parameters for one object obj according to pp.

otype = obj.type;
if ~isempty(pp.color)
    obj.pp.color = pp.color;
%     if otype == 3 % grids
%         set(obj.h.c,'EdgeColor',pp.color);
%     else % polylines and sets of nodes
        set(obj.h.c,'Color',pp.color);
        if otype==2 % polyline
            set(obj.h.c1st, 'Color',pp.color);
            set(obj.h.clast,'Color',pp.color);
        end
%     end
end
if otype==1 ||otype==2 || otype==3 % any object containing nodes
    if ~isempty(pp.marker)
        obj.pp.marker = pp.marker;
        set(obj.h.c,'Marker',pp.marker);
    end
    if ~isempty(pp.markersize)
        obj.pp.markersize = pp.markersize;
        set(obj.h.c,'MarkerSize',pp.markersize);
    end
    if ~isempty(pp.codemarkerflag)
        obj.pp.codemarkerflag = pp.codemarkerflag;
        if pp.codemarkerflag
            set(obj.h.codemarker,'Visible','on');
        else
            set(obj.h.codemarker,'Visible','off');
        end
    end
end
if otype==1 || otype==3 % node set or grid
    if ~isempty(pp.bathycolorflag)
        obj.pp.bathycolorflag = pp.bathycolorflag;
        if pp.bathycolorflag
            x = obj.x;
            y = obj.y;
            z = obj.z;
            if otype==1
                tri = delaunay(x,y);
            else
                tri = obj.tri;
            end
            el = [tri tri(:,1)]'; % closed triangle patches
            set(obj.h.bathycolor,'XData',x(el),'YData',y(el),'CData',z(el),...
                'ZData',repmat(obj.objn(1),size(el)));
            set(obj.h.bathycolor,'Visible','on');
        else
            set(obj.h.bathycolor,'Visible','off');
        end
    end
end
if otype==2 || otype==3 % polyline or grid
    if ~isempty(pp.width)
        obj.pp.width = pp.width;
        set(obj.h.c,'LineWidth',pp.width);
    end
    if ~isempty(pp.style)
        obj.pp.style = pp.style;
        set(obj.h.c,'LineStyle',pp.style);
    end
    
    % update markers if settings changed
    if ~isempty(pp.badlenthres) || (~isempty(pp.badlenflag) && pp.badlenflag)
        obj.pp.badlenthres = pp.badlenthres;
        if otype==2
            [xa,ya] = polyline_quality_edgelen(obj.x,obj.y,obj.pp.badlenthres);
        else
            [xa,ya] = grid_quality_edgelen(obj.x,obj.y,obj.tri,obj.pp.badlenthres);
        end
        set(obj.h.badlen,'XData',xa,'YData',ya,...
            'ZData',-ones(size(ya))*obj.objn(1)); % adjust markers location
    end
    if ~isempty(pp.badlenflag)
        obj.pp.badlenflag = pp.badlenflag;
        if pp.badlenflag
            set(obj.h.badlen,'Visible','on');
        else
            set(obj.h.badlen,'Visible','off');
        end
    end
end
% % fixed (uneditable) properties
% if ~isempty(pp.marker)
%     obj.pp.marker = pp.marker;
%     set(obj.h.c,'Marker',pp.marker);
% end
% if ~isempty(pp.markersize)
% end
% if ~isempty(pp.colornodes_marker)
% end
% if ~isempty(pp.colornodes_size)
% end
if otype==2 % polyline
    if ~isempty(pp.endmarkerflag)
        obj.pp.endmarkerflag = pp.endmarkerflag;
        if pp.endmarkerflag
            set(obj.h.c1st,'Visible','on');
            set(obj.h.clast,'Visible','on');
        else
            set(obj.h.c1st,'Visible','off');
            set(obj.h.clast,'Visible','off');
        end
    end
    
    % update markers if settings changed
    if ~isempty(pp.badangthres) || (~isempty(pp.badangflag) && pp.badangflag)
        obj.pp.badangthres = pp.badangthres;
        [xa,ya] = polyline_quality_angle(obj.x,obj.y,obj.pp.badangthres);
        set(obj.h.badang,'XData',xa,'YData',ya,...
            'ZData',-ones(size(ya))*obj.objn(1)); % adjust markers location
    end
    if ~isempty(pp.badangflag)
        obj.pp.badangflag = pp.badangflag;
        if pp.badangflag
            set(obj.h.badang,'Visible','on');
        else
            set(obj.h.badang,'Visible','off');
        end
    end
    if ~isempty(pp.badratthres) || (~isempty(pp.badratflag) && pp.badratflag)
        obj.pp.badratthres = pp.badratthres;
        [xr,yr] = polyline_quality_ratio(obj.x,obj.y,obj.pp.badratthres);
        set(obj.h.badrat,'XData',xr,'YData',yr,...
            'ZData',-ones(size(yr))*obj.objn(1)); % adjust markers location
    end
    if ~isempty(pp.badratflag)
        obj.pp.badratflag = pp.badratflag;
        if pp.badratflag
            set(obj.h.badrat,'Visible','on');
        else
            set(obj.h.badrat,'Visible','off');
        end
    end
end
if otype==3 % grid
    % triangle quality markers
    if ~isempty(pp.eqlthres) || (~isempty(pp.eqlflag) && pp.eqlflag)
        obj.pp.eqlthres = pp.eqlthres;

        x = obj.x';
        y = obj.y';
        tri = obj.tri;
        eql = 1./quality([x y], tri); % "equilateralness"
        lmark = eql>pp.eqlthres; % logical indices
        xc = sum(x(tri),2)/3.0; % coordinates of triangle centroids
        yc = sum(y(tri),2)/3.0;
        set(obj.h.eql,'XData',xc(lmark),'YData',yc(lmark),...
            'ZData',-ones(size(yc(lmark)))*obj.objn(1));
    end
    if ~isempty(pp.eqlflag)
        obj.pp.eqlflag = pp.eqlflag;
        if pp.eqlflag
            set(obj.h.eql,'Visible','on');
        else
            set(obj.h.eql,'Visible','off');
        end
    end
    
    % dh/h markers
    if ~isempty(pp.dhhthres) || (~isempty(pp.dhhflag) && pp.dhhflag)
        obj.pp.dhhthres = pp.dhhthres;

        x = obj.x';
        y = obj.y';
        z = obj.z';
        tri = obj.tri;
        dhh = dh_over_h(tri,z);
        lmark = dhh>pp.dhhthres; % logical indices
        xc = sum(x(tri),2)/3.0; % coordinates of triangle centroids
        yc = sum(y(tri),2)/3.0;
        set(obj.h.dhh,'XData',xc(lmark),'YData',yc(lmark),...
            'ZData',-ones(size(yc(lmark)))*obj.objn(1));
    end
    if ~isempty(pp.dhhflag)
        obj.pp.dhhflag = pp.dhhflag;
        if pp.dhhflag
            set(obj.h.dhh,'Visible','on');
        else
            set(obj.h.dhh,'Visible','off');
        end
    end
    
    if ~isempty(pp.bridgemarkerflag)
        obj.pp.bridgemarkerflag = pp.bridgemarkerflag;
        if pp.bridgemarkerflag
            ebr = grid_bridge(obj.tri);
            xb = mean(obj.x(ebr),2);
            yb = mean(obj.y(ebr),2);
            set(obj.h.bridge,'XData',xb,'YData',yb,'ZData',repmat(-obj.objn(1),size(xb)));
            set(obj.h.bridge,'Visible','on');
        else
            set(obj.h.bridge,'Visible','off');
        end
    end
    
%     if ~isempty(pp.eqlcolor)
%     end
end


function pp = ppCombine(obj)
% Fill in pp structure with parameters common for all selected objects, set the
% rest of the fields to empty.

pp = obj(1).pp;
field = fieldnames(pp);
cf = struct2cell(pp);
for io = 2:length(obj)
    for ifl = 1:length(field)
        f = pp.(field{ifl});
        if any(any(cf{ifl}~=f))
            cf{ifl} = [];
        end
    end
end
pp = cell2struct(cf, field, 1);
if isempty(pp.color)
    pp.color = [1 1 1]; % white color instead of empty
end


function cbHelpDoc(src,evd) %#ok<INUSD>
H = guidata(src);
web(fullfile(H.vgpath,'docs','docs.html'))


function cbHelpAbout(src,evd) %#ok<INUSD>
H = guidata(src);
msg = {[H.vg_name ' ' num2str(H.vg_version)]
    [H.vg_author ' ' H.vg_year]
    H.vg_affiliation};
msgbox(msg,'About VisualGrid') % ,'custom',IconData,IconCMap)



% tools callbacks
%-----------------------------

function toolEdit(src,evd) %#ok<INUSD>
% Edit (arrow) tool callback.

if strcmp(get(src,'State'),'on')
    H = guidata(src);
    
    % disable other tools
    set(setdiff(H.toolgroupex,src),'State','off')
    zoom(gcbf,'off') % State off is not enough, we have to switch off the tools
    pan(gcbf,'off')
    if ~verLessThan('matlab', '7.6')
        brush(gcbf,'off')
    end

    % listen to the mouse
    wbm_old = get(gcbf,'WindowButtonMotionFcn');
    set(gcbf,'WindowButtonMotionFcn',{@wbmcb,wbm_old},'WindowButtonDownFcn',@wbdcb,...
        'Pointer','arrow');
else
	% force the button to stay 'on' (can be switched off only by pressing on
    % other button in the group)
    set(src,'State','on')
end


function toolFreehand(src,evd) %#ok<INUSD>
% Polyline freehand tool callback. 

if strcmp(get(src,'State'),'on')
    H = guidata(src);
    
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
    
    % disable other tools
    set(setdiff(H.toolgroupex,src),'State','off')
    zoom(gcbf,'off') % State off is not enough, we have to switch off the tools
    pan(gcbf,'off')
    if ~verLessThan('matlab', '7.6')
        brush(gcbf,'off')
    end

    % listen to the mouse
    set(H.hfm,'WindowButtonDownFcn',@wbdcb_draw_idle,...
          'WindowButtonMotionFcn',[],...
          'KeyPressFcn',{@wkpcb,''},...
          'Pointer','crosshair');
else
    % force the button to stay 'on' (can be switched off only by pressing on
    % other button in the group)
    set(src,'State','on')
end


function toolFreenode(src,evd) %#ok<INUSD>
% Node placement tool callback.

if strcmp(get(src,'State'),'on')
    H = guidata(src);
    
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
    
    % disable other tools
    set(setdiff(H.toolgroupex,src),'State','off')
    zoom(gcbf,'off') % State off is not enough, we have to switch off the tools
    pan(gcbf,'off')
    if ~verLessThan('matlab', '7.6')
        brush(gcbf,'off')
    end
    
    % appearance for the new object
    [okpressed,pp] = dlgPlotParam(H.ppdef); % get new params via the dialogue
    if ~okpressed % Cancel was pressed
        pp = H.ppdef; % default
    end
    
    % create a new node set
    r.x = NaN;
    r.y = NaN;
    r.z = NaN;
    [H.obj,undo] = obj_load(H.obj, oCreate(r,1,1), H.ah, pp, H.tbl);
    H.undo = undo_register(H,undo);
    
    % start the node placement cycle
    [ok,z] = dlgFreeNodeZ_2(0); % get Z for subsequently created nodes
    if ~ok
        [H.obj,H.undo] = freenode_finalize(H);
%         set(src,'State','off') % stop freenode operation
%         set(H.tools.edit,'State','on')
%         toolEdit(H.tools.edit,[]) % go back to Edit state
%         return
    else
        H.freenode_z = z; % current z for newly inserted nodes
        
        % remember the start time to show user's performance at the end of operation
        set(H.statstr,'UserData',now)
        
        set(H.hfm,'WindowButtonDownFcn',@wbdcb_freenode,...
                  'KeyPressFcn',{@wkpcb,'freenode'},...
                  'Pointer','crosshair');
    end
    guidata(src,H)
else
    % force the button to stay 'on' (can be switched off only by pressing on
    % other button in the group)
    set(src,'State','on')
end


function toolCleave(src,evd) %#ok<INUSD>
% Cleave node tool callback.

if strcmp(get(src,'State'),'on')
    H = guidata(src);
    
    H.sel = clear_selection(H.hsel,[],[],H.tbl);
    
    % disable other tools
    set(setdiff(H.toolgroupex,src),'State','off')
    zoom(gcbf,'off') % State off is not enough, we have to switch off the tools
    pan(gcbf,'off')
    if ~verLessThan('matlab', '7.6')
        brush(gcbf,'off')
    end
    
    p = imread('pointer_arrow_pick.bmp'); % 1 - white, 0 - black
    %       1's where you want the pixel black
    %       2's where you want the pixel white
    %       NaNs where you want the pixel transparent
    p = double(~p); % now 1 is black
    p(~p) = NaN; % make 0-pixels transparent
    
    % listen to the mouse
    set(H.hfm,'WindowButtonDownFcn',@wbdcb_cleave,...
          'WindowButtonMotionFcn',[],...
          'KeyPressFcn',{@wkpcb,''},...
          'Pointer','custom','PointerShapeCData',p,'PointerShapeHotSpot',[9 1]);
else
    % force the button to stay 'on' (can be switched off only by pressing on
    % other button in the group)
    set(src,'State','on')
end


function wbdcb_cleave(src,evnt) %#ok<INUSD>
H = guidata(src);

% cursor position
cp = get(H.ah,'CurrentPoint');  
xc = cp(1,1);
yc = cp(1,2);

% work on unlocked objects only
obj = H.obj(obj_isfree(H.tbl));

% nodes' coordinates
x = [obj.x];
y = [obj.y];
ifoc = ffocus1(H.ah,x,y,xc,yc,H.pthres); % only one ifoc returned

% clean up the focus markers
cleanfoc(H.hfoc)

if ~isempty(ifoc)
    objn = [obj.objn];
    noden = [obj.noden];
    
    objn = objn(ifoc)'; % object and node in focus
    noden = noden(ifoc)';
    
    obj = H.obj(objn);
    
    % allow editing only grid objects
    if obj.type~=3
        return
    end
    
    [p,tri,z,code,err] = grid_cleave([obj.x' obj.y'],obj.tri,noden,obj.z,obj.code);
    
    if ~err % if changes have been made
        undothis.action = 'gridedit';
        undothis.label = 'cleave node';
        undothis.r = obj; %%%%% saving the entire grid object - this can inflate undo structure considerably;
                          %%%%% if cleaving only one node at a time is allowed,
                          %%%%% then maybe we can save only changes to the grid???
        H.undo = undo_register(H,undothis);

        obj = obj_update_data(obj,H.tbl,p(:,1)',p(:,2)',objn,z,code,tri);
        H.obj(objn) = obj;

        H.sel = clear_selection(H.hsel,[],[],H.tbl);
    else
        warndlg('The selected nodes can not be cleaved.','Warning');
    end

    guidata(src,H)
end


function toolColorNodes(src,evd) %#ok<INUSD>
% Color Nodes tool callback.

if strcmp(get(src,'State'),'on')
    % any actions when the tool is switched on?
    
    %-----
    % coloured triangulation of close nodes
    H = guidata(src);
    set(H.colortri,'Visible','on')
else
    H = guidata(src);
    
%     %-----
%     % coloured scatterplot
%     % remove markers for all objects
%     for io = 1:length(H.obj)
%         if H.obj(io).type~=4 %%%%% except for images
%             set(H.obj(io).h.colornodes,'XData',[],'YData',[],'CData',[]);
%         end
%     end
    
    %-----
    % coloured triangulation of close nodes
    set(H.colortri,'Visible','off')
end


function toolGoogleMaps(src,evd) %#ok<INUSD>
% Load Google Maps image in the background.

H = guidata(src);

xmid = mean(get(H.ah,'XLim'));
ymid = mean(get(H.ah,'YLim'));

%%% Call a parameter dialog instead of using a menu Edit->Google Map Settings.
%%% Make the 'MapType' another parametr.
if H.gmap.utmzone~=0
    [ymid,xmid] = utm2geo(xmid,ymid,H.gmap.utmzone);
end
if isempty(H.gmap.zoomlevel)
    H.gmap.zoomlevel = 12; % default
end

[r.imx,r.imy,cdata,r.cmap] = get_google_map(ymid,xmid,...
    'utmzone',H.gmap.utmzone,'zoom',H.gmap.zoomlevel); %%%% ,'MapType','terrain'
nc = size(cdata,3);
for ic = 1:nc
    cdata(:,:,ic) = flipud(cdata(:,:,ic));
end
r.cdata = cdata;
otype = 4;
onew = oCreate(r,[],otype);
[H.obj,undo] = obj_load(H.obj,onew,H.ah,[],H.tbl);
H.undo = undo_register(H,undo);

guidata(src,H)


function toolNotes(src,evd) %#ok<INUSD>
% Color Nodes tool callback.

H = guidata(src);

if ~isfield(H,'notes')
    H.notes = {''};
end

[ok,H.notes] = dlgNotes(H.notes,'File notes','Save','Cancel');
if ~ok
    return
end

guidata(src,H)
    

function zoomOnCallback(src,evd) %#ok<INUSD>

H = guidata(src);

ion = strcmp(get(H.toolgroupex,'State'),'on');
H.toolprev = setdiff(H.toolgroupex(ion),src);

% disable other tools
set(setdiff(H.toolgroupex,src),'State','off')

% % don't listen to the mouse
% set(gcbf,'WindowButtonMotionFcn','','WindowButtonDownFcn','','WindowButtonUpFcn','');
% switch callbacks to zoom mode
set(gcbf,'WindowButtonMotionFcn','','WindowButtonDownFcn',@wbdcb_zoom,...
    'WindowButtonUpFcn','');

% change cursor
zoomicon = load([matlabroot,'/toolbox/matlab/icons/zoomplus.mat']);
zoomicon = zoomicon.cdata;
zoomicon = fliplr(squeeze(zoomicon(:,:,1)));
% zoomicon(isnan(zoomicon)) = 0;
zoomicon(zoomicon~=0) = NaN;
zoomicon(zoomicon==0) = 1;
set(H.hfm,'Pointer','custom','PointerShapeCData',zoomicon,...
        'PointerShapeHotSpot',[7 7])

guidata(src,H)


function zoomOffCallback(src,evd) %#ok<INUSD>

H = guidata(src);

% need to switch it off manually (why?)
set(src,'State','off')
% zoom('off')

% enable previuos tool
set(H.toolprev,'State','on')

% restore cursor
set(H.hfm,'Pointer','arrow')

% guidata(src,H)


function wswcb(src,evnt)
% Mouse wheel scroll.

H = guidata(src);

xlim = get(H.ah,'XLim');
ylim = get(H.ah,'YLim');

p = get(H.ah,'CurrentPoint');
p = p(1,1:2);

%%%%% alt calls the figure menu - get rid of this functionality
if H.keypressed.shift % zoom
    % % check if click is outside of the main axes
    % if p(1)<xlim(1) || p(1)>xlim(2) || p(2)<ylim(1) || p(2)>ylim(2)
    %     return
    % end

    % zoom in/out by a factor of 1.2 relative to the pointer location
    fac = 1.2^evnt.VerticalScrollCount;
    dx = (xlim - p(1))*fac; % distnaces from p to the zoomed area bounds
    dy = (ylim - p(2))*fac;
    xlimnew = p(1) + dx;
    ylimnew = p(2) + dy;
    
    set(H.ah,'XLim',xlimnew,'YLim',ylimnew)

    H.zoomprev.xlim = xlim;
    H.zoomprev.ylim = ylim;
elseif H.keypressed.z && ~isempty(get(H.hfoc(2),'String')) % change z
    % cursor position
    cp = get(H.ah,'CurrentPoint');  
    xc = cp(1,1);
    yc = cp(1,2);

    % work on unlocked objects only
    obj = H.obj(obj_isfree(H.tbl));

    % nodes' coordinates for all objects
    x = [obj.x];
    y = [obj.y];

    xlim = get(H.ah,'XLim');
    ylim = get(H.ah,'YLim');

    d = ((x-xc).^2+(y-yc).^2).^0.5; % distance from cursor position to each node

    dthres = (diff(xlim).^2 + diff(ylim).^2).^0.5 * H.pthres;
    ifoc = find(d==min(d) & d<dthres, 1, 'first');
    
    if ~isempty(ifoc)
        z = [obj.z];
        
        if evnt.VerticalScrollCount<0
            ipredef = find(H.zpredef<=z(ifoc),1,'last'); % increasing z
            if isempty(ipredef)
                ipredef = 1;
            end
        else
            ipredef = find(H.zpredef>=z(ifoc),1,'first'); % decreasing z
            if isempty(ipredef)
                ipredef = length(H.zpredef);
            end
        end
        inew = ipredef - evnt.VerticalScrollCount;
        inew = min(inew,length(H.zpredef)); % not more than max value
        inew = max(inew,1); % not less than min value
        znew = H.zpredef(inew);
        
        H.freenode_z = znew;
        
        objf = [obj.objn];
        objf = objf(ifoc);
        nodf = [obj.noden];
        nodf = nodf(ifoc);
        H.obj(objf).z(nodf) = znew;
        H.obj(objf) = obj_update_data(H.obj(objf),H.tbl);
        
        undo.action = 'redepth';
        undo.label = 'change Z';
        undo.r.objn = objf;
        undo.r.noden = nodf;
        undo.r.z = z(ifoc);
        
        
        % "original" object in manual edit mode
        isman = length(objf) == 1 && obj_manedit(H.obj(objf)) == 1;
        if isman
            cn = H.obj(objf).meta.obj_confirmed; % corrsponding "confirmed" object number
            % move the node to the "confirmed" object
            [H.obj(objf),H.obj(cn),undothis] = oConfirm(H.obj(objf),H.obj(cn),nodf,H.tbl);
            % "original" object in manual edit mode
            undothis.r.edit = undo; % record the edit action
        else
            undothis = undo;
        end

        
        H.undo = undo_register(H,undothis);

        code = [obj.code];
        fstr = {['x: ' num2str(x(ifoc))];
                ['y: ' num2str(y(ifoc))];
                ['z: ' num2str(znew)];
                ['code: ' num2str(code(ifoc))]};
        set(H.hfoc(2),'String',fstr); % show z value
    end
else % scroll
    if H.keypressed.ctrl % horizontal scroll
        xlimnew = xlim - evnt.VerticalScrollCount*diff(xlim)*0.1;
        ylimnew = ylim;
    else % vertical scroll
        xlimnew = xlim;
        ylimnew = ylim - evnt.VerticalScrollCount*diff(ylim)*0.1;
    end
    
    set(H.ah,'XLim',xlimnew,'YLim',ylimnew)

    H.zoomprev.xlim = xlim;
    H.zoomprev.ylim = ylim;
end


guidata(src,H)


function wbdcb_zoom(src,evnt) %#ok<INUSD>

H = guidata(src);

xlim = get(H.ah,'XLim');
ylim = get(H.ah,'YLim');

p = get(H.ah,'CurrentPoint');   % button down detected
p = p(1,1:2);

seltype = get(src,'SelectionType');

if strcmp(seltype,'normal') % zoom in
    % % check if click is outside of the main axes
    % if point1(1)<xlim(1) || point1(1)>xlim(2) || point1(2)<ylim(1) || point1(2)>ylim(2)
    %     return
    % end

    % get user specified rectangle
    rbbox; % finalRect = rbbox; % return figure units

    point2 = get(H.ah,'CurrentPoint');    % button up detected
    point2 = point2(1,1:2);

    offset = abs(p-point2); % calculate dimensions
    p = min(p,point2); % calculate locations
    
    if all(offset==0) % just a click
        % zoom by a factor of 2 relative to the clicked location
        dx = diff(xlim)/4; % half-range for the zoomed area
        dy = diff(ylim)/4;
        xlimnew = p(1) + [-dx dx];
        ylimnew = p(2) + [-dy dy];
    else % a box has been drawn
        % xbox = [p1(1) p1(1)+offset(1) p1(1)+offset(1) p1(1) p1(1)];
        % ybox = [p1(2) p1(2) p1(2)+offset(2) p1(2)+offset(2) p1(2)];
        xlimnew = [p(1) p(1)+offset(1)];
        ylimnew = [p(2) p(2)+offset(2)];
        [xlimnew,ylimnew] = fit_aspect(xlimnew,ylimnew,xlim,ylim);
    end
    
    H.zoomprev.xlim = xlim;
    H.zoomprev.ylim = ylim;
    
    set(H.ah,'XLim',xlimnew,'YLim',ylimnew)
    
elseif strcmp(seltype,'alt') % Ctrl-click left mouse button or click right mouse button
    % zoom out
    if isfield(H,'zoomprev') && all(~isnan(H.zoomprev.xlim))
        % zoom out to previous view
        xlimnew = H.zoomprev.xlim;
        ylimnew = H.zoomprev.ylim;
    else
        % zoom out by a factor of 2
        dx = diff(xlim); % half-range for the zoomed-out area
        dy = diff(ylim);
        xlimnew = p(1) + [-dx dx];
        ylimnew = p(2) + [-dy dy];
    end
    H.zoomprev.xlim = NaN;
    H.zoomprev.ylim = NaN;
    
    set(H.ah,'XLim',xlimnew,'YLim',ylimnew)
    
elseif strcmp(seltype,'open') % double-click
    H.zoomprev = zoom_all(H); % zoom to all objects
end
% strcmp(seltype,'extend') % shift-click

guidata(src,H)


function zoomprev = zoom_all(H)
% zoom to all objects

xlim = get(H.ah,'XLim');
ylim = get(H.ah,'YLim');

x = [H.obj.x];
y = [H.obj.y];
%%%%% images?

xlimnew = [min(x) max(x)];
ylimnew = [min(y) max(y)];

if isempty(xlimnew) % no objects
    if isfield(H,'zoomprev') && all(~isnan(H.zoomprev.xlim))
        % zoom out to previous view
        xlimnew = H.zoomprev.xlim;
        ylimnew = H.zoomprev.ylim;
    else
        xlimnew = [0 1];
        ylimnew = [0 1];
    end
else
    % expand identical limmits
    if xlimnew(1) == xlimnew(2)
        xlimnew = xlimnew + [-1 1];
    end
    if ylimnew(1) == ylimnew(2)
        ylimnew = ylimnew + [-1 1];
    end
end

[xlimnew,ylimnew] = fit_aspect(xlimnew,ylimnew,xlim,ylim);

zoomprev.xlim = NaN;
zoomprev.ylim = NaN;

set(H.ah,'XLim',xlimnew,'YLim',ylimnew)


function [xf,yf] = fit_aspect(xr,yr,xa,ya)
% Adjust ranges xr,yr so their aspect ratio matches that of xa,ya.

% convert to extents
dx = abs(diff(xr));
dy = abs(diff(yr));

% aspect ratios
aasp = abs(diff(xa))/abs(diff(ya));
rasp = dx/dy;

if rasp<aasp % adjust x-range
    xm = mean(xr);
    xrf = dy*aasp; % x-range adjusted
    xf = xm + [-xrf xrf]/2;
    yf = yr; % unchanged
else % adjust y-range
    ym = mean(yr);
    yrf = dx/aasp; % y-range adjusted
    yf = ym + [-yrf yrf]/2;
    xf = xr; % unchanged
end


%---------------------------
% Undo functions

function undostack = undo_register(H,undonew)
undostack = H.undo;
if isempty(undonew)
    return
end
fileSetUptodate(H,false);
undonew.filesaved = false;
undostack(end+1) = undonew;
if length(undostack) > H.undolim
    undostack(1) = [];
end
set(H.tools.undo,'enable','on')
set(H.tools.undo,'ToolTip',['Undo ' undonew.label])


function undostack = undo_unregister(H)
fileSetUptodate(H, H.undo(end).filesaved);
undostack = H.undo;
undostack(end) = [];
if isempty(undostack)
    set(H.tools.undo,'enable','off')
    set(H.tools.undo,'ToolTip','Undo')
else
    set(H.tools.undo,'ToolTip',['Undo ' undostack(end).label])
end


function undofcn(src,evd) %#ok<INUSD>
H = guidata(src);

if isempty(H.undo) % undo stack is empty
    return
end

action = H.undo(end).action; % action type
label = H.undo(end).label; % action label
r = H.undo(end).r; % action record

H.undo = undo_unregister(H);
H = undo_action(H,action,r,label);

guidata(src,H)


function H = undo_action(H,action,r,label) % label will be needed for redo

switch action
    case 'cedit' % generic edit operation - just restore previous values for all fields
        objn = r.objn;
        for io = 1:length(objn)
            obj = r.obj(io);
            H.obj(objn(io)) = obj_update_data(H.obj(objn(io)),H.tbl,...
                obj.x, obj.y, obj.objn(1), obj.z, obj.code, obj.tri, obj.imx, obj.imy, obj.cdata);
        end
        H.sel = clear_selection(H.hsel,[],[],H.tbl);
    case 'oNodeMove'
        if isfield(r,'zold')
            H.obj = oNodeMove(H.obj,H.sel,H.hsel,r.objn,r.noden,-r.dx,-r.dy,r.zold);
        else
            H.obj = oNodeMove(H.obj,H.sel,H.hsel,r.objn,r.noden,-r.dx,-r.dy);
        end
        
    case 'oNodeDelete'
        if ~isempty(r.ro)
            objins = obj_draw(H.ah, r.ro.r.obj, r.ro.r.pp);
            H.obj = oInsert(H.obj, objins, r.ro.r.objn, H.tbl);
        end
        H.obj = oNodeInsert(H.obj,r.objn,r.noden,r.x,r.y,r.z,r.code,H.tbl);
    case 'oNodeInsert'
        H.obj = oNodeDelete(H.obj,r.objn,r.noden,H.tbl);
    case 'oExtract'
        H = undo_action(H,'oInsert',r.undoload.r);
        H = undo_action(H,'oNodeDelete',r.undodelete.r);
    case 'oCombine'
        H = undo_action(H,'oDelete',r.undodelete.r);
        H = undo_action(H,'oNodeInsert',r.undonodeinsert.r);
    case 'oConnect'
        H = undo_action(H,'oNodeInsert',r.undonodeinsert.r);
        H = undo_action(H,'oNodeDelete',r.undonodedelete.r);
    case 'oInsert'
        H.obj = oDelete(H.obj,r.objn,H.tbl);
    case 'oDelete'
        objins = obj_draw(H.ah, r.obj, r.pp);
        H.obj = oInsert(H.obj, objins, r.objn, H.tbl);
    case 'replace'
        H = undo_action(H,'oInsert',r.undoload.r);
        H = undo_action(H,'oDelete',r.undodelete.r);
    case 'oRearrange'
        H.obj = oRearrange(H.obj,r.oorder,H.tbl);
    case 'oConfirm'
        % to un-confirm, simply swap "orig" and "confirmed" objects in the
        % oConfirm call
        [H.obj(r.objnc),H.obj(r.objno),redo] = oConfirm(H.obj(r.objnc),H.obj(r.objno),r.noden,H.tbl);
        if isfield(r,'edit') && ~isempty(r.edit) % some action was performed on the node
            H = undo_action(H,r.edit.action,r.edit.r);
        end
    case 'obj_manedit_set'
        H = undo_action(H, r.undoload.action, r.undoload.r); % remove 'confirmed' objects
        % remove special field
        for k = 1:length(r.objn)
            H.obj(r.objn(k)).meta = rmfield(H.obj(r.objn(k)).meta,'obj_confirmed');
            obj_update_data(H.obj(r.objn(k)),H.tbl); % update table
        end
    case 'obj_manedit_stop'
        H = undo_action(H, r.undonan.action, r.undonan.r); % restore NaN nodes
        % restore special fields with connections
        for k = 1:length(r.objn)
            if r.isman(k)==2 % "confirmed" object
                H.obj(r.objn(k)).meta.obj_orig = r.conn(k);
            elseif r.isman(k)==1 % "original" object
                H.obj(r.objn(k)).meta.obj_confirmed = r.conn(k);
            end
            obj_update_data(H.obj(r.objn(k)),H.tbl); % update table
        end
    case 'coordoffset'
        [H.obj(r.objn),redo] = obj_offset(H.obj(r.objn),-r.xoffset,-r.yoffset,-r.zoffset);
    case 'coordscale'
        [H.obj(r.objn),redo] = obj_scale(H.obj(r.objn),1/r.xscale,1/r.yscale,1/r.zscale);
    case 'coordtransform'
        [H.obj(r.objn),redo] = obj_undocoordtransform(H.obj(r.objn),r.obj,r.remsizexdata);
    case 'gridedit'
        if isfield(r,'ro') && ~isempty(r.ro) % the entire grid was deleted
            objins = obj_draw(H.ah, r.ro.r.obj, r.ro.r.pp);
            H.obj = oInsert(H.obj, objins, r.ro.r.objn, H.tbl);
        else % the grid was edited, but not deleted entirely
            on = r.objn(1); % only one grid can be edited at a time
            H.obj(on) = obj_update_data(H.obj(on),H.tbl,r.x,r.y,on,r.z,r.code,r.tri);
            H.sel = clear_selection(H.hsel,[],[],H.tbl);
        end
    case 'gridedgeexchange'
        on = r.objn;
        tri = H.obj(on).tri;
        tri(r.lt,:) = r.tex;
        H.obj(on) = obj_update_data(H.obj(on),H.tbl,[],[],[],[],[],tri);
        H.sel = clear_selection(H.hsel,[],[],H.tbl);
    case 'gridtrisub' % substitution of triangles
%         % save redo information
%         redo.action = 'gridtrisub'; % substitution of triangles
%         redo.label = label;
%         redo.r.objn = r.objn;
%         redo.r.itex = r.itex;
%         redo.r.torig = r.tri(r.itex,:);
%         H.redo = redo_register(H,redo);

        H.obj(r.objn) = obj_update_data(H.obj(r.objn),[],[],[],[],[],[],r.tri);

    case 'gridsplit'
        H.obj(r.objn) = obj_update_data(H.obj(r.objn),H.tbl,r.x,r.y,[],r.z,r.code,r.tri);
        H.obj = oDelete(H.obj,r.objn+1,H.tbl);
        H.sel = clear_selection(H.hsel,[],[],H.tbl);
    case 'gridmerge'
        H.obj = obj_load(H.obj,r.objins,H.ah,r.objins.pp,H.tbl);
        H.obj(r.objn) = obj_update_data(H.obj(r.objn),H.tbl,r.x,r.y,[],r.z,r.code,r.tri);
        H.sel = clear_selection(H.hsel,[],[],H.tbl);
    case 'redepth'
        z = H.obj(r.objn).z;
        z(r.noden) = r.z;
        H.obj(r.objn) = obj_update_data(H.obj(r.objn),H.tbl,[],[],[],z,[],[]);
end


%------------------------------------------------------
% Object manipulation routines

%%%%%%%%%%%%%%%%%%%%%%%
function sel = oNodeSelect(hsel,sel,x,y,objn,noden,htbl)
% same selection markers for all nodes of all objects
%%% htbl is needed for table cell selection (below).
%%% Not all calls to oNodeSelect are changed to accept htbl parameter.

% add to the list
sel(end+1:end+length(x), :) = [objn(:) noden(:)]; % append to selection list

% adjust selection markers
xdata = get(hsel,'XData');
ydata = get(hsel,'YData');
set(hsel,'XData',[xdata x],'YData',[ydata y])


% make the corresponding object selected in the object table
if exist('htbl','var') && ishandle(htbl)
    tableSelect(htbl,objn)
end


% function sel = oNodeSelect(sel,hsel,ifoc,obj)
% 
% %%%%%%% different selection markers for 1st and last nodes of polylines
% x = [obj.x];
% y = [obj.y];
% objn = [obj.objn];
% noden = [obj.noden];
% 
% otype = [obj(objn).type]; % expand object type into size(x) array
% is1st = noden==1 & otype==2; % size(x)
% islast = circshift([diff(objn)~=0 false] & otype==2, [0 -1]); % size(x)
%     % shift logical indices 1 pos to the front to account for terminating NaNs
% isl = x(is1st)==x(islast) & y(is1st)==y(islast); % size(find(is1st))
% % isisland = false(size(x));
% % isisland(is1st) = isl;
% % isisland(islast) = isl;
% 
% %%%%% check if this works
% % make matching nodes selected too
% lfoc = false(size(x));
% lfoc(ifoc) = true;
% m = lfoc(is1st) & isl;
% fislast = find(islast);
% lfoc(fislast(m)) = true;
% 
% % this case should never happen, sinse the 1st node always
% % has a preference for being in focus, unless custom node
% % selection is implemented
% m = lfoc(islast) & isl;
% fis1st = find(is1st);
% lfoc(fis1st(m)) = true;
% 
% ifoc = find(lfoc); % back to numerical indices
% 
% sel(end+1:end+nf,1:2) = [objn(ifoc)' noden(ifoc)']; % append to selection list
% 
% % adjust selection markers
% xdata = get(hsel.regular,'XData');
% ydata = get(hsel.regular,'YData');
% set(hsel.regular,'XData',[xdata x(lfoc & ~is1st & ~islast)],'YData',[ydata y(lfoc & ~is1st & ~islast)])
% 
% xdata = get(hsel.first,'XData');
% ydata = get(hsel.first,'YData');
% set(hsel.first,'XData',[xdata x(lfoc & is1st)],'YData',[ydata y(lfoc & is1st)])
% 
% xdata = get(hsel.last,'XData');
% ydata = get(hsel.last,'YData');
% set(hsel.last,'XData',[xdata x(lfoc & islast)],'YData',[ydata y(lfoc & islast)])
% 
%%%%%%%%%%%% individual handles for each selection marker 
% for is = ifoc(:)'
%     if is1st(is)
%         h = plot(ah,x(is),y(is),'o','Color',selcolor,'MarkerSize',10,'HitTest','off');
%         if isisland(is)
%             hm = plot(ah,x(is),y(is),'+','Color',selcolor,'MarkerSize',10,'HitTest','off');
%             nodematch = length(obj(objn(is)).x)-1;
%         end
%     elseif islast(is)
%         h = plot(ah,x(is),y(is),'+','Color',selcolor,'MarkerSize',10,'HitTest','off');
%         if isisland(is)
%             % this case should never happen, sinse the 1st node always
%             % has a preference for being in focus, unless custom node
%             % selection is implemented
%             hm = plot(ah,x(is),y(is),'o','Color',selcolor,'MarkerSize',10,'HitTest','off');
%             nodematch = 1;
%         end
%     else
%         h = plot(H.ah,x(is),y(is),'s','Color',selcolor,'MarkerSize',8,'HitTest','off');
%     end
% 
%     sel(end+1,1:3) = [h objn(is) noden(is)];
% end
% set(obj(objn(is)).h.c,'LineWidth',obj(objn(is)).pp.width*2,'MarkerSize',12+obj(objn(is)).pp.width*2) % ,'Marker','s'
% set(obj(objn(is)).h.rsz,'Visible','on') % resize markers


function [sel,objn] = clear_selection(hsel,sel,iclear,htbl)
% Clear selection markers, iclear  index (indices) into sel of the nodes to
% de-select.
% sel   [objf nodef] 2-column array of selected nodes
%
% Signatures:
%   clear_selection(hsel); % de-select all nodes; do not de-select in table
%   clear_selection(hsel,[],[],htbl); % de-select all nodes; and de-select in table
%   sel = clear_selection(hsel,sel,iclear,htbl); 
%       % de-select specified nodes; and de-select specified objects in the table
%   [sel,objn] = clear_selection(hsel,sel,iclear);
%       % returns objn to clear table selection later;
%       % used only in wbdcb to deal with 'axes lost focus' situation.

if ~exist('iclear','var') || isempty(iclear)
    set(hsel,'XData',[],'YData',[])
    sel = zeros(0,2);
    
    objn = Inf; % clear all selections in the table
else
    % adjust selection markers
    xdata = get(hsel,'XData');
    if ~isempty(xdata)
        ydata = get(hsel,'YData');
        xdata(iclear) = [];
        ydata(iclear) = [];
        
        set(hsel,'XData',xdata,'YData',ydata)
    end
    
    objn = sel(:,1); % initially selected objects
    
    sel(iclear,:) = [];
    
    objn = setdiff(objn,sel(:,1)); % objects that have become completely de-selected
end

if exist('htbl','var') && ishandle(htbl)
    tableDeselect(htbl,objn) % clear specified selections in the table
end


function [obj,undo] = oNodeEdit(obj,sel)
% Edit properties for selected nodes.

% structure with default values for dlgNodeData
def = struct('x',[],'y',[],'z',[],'code',[]);

objn = [obj.objn];
noden = [obj.noden];
isel = find(ismember([objn' noden'],sel,'rows'));

x = [obj.x];
xu = unique(x(isel));
if length(xu)==1; def.x = xu; end

y = [obj.y];
yu = unique(y(isel));
if length(yu)==1; def.y = yu; end

z = [obj.z];
zu = unique(z(isel));
if length(zu)==1; def.z = zu; end

code = [obj.code];
codeu = unique(code(isel));
if length(codeu)==1; def.code = codeu; end

[ok,dnew] = dlgNodeData(sel_string(obj,sortrows(sel)), def);
% empty fields in dnew are those unchanged

if ~ok
    undo = [];
    return
end

objs = sel(:,1);
nodes = sel(:,2);
objsu = unique(objs);
undo.action = 'cedit';
undo.label = 'edit properties';
undo.r.objn = objsu;
undo.r.obj = obj(objsu);
for io = objsu'
    in = nodes(objs==io);

    if ~isempty(setdiff(dnew.x,def.x)) % treats empty defaults correctly
        xn = obj(io).x;
        xn(in) = dnew.x;
    else
        xn = [];
    end
    if ~isempty(setdiff(dnew.y,def.y)) % treats empty defaults correctly
        yn = obj(io).y;
        yn(in) = dnew.y;
    else
        yn = [];
    end
    if ~isempty(setdiff(dnew.z,def.z)) % treats empty defaults correctly
        zn = obj(io).z;
        zn(in) = dnew.z;
    else
        zn = [];
    end
    if ~isempty(setdiff(dnew.code,def.code)) % treats empty defaults correctly
        coden = obj(io).code;
        coden(in) = dnew.code;
    else
        coden = [];
    end

    obj(io) = obj_update_data(obj(io),[],xn,yn,[],zn,coden);            
end


function [obj,ud] = oNodeMove(obj,sel,hsel,objn,noden,dx,dy,znew)
if exist('znew','var') && ~isempty(znew)
    zold = NaN(size(znew));
end
objnu = unique(objn);
for io = objnu(:)'
    inode = noden(objn==io);
    x = obj(io).x;  x(inode) = x(inode) + dx;
    y = obj(io).y;  y(inode) = y(inode) + dy;

%         % control for a node in a grid to stay within its neighbouring
%         % triangles' area
%         %%%%% comment out for speed
%         if obj(io).type==3 && ~grid_validmove([x' y'],objc.tri,inode) %%%%%
%             nmove(objn==io) = false;
%             continue
%         end

    if exist('znew','var') && ~isempty(znew) && all(~isnan(znew(objn==io)))
        z = obj(io).z;
        zold(objn==io) = z(inode);
        zsub = znew(objn==io);
        iival = ~isnan(zsub);
        z(inode(iival)) = zsub(iival); %%%%% test this
    elseif obj(io).type==3 % interpolate z for triangular grids if no znew supplied
        %%% or any NaN occurs in znew for this object
        objc = obj(io);
        
        % interpolate depths
        tri = double(objc.tri);
        ii = ~ismember(inode,grid_boundarynodes(tri)); % interior nodes
        if any(ii)
            xo = objc.x'; % original coordinates
            yo = objc.y';
            inodei = inode(ii); % interior nodes in focus
            xi = x(inodei)'; % new nodes requiring interpolation
            yi = y(inodei)';
            tri = tri( any(ismember(tri,inodei),2), :); % triangles containing interior nodes in focus
            k = tsearch(x,y,tri,xi,yi); %#ok<DGEO4>
%             k = tsearch_mex(x,y,tri,xi,yi);
            z = objc.z;
            zold(objn==io) = z(inodei); %%%%%%%%%%%%%%% zold is undefined
            z(inodei) = tinterp([xo yo], tri, z, [xi yi], k);
        else
            z = [];
        end
    else
        z = [];
    end

    obj(io) = obj_update_data(obj(io),[],x,y,io,z);
    
    % move selection markers
    [~,im,in] = intersect(sel,[objn(objn==io) inode],'rows'); % selected nodes that are being moved
    xdata = get(hsel,'XData');
    ydata = get(hsel,'YData');
    xdata(im) = obj(io).x(inode(in));
    ydata(im) = obj(io).y(inode(in));
    set(hsel,'XData',xdata,'YData',ydata)
end
% save undo information
if dx ~= 0 || dy ~= 0
    ud.action = 'oNodeMove';
    ud.label = 'node move';
    ud.r.objn = objn;
    ud.r.noden = noden;
    ud.r.dx = dx;
    ud.r.dy = dy;
    if exist('zold','var')
        ud.r.zold = zold;
    end
else
    ud = struct('action',[],'r',[]); ud(1) = [];
end


function [obj,ud] = oNodeInsert(obj,objn,noden,xi,yi,zi,codei,htbl)
% noden     node number in the new polyline (with the node inserted)

% save undo information
ud.action = 'oNodeInsert';
ud.label = 'node insert';
ud.r.objn = objn;
ud.r.noden = noden;

% ensure row vectors
xi = xi(:)';
yi = yi(:)';
zi = zi(:)';
codei = codei(:)';
noden = noden(:)';

objnu = unique(objn);
for io = objnu(:)'
    inode = objn==io;
    obj1 = obj(io);
    
    % append nodes at the back
    x = [obj1.x xi(inode)];
    y = [obj1.y yi(inode)];
    z = [obj1.z zi(inode)];
    code = [obj1.code codei(inode)];
    
    % determine indices for inserted and old nodes
    ind = [setdiff(1:length(x),noden(inode)) noden(inode)];
    [~,ind] = sort(ind);

    obj(io) = obj_update_data(obj(io),htbl,x(ind),y(ind),io,z(ind),code(ind),htbl);
        % nodes are rearranged according to provided node indices
end


function [obj,ud] = oNodeDelete(obj,objn,noden,htbl)

% ensure column vectors
objn = objn(:);
noden = noden(:);
objnu = unique(objn);

if any(obj_manedit(obj(objnu)))
    ud = [];
    errordlg('Deleting nodes from objects in "manual edit" mode is not aloowed.',...
        'Deleting prohibited')
    return
end

% save undo information
if all([obj(objnu).type]==3) % triangular grid
    if length(objnu)==1
        ud.action = 'gridedit';
        ud.label = 'edit grid';
        ud.r = obj(objnu);
    else
        warndlg('Editing only one grid object at a time is allowed.','Warning')
        ud = [];
        return
    end
else
    ud.action = 'oNodeDelete';
    ud.label = 'delete node';
    ud.r.objn = objn;
    ud.r.noden = noden;
    x = [obj.x];
    y = [obj.y];
    z = [obj.z];
    code = [obj.code];
    [~,i1,i2] = intersect([[obj.objn]' [obj.noden]'],[objn noden],'rows');
    ud.r.x(i2) = x(i1)';
    ud.r.y(i2) = y(i1)';
    ud.r.z(i2) = z(i1)';
    ud.r.code(i2) = code(i1)';
end


oinv = false(1,length(obj)); % "empty object" flags
for io = objnu(:)'
    inode = noden(objn==io);
    x = obj(io).x;
    if isempty(setdiff(obj(io).noden(~isnan(x)), inode)) % except the terminating NaNs
        oinv(io) = true; % removing all nodes, the entire object will be removed later
    else % only some nodes are removed
        x(inode) = [];
        y = obj(io).y;  y(inode) = [];
        z = obj(io).z;  z(inode) = [];
        code = obj(io).code;  code(inode) = [];
        if obj(io).type==3 % triangular grid
            tri = obj(io).tri;
            tri(any(ismember(tri,inode),2), :) = [];
            % adjust indices
            for ia = sort(inode,'descend')'
                tri(tri>ia) = tri(tri>ia) - 1;
            end
        else
            tri = [];
        end
        obj(io) = obj_update_data(obj(io),htbl,x,y,io,z,code,tri);
    end
end

% treat fully removed objects
if any(oinv)
    oinv = find(oinv);
    
    % correct the undo list: exclude nodes belonging to the fully removed objects
    ninv = ismember(objn,oinv);
    ud.r.objn(ninv) = [];
    ud.r.noden(ninv) = [];
    ud.r.x(ninv) = [];
    ud.r.y(ninv) = [];
    ud.r.z(ninv) = [];
    ud.r.code(ninv) = [];

    [obj,ud.r.ro] = oDelete(obj,oinv,htbl); % delete empty objects
else
    ud.r.ro = [];
end


function [obj,ud] = oDelete(obj,objn,htbl)
% Delete objects from the object structure.

% save undo data
ud.action = 'oDelete';
ud.label = 'delete object';
ud.r.obj = obj(objn);
ud.r.objn = objn;
ud.r.pp = getPlotParam(obj(objn));

% lenorig = length(obj);

clearhandlestruct([obj(objn).h])
obj(objn) = [];

if ishandle(htbl)
    dat = get(htbl,'Data');
    dat(objn,:) = [];
    set(htbl,'Data',dat)
end

% re-generate arrays of object numbers
for io = objn(1):length(obj) % 1:objnu(1)-1 remain unchanged
    obj(io).objn = ones(1,length(obj(io).objn))*io;
end


function [obj,ud] = oInsert(obj,objins,objn,htbl)
% Insert objects in the object structure.
% objn  indices of inserted objects in the resulting obj struct array

if isempty(objn)
    objnex = [objins.objn]; % expanded object numbers for objnew
    [~,iu] = unique(objnex);
    objn = objnex(sort(iu)); % in the original order
end

% save undo data
ud.action = 'oInsert';
ud.label = 'insert object';
ud.r.objn = objn;

% insert objects
obj = [obj objins];

% insert new lines in the object list table
if ishandle(htbl)
    if verLessThan('matlab', '7.6')
        dat = cell(get(htbl,'Data')); % old stock uitable function
    else
        dat = get(htbl,'Data');
    end
    nn = length(objins);
    datnew = cell(nn,1);
    for io = 1:nn
        datnew{io} = obj_string(objins(io));
    end
    datnew = [repmat({true},nn,2) datnew];
    dat = [dat; datnew];
    set(htbl,'Data',dat);
end

% rearrange in the new order
ind = [setdiff(1:length(obj),objn) objn];
[~,ind] = sort(ind);
obj = oRearrange(obj,ind,htbl);

% % adjust objfree list
% [objfree,objfree] = intersect(ind,objfree); % new objfree indices
% objfree = sort([objfree objn]); % inserted object are unlocked (free), add their indices to the list

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% obj = obj(ind);
% 
% % adjust object number for all objects after the 1st inserted object
% for ir = (min(objn)+1):length(obj)
%     obj(ir).objn = repmat(ir,1,length(obj(ir).x));
% end

% % draw in axes
% if ~isempty(ah)
%     for io = 1:length(objins)
%         obj(objn(io)).h = drawPolyline(ah,objins(io).x, objins(io).y, pp);
%     end
% end


function [obj,undo] = oBreak(obj,sel,htbl)
% Break continuous polyline segments.
% sel   two-column [objn noden]

sel = sortrows(sel);

objf = sel(:,1);
nodef = sel(:,2);

objn = [obj.objn];
noden = [obj.noden];
otype = [obj.type];
x = [obj.x];
y = [obj.y];
z = [obj.z];
code = [obj.code];

ifoc = find(ismember([objn' noden'],sel,'rows'));

% disregard 1st and last nodes of segments
nodepos = node1storlast(ifoc,otype(objf)',find(diff(objn)),find(isnan(x)),length(x));
idis = find(nodepos);
objf(idis) = [];
nodef(idis) = [];
ifoc(idis) = [];

objfu = unique(objf);
for io = objfu(:)'
    ic = objf==io;
    nodef(ic) = nodef(ic) + (0:length(find(ic))-1)'*2; % nodef needs to be sorted
end
    
termnan = NaN(size(objf))';
xins = [termnan; x(ifoc)];
yins = [termnan; y(ifoc)];
zins = [termnan; z(ifoc)];
codeins = [code(ifoc); code(ifoc)]; %%%%% so that no NaN codes
objf = [objf objf]';
nodef = [nodef+1 nodef+2]'; % last nodes excluded so we're safe here

[obj,undo] = oNodeInsert(obj,objf(:),nodef(:),xins(:),yins(:),zins(:),codeins(:),htbl); % reshape to vectors


function [obj,undo] = oConnect(obj,sel,htbl)
% Connect any two segments within one multiply-connected polyline.
% sel   two-column [objn noden]

objf = unique(sel(:,1));
if length(objf)>1 || obj(objf).type~=2
    undo = 'Segment connect requires two segments within one polyline object.';
    return
end
nodef = sort(sel(:,2));
obj1 = obj(objf);

cnoden = obj1_segment_get(obj1,nodef); % 2�1 cell array of node indices

% prepare nodes to insert
is = cnoden{2}(1:end-1); % exclude terminating NaN of 2nd segment

% if the joint points are identical we want to keep only one
if obj1.x(nodef(1)) == obj1.x(nodef(2)) && obj1.y(nodef(1)) == obj1.y(nodef(2))
    is(is==nodef(2)) = [];
end
x2 = obj1.x(is);
y2 = obj1.y(is);
z2 = obj1.z(is);
code2 = obj1.code(is);

% deleting later segment, so no effect on node indices for the subsequent
% node insert:
[obj,undonodedelete] = oNodeDelete(obj,repmat(objf,size(cnoden{2})),cnoden{2},htbl);

nodestatus = node1storlast(nodef,[],[],find(isnan(obj1.x)),length(obj1.x));

% the idea is to flip only 2nd segment if necessary
if (nodestatus(1) ~= 1 && nodestatus(2) ~= 1) || (nodestatus(1) == 1 && nodestatus(2) == 1)
    x2 = fliplr(x2);
    y2 = fliplr(y2);
    z2 = fliplr(z2);
    code2 = fliplr(code2);
end

% determine the start index where to insert the 2nd segment
if (nodestatus(1) ~= 1 && nodestatus(2) == 1) || (nodestatus(1) ~= 1 && nodestatus(2) ~= 1)
    % append to the end of the 1st segment (before terminating NaN)
    n1 = cnoden{1}(end); % index of terminating NaN of the 1st segment
else
    % prepend to the beginnig of the 1st segment (before 1st node)
    n1 = cnoden{1}(1); % index of the 1st node of the 1st segment
end

noden = n1:n1+length(x2)-1;
[obj,undonodeinsert] = oNodeInsert(obj,repmat(objf,size(noden)),noden,x2,y2,z2,code2,htbl);

undo.action = 'oConnect';
undo.label = 'connect segments';
undo.r.undonodedelete = undonodedelete; 
undo.r.undonodeinsert = undonodeinsert;


function [obj,undo] = oExtract(obj,sel,ah,htbl)
% Extract selected parts of node sets 
% or continuous polyline segment(s) into separate object(s).
% sel   two-column [objn noden]

% sel = sortrows(sel);

objf = sel(:,1);
objfu = unique(objf);
% if any([obj(objfu).type] ~= 2)
%     undo = 'Segment extraction works only for polylines.';
%     return
% end
if all([obj(objfu).type] == 2)
    otype = 2;
elseif all([obj(objfu).type] == 1)
    otype = 1;
else
    undo = 'Segment extraction works only for a set of polyline objects or for a set of node objects.';
    return
end
nodef = sel(:,2); % node numbers of the selected nodes

% raw = struct('x',[],'y',[],'z',[],'code',[]);
% raw(1) = []; % empty array
% r = raw;
r = obj_struct(0);
objnrem = [];
nodenrem = [];
pp = obj_default_pp;
pp(1) = []; % empty array
for io = objfu(:)' % for each selected object
    ic = objf==io; % nodes selected for this object
    obj1 = obj(io);
    if otype == 2
        % get entire segments for the selected nodes
        cnoden = obj1_segment_get(obj1,nodef(ic)); % cell array of node indices
    else % for node sets
        cnoden = {nodef(ic)};
    end
    
    % create raw structures
%     ro = raw;
    ncnoden = length(cnoden);
    ro = obj_struct(ncnoden);
    for is = 1:ncnoden
        ro(is).x = obj1.x(cnoden{is});
        ro(is).y = obj1.y(cnoden{is});
        ro(is).z = obj1.z(cnoden{is});
        ro(is).code = obj1.code(cnoden{is});
    end
    r = [r ro]; %#ok<AGROW>
    
    % record nodes to delete
    cnoden = cell2mat(cnoden);
    objnrem = [objnrem; repmat(io,size(cnoden))];  %#ok<AGROW>
    nodenrem = [nodenrem; cnoden]; %#ok<AGROW>
    pp = [pp; repmat(obj1.pp,size(cnoden))]; %#ok<AGROW>
end
[obj,undodelete] = oNodeDelete(obj,objnrem,nodenrem,htbl);
onew = oCreate(r, 1:length(r), otype);
[obj,undoload] = obj_load(obj, onew, ah, pp, htbl);

undo.action = 'oExtract';
if otype == 2
    undo.label = 'extract segement';
else
    undo.label = 'extract selected nodes';
end
undo.r.undodelete = undodelete;
undo.r.undoload = undoload;


function [obj,undo] = oCombine(obj,sel,htbl)
% Combine polyline objects in one object.
% sel   two-column [objn noden]

objf = unique(sel(:,1));
if ~(all([obj(objf).type] == 1) || all([obj(objf).type] == 2)) || length(objf)<2
    undo = 'Only several node sets or polylines can be combined.';
    return
end

% keep the 1st selected object 1st in the list
objf = [sel(1,1); setdiff(objf,sel(1,1))];

% updated data for the 1st object
x = [obj(objf).x];
y = [obj(objf).y];
z = [obj(objf).z];
code = [obj(objf).code];

% save undo information
noden = (length(obj(objf(1)).x)+1) : length(x);
undonodeinsert.action = 'oNodeInsert';
undonodeinsert.label = 'insert node';
undonodeinsert.r.objn = repmat(obj(objf(1)).objn(1), size(noden));
undonodeinsert.r.noden = noden;

obj(objf(1)) = obj_update_data(obj(objf(1)),htbl,x,y,[],z,code);

% delete the rest of the objects
[obj,undodelete] = oDelete(obj,objf(2:end),htbl);

% save undo information
undo.action = 'oCombine';
undo.label = 'combine objects';
undo.r.undodelete = undodelete;
undo.r.undonodeinsert = undonodeinsert;


function [obj,undo] = oDeleteSegment(obj,sel,htbl)
% Delete continuous polyline segment(s).
% sel   two-column [objn noden]

objf = sel(:,1);
objfu = unique(objf);
if any([obj(objfu).type] ~= 2)
    undo = 'Segments can be deleted only from polylines.';
    return
end
nodef = sel(:,2);

objnrem = [];
nodenrem = [];
for io = objfu(:)'
    ic = objf==io;
    obj1 = obj(io);
    cnoden = obj1_segment_get(obj1,nodef(ic)); % cell array of node indices
    
    % record nodes to delete
    cnoden = cell2mat(cnoden);
    objnrem = [objnrem; repmat(io,size(cnoden))];  %#ok<AGROW>
    nodenrem = [nodenrem; cnoden]; %#ok<AGROW>
end

[obj,undo] = oNodeDelete(obj,objnrem,nodenrem,htbl);



function seg = obj1_segment_get(obj1,noden)
% Returns cell array of node indices each corresponding to a continous
% segment containing one or more of the specified nodes in object obj1.

edges = [1 find(isnan(obj1.x))+1]; % segment edges
[~,bin] = histc(noden,edges);
bin = unique(bin);
seg = cell(length(bin),1); % column cell array
for ib = 1:length(bin)
    seg{ib} = (edges(bin(ib)):edges(bin(ib)+1)-1)'; % column vector in each cell
end


function cbPolyInterp(src,evd) %#ok<INUSD>
H = guidata(src);

allowableobjects = 2; % pick polylines only (Type 2 object)

% get LW contour from user
hmsg = msgbox('Pick outside polyline(s), e.g. Low Water Line for an island. When done press Enter to proceed. Esc to quit interpolation task.','Pick LWL','modal');
uiwait(hmsg)
olwn = [];
while true
    objn = pickobj(H.hfm,allowableobjects);
    if objn==0 % Escape pressed
        return
    elseif objn<0 % Enter or Space pressed
        break
    end
    olwn(end+1) = objn; %#ok<AGROW>
    set(H.statstr,'String',['Object picked: ' obj_string(H.obj(objn))])
end

% get HW contour from user
hmsg = msgbox('Pick inside polyline(s), e.g. High Water Line for an island. When done press Enter to proceed. Esc to quit interpolation task.','Pick HWL','modal');
uiwait(hmsg)
ohwn = [];
while true
    objn = pickobj(H.hfm,allowableobjects);
    if objn==0 % Escape pressed
        return
    elseif objn<0 % Enter or Space pressed
        break
    end
    ohwn(end+1) = objn; %#ok<AGROW>
    set(H.statstr,'String',['Object picked: ' obj_string(H.obj(objn))])
end

rdummy = struct('x',[],'y',[]);
poly = [[H.obj(olwn).x]' [H.obj(olwn).y]'];
rlw = poly2coast(poly,rdummy);
poly = [[H.obj(ohwn).x]' [H.obj(ohwn).y]'];
rhw = poly2coast(poly,rdummy);

% request interpolation parameters from user
zlw = [H.obj(olwn).z];
zlw = unique(zlw(~isnan(zlw)));
if length(zlw)~=1
    zlw = -1;
end
zhw = [H.obj(ohwn).z];
zhw = unique(zhw(~isnan(zhw)));
if length(zhw)~=1
    zhw = 1;
end
[ok,zlw,zhw,orphLWinc,orphHWinc] = dlgInterpParam(zlw,zhw);
if ~ok
    return
end

% perform interpolation
[rmw,rlw,rhw] = coast_mwl_x(rlw,rhw,zlw,zhw);
if orphLWinc
    rmw = [rmw rlw];
end
if orphHWinc
    rmw = [rmw rhw];
end

% form a raw structure by combining all segments in one object
r.x = [rmw.x];
r.y = [rmw.y];
r.z = zeros(size(r.x)); %%%%% need to place NaNs in place of terminating NaNs in r.x?

% insert interpolated contour as a new object
otype = 2;
onew = oCreate(r,[],otype); % length(H.obj)+1
H.obj = obj_load(H.obj,onew,H.ah,H.ppdef,H.tbl);

undo.action = 'oInsert';
undo.label = 'interpolate between polylines';
undo.r.objn = onew.objn(1);

H.undo = undo_register(H,undo);

set(H.statstr,'String',['Interpolated polyline created: ' obj_string(onew)])

guidata(src,H);


function cbPolySimplify(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1));
objn([H.obj(objn).type]~=2) = []; % leave only polylines

if isempty(objn)
    warndlg('No action was performed: Only polylines can be simplified.','Warning');
    return
end

% request parameters
% [ok,reduc,xyflag,inflag,mhel] = dlgSimplifyInPoly;
[ok,algorithm,dmin,dmax,reduc,xyflag,smallislandtreat,inflag] = dlgSimplify;
if ~ok
    return
end

if isnan(inflag)
    bound = [];
else
    % request a polygon to determine the area of operation
    hmsg = msgbox('Pick polygon to determine the area of operation. Esc to cancel the simplification task.','Area Specification','modal');
    uiwait(hmsg)
    allowableobjects = 2; % pick polylines only (Type 2 object)
    while true
        obound = pickobj(H.hfm,allowableobjects);
        if obound==0 % Escape pressed
            return
        elseif obound>0 % Object picked 
            break
        end
        % Enter or Space pressed, continue waiting for user picking an object or Cancel press
    end
    bound = [H.obj(obound).x' H.obj(obound).y'];
end

switch algorithm
    case 1 % Radial distance
        param = [];
    case 2 % Opheim
        param = dmax;
    case 3 % Douglas-Peucker
        dmin = reduc;
        param = xyflag;
end

% save undo data
undo.action = 'cedit';
undo.label = 'simplify polyline';
undo.r.objn = objn;
undo.r.obj = H.obj(objn);

set(H.hfm,'Pointer','watch')
H.obj(objn) = oSimplify(H.obj(objn),algorithm,param,dmin,bound,inflag,smallislandtreat,H.tbl);
set(H.hfm,'Pointer','arrow')

H.undo = undo_register(H,undo);

guidata(src,H);


function cbPolyUpsample(src,evd) %#ok<INUSD>
H = guidata(src);

objn = unique(H.sel(:,1));
objn([H.obj(objn).type]~=2) = []; % leave only polylines

if length(objn)~=1
    warndlg('No action was performed: Only one polyline at a time can be upsampled.','Warning');
    return
end

% request target spacing
prompt = {'Target spacing:'};
dlg_title = 'Setting the parameter';
d = inputdlg(prompt,dlg_title,1,{'10'});
if isempty(d) % if Cancel was pressed
    return
end
d = str2double(d{1});

o = H.obj(objn);

% save undo data
undo.action = 'cedit';
undo.label = 'upsample polyline';
undo.r.objn = objn;
undo.r.obj = o;

set(H.hfm,'Pointer','watch')
[x,y,z,code] = incres(o.x, o.y, d, o.z, o.code);
H.obj(objn) = obj_update_data(o,H.tbl,x,y,[],z,code);
set(H.hfm,'Pointer','arrow')

H.undo = undo_register(H,undo);

guidata(src,H);


function obj = oSimplify(obj,algorithm,param,dmin,bound,inflag,smallislandtreat,htbl)
% Simplify polyline objects.

for io = 1:length(obj)
    [xs,ys] = polyline_simplify(obj(io).x, obj(io).y, ...
        algorithm,param,dmin,bound,inflag,smallislandtreat);
    if isempty(xs)  % all small islands
        warndlg('For one or more polylines all segments have been eliminated in the process of simplification. No changes have been made to these objects. Delete the object if necessary.','Warning')
        %%%%% return ok flags for each object and remove the corresponding
        %%%%% objects from undo in cbPolySimplifyInOut and cbPolySimplify
        continue
    end
    obj(io) = obj_update_data(obj(io),htbl,xs,ys); % old values of z(1),objn(1) are expanded to all nodes 
end


function [xs,ys] = polyline_simplify(x,y,algorithm,param,dmin,bound,inflag,smallislandtreat)
% Simplify a coastline in x,y using specified algorithm
% dmin  shortest edge in the resulting polyline (also used for islands approximated with hexagons)
% For Douglas-Peuker, dmin is reduction parameter (defines a fraction of initial nodes
% to leave in the simplified polyline)

br = [0 find(isnan(x))];
xs = [];
ys = [];
for ibr = 1:length(br)-1 % for each continuous segment
    ic = (br(ibr)+1) : (br(ibr+1)-1); % excluding treminating NaN
    
    %%% there can occure zero-nodes segments 
    %%% -- a consequence of a bug in clip-by-polygon or polyline_simplify
    if ~isempty(ic) % normal case
        
        if isempty(bound)
            [xc,yc] = polyline_simplify_one(x(ic),y(ic),algorithm,param,dmin);
        else
            [xc,yc] = polyline_simplify_bound(x(ic),y(ic),algorithm,param,dmin,bound,inflag);
        end
        
%         %%%%debug
%         if any(poly_verify(xc,yc))
%             'stop'
%         end

        % treatment of small islands
        if xc(1)==xc(end) && yc(1)==yc(end) && length(xc)<6 % m/b 4 distinct nodes at least (+1 ending node)
            switch smallislandtreat
                case 2 % leave as it is
                    xc = x(ic);
                    yc = y(ic);
                case 1 % remove
                    continue % discard the island
                case 3 % try to approximate with a hexagon, if too small remove altogether
                    [xcc,ycc,r] = SmallestEnclosingCircle(x(ic), y(ic));
                    if r < dmin % the hexagon is too small
                        continue % discard the island
                    else % approximate with hexagon
                        rc = r/2; % r*cos(60*pi/180);
                        rs = r*sin(60*pi/180);
                        xc = [-rs 0 rs]; % offsets from center for the upper three nodes of the hexagon
                        yc = [ rc r rc];
                        xc = [xcc+xc xcc+fliplr(xc) xcc+xc(1)]; % upper nodes and lower nodes and close the hexagon
                        yc = [ycc+yc ycc-yc         ycc+yc(1)];
                    end
            end
        end
        
%         %%%%debug
%         if any(poly_verify(xc,yc))
%             'stop'
%         end

        %%% possible to avoid AGROW if collect in a cell array and then use
        %%% cell2mat
        xs = [xs xc NaN]; %#ok<AGROW>
        ys = [ys yc NaN]; %#ok<AGROW>
    end
end


function [xs,ys] = polyline_simplify_bound(x,y,algorithm,param,dmin,bound,inflag)
% Simplify one continuous segment of coastline within the area determined
% by bound and inflag. 
% bound     n�2 polygon, possibly multiply-connected
% inflag    if true operation is done on the parts of the polyline inside
%           of the bound, and on the outside part otherwise

% nb = size(bound,1);
% bnan = find(isnan(bound(:,1))); % terminating NaNs in a multiply-connected polygon
% bound(bnan,:) = [];
% edge = [(1:nb-1)' (2:nb)']; % raw edge array
% enan = [bnan-1; bnan];
% enan(enan>size(edge,1)) = [];
% edge(enan,:) = []; % remove NaN-containing edges
% %%%%% re-index edge to account for deleted NaN-nodes

[node,edge] = prepnodes_poly(bound);

try %%%%
in = inpoly([x(:) y(:)], node, edge);
catch err
    errordlg({'INPOLY error in polyline_simplify_bound.',err.message},'Error')
end
if ~inflag
    in = ~in;
end

db = diff(in); % in/out bounds
be = [0; find(db); length(x)]; % segment bounds
xs = [];
ys = [];
for ic = 1:length(be)-1 % for each in/out segment
    is = be(ic)+1 : be(ic+1);
    xc = x(is);
    yc = y(is);
    if in(is(1))
        [xc,yc] = polyline_simplify_one(xc,yc,algorithm,param,dmin);
    end
    xs = [xs xc]; %#ok<AGROW>
    ys = [ys yc]; %#ok<AGROW>
end

% if length(xs)<6 % m/b 4 distinct nodes at least (+1 ending node)
%     xs = x; % return to original values if the simplified curve is invalid
%     ys = y;
% end


function [x,y] = polyline_simplify_one(x,y,algorithm,param,dmin)
% Simplify one continuous segment of coastline.

if length(x)<6 % m/b 4 distinct nodes at least (+1 ending node)
    return
end

switch algorithm
    case 1 % Radial distance
        [x,y] = poly_simplify_dist(x,y,dmin);
    case 2 % Opheim
        dmax = param;
        [x,y] = poly_simplify_opheim(x,y,dmin,dmax);
    case 3 % Douglas-Peucker
        tol = 0;
        nmax = floor(length(x)/dmin); % reduc>1; invert, so the ratio<1;
        if param % true if 'XY' was selected
            [x,y] = geo_simplify(x, y, nmax, tol);
        else
            [x,y] =  xy_simplify(x, y, nmax, tol);
        end
end

% ensure rows
x = x(:)';
y = y(:)';



% Changing object order and layering objects in the axes
%-------------------------------------------------------

function [obj,ud] = oToFrontOnce(obj,objn,htbl)
% objn  unique sorted indices

%%%%%%%%%%%%%%%%%% Built-in function:
% uistack(h,stackopt)  moves the objects specified by h in the stacking
%                      order, where stackopt is one of the following:
% 'up' � moves h up one position in the stacking order
% 'down' � moves h down one position in the stacking order
% 'top' � moves h to the top of the current stack
% 'bottom' � moves h to the bottom of the current stack
      
no = length(obj);

if objn(1)==1
    i1st = find(diff(objn)~=1,1,'first')+1; % 1st object to move in the list
    objn = objn(i1st:end);
end

oorder = 1:no;
for im = objn(:)'
    oorder(im-1) = im;
    oorder(im) = im-1;
end

obj = oRearrange(obj,oorder,htbl);

% save undo data
ud.action = 'oRearrange';
ud.label = 'to front one level';
ud.r.oorder = sort(oorder);


function [obj,ud] = oToBackOnce(obj,objn,htbl)
% objn  unique sorted indices

no = length(obj);

if objn(end)==no
    ilast = find(diff(objn)~=1,1,'last'); % last object to move in the list
    objn = objn(1:ilast);
end

oorder = 1:no;
for im = fliplr(objn(:)') % start from the last object
    oorder(im+1) = im;
    oorder(im) = im+1;
end

obj = oRearrange(obj,oorder,htbl);

% save undo data
ud.action = 'oRearrange';
ud.label = 'to back one level';
ud.r.oorder = sort(oorder);


function [obj,ud] = oToFront(obj,objn,htbl)
% objn  unique sorted indices

no = length(obj);
oorder = [objn setdiff(1:no,objn)];
obj = oRearrange(obj,oorder,htbl);

% save undo data
ud.action = 'oRearrange';
ud.label = 'to front';
ud.r.oorder = sort(oorder);


function [obj,ud] = oToBack(obj,objn,htbl)
% objn  unique sorted indices

no = length(obj);
oorder = [setdiff(1:no,objn) objn];
obj = oRearrange(obj,oorder,htbl);

% save undo data
ud.action = 'oRearrange';
ud.label = 'to back';
ud.r.oorder = sort(oorder);


function obj = oRearrange(obj,oorder,htbl)

no = length(oorder); % number of objects

% indices of objects which will change their place in the list
objm = find((1:no)-oorder);
if isempty(objm)
    return
end

% horig = getHandles(obj(objm)); % graphic handles in the origial list
obj = obj(oorder); % rearrange objects
% hmove = getHandles(obj(objm)); % graphic handles in the changed list
% 
% % for objects without some handles, e.g. node sets we need to exclude
% % NaN-elements
% horig = horig(~isnan(horig));
% hmove = hmove(~isnan(hmove));
% 
% % rearrange axes children accordingly
% hp = get(ah,'Children');
% [ihp,ihp,ihorig] = intersect(hp,horig);
% hp(ihp) = hmove(ihorig);
% set(ah,'Children',hp)

% rearrange lines in the object list table
if ishandle(htbl)
    dat = get(htbl,'Data');
    dat = dat(oorder,:);
    set(htbl,'Data',dat)
end

% adjust object numbers
for io = objm
    obj(io).objn = repmat(io,1,length(obj(io).objn));
    setlayer(obj(io),io) % make it visually in the correct layer
end


function setlayer(obj,objn)
% Make the object visually in the correct layer by setting ZData property
% to objn.

%%%%% images are always below all other objects
if obj.type==4
    uistack(obj.h.img,'bottom')
    return
end

h = obj.h;
fn = fieldnames(h);
for ifn = 1:length(fn)
    fni = fn{ifn};
    if length(h.(fni))==1 && (~ishandle(h.(fni)) || strcmp(fni,'rsz')) % resize markers are always on top
        continue
    end
%     if strcmp(fni,'c') && obj.type==3 % for grids plotted as patches with Faces,Vertices
%         set(h.(fni),'ZData',-ones(size(obj.tri'))*objn)
%     else
        for ih = 1:length(h.(fni)) % codemarker is an array of handles
            set(h.(fni)(ih),'ZData',-ones(size(get(h.(fni)(ih),'XData')))*objn)
        end
%     end
end



%---------------------------------------------------
% Graphics routines

function h = handles_struct()
% Common graphic handles structure for all objects.
% The order of handles in h determines layering of graphic ojects: first handle
% is of the object on top of the others.

if verLessThan('matlab','9.1') % R2016b
    h = struct('colornodes',NaN,'c',NaN,'c1st',NaN,'clast',NaN,'rsz',NaN,...
         'badang',NaN,'badrat',NaN,'badlen',NaN,'eql',NaN,'codemarker',NaN,...
         'bathycolor',NaN,'bridge',NaN);
else
    hd = gobjects(1); % dummy graphics object
    h = struct('colornodes',hd,'c',hd,'c1st',hd,'clast',hd,'rsz',hd,...
         'badang',hd,'badrat',hd,'badlen',hd,'eql',hd,'codemarker',hd,...
         'bathycolor',hd,'bridge',hd);
end


function p = getPlotParam(obj)
% for io = 1:length(obj)
%     switch obj(io).type
%         case 2
%             p(io).color = get(obj(io).h.c,'Color');
%             p(io).flags.badang = strcmp(get(obj(io).h.badang,'Visible'),'on');
%             p(io).flags.badlen = strcmp(get(obj(io).h.badlen,'Visible'),'on');
%     end
% end
p = [obj.pp];


function obj = obj_draw(ah,obj,pp)
% Draw objects in axes ah.

if ~exist('pp','var') || isempty(pp)
    if isfield(obj,'pp') % && ~isempty(obj.pp)
        try
            pp = [obj.pp];
        catch err
            disp(err.message) %%% diagnostic message to Command Window
            ppdef = obj_default_pp;
            for io = 1:length(obj)
                obj(io).pp = obj_matchfields(ppdef, obj(io).pp);
            end
            pp = [obj.pp];
        end
    elseif obj.type~=4
        error('Required pp (plotting parameters) structure is absent.')
    end
end
if length(pp) == 1 && length(obj)>1
    pp = repmat(pp,size(obj));
end

%%% ensure backwards compatibility with V2.2
if ~isfield(pp,'codemarkerflag')
    [pp.codemarkerflag] = deal(false);
end
%%% ensure backwards compatibility with V2.80
if ~isfield(pp,'bathycolorflag')
    [pp.bathycolorflag] = deal(false);
end
%%% ensure backwards compatibility with V2.83
if ~isfield(pp,'bridgemarkerflag')
    [pp.bridgemarkerflag] = deal(false);
end

% first draw images (should be below other objects)
for io = length(obj):-1:1
    if obj(io).type == 4
        obj(io).h = obj_draw_one(ah, obj(io), pp(io));
        obj(io).pp = pp(io);
    end
end
% draw the rest of the objects
for io = length(obj):-1:1
    if obj(io).type ~= 4
        obj(io).h = obj_draw_one(ah, obj(io), pp(io));
        obj(io).pp = pp(io);
    end
end

% make sure colortri is on top
H = guidata(ah);
hc = get(ah,'Children');
set(ah,'Children',[H.colortri; hc(~ismember(hc,H.colortri))])

% make sure focus and selection markers are on top
hc = get(ah,'Children');
set(ah,'Children',[H.hfoc(:); H.hsel(:); hc(~ismember(hc,H.hfoc))])


function h = obj_draw_one(ah,obj,pp)
% Draw polyline [x,y] in axes ah.

% if ~exist('pp','var') || isempty(pp)
%     if isfield(obj,'pp') && ~isempty(obj.pp)
%         pp = obj.pp;
%     else
%         error('Required pp (plotting parameters) structure is absent.')
%     end
% end

h = handles_struct; % common graphic handles structure for all objects

x = obj.x;
y = obj.y;
z = obj.z;
switch obj.type(1)
    case 1 % set of nodes
        h = drawNodes(ah,h,x,y,z,pp);
    case 2 % polyline
        h = drawPolyline(ah,h,x,y,pp);
    case 3 % triangular grid
        h = drawTri(ah,h,x,y,z,obj.tri,pp);
    case 4 % image
        h = drawImage(ah,h,obj.imx,obj.imy,obj.cdata,obj.cmap);
end

if obj.type ~= 4
    h.colornodes = scatter([],[],pp.colornodes_size,[],'filled',...
        'Parent',ah,...
        'MarkerEdgeColor',pp.color,'Marker',pp.colornodes_marker,'HitTest','off');

    % colors for node codes 4-column array: [code rgb]
    %%%%% only these codes are allowed so far: 0 1 2 5 6
    %%%%% include nodecolormap in pp structure
    nodecolormap = [0 0 1 1; 1 .8 .4 0; 2 1 .5 0; 5 0 .35 .75; 6 .5 0 0];
    code = obj.code;
    for ic = 1:size(nodecolormap,1)
        in = code==nodecolormap(ic,1);
        if any(in)
            xin = x(in);
            yin = y(in);
        else
            xin = NaN;
            yin = NaN;
        end
        color = nodecolormap(ic,2:4);
        h.codemarker(ic) = plot(ah,xin,yin,'LineStyle','none','Marker','s','Color',color);
    end
    if ~pp.codemarkerflag
        set(h.codemarker,'Visible','off')
    end
end

% the resize handles (small squares on the bounds of the object)
% x1 = nanmin(x); x3 = nanmax(x); x2 = mean([x1 x3]);
% y1 = nanmin(y); y3 = nanmax(y); y2 = mean([y1 y3]);
x1 = min(x,[],'omitnan'); x3 = max(x,[],'omitnan'); x2 = mean([x1 x3]);
y1 = min(y,[],'omitnan'); y3 = max(y,[],'omitnan'); y2 = mean([y1 y3]);
xh = [x1 x1 x1 x2 x3 x3 x3 x2];
yh = [y1 y2 y3 y3 y3 y2 y1 y1];
rszcolor = [.5 .5 .5]; % resize handles color
h.rsz = plot(xh,yh,'LineStyle','none','Marker','s','MarkerSize',6,...
    'Color',rszcolor,'MarkerFaceColor',rszcolor,'Visible','off');
set(h.c,'UserData',h.rsz)


function h = drawNodes(ah,h,x,y,z,pp)
% Draw a set of nodes [x,y] in axes ah.

% smoothly colored bathymetry
if any(isnan(x)) || length(x)<3 % when starting freenode operation
    el = [1 1 1 1]';
else
    tri = delaunay(x,y);
    el = [tri tri(:,1)]'; % closed triangle patches
end
h.bathycolor = patch(x(el),y(el),z(el),'EdgeColor','none','Visible','off');
if pp.bathycolorflag
    set(h.bathycolor,'Visible','on')
end

% the nodes
h.c = plot(ah,x,y,'Color',pp.color,...
    'Marker',pp.marker,'MarkerSize',pp.markersize,'LineStyle','none');


function h = drawPolyline(ah,h,x,y,pp)
% Draw polyline [x,y] in axes ah.

% h.bathycolor = NaN;

[xs,ys,xe,ye] = polyline_1standlast(x,y);

h.c = plot(ah,x,y,'Color',pp.color,'LineWidth',pp.width,'LineStyle',pp.style,...
    'Marker',pp.marker,'MarkerSize',pp.width*2+12);  %,'ButtonDownFcn',@selectobject);
h.c1st = plot(ah,xs,ys,'Color',pp.color,'Marker','o','LineStyle','none',...
    'HitTest','off');
h.clast = plot(ah,xe,ye,'Color',pp.color,'Marker','+','LineStyle','none',...
    'HitTest','off');

if ~pp.endmarkerflag
    set(h.c1st,'Visible','off')
    set(h.clast,'Visible','off')
end

% polyline quality markers

% bad angle markers
[xa,ya] = polyline_quality_angle(x,y,pp.badangthres);
if isempty(xa) %%%%%%% can we leave them empty?
    xa = NaN; ya = NaN;
end
h.badang = plot(xa,ya,'^r'); 
if ~pp.badangflag
    set(h.badang,'Visible','off')
end

% bad ratio markers
[xr,yr] = polyline_quality_ratio(x,y,pp.badratthres);
if isempty(xr) %%%%%%% can we leave them empty?
    xr = NaN; yr = NaN;
end
h.badrat = plot(xr,yr,'vm'); 
if ~pp.badratflag
    set(h.badrat,'Visible','off')
end

% short edge markers
[xa,ya] = polyline_quality_edgelen(x,y,pp.badlenthres);
if isempty(xa) %%%%%%% can we leave them empty?
    xa = NaN; ya = NaN;
end
h.badlen = plot(xa,ya,'*r'); 
if ~pp.badlenflag
    set(h.badlen,'Visible','off')
end

if verLessThan('matlab','8.4') % R2014b
    set([h.c h.c1st h.clast h.badang h.badrat h.badlen], 'LineSmoothing',pp.linesmoothing)
end

% % fill
% [f,v] = poly2fv(x,y); % mapping toolbox %%%% very slow for long polylines
% h.bathycolor = patch('faces',f,'vertices',v,'facecolor',pp.color,'edgecolor','none','facealpha',.8,'Visible','off');
% if pp.bathycolorflag
%     set(h.bathycolor,'Visible','on')
% end


function h = drawTri(ah,h,x,y,z,tri,pp)
% Draw a triangular grid with nodes [x,y] and triangle array tri in axes ah.

% smoothly colored bathymetry
el = [tri tri(:,1)]'; % closed triangle patches
h.bathycolor = patch(x(el),y(el),z(el),'EdgeColor','none','Visible','off');
if pp.bathycolorflag
    set(h.bathycolor,'Visible','on')
end

% h.c = patch('parent',ah,'faces',tri,'vertices',[x' y'],...
%     'EdgeColor',pp.color,'facecolor','none', 'LineSmoothing','on');
% h.c = patch('parent',ah,'XData',x(tri'),'YData',y(tri'),...
%     'EdgeColor',pp.color,'facecolor','none', 'LineSmoothing','on');

[ebnd,eint] = grid_edge(tri);
e = [ebnd; eint];
xe = x(e);
ye = y(e);
xe = [x(e) NaN(size(xe,1),1)]';
ye = [y(e) NaN(size(ye,1),1)]';
xe = xe(:);
ye = ye(:);
h.c = plot(ah,xe,ye,'Color',pp.color,...
    'LineWidth',pp.width,'LineStyle',pp.style,'Marker',pp.marker);

% short edge markers
[xa,ya] = grid_quality_edgelen(x,y,tri,pp.badlenthres);
if isempty(xa) %%%%%%% can we leave them empty?
    xa = NaN; ya = NaN;
end
h.badlen = plot(xa,ya,'*r'); 
if ~pp.badlenflag
    set(h.badlen,'Visible','off')
end

% triangle quality markers
eql = 1./quality([x' y'],tri); % "equlateralness"
lmark = eql>pp.eqlthres; % logical indices
if any(lmark)
    tric = tri(lmark,:);
    xc = sum(x(tric),2)/3.0; % coordinates of triangle centroids
    yc = sum(y(tric),2)/3.0;
else
    xc = NaN; yc = NaN;
end
h.eql = plot(ah,xc,yc,'Marker','*','Color',pp.eqlcolor,'LineStyle','none','Clipping','on');
if ~pp.eqlflag
    set(h.eql,'Visible','off')
end

% dh/h markers
dhh = dh_over_h(tri, z); % "equlateralness"
lmark = dhh>pp.dhhthres; % logical indices
if any(lmark)
    tric = tri(lmark,:);
    xc = sum(x(tric),2)/3.0; % coordinates of triangle centroids
    yc = sum(y(tric),2)/3.0;
else
    xc = NaN; yc = NaN;
end
h.dhh = plot(ah,xc,yc,'Marker','*','Color',pp.dhhcolor,'LineStyle','none','Clipping','on');
if ~pp.dhhflag
    set(h.dhh,'Visible','off')
end

% bridge markers
ebr = grid_bridge(tri);
if isempty(ebr) %%%%%%% can we leave them empty?
    xb = NaN; yb = NaN;
else
    xb = mean(x(ebr),2);
    yb = mean(y(ebr),2);
end
h.bridge = plot(xb,yb,'*r'); 
if ~pp.bridgemarkerflag
    set(h.bridge,'Visible','off')
end

if verLessThan('matlab','8.4') % R2014b
    set([h.c h.badlen h.bridge], 'LineSmoothing',pp.linesmoothing)
end


function h = drawImage(ah,h,x,y,cdata,cmap)
% h.bathycolor = NaN;
h.c = plot(ah,x(1),y(1),'Color','k','LineStyle','none','Marker','s','MarkerSize',8);
% freezeColors(ah) %%%%%
h.img = image('XData',x,'YData',y,'CData',cdata,'Parent',ah); % low-level image drawing
if ~isempty(cmap)
    set(gcbf,'Colormap',cmap)
end
set(ah,'Layer','top')


function h = getHandles(obj)
no = length(obj);
fl = flipud(fieldnames(obj(1).h)); % fields in reversed order to presereve the order in the axes children list
nf = length(fl);
h = [];
for io = 1:no
    for ifl = 1:nf
        h = [h obj(io).h.(fl{ifl})];
    end
end

% function h = obj1_gethandles(obj1)


%%%% can use structdelete from Tools\Misc
function clearhandlestruct(h)
if isstruct(h)
    fl = fields(h);
    for ih = 1:length(h)
        for ifl = 1:length(fl)
            if ishandle(h(ih).(fl{ifl}))
                delete(h(ih).(fl{ifl}))
            end
        end
    end
end


function cleanfoc(hfoc)
% clean up the focus markers

set(hfoc(1),'XData',NaN,'YData',NaN)
set(hfoc(2),'String','')
