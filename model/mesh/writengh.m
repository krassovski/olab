function writengh(fname,x,y,z,code,nnum,con,offset_scale,comment)
% Write Trigrid NGH file.

ncon = size(con,2);

fid = fopen(fname,'w');

if ~isempty(comment)
    for ic = 1:length(comment)
        fprintf(fid,'%s\r\n',comment{ic});
    end
end
if ~isempty(comment) & strcmp(comment{1},'#NGH')    
    fprintf(fid,'%f %f %f %f %f\r\n',offset_scale);
    fprintf(fid,'%d\r\n',length(x));
    fprintf(fid,'%d\r\n',ncon);
else
    fprintf(fid,'%d\r\n',length(x));
    fprintf(fid,'%d\r\n',ncon);
    for io = 1:length(offset_scale)
        fprintf(fid,'%f ',offset_scale(io));
    end
    fprintf(fid,'\r\n');
end

formatstr = ['%7d%16.7E%16.7E%3d%16.7E' repmat('%8d',1,ncon) '\r\n'];

for ix = 1:length(x)
    fprintf(fid,formatstr,...
        nnum(ix),x(ix),y(ix),code(ix),z(ix),con(ix,:));
end

fclose(fid);