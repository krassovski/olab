function pins = grid_channelmid(p,t)
% Create nodes at mid-points of triangulation edges that have both boundary
% nodes (from different boundary edges) as their vertices.

% % coastline structure is specified
% bn = find(grid_iscoastalnode(po,r,1e-8)); % this picks only coastal boundary
% 

% lb = ismember(p,node,'rows'); % logical indices
% ib = find(lb); % numerical indices
% itb = all(ismember(t',ib)); % boundary triangle indices

% Get the unique edges and boundary nodes in a triangulation.
e = sortrows( sort([t(:,[1,2]); t(:,[1,3]); t(:,[2,3])],2) );
idx = all(diff(e,1)==0, 2);    % Find shared edges
idx = [idx;false]|[false;idx]; % True for all shared edges
eb = e(~idx,:);                % Boundary edges
ei = e(idx,:);                 % Internal edges
ei = ei(1:2:end-1,:);          % Unique internal edges

bn = unique(eb(:));
es = ei(all(ismember(ei',bn)) , :);    % internal edges to split
pins = 0.5*(p(es(:,1),:) + p(es(:,2),:)); % midpoints of the edges to split