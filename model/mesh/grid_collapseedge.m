function [x,y,tri,z,code] = grid_collapseedge(x,y,tri,icl,z,code)
% Collapse an edge.
% p     [x y] node coordinates
% tri   triangulation array (see dalaunay)
% icl   nodes defining the edge to collapse (numerical indices into p)
% code  node codes; the new nodes are coded same as the cleaved ones
% Returns updated p,tri,z,code.
% No checks for consistency of inputs are made.

% calls routines in: addpath('D:\Max\Projects\Femgrids\Tools\geom2d')

% place the new node in the middle of the edge
x(icl(1)) = mean(x(icl));
y(icl(1)) = mean(y(icl));
z(icl(1)) = mean(z(icl)); % linearly interpolate z as well
code(icl(1)) = max(code(icl)); % take higher code: the boundary codes prevail over the interior codes
tri(tri==icl(2)) = icl(1); % substitute Node 1 in place of Node 2
tri(sum(ismember(tri,icl),2)==2,:) = []; % delete triangles containing the edge