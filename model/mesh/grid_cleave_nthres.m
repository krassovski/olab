function [p,tri,z,code,perr] = grid_cleave_nthres(p,tri,nthres,z,code)
% Cleave nodes with more than n neighbours.
% p     [x y] node coordinates
% tri   triangulation array (see dalaunay)
% nthres     cleave nodes with more than nthres neighbours
% code  node codes; the new nodes are coded same as the cleaved ones
% Returns updated p,tri,z,code.

if ~exist('z','var') || isempty(z)
    z = zeros(size(p,1),1);
end

perr = [];
iclprev = [];
while true % iterate until all nn>n nodes are cleaved
    np = size(p,1); % number of nodes
    [ebnd,eint] = grid_edge(tri); % boundary and unique internal edges
    nn = histc(eint(:),1:np); % number of neigbours for each node in nint is the number of occurencies in edges
    
    icl = find(nn>=nthres & ~ismember((1:np)',unique(ebnd))); % nodes to cleave (too many neighbours and not boundary)
    
    if isempty(icl) || (length(icl)==length(iclprev) && all(icl==iclprev))
        break % quit if no more nodes to cleave or the nodes can not be cleaved in grid_cleave
    end
    iclprev = icl;
    
    [p,tri,z,code,perrit] = grid_cleave_n(p,tri,icl,z,code);
    perr = [perr; perrit];
end