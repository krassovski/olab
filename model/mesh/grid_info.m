function [inf,txt] = grid_info(x,y,tri,z,code,pp)
% Get info on the grid.

inf.nnode = length(x);
inf.ntri = size(tri,1);

[ebnd,eint] = grid_edge(tri);
e = [ebnd; eint];
el = (diff(x(e),1,2).^2 + diff(y(e),1,2).^2).^.5; % Euclidean distance

inf.elmin = min(el);
inf.elmax = max(el);

% number of neigbours for each node is the number of occurencies in edges
inf.nnei = max(histc(e(:),unique(e)));

txt = {'Grid info:','',...
    ['Number of nodes: ' num2str(inf.nnode)]};

% number of nodes by code
if exist('code','var') && ~isempty(code)
    codeu = unique(code(:));
    ncode = histc(code(:),codeu);
    inf.ncode = [codeu ncode];
    txt = [txt,{'Number of nodes by code:'}, {[repmat('    ',size(ncode)) num2str(inf.ncode)]}];
end

txt = [txt,...
    {['Number of triangles: ' num2str(inf.ntri)]},...
    {['Shortest edge: ' num2str(inf.elmin)]},...
    {['Longest edge: ' num2str(inf.elmax)]},...
    {['Largest number of neighbours: ' num2str(inf.nnei)]}];

% extract info from pp structure (VisualGrid specific)
if exist('pp','var') && ~isempty(pp)
    if ~isempty(pp.eqlthres) % triangle quality markers
        eql = 1./quality([x(:) y(:)], tri); % "equilateralness" with current threshold
        inf.neql = length(find(eql>pp.eqlthres)); % number of triangles not passing EQL test
        txt = [txt,{['Number of non-EQL triangles: ' num2str(inf.neql)]}];
    end
end

ebr = grid_bridge(tri); % "bridge" edges
inf.nbr = size(ebr,1); % number of bridges
txt = [txt,{['Number of "bridges": ' num2str(inf.nbr)]}];

ihang = tri_hangingnode(tri,length(x));
inf.nhang = length(ihang); % number of bridges
txt = [txt,{['Number of hanging nodes: ' num2str(inf.nhang)]}];

% z
if exist('z','var') && ~isempty(z)
    inf.zmin = min(z);
    inf.zmax = max(z);
    txt = [txt,{['Zmin: ' num2str(inf.zmin)],['Zmax: ' num2str(inf.zmax)]}];
end

