function dhh = dh_over_h(tri,h)
% dh/h for elements of a triangular grid

depi = h(tri);

% find nodes with min and max depth in this element
[dmin,kmin] = min(depi,[],2);
[dmax,kmax] = max(depi,[],2);

% for wetting and drying: use adjusted depths in the inter-tidal zone:
% deepen the entire element such that the shallowest node gets depth=min_depth
if exist('lwl','var') && ~isempty(lwl) && exist('min_depth','var') && ~isempty(min_depth)
    iadj = dmin<(lwl-min_depth); % if depth in the model can go below min_depth
    depi(iadj,:) = depi(iadj,:) - dmin(iadj)*ones(1,3) + min_depth; % shallowest node gets depth=min_depth
end

hav = mean(depi,2);

dhh = (dmax-dmin)./hav;