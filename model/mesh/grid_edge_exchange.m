function [t,err,lt,texold] = grid_edge_exchange(t,e)
% Replace an internal edge e, [i1 i2], in triangulation t with the
% alternative (crossing) edge.

lt = any(t'==e(1)) & any(t'==e(2)); % logical indices of triangles containing nodes in e
if length(find(lt))~=2
    err = 'grid_edge_exchange: an internal edge must be supplied; no exchange was performed';
    return
else
    err = 0;
end

tex = t(lt,:); % triangles sharing the edge to exchange
[ebnd,ediag] = grid_edge(tex); % diagonal edge is the only internal edge
n = unique(ebnd(:)'); % all nodes in the two triangles (there are no internal nodes)
ealt = setdiff(n,ediag); % the alternative diagonal edge

% exchange edges
nnew1 = setdiff(ealt,tex(1,:)); % node to sub into 1st triangle
nnew2 = ealt(ealt~=nnew1); % take the other node in ealt to sub into 2nd triangle
[nold1,nt] = setdiff(tex(1,:),nnew2);
tex(1,nt(1)) = nnew1; % sub into 1st available space

nt = tex(2,:)~=nnew1 & tex(2,:)~=nold1(1);
tex(2,nt) = nnew2;

texold = t(lt,:);

% substitute modified triangles
t(lt,:) = tex;