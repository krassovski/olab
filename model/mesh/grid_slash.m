function [pin,triin,zin,codein,pout,triout,zout,codeout] = ...
    grid_slash(p,tri,z,code,poly,boundaryaction,codesub)
% Cut a grid by a polygon.
% p     node coordinates [x1 y1; x2 y2,...].
% tri   connectivity array (see NGH format) or triangulation TRI array (see
%       dalaunay).
% poly  polygon to apply [x1 y1; x2 y2,...].
% boundaryaction    0   loses connecting edges between two parts (default)
%                   1   the inside part extended to keep connecting nodes with
%                       the outside part (these nodes become duplicated in
%                       inside and outside part)
%                   2   the outside part keeps connecting nodes
%                   3   bounding nodes are snapped to the polygon and kept in
%                       both parts; this makes the cut a straight line.
% 
% codesub  [codeold codenew] code substitution table for the new boundary nodes,
%          e.g codesub = [0 5; 1 6] changes codes 0 to 5, 1 to 6.
%          Used only if boundaryaction is 3

% Uses seg_dist.m from k_segments package 'D:\Max\MatlabTools\ks'

if ~exist('boundaryaction','var') || isempty(boundaryaction); boundaryaction = 0; end
if ~exist('z','var'); z = []; end
if ~exist('code','var'); code = []; end
if ~exist('codesub','var'); codesub = []; end

% check input
if size(tri,2) ~= 3
    error('Third argument should be triangles array (see dalaunay).')
end

% close the polygon
if poly(1,1) ~= poly(end,1) || poly(1,2) ~= poly(end,2)
    poly(end+1,:) = poly(1,:);
end

if boundaryaction==3
    [p,tri,z,code] = grid_snap2poly(p,tri,z,code,poly,codesub);
end

[in,on] = inpoly(p,poly,[],1e-4); % in includes on
% [in,on] = inpolygon(p(:,1),p(:,2),poly(:,1),poly(:,2)); % better detection of on, but slower
out = ~in | on;
in = find(in);
out = find(out);

% exclude triangles that are entirely in the other part;
% this is needed to exclude possible duplicating triangles (for boundaryaction 1 or 2)
tri_in  = tri(sum(ismember(tri,out),2)<3,:);
tri_out = tri(sum(ismember(tri, in),2)<3,:);

if boundaryaction==1 || boundaryaction==2
    [ebnd,eint] = grid_edge(tri);
    e = [ebnd; eint];
    e = e( sum(ismember(e,in),2)==1, : ); % only edges connecting two parts;
                                          % both in or out give same result
    if boundaryaction==1 % include connecting nodes from outside
        in = [in; setdiff(unique(e),in)];
    elseif boundaryaction==2 % include connecting nodes from inside
        out = [out; setdiff(unique(e),out)];
    end
end

% [pin,triin,zin,codein] = grid_extract(p,tri,z,code,in);
% [pout,triout,zout,codeout] = grid_extract(p,tri,z,code,out);
[pin,triin,zin,codein]     = grid_extract(p,tri_in, z,code,in);
[pout,triout,zout,codeout] = grid_extract(p,tri_out,z,code,out);

% % exclude possible duplicating triangles (for boundaryaction 1 or 2)
% if boundaryaction==1
%     % possible redundant triangels in the internal part
%     
% elseif boundaryaction==2
%     % possible redundant triangels in the external part
%     
% end