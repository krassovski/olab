function [ipl,ipu] = grid_localextrema(p,tri,z,excludeboundary)
% Find local lower and upper extrema of z defined on a triangular grid [p,tri].
% excludeboundary  [default is true] if true returns only internal extrema
%                  (excluding boundary nodes).
% Returns indices ipl and ipu in the rows of p array corresponding to lower
% and upper extrema.
% 
% M.Krassovski  26-Sep-2013

n = size(p,1); % number of nodes in the grid
if numel(z) ~= n
    error('Number of elements in Z should match number of rows in P.')
end

con = tri2con(tri,p(:,1),p(:,2)); % connectivity matrix

z = z(:); % ensure column
z = [z; NaN]; % add a fill-in value for zeros in con
con(con==0) = n+1; % make zero-elements point to NaN in z array

zcon = z(con); % z at connected nodes
z = repmat(z(1:n),1,size(con,2)); % expand z (excluding last NaN) to the size of con
inv = isnan(zcon); % consider only valid nodes in con
ipl = all(zcon>=z | inv, 2); % all values in each row are greater than z for the corresponding node
ipu = all(zcon<=z | inv, 2); % all values in each row are less than z for the corresponding node

% exclude boundary nodes if needed
if ~exist('excludeboundary','var') || isempty(excludeboundary) || excludeboundary
    ebnd = grid_edge(tri); % boundary edges
    ebnd = unique(ebnd(:));
    ipl(ebnd) = false;
    ipu(ebnd) = false;
end
