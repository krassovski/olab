function [rland,risland,rdiscarded,aw,gapp] = coast_joinseg(r,coordtype,tress)
% Combine coastline pieces into one continuous coastline and identify islands.
% For processing of CHS coastlines.
% coordtype     one of 'geo' or 'xy'

%FIXME: can place some islands in the land array

if ~exist('coordtype','var') || isempty(coordtype); coordtype = 'geo'; end
if ~exist('tress','var') || isempty(tress); tress = 10:10:290; end
% tres = 100; % m
% tress = 10:10:290; % m

if strncmpi(coordtype,'geo',3) && isempty(which('m_lldist'))
    olab_addpath('plot\m_map1.4') % m_lldist
end

% remove extra NaNs at the end of segments and remove empty segments
irem = false(size(r));
for ir = 1:length(r)
    while length(r(ir).x)>1 && isnan(r(ir).x(end)) && isnan(r(ir).x(end-1))
        r(ir).x(end) = [];
        r(ir).y(end) = [];
    end
    if length(r(ir).x)<2
        irem(ir) = true;
    end
end
r(irem) = [];

if length(r) == 1
    rland = r;
    risland = r; risland(1) = []; % initialize with dummy element
    rdiscarded = []; aw = []; gapp = [];
    return
end

if isfield(r,'Feature')
    % AW are artificial features
    % iaw = find(strncmpi([r.Feature],'AW',2) | strncmpi([r.Feature],'BRIDGE',6));
    iaw = find(strcmpi([r.Feature],'AWPRPIERRU') | ...
               strcmpi([r.Feature],'AWPRWHARFRU')); % remove ruined warfs and piers
    aw = r(iaw);
    r(iaw) = [];
else
    aw = struct_emptyfields(r(1));
end

% find islands
ixisl = false(size(r));
for ir = 1:length(r)
    [r(ir),ixisl(ir)] = islandcheck(r(ir),100,0,coordtype);
end
risland = r(ixisl);
rlseg = r(~ixisl);

[rland,risl_1,rdiscarded] = joinseg0(rlseg,coordtype);

risland = [risland risl_1];

% iterate through increasing thresholds
gapp = [];
for tres = tress
    if length(rland)<2
        break
    end
    disp(tres)
    [rland,risl_2,rdiscarded_,gappi] = joinsegtres(rland,tres,coordtype);
    gapp = [gapp; gappi];

    rdiscarded = [rdiscarded rdiscarded_];

    % second run for islands (with threshold)
    ixisl = false(size(rland));
    for ir = 1:length(rland)
        [r(ir),ixisl(ir),gappi] = islandcheck(rland(ir),100,tres,coordtype);
        if ~isempty(gappi)
            gapp(end+1,:) = gappi;
        end
    end
    risl_3 = rland(ixisl);
    rland = rland(~ixisl);
    
    risland = [risland risl_2 risl_3];
end

% check if all points in the initial dataset were accounted for
d = setdiff([[r.x]' [r.y]'],...
 [[rland.x,risland.x,rdiscarded.x,aw.x]' [rland.y,risland.y,rdiscarded.y,aw.y]'],...
            'rows');
if ~isempty(d(~isnan(d)))
    warning('Not all points in the initial dataset were accounted for.')
end

% rj = [rlandj risland risl_1 risl_2];


function [rland,risland,rdiscarded] = joinseg0(rlseg,coordtype)
% Join segments with exactly coinciding end points.

rland = rlseg([]);
risland = rlseg([]);
rdiscarded = rlseg([]);

% deal with coast that has already been combined from several segment and have
% several features
for ir = 1:length(rlseg)
    if isfield(rlseg(ir),'Feature') && length(rlseg(ir).Feature) > 1
        icl = find(strcmpi(rlseg(ir).Feature,'CLSL'), 1 );
        if isempty(icl)
            rlseg(ir).Feature = rlseg(ir).Feature(1); % take 1st feature
        else
            rlseg(ir).Feature = rlseg(ir).Feature(icl); % take CLSL
        end
    end
end

while ~isempty(rlseg)
    if isfield(rlseg,'Feature')
        % take 1st 'CLSL' segment (preference is given to CLSL)
        icl = find(strcmpi([rlseg.Feature],'CLSL'), 1 );
    else
        icl = [];
    end
    if isempty(icl)
        icl = 1; % if no CLSL elements, take the 1st one
    end
    rlone = rlseg(icl);
    rlseg(icl) = []; % remove selected element not to compare to itself
    ij = 1; % anything to start the cycle
    while ~isempty(ij) && ~isempty(rlseg)
        xb = rlone.x(1);
        yb = rlone.y(1);
        xe = rlone.x(end-1);
        ye = rlone.y(end-1);
        
        x = [rlseg.x];
        y = [rlseg.y];
        ib = [1 find(isnan(x(1:end-1)))+1]; % start of each segment
        ie = find(isnan(x))-1; % end of each segment

        ij = find(x(ib)==xe & y(ib)==ye); % compare end to beginnings
        if ~isempty(ij)
            if length(ij)>1
                isel = seg_select(rlseg(ij));
                idis = 1:length(ij) ~= isel;
                if ~isempty(rdiscarded)
                    rdiscarded = [rdiscarded rlseg(ij(idis))];
                else
                    rdiscarded = rlseg(ij(idis));
                end
            else
                isel = 1;
            end
            rlone = appendseg(rlone,rlseg(ij(isel)),0,1); % no flipping
            rlseg(ij) = [];
            continue
        end

        ij = find(x(ie)==xe & y(ie)==ye); % compare end to ends
        if ~isempty(ij)
            if length(ij)>1
                isel = seg_select(rlseg(ij));
                idis = 1:length(ij) ~= isel;
                if ~isempty(rdiscarded)
                    rdiscarded = [rdiscarded rlseg(ij(idis))];
                else
                    rdiscarded = rlseg(ij(idis));
                end
            else
                isel = 1;
            end
            rlone = appendseg(rlone,rlseg(ij(isel)),2,1); % flip rlseg(ij) and append
            rlseg(ij) = [];
            continue
        end

        ij = find(x(ib)==xb & y(ib)==yb); % compare beginning to beginnings
        if ~isempty(ij)
            if length(ij)>1
                isel = seg_select(rlseg(ij));
                idis = 1:length(ij) ~= isel;
                if ~isempty(rdiscarded)
                    rdiscarded = [rdiscarded rlseg(ij(idis))];
                else
                    rdiscarded = rlseg(ij(idis));
                end
            else
                isel = 1;
            end
            rlone = appendseg(rlseg(ij(isel)),rlone,1,1); 
                % flip rlseg(ij) and append at the beginning
            rlseg(ij) = [];
            continue
        end

        ij = find(x(ie)==xb & y(ie)==yb); % compare beginning to ends
        if ~isempty(ij)
            if length(ij)>1
                isel = seg_select(rlseg(ij));
                idis = 1:length(ij) ~= isel;
                if ~isempty(rdiscarded)
                    rdiscarded = [rdiscarded rlseg(ij(idis))];
                else
                    rdiscarded = rlseg(ij(idis));
                end
            else
                isel = 1;
            end
            rlone = appendseg(rlseg(ij(isel)),rlone,0,1); % no flipping
            rlseg(ij) = [];
            continue
        end
    end
    
    % check if it became an island
    [rlone,isisl] = islandcheck(rlone,100,0,coordtype);
    if isisl
        if ~isempty(risland)
            risland(end+1) = rlone;
        else
            risland = rlone;
        end
    else
        if ~isempty(rland)
            rland(end+1) = rlone;
        else
            rland = rlone;
        end
    end
end


function [rland,risland,rdiscarded,gapp] = joinsegtres(rlseg,tres,coordtype)
% Join segments with end points within distance of TRES metres.

rland = rlseg([]);
risland = rlseg([]);
rdiscarded = rlseg([]);
gapp = [];

while ~isempty(rlseg)
    rlone = rlseg(1);
    rlseg(1) = []; % remove 1st element not to compare to itself
    ij = 1; % anything to start the cycle
    while ~isempty(ij) && ~isempty(rlseg)
        ij = [];
        
        xb = rlone.x(1);
        yb = rlone.y(1);
        xe = rlone.x(end-1);
        ye = rlone.y(end-1);
        
        x = [rlseg.x];
        y = [rlseg.y];
        ib = [1 find(isnan(x(1:end-1)))+1]; % start of each segment
        ie = find(isnan(x))-1; % end of each segment

        % compare end to beginnings
        if strncmpi(coordtype,'geo',3)
            xx = [repmat(xe,size(ib)); x(ib)];
            xx = xx(:);
            yy = [repmat(ye,size(ib)); y(ib)];
            yy = yy(:);
            d = m_lldist(xx,yy);
            d(2:2:end) = [];
        else % Euclidean
            d = sqrt((x(ib)-xe).^2 + (y(ib)-ye).^2);
        end
        if min(d)<=tres
            ij = find(d==min(d));
            gapp(end+1,:) = [xe ye x(ib(ij(1))) y(ib(ij(1)))];
            if length(ij)>1
                isel = seg_select(rlseg(ij));
                idis = 1:length(ij) ~= isel;
                if ~isempty(rdiscarded)
                    rdiscarded = [rdiscarded rlseg(ij(idis))];
                else
                    rdiscarded = rlseg(ij(idis));
                end
            else
                isel = 1;
            end
            rlone = appendseg(rlone,rlseg(ij(isel)),0,0); % no flipping
            rlseg(ij(isel)) = [];
            continue
        end

        % compare end to ends
        if strncmpi(coordtype,'geo',3)
            xx = [repmat(xe,size(ie)); x(ie)];
            xx = xx(:);
            yy = [repmat(ye,size(ie)); y(ie)];
            yy = yy(:);
            d = m_lldist(xx,yy);
            d(2:2:end) = [];
        else % Euclidean
            d = sqrt((x(ie)-xe).^2 + (y(ie)-ye).^2);
        end
        if min(d)<=tres
            ij = find(d==min(d));
            gapp(end+1,:) = [xe ye x(ie(ij(1))) y(ie(ij(1)))];
            if length(ij)>1
                isel = seg_select(rlseg(ij));
                idis = 1:length(ij) ~= isel;
                if ~isempty(rdiscarded)
                    rdiscarded = [rdiscarded rlseg(ij(idis))];
                else
                    rdiscarded = rlseg(ij(idis));
                end
            else
                isel = 1;
            end
            rlone = appendseg(rlone,rlseg(ij(isel)),2,0); % flip rlseg(ij) and append
            rlseg(ij(isel)) = [];
            continue
        end

        % compare beginning to beginnings
        if strncmpi(coordtype,'geo',3)
            xx = [repmat(xb,size(ib)); x(ib)];
            xx = xx(:);
            yy = [repmat(yb,size(ib)); y(ib)];
            yy = yy(:);
            d = m_lldist(xx,yy);
            d(2:2:end) = [];
        else % Euclidean
            d = sqrt((x(ib)-xb).^2 + (y(ib)-yb).^2);
        end
        if min(d)<=tres
            ij = find(d==min(d));
            gapp(end+1,:) = [xb yb x(ib(ij(1))) y(ib(ij(1)))];
            if length(ij)>1
                isel = seg_select(rlseg(ij));
                idis = 1:length(ij) ~= isel;
                if ~isempty(rdiscarded)
                    rdiscarded = [rdiscarded rlseg(ij(idis))];
                else
                    rdiscarded = rlseg(ij(idis));
                end
            else
                isel = 1;
            end
            rlone = appendseg(rlseg(ij(isel)),rlone,1,0);
                % flip rlseg(ij) and append at the beginning
            rlseg(ij(isel)) = [];
            continue
        end

        % compare beginning to ends
        if strncmpi(coordtype,'geo',3)
            xx = [repmat(xb,size(ie)); x(ie)];
            xx = xx(:);
            yy = [repmat(yb,size(ie)); y(ie)];
            yy = yy(:);
            d = m_lldist(xx,yy);
            d(2:2:end) = [];
        else % Euclidean
            d = sqrt((x(ie)-xb).^2 + (y(ie)-yb).^2);
        end
        if min(d)<=tres
            ij = find(d==min(d));
            gapp(end+1,:) = [xb yb x(ie(ij(1))) y(ie(ij(1)))];
            if length(ij)>1
                isel = seg_select(rlseg(ij));
                idis = 1:length(ij) ~= isel;
                if ~isempty(rdiscarded)
                    rdiscarded = [rdiscarded rlseg(ij(idis))];
                else
                    rdiscarded = rlseg(ij(idis));
                end
            else
                isel = 1;
            end
            rlone = appendseg(rlseg(ij(isel)),rlone,0,0); % no flipping
            rlseg(ij(isel)) = [];
            continue
        end
    end
    
    % check if it became an island
    [rlone,isisl] = islandcheck(rlone,100,0,coordtype);
    if isisl
        if ~isempty(risland)
            risland(end+1) = rlone;
        else
            risland = rlone;
        end
    else
        if ~isempty(rland)
            rland(end+1) = rlone;
        else
            rland = rlone;
        end
    end
end
    

function rapp = appendseg(r1,r2,iflip,discardjoint)
% iflip 0, 1, or 2 (0 - no flipping)
% discardjoint: if the end points are identical you want to keep only one,
% set discardjoint to true, otherwise keep both, set discardjoint=false.

switch iflip
    case 0
        % do nothing
    case 1
        r1.x = fliplr(r1.x);
        r1.y = fliplr(r1.y);
        if isfield(r1,'z'); r1.z = fliplr(r1.z); end
    case 2 
        r2.x = fliplr(r2.x); % NaN is in front now
        r2.y = fliplr(r2.y);
        if isfield(r2,'z'); r2.z = fliplr(r2.z); end
        
        r2.x = [r2.x(2:end) NaN]; % move NaN to the end
        r2.y = [r2.y(2:end) NaN];
        if isfield(r2,'z'); r2.z = [r2.z(2:end) NaN]; end
    otherwise
        error('Wrong flip code')
end

% remove NaN from the 1st line
r1.x(isnan(r1.x)) = [];
r1.y(isnan(r1.y)) = [];
if isfield(r1,'z'); r1.z(isnan(r1.z)) = []; end

if discardjoint
    xa = [r1.x r2.x(2:end)];
    ya = [r1.y r2.y(2:end)];
    if isfield(r1,'z'); za = [r1.z r2.z(2:end)]; end
    if isfield(r1,'m'); ma = [r1.m; r2.m(2:end)]; end
else
    xa = [r1.x r2.x];
    ya = [r1.y r2.y];
    if isfield(r1,'z'); za = [r1.z r2.z]; end
    if isfield(r1,'m'); ma = [r1.m; r2.m]; end
end

% copy underlying structure fields
% rapp = r1;
% flds = fieldnames(rapp);
% for ifl = 1:length(flds)
%     rapp.(flds{ifl}) = [];
% end
rapp = struct_emptyfields(r1);

rapp.x = xa;
rapp.y = ya;

% populate the rest of the fields
if ~isempty(setdiff(fieldnames(rapp),{'x','y'}))
    rapp.number = r1.number;
    rapp.boundbox = [min(xa); min(ya); max(xa); max(ya)];
    rapp.numparts = 1;
    rapp.numpoints = length(xa);
    rapp.parts = 0;
    
    if isfield(r1,'z')
        rapp.zmin = min(za);
        rapp.zmax = max(za);
        rapp.z = za;
    end
    if isfield(r1,'m')
        rapp.mmin = min(ma);
        rapp.mmax = max(ma);
        rapp.m = ma;
    end

    %dbf fields
    % rapp.Key = unique([r1.Key, r2.Key]);
    if isfield(r1,'Feature'); rapp.Feature = unique([r1.Feature r2.Feature]); end
    if isfield(r1,'Source');rapp.Source = unique([r1.Source r2.Source]); end
    if isfield(r1,'Theme');rapp.Theme = unique([r1.Theme r2.Theme]); end
    if isfield(r1,'Length');rapp.Length = r1.Length + r2.Length; end
    if isfield(r1,'MinZ');rapp.MinZ = unique([r1.MinZ r2.MinZ]); end
end


function isel = seg_select(r)
% Select one element of coastline based on preferense rules.

if isfield(r,'Feature')
    isel = find(strcmpi([r.Feature],'CLSL'));
else
    isel = [];
end
switch length(isel)
    case 1
        return
    case 0
        isel = 1:length(r);
end % otherwise leave isel as it is

if ~isfield(r,'Length') || isempty(r(1).Length) %%%% checking only 1st element
    for ir = 1:length(r)
        r(ir).Length = sum(sqrt(diff(r(ir).x).^2 + diff(r(ir).y).^2),'omitnan');
    end
end

% if more than 1 CLSL piece or not at all, find the shortest one
d = [r(isel).Length];

% if more than 2 of same length, take 1st shortest
isel = isel( find(d==min(d), 1) );