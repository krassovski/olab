function t = cdt_mk(p,node,edge)
% Approximate geometry-constrained Delaunay triangulation.

t = mydelaunayn(p);                   % Delaunay triangulation via QHULL

% Impose geometry constraints
i = inpoly(tricentre(t,p),node,edge); % Take triangles with internal centroids
t = t(i,:);


function fc = tricentre(t,f)
% Interpolate nodal F to the centroid of the triangles T.

fc = (f(t(:,1),:)+f(t(:,2),:)+f(t(:,3),:))/3.0;