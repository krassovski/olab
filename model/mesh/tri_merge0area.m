function [p,tri,nremoved,nkept] = tri_merge0area(p,tri)
% Eliminate zero-area triangles: internal triangles are eliminated by merging
% nodes belonging to the shortest edge in the triangle; boundary triangles are
% deleted.
% nremoved,nkept    indices of nodes in the original p which were removed
%                   and kept in the proces of merging; can be used to
%                   update z and code in grid manipulations.

TOL = 1.0e-10;

% Triangle area
a = triarea(p,tri); % Mesh2d package %%%%% negative A is for triangles oriented CW
k = abs(a)<TOL*norm(a,'inf');
if all(~k)
    return
end
ebnd = grid_edge(tri);
ek = sort([tri(k,[1,2]); tri(k,[1,3]); tri(k,[2,3])],2); % each edge is a row, sorted only within each row
iib = find(ismember(ek,ebnd,'rows'));
iib = unique(ceil(iib/3)); % indices into kn
kn = find(k);
tri(kn(iib),:) = []; % remove boundary 0-area triangles
k(kn(iib)) = []; % corresponding logical indices
% kn = find(k); % updated numerical indices

np = size(p,1);

no = 1:np; % original node order
nremoved = false(np,1); % initialize as logical arrays to keep the size unchanged in the while-loop
nkept = nremoved;

while any(k)
    i = find(k,1);
    ec = sort([tri(i,[1,2]); tri(i,[1,3]); tri(i,[2,3])],2); % current triangle edges
    len = sqrt(sum((p(ec(:,1),:) - p(ec(:,2),:)).^2,2));
    im = ec(find(len==min(len),1),:); % nodes to merge
    [p,tri,irem] = tri_mergenodes(p,tri,im);
    
    nkept(no(im(1))) = true;
    nremoved(no(im(2))) = true;
    no(im(2)) = []; % this node has just been removed
    
    k(irem) = [];
end
nkept = find(nkept);
nremoved = find(nremoved);