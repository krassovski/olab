function inv = whichside(polyl,polyh,px,posxl,posxh,projl)
% polyl and polyh must be ordered in same direction, e.g. high values are on the
% same side of both LW and HW contours
% Assuming posxl is in increasing order.
%%%%% posxh in non-increasing order can give troubles; NaNs will be in polyh if
%%%%% it consits of several parts

% remove trailing NaN in LW
if isnan(polyl(end,1))
    polyl(end,:) = [];
end

nl = size(polyl,1);

% check both posxl and posxh are in increasing order
if any(diff(posxl<=0)) % || any(diff(posxh<=0)) posxh can be in any order, e.g. when HW islands are present or LW is an island
    warning('posxl should be in increasing order')
end

% check if it's an island
if all((polyl(1,:)-polyl(end,:))==0)
    isisl = true;
else
    isisl = false;
end

% to treat islands correctly
% prepend the penultimate node (the last before the seam) and append the 2nd node
polyl = [polyl(end-1,:); polyl; polyl(2,:)];
% also expand projections array so the sizes are the same, the prepended value
% itself is not used
projl = [projl(end-1,:); projl; projl(2,:)];

%%%%% Test for all except the last projection, assuming the last projection is ok;
%%%%% for the 1st and last there is no objective way to determine the side,
%%%%% unless it is an island. For an island, the last node (~= the 1st) is checked.
% vector = edge end - edge start
v1 = polyl(1:end-2,:) - polyl(2:end-1,:); % edge before the tested node
v2 = polyl(3:end,:)   - polyl(2:end-1,:); % edge after the tested node
v3 = projl(2:end-1,:) - polyl(2:end-1,:); % connection edge (projection edge)
theta1 = atan2(v1(:,2),v1(:,1));
theta2 = atan2(v2(:,2),v2(:,1));
theta3 = atan2(v3(:,2),v3(:,1));
aspan = mod(theta2-theta1+2*pi, 2*pi); % angle between the before-edge and after-edge (0 to 2*pi, counter-clockwise)
aproj = mod(theta3-theta1+2*pi, 2*pi); % angle between the before-edge and projection edge
side = aproj < aspan; % 1 if projection goes to the right of the contour, 0 if to the left
if ~isisl
    side(1) = true; side(end) = true;
end

% remove extra nodes in front and at the end added earlier
polyl(1,:) = [];
polyl(end,:) = [];
% projl(1,:) = [];
% projl(end,:) = [];

if ~isempty(px) % if there are intersections
    % remove 1st and last intersection points if they fall on 1st or last node
    if posxl(1) == 0 %%%%% use tolerance comparison?
        posxl(1) = [];
        posxh(1) = [];
        px(1,:) = [];
    end
    if posxl(end) == nl-1
        posxl(end) = [];
        posxh(end) = [];
        px(end,:) = [];
    end
    if posxh(1) == 0 % 1st HW node is an intersection
        if ~isisl
            side(1:floor(posxl(1))) = NaN; % no sense in determining side for LW before this point
        end
        posxl(1) = [];
        posxh(1) = [];
        px(1,:) = [];
    end
    if posxh(end) == size(polyh,1)-1 % last HW node is an intersection
        if ~isisl
            side(ceil(posxl(end)):end) = NaN; % no sense in determining side for LW after this point
        end
        posxl(end) = [];
        posxh(end) = [];
        px(end,:) = [];
    end
        
    iprev = floor(posxl)+1; % index of the node before intersection
    ixnode = abs(posxl-round(posxl))<1e-9; % indices of intersection points that fall on LW nodes (or very close to)
    iprev(ixnode) = iprev(ixnode)-1; % take the previous node for the intersection points which fall on nodes
    
%         % calculate correct side for the part before 1st intersection
%         % calculate angles backwards from 1st intersection
%         %%%%% suppose there is at least one node after 1st intersection
%         v1 = polyl(inextl(1),:) - px; % lw edge "before" the intersection
%         v2 = polyl(iprev(1),:) - px; % lw edge "after" the intersection
        
%         v3 = polyh(iprevh0,:) - px; % hw edge "after" the intersection
%         theta1 = atan2(v1(:,2),v1(:,1));
%         theta2 = atan2(v2(:,2),v2(:,1));
%         theta3 = atan2(v3(:,2),v3(:,1));
%         aspan = mod(theta2-theta1+2*pi, 2*pi); % angle between the before-edge and after-edge (0 to 2*pi, counter-clockwise)
%         aproj = mod(theta3-theta1+2*pi, 2*pi); % angle between the before-edge and projection edge
%         corside0 = aproj < aspan; % 1 if HW goes to the right of the LW contour, 0 if to the left

    v1 = polyl(iprev,:) - px; % lw edge before the intersection
    v2 = polyl(floor(posxl)+2,:) - px; % lw edge after the intersection
    v3 = polyh(floor(posxh)+2,:) - px; % hw edge after the intersection
    
    % prepend vertices in reversed direction to determine the correct side for
    % the part before 1st intersection
    v1 = [v2(1,:); v1];
    v2 = [v1(2,:); v2]; %!!! v1 is changed, now we need 2nd node, which was the 1st node before the change
    if abs(posxh(1)-round(posxh(1)))<1e-9 % 1st intersection point is a HW node (but not the 1st node)
        iprevh0 = floor(posxh(1));
    else
        iprevh0 = floor(posxh(1))+1;
    end
    v3 = [polyh(iprevh0,:) - px(1,:); v3];
    
    theta1 = atan2(v1(:,2),v1(:,1));
    theta2 = atan2(v2(:,2),v2(:,1));
    theta3 = atan2(v3(:,2),v3(:,1));
    aspan = mod(theta2-theta1+2*pi, 2*pi); % angle between the before-edge and after-edge (0 to 2*pi, counter-clockwise)
    aproj = mod(theta3-theta1+2*pi, 2*pi); % angle between the before-edge and projection edge
    corside = aproj < aspan; % 1 if HW goes to the right of the LW contour, 0 if to the left
    corside(1) = ~corside(1); % invert corside for the part before 1st intersection since it was calculated "backwards"

%     v1 = polyl(floor(posxl)+2,:)-px;
%     v2 = polyh(floor(posxh)+2,:)-px;
%     corside = mod(atan2(v2(:,2),v2(:,1)) - atan2(v1(:,2),v1(:,1))+2*pi, 2*pi)>pi;
%     %%%%%%% this will not work when LW and HW intersect at the nodes where they both bend
%     %%%%%%% backwards: need to calculate crossing points at nodes separately
%%%% debug dump:     load whichside_test01
%     plot(diff(corside),'.')
%     inode = find(abs(posxl-round(posxl))<1e-9 | abs(posxh-round(posxh))<1e-9);
%     difcor = diff(corside); 
%     plot(inode,difcor(inode),'or');
%
%     plot(polyl(:,1),polyl(:,2),'.-r',polyh(:,1),polyh(:,2),'.-b')
%     axis equal; grid on; hold on
%     inodi = abs(posxl-round(posxl))<1e-9 | abs(posxh-round(posxh))<1e-9;
%     plot(px(difcor==0 & ~inodi,1),px(difcor==0 & ~inodi,2),'ok')
%     plot(px(4401,1),px(4401,2),'ok')
%     plot(px(4402,1),px(4402,2),'*k')
%%%%

%     % correct side should change after each intersection which is not at node
%     if ~all(diff(corside)==1 | diff(corside)==-1)
%         warning('Incorrectly determined internal side between LW and HW contours.')
%     end
    
%     % add side for the beginning part (until 1st intersection point)
%     % assuming side changes after each intersection, the 1st should be opposite
%     % to the 2nd
%     corside = [~corside(1); corside];

    [bin,bin] = histc(0:nl-1, [0; posxl; nl]); 
    corside = corside(bin);
else
    % if no intersections, define correct side as the one where most of the
    % projections go
    if length(find(side)) > length(find(~side))
        corside = true;
    else
        corside = false;
    end
    corside = repmat(corside,nl,1);
end

inv = side~=corside;

% %%%%% test for all except the last projection, assuming the last projection is ok
% v1 = polyl(2:end,:)   - polyl(1:end-1,:);
% v2 = projl(1:end-1,:) - polyl(1:end-1,:);
% side = mod(atan2(v2(:,2),v2(:,1)) - atan2(v1(:,2),v1(:,1))+2*pi, 2*pi)>pi;
% inv = side~=corside;