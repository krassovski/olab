function [c,errn,wrn,e] = edge_extract_loop(e,n1)
% Extract a closed path connecting edges in e.
% e     N-by-2 array with each row containing a pair of node indices; e is not
%       arranged consequitively.
% n1    [optional] node to start from. Default: 1st node of the 1st edge.
% Output is an array of consequitive node indices (1st and last are the same).

% ensure nodes are distinct for each edge
idup = e(:,1)==e(:,2);
if any(idup)
    e(idup,:) = [];
    wrn = 'Zero-length edges have been eliminated.';
else
    wrn = false;
end

errn = false;

c = e; % initialize output array
if ~exist('n1','var') || isempty(n1)
    c(1:2) = e(1,:);
    n1 = c(1); % marker for the start node of the current edge
    n2 = c(2); % marker for the end node of the current edge
else
    e1 = e(any(e==n1,2),:); % edges containing n1
    e1 = e1(1,:); % take one
    n2 = e1(e1~=n1);
    c(1) = n1;
    c(2) = n2;
end
enext = e(any(e==n2,2) & all(e~=n1,2), :);
n1 = n2;
ic = 3;
while true
    if size(enext,1) == 1 % continuing a path without branching
        n2 = enext(enext~=n1); % marker for the end node of the current edge
        c(ic) = n2;
        ic = ic + 1;
         
        if n2==c(1) % back at the start node, return
            c(ic:end) = []; % remove unused part
            return
        end

        enext = e(any(e==n2,2) & all(e~=n1,2), :); % find next edge (do not go back)
        n1 = n2; % move the start node marker
    else % boundary branches
        errn = n1;
        return
    end
end