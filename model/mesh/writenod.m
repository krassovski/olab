function writenod(fname,polyz,xyz)
% Write nodes to Trigrid NOD file (old format).
% fname     full file name to write to
% polyz     np-by-3 array [x,y,z] a set of continuous contours separated by rows
%           of NaNs
% xyz       nu-by-3 array [x,y,z] of unordered points 
% polyz and/or xyz can be np-by-2 arrays, in which case z will be set to zero.

inan = find(isnan(polyz(:,1)))'; % row vector

[np,nc] = size(polyz);
if nc == 2
    polyz = [polyz zeros(np,1)];
end

[nu,nc] = size(xyz);
if nc == 2
    xyz = [xyz zeros(nu,1)];
end

fid = fopen(fname,'w');

istart = 1;

% boundary nodes
if ~isempty(polyz)
    % total number of nodes
    fprintf(fid,'%d\r\n', np+nu);

    % number of boundaries (continuous segments in polyz)
    fprintf(fid,'%d\r\n', length(inan));

    for iend = inan % for each boundary
        nn = iend-istart;
        fprintf(fid,'%d\r\n', nn);
        for in = istart:iend-1 % discard terminating NaNs
            fprintf(fid,'%d %d %d\r\n',polyz(in,:));
        end
        istart = iend+1;
    end
else
    % total number of nodes including bounding rectangle
    fprintf(fid,'%d\r\n%d\r\n',nu+4,1);
    
    % number of boundaries, 1
    fprintf(fid,'%d\r\n', length(inan));
    
    % add rectangular outer boundary: bounding rectangle with margins
    write_rect(fid,[min(xyz(:,1)) max(xyz(:,1))],[min(xyz(:,2)) max(xyz(:,2))])
end

% interior nodes
if ~isempty(xyz)
    fprintf(fid,'%d\r\n', nu);
    for in = 1:nu % no terminating NaNs in xyz
        fprintf(fid,'%d %d %d\r\n',xyz(in,:));
    end
end

fclose(fid);


% header
% fprintf(fid,'#NODE\r\n %f %f %f %f %f\r\n',x0,y0,xscale,yscale,itype); 
        % new format



function write_rect(fid,x,y)
% Write one section (boundary/island/interior)

% add margins
mx = diff(x)*0.01;
my = diff(y)*0.01;

fprintf(fid,'%d\r\n', 4);
fprintf(fid,'%12.7f %12.7f %9.2f\r\n',x(1)-mx,y(1)-my,0); % set all z to 0
fprintf(fid,'%12.7f %12.7f %9.2f\r\n',x(2)+mx,y(1)-my,0);
fprintf(fid,'%12.7f %12.7f %9.2f\r\n',x(2)+mx,y(2)+my,0);
fprintf(fid,'%12.7f %12.7f %9.2f\r\n',x(1)-mx,y(2)+my,0);