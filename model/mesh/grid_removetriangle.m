function [p,tri,z,code,err] = grid_removetriangle(noden,p,tri,z,code)
% Remove triangles with ALL nodes in the noden list.

% code notation
cint = 0;
cms = 1; % solid outer boundary
cisl = 2; % island
cml = 5; % liquid outer boundary
cme = 6; % edge of the solid outer boundary

err = false;

po = p; % reserve a copy of the original set of nodes
bno = grid_boundarynodes(tri); % boundary nodes for the original grid

% remove requested triangles AND
% avoid creating branching boundary, i.e. do not delete triangles with
% exactly one boundary node
ldel = sum(ismember(tri,noden),2)==3; % & sum(ismember(tri,bno),2)~=1; - NO; works only if one triangle is deleted

if all(ldel) % entire grid is deleted
    p = []; tri = []; z = []; code = [];
    return
end

% delete triangles
tri(ldel,:) = [];
[p,tri,z,code] = grid_fix(p,tri,z,code); % removes unreferenced nodes

% adjust codes and z for the new boundary nodes
be = grid_edge(tri); % boundary nodes for changed grid

% branching boundary test: any node that occurs in boundary edges more than twice
bnc = unique(be); % unique boundary nodes
n = histc(be(:),bnc);

% % return error if boundary became branching
% if any(n>2)
%     err = bnc(n>2);
%     p = []; tri = []; z = []; code = [];
%     return
% end

% allow branching boundary
%%%% may screw up re-coding below
if any(n>2)
    err = bnc(n>2);
end

% now we have to adjust codes for the new boundary nodes
% based on the two non-interior neighbours for each node:
%   - if all nodes in the loop are interior-coded, we have formed a new island
%   - if they have the same code and this code is 1, 2, or 5 - assign this code;
%   - if one is Code 1 and the other is Code 5 or 6 -> the new node next to
%   Code 5 receives Code 6, others receive Code 1
%   - if one is an outer boundary (Code 1, 5, or 6) and the other is an
%   island, an island became connected to the main boundary; assign the
%   outer boundary code to the new one and all island nodes in this boundary loop;


[bnew,bnew] = setdiff(p(bnc,:),po(bno,:),'rows'); %#ok<ASGLU> % new boundary nodes
bnew = bnc(bnew); % new boundary nodes (indices into the new p array)
while ~isempty(bnew) % initially empty only when a "tip" triangle deleted
    bloop = edge_extract_loop(be,bnew(1)); % extract a boundary loop containing 1st node
    bloop(end) = []; % we do not need the duplicate of the first node
    lnew = ismember(bloop,bnew);
    bloopcode = code(bloop); % codes for this loop
    uloopcode = unique(bloopcode(~lnew)); % codes for old nodes, sorted
%     uloopcode(uloopcode==cint) = []; % unique non-interior codes
    la = ismember(bnew,bloop); % new boundary nodes belonging to this loop
    if isempty(uloopcode) % a new island
        code(bloop) = cisl;
%         z(bloop) = ; %%%%%%%%%%%%
    elseif length(uloopcode)==1 % all uniformly coded
        if uloopcode~=cme
            code(bnew(la)) = uloopcode;
            % z%%%%%%%%%%%%%
        else % only cme (Code 6) nodes are left in the outer boundary
            code(bloop) = cml; % make the entire loop a liquid boundary
        end
    else % various boundary codes are in this loop
        if any(uloopcode==cisl) % island became a part of the outer boundary
            % make former islands coded as the outer boundary
            bloopcode(bloopcode==cisl) = cms;
            uloopcode = unique(bloopcode(~lnew)); % recalculate
        end
        
        if length(uloopcode)==1 % all island or uniform outer boundary
            bloopcode = uloopcode;
            % z%%%%%%%%%%%%%
        else % still various codes remain in the loop
            % adjust each node individually based on the nearest
            % non-interior coded neighbours
            iref = find(~lnew); % only old nodes
            for iadj = find(lnew) % do not adjust old boundary nodes
                iiprev = find(iref<iadj,1,'last');
                if isempty(iiprev)
                    iiprev = length(iref);
                end
                iinext = find(iref>iadj,1,'first');
                if isempty(iinext)
                    iinext = 1;
                end
                codeprev = bloopcode(iref(iiprev));
                codenext = bloopcode(iref(iinext));
                codenei = [codeprev codenext];
                if codeprev==codenext % new boundary node is in between two same-code nodes
                    bloopcode(iadj) = codeprev;
                    % z%%%%%%%%%%%%%
                elseif all(ismember(codenei,[cms cme]))
                    % is in between solid boundary and solid edge
                    bloopcode(iadj) = cms;
                elseif all(ismember(codenei,[cml cme]))
                    % is in between liquid boundary and solid edge
                    bloopcode(iadj) = cml;
                elseif all(ismember(codenei,[cms cml]))
                    % is in between liquid boundary and solid boundary
                    if codeprev==cml
                        iml = iref(iiprev);
                    else
                        iml = iref(iinext);
                    end
                    if abs(iadj-iml)==1 % next to the liquid boundary
                        bloopcode(iadj) = cme;
                    else
                        bloopcode(iadj) = cms;
                    end
                    % z%%%%%%%%%%%%%
                end
            end
        end
        code(bloop) = bloopcode; % substitute adjusted codes for this loop
        % end of the case: various boundary codes are in this loop
    end
    bnew(la) = []; % remove adjusted nodes
end % while