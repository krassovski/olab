function [on,nn] = bathy_dominate(objr, obj, rd)
% Determine object and node numbers for the nodes in obj within radius rd
% of any point in objr.
% Examples:
%   [on,nn] = bathy_dominate(objr, obj, rd); % can be used in VisualGrid

if isempty(which('pdist2'))
    olab_addpath('toolboxfix/stat')
end

hwait = waitbar(0,{'Removing competing bathymetry nodes.','0% done. ETA: '},'CreateCancelBtn','delete(gcbf)');

% nodes of the dominating object
xd = objr.x;
yd = objr.y;

% nodes of the suppressed objects
[m,n] = size(obj(1).x);
if m==1 % && n~=1 % no n~=1: important for one-node objects
    x = [obj.x];
    y = [obj.y];
else
    try
    x = cat(1,obj.x)';
    y = cat(1,obj.y)';
    catch
        ''
    end
end

if ~isfield(obj,'objn')
    for io = 1:length(obj)
        obj(io).objn = ones(1,length(obj(io).x))*io;
    end
end
if ~isfield(obj,'noden')
    for io = 1:length(obj)
        obj(io).noden = 1:length(obj(io).x);
    end
end
on = [obj.objn];
nn = [obj.noden];

% process only potentially conflicting nodes (within the bounding box of each other)
try
in = find(x>min(xd)-rd & x<max(xd)+rd & y>min(yd)-rd & y<max(yd)+rd);
catch
    ''
end
xin = x(in);
yin = y(in);

ind = find(xd>min(x)-rd & xd<max(x)+rd & yd>min(y)-rd & yd<max(y)+rd);
xdin = xd(ind);
ydin = yd(ind);

% bin datasets into rd-wide square cells
xall = [xin(:); xdin(:)];
xbin = min(xall):rd*10:max(xall);
% xbin = min(xall): (max(xall)-min(xall))/100 :max(xall); %%% this deletes nodes which should not be deleted
xbin(1) = -Inf;
xbin(end+1) = Inf;
yall = [yin(:); ydin(:)];
ybin = min(yall):rd*10:max(yall);
% ybin = min(yall): (max(yall)-min(yall))/100 :max(yall); %%% this deletes nodes which should not be deleted
ybin(1) = -Inf;
ybin(end+1) = Inf;

[~,cxin] = histc(xin,xbin);
[~,cyin] = histc(yin,ybin);
[~,cxdin] = histc(xdin,xbin);
[~,cydin] = histc(ydin,ybin);

%%% check if there is enough memory to process the most populated cell
unique([cxin(:) cyin(:)],'rows');

    
% process each square cell
nx = length(xbin)-1; % last bin is always empty (no coordinate matches Inf)
ny = length(ybin)-1;
ir = false(size(xin)); % to hold conflicting nodes
tst = now;
nc = nx*ny; % number of cells
dchk = nc*0.001; % waitbar step in num of cells
chkdone = dchk; % next checkpoint for waitbar
ps = [xin(:) yin(:)];
pd = [xdin(:) ydin(:)];
% ndmax = 0;
for ix = 1:nx
    cxs = cxin==ix | cxin==ix-1 | cxin==ix+1; % take neighbouring cells as well
    cxd = cxdin==ix;
    for iy = 1:ny
        cs = cxs & (cyin==iy | cyin==iy-1 | cyin==iy+1); % take neighbouring cells as well
        cd = cxd & (cydin==iy);
        
%         ndmax = max(ndmax,sum(cs)*sum(cd));
        
        if any(cd)
            d = pdist2(pd(cd,:),ps(cs,:)); % distance matrix
%             ir(cs) = ir(cs) | any(d<rd,1)';
            ir(cs) = ir(cs) | any(d<rd,1);
            %%%IMPORTANT: specify 1st dimension in ANY explicitly in case d becomes a vector
        end
        
        if ~ishandle(hwait) % waitbar Cancel pressed
            delete(hwait)
            on = NaN;
            nn = NaN;
            return
        end

        if ix*iy > chkdone
            pdone = chkdone/nc;
            eta = tst + (now-tst)/pdone;
            ustr = {'Removing competing bathymetry nodes.',...
                [num2str(pdone*100) '% done. ETA: ' datestr(eta)]};
            waitbar(pdone,hwait,ustr)
            chkdone = chkdone+dchk;
        end
    end
end
% disp(ndmax)


    
% ir = false(size(xin)); % to hold conflicting nodes
% tst = now;
% cancel = false;
% if length(in)>length(ind) % choose the shortest set to cycle through
%     % cycle throught the reference dataset
%     n = length(xdin);
%     dchk = n*0.01; % waitbar step in num of values
%     chkdone = dchk; % next checkpoint for waitbar
%     for j = 1:n
% %         d = ((xin-xd(j)).^2 + (yin-yd(j)).^2).^0.5; % dist from jth point in xd,yd to all in xin,yin
% %         ir = ir | (d<rd); % record nodes within the range
% 
%         % reduce array for speed
%         xdinj = xdin(j);
%         xdinjm = xdinj-rd;
%         xdinjp = xdinj+rd;
%         ydinj = ydin(j);
%         ydinjm = ydinj-rd;
%         ydinjp = ydinj+rd;
%         inin = xin>xdinjm & xin<xdinjp & yin>ydinjm & yin<ydinjp;
%         
%         % dist from jth point in xdin,ydin to all in xin,yin
%         d = ((xin(inin)-xdinj).^2 + (yin(inin)-ydinj).^2).^0.5;
%         ir(inin) = ir(inin) | (d<rd); % record nodes within the range
%         
%         if ~ishandle(hwait) % waitbar Cancel pressed
%             cancel = true;
%             break
%         end
% 
%         if j>chkdone
%             pdone = chkdone/n;
%             eta = tst + (now-tst)/pdone;
%             ustr = {'Removing competing bathymetry nodes.',...
%                 [num2str(pdone*100) '% done. ETA: ' datestr(eta)]};
%             waitbar(pdone,hwait,ustr)
%             chkdone = chkdone+dchk;
%         end
%     end
% else % cycle throught the test dataset
%     n = length(xin);
%     dchk = n*0.01; % waitbar step in num of values
%     chkdone = dchk; % next checkpoint for waitbar
%     for j = 1:length(xin)
%         % reduce array for speed
%         inin = xdin>xin(j)-rd & xdin<xin(j)+rd & ydin>yin(j)-rd & ydin<yin(j)+rd;
%         
%         % dist from jth point in xin,yin to all in xdin,ydin
%         d = ((xdin(inin)-xin(j)).^2 + (ydin(inin)-yin(j)).^2).^0.5;
%         
%         ir(j) = any(d<rd); % record this node if it is within the range
%         
%         if ~ishandle(hwait) % waitbar Cancel pressed
%             cancel = true;
%             break
%         end
% 
%         if j>chkdone
%             pdone = chkdone/n;
%             eta = tst + (now-tst)/pdone;
%             ustr = {'Removing competing bathymetry nodes.',...
%                 [num2str(pdone*100) '% done. ETA: ' datestr(eta)]};
%             waitbar(pdone,hwait,ustr)
%             chkdone = chkdone+dchk;
%         end
%     end
% end
% 
% if cancel % if waitbar Cancel wasn't pressed
%     delete(hwait)
%     on = NaN;
%     nn = NaN;
% else
    close(hwait)
    on = on(in(ir));
    nn = nn(in(ir));
% end