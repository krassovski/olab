function [x,y,zf,ir] = bathy_decimate_median(x,y,z,rd,usemedian)
% Decimate samplings z defined on locations x,y.
% Input:
% rd    radius of decimation
%       default (0, NaN, or empty) is no decimation
% usemedian     replaces each value with the median 
% 
% Output:
% x,y,z     decimated dataset
% ir        logical indices into the original dataset (x,y,z) of points that have been removed

if ~exist('usemedian','var') || isempty(usemedian) || isnan(usemedian)
    usemedian = false;
end

if isempty(rd) || isnan(rd)
    rd = false;
end

zf = z; % array holding filtered dataset

if rd==0 % nothing to do
    ir = [];
    return
end

% create a waitbar
hwait = waitbar(0,'Decimating bathymetry. 0% done. ETA: ','CreateCancelBtn','delete(gcbf)');
tst = now;

zv = true(size(z)); % this array keeps track of points tht have not been decimated

n = length(x);
k = 1; % current point to process
dchk = n*0.001; % waitbar step in num of values
chkdone = dchk; % next checkpoint for waitbar
while ~isempty(k) && k<=n
    xk = x(k);
    yk = y(k);
    
    % work only in the vicinity
    in = x>xk-rd & x<xk+rd & y>yk-rd & y<yk+rd; % points in the vicinity
    xin = x(in);
    yin = y(in);
    d = ((xin-xk).^2 + (yin-yk).^2).^0.5; % dist from k-th point to all the rest in the vicinity
    
    in = find(in);

    id = d<rd;
    
    % filter if needed
    if usemedian
        zf(k) = median(z(in(id)));  % within the radius
    else
        zf(k) = z(k);
    end
    
    % decimate
    idec = in(id); % within the radius
    zv(idec) = false;
    idec(idec==k) = [];
    zf(idec) = NaN;
    
    k = find(zv,1,'first'); % empty if no more valid nodes after previous k
    
    if ~ishandle(hwait) % waitbar Cancel pressed
        ir = NaN;
        return
    end
    
    if k>chkdone
        pdone = chkdone/n;
        eta = tst + (now-tst)/pdone;
        waitbar(pdone,hwait,['Decimating... ' num2str(pdone*100,'%2.2f') '% done. ETA: ' datestr(eta)])
        chkdone = chkdone+dchk;
    end
end

delete(hwait);

% form output
ir = isnan(zf);
x(ir) = [];
y(ir) = [];
zf(ir) = [];