function r = poly2coast(poly,r)
% Coastline struct array from nan-separated polygon poly.
% r     target coastline structure (to preserve the set of fields in the output)

% ensure poly has it 3 columns; append a column with zeros if necessary
[np,nc] = size(poly);
if nc == 2
    zcol = zeros(np,1);
    zcol(isnan(poly(:,1))) = NaN; % preserve terminating NaNs
    poly = [poly zcol];
end

% terminate with NaNs if poly is not terminated
if ~isnan(poly(end,1))
    poly(end+1,:) = [NaN NaN NaN];
end

br = [0; find(isnan(poly(:,1)))]; % NaNs in the 1st column

nr = length(br)-1; % number of segments
if ~exist('r','var') || isempty(r)
    r = struct('x',[],'y',[],'z',[]);
end
r = repmat(r(1),1,nr);
for ix = 1:nr % for each continous segment
    in = br(ix)+1:br(ix+1); % indices for this segment, including the terminating NaN
    r(ix).x = poly(in,1)'; 
    r(ix).y = poly(in,2)';
    if isfield(r,'z')
        r(ix).z = poly(in,3);
    end
end