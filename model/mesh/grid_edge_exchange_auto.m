function [t,itex,torig] = grid_edge_exchange_auto(t,x,y,eqlthres)
% Exchange edge for non-EQL triangles according to the EQL threshold.
% Only triangles containing boundary node(s) are considered.
% t     [Nx3] triangles array
% x,y   columns of node coordinates
% eqlthres  EQL threshold
% Output:
% t     [Nx3] modified triangles array
% itex  [Mx1] num indices of modified triangles (row index in t)
% torig [Mx3] original triangles at itex (for undo)

olab_addpath('model/mesh/Mesh2d v24') % quality

x = x(:);
y = y(:);

eql = 1./quality([x y], t); % "equilateralness"
lte = eql>eqlthres; % logical indices of non-eql triangles

[eb,~,tib] = grid_edge(t);


% for triangles containing one boundary edge: exchange the longer of the
% two adjacent edges

ltbe = false(size(t,1),1); % initialize
ltbe(tib) = true; % consider only with boundary edges to exclude possible bridges

bn = unique(eb(:)); % boundary nodes
lbn = ismember(t,bn); % boundary nodes in tri array

ltbe = ltbe & sum(lbn,2)==2 & lte; % non-eql triangles containing one boundary edge

% % exclude possible bridges from here
% t1 = t(ltbe,:);
% lbne = ismember(t1,bn); % boundary nodes in non-eql triangles
% [row,col] = find(lbne);
% [row,isort] = sort(row);
% col = col(isort);
% be1 = t1(sub2ind(size(t1),row,col)); % boundary edges in ltbe triangles
% inv = ~ismember(sort(be1,2),eb,'rows');
%%%

e = t(ltbe,[2 3 1 2]); % edge bounding nodes for each ltbe triangle

% %%% exclude those for which the opposing node has a NON-ADJACENT(!) boundary node among its'
% %%% neighbours (besides the initial two): these are in narrow channels;
% con = tri2con(t,x,y); % neighbours for each node
% % nodes with neighbouring boundary nodes except the initial two
% inv = find(  any(  ismember(con,bn) & ~ismember(con,e(:)),  2)  ); %%% should be also NON-ADJACENT(!) boundary edges
% inv = setdiff(inv,bn); % test only internal nodes
% ltbe(any(ismember(t,inv),2)) = false;
% e = t(ltbe,[2 3 1 2]); % update edge bounding nodes for each triangle

elen = (diff(x(e),1,2).^2 + diff(y(e),1,2).^2).^0.5; % edge lengths

% lbne = ismember(t(ltbe,:),bn); % boundary nodes in non-eql triangles
lbne = lbn(ltbe,:); % boundary nodes in non-eql triangles

% record base edge lengths
[row,col] = find(~lbne);
[row,isort] = sort(row);
col = col(isort);
elsz = size(elen);
elenbase = elen(sub2ind(elsz,row,col));

elen(~lbne) = 0; % keep only internal edge lengths
[~,ilong] = max(elen,[],2); % location of the maximum element in each row
% (longest internal edge in each triangle)
esz = size(e);
row = (1:esz(1))';
ex = [e(sub2ind(esz,row,ilong)) e(sub2ind(esz,row,ilong+1))]; % edges to exchange

% do not exchange if the base (boundary) edge is longer than internal
% edges: this usually happens in the narrows
% esz = size(e);
elenlong = elen(sub2ind(elsz,(1:elsz(1))',ilong));
ival = elenlong>elenbase;
ex = ex(ival,:); % edges to exchange
% lbne = lbne(ival,:); % keep track of boundary nodes
ilong = ilong(ival); % keep track of edges to exchange
itbe = find(ltbe);
itbe = itbe(ival); % keep track of non-eql triangles to work on


% for triangles containing one boundary node: exchange the edge opposing
% the boundary node
ltbn = sum(lbn,2)==1 & lte; % non-eql triangles containing one boundary node
% en = t(ltbn,[2 3 1 2]); % edge bounding nodes for each ltbn triangle
lbnn = lbn(ltbn,:); % boundary nodes in non-eql triangles with one boundary node
[ieex,~] = find(lbnn'); % keep track of triangles and edges to exchange
exn = t(ltbn,:)'; % transpose for correct squeezing out of the boundary nodes
exn(lbnn') = []; % remove boundary nodes; 
% remains exactly 2 nodes per triangle, which is the edge to exchange:
exn = reshape(exn,2,[])'; % reshape
itbn = find(ltbn);


ex = [ex; exn]; % all edges to exchange
% edges in ex are not unique for the pairs of non-eql triangles sharing same edge
it = [itbe; itbn]; % keep track of non-eql triangles to work on
ieex = [ilong; ieex]; % keep track of edges to exchange


% do not duplicate pairs of non-eql triangles sharing same edge: 
% remove duplicates
ex = sort(ex,2); % sort each row
[~,ia,ic] = unique(ex,'rows');
idup = setdiff(ic,ia); % for non-unique edges in ex
ex(idup,:) = [];
it(idup) = [];
ieex(idup) = [];


% [ltn,locex] = ismember(t,ex);
% [iex,col] = ind2sub(size(ex),locex); % indices of edges to exchange
% lt = sum(ltn,2)>=2; % triangles containing edges to exchange %%%% NO!!! Must belong to the same ex row!!!

% opposing triangles
e = t(:,[2 3  3 1  1 2]); % edges for each triangle
e = sort(reshape(e',2,[]))'; % all triangle edges, one edge per row; nt*3 rows
[lt,locex] = ismember(e,ex,'rows');
% findlt = find(lt);
% it2 = ceil(findlt/3); % indices of triangles containing edges to exchange
% ieex2 = findlt - (it2-1)*3; % keep track of edges to exchange
[ieex2,it2] = ind2sub([3 sum(lt)],find(lt)); % edges and triangles to exchange
locex = locex(lt); % edge number for each triangle

% t(te(locex==1),:); %%% a pair of triangles for edge exchange (Edge 1)
% [~,itf] = unique(locex); % 1st triangle in each pair
% [~,itl] = unique(locex,'last'); % 2nd triangle in each pair
ltf = ismember(it2,it); % non-eql triangle in each pair
% en = locex(ltf); % edge number in ex list for each non-eql triangle
locexm = locex(~ltf); % edge number in ex list for each matching triangle
% [~,isort] = sort(en2); % arrange to match the list of edges in ex
itm = it2(~ltf);
ieexm = ieex2(~ltf);
% arrange in the same order as ex
[~,isort] = sort(locexm);
itm = itm(isort);
ieexm = ieexm(isort);

% % find 4 unique nodes in each triangle pair
% tt = [t(it2(itf),:) t(it2(itl),:)]; % each row contains a triangle pair
% [tts,itts] = sort(tt,2);
% dtts = diff(tts,1,2);
% % remove elements in tts corresponding to zeros in dtts
% ival = [dtts ones(size(dtts,1),1)]'~=0; % expand by one column of non-zero elements
% tts = tts';
% tts4 = tts(ival); % output in 1-column
% tts4 = reshape(tts4,4,[])'; % 4 nodes in two opposing triangles
% itts = itts';
% itts4 = itts(ival); % output in 1-column
% itts4 = reshape(itts4,4,[])';

% of the 4 nodes in two opposing triangles:
% lcon = ismember(tts4,ex); % 2 connected nodes %%% not necesserily two!!!
% 2 free nodes
tt = t'; % transpose for correct squeezing out of the edge nodes
freen = tt(sub2ind(size(tt),ieex,it)); % keep free nodes
freenm = tt(sub2ind(size(tt),ieexm,itm)); % keep free nodes for matching triangles

% First, exclude those bounding another triangle with opposing boundary node:
% these are in narrow channels; exchanging such edge would create a "bridge".
inv = ismember(freen,bn) & ismember(freenm,bn);
ex(inv,:) = [];
it(inv) = [];
% ieex(inv) = [];
freen(inv) = [];
itm(inv) = [];
% ieexm(inv) = [];
freenm(inv) = [];

%%%%%%%%%%%%%%%%%%%% check this: some freenm boundary nodes remain


% "cat ears" -- same matching triangle for two non-eql triangles
%%% Next, exclude one of the "cat ears": the one, for which the opposing
%%% node has more neighbours, because this will not make boundary nodes with
%%% too many neighbours.
% % tts4t = tts4'; 
% % inv2 = ismember(tts4t(~(lbn4 | lcon)),ex); % free non-boundary is part of other triangle
% [ltdup,locdup] = ismember(it2(itf),it2(itl));
% loccat = find(ltdup);
% locdup = locdup(ltdup);
% % 3 cat-ear triangles:
% % te(itf(loccat)) % common pairing triangle
% % te(itl(loccat)) it2(itf(locdup)) % two non-eql triangles

[~,ia,ic] = unique(itm);
idup = setdiff(ic,ia); % for non-unique matching triangles
[lcat,loccat] = ismember(itm,itm(idup));
icat = find(lcat);
loccat = loccat(lcat);
% arrange duplicates one pair per column
ndup = length(idup);
ind = sub2ind([2 ndup],ismember(icat,idup)+1,loccat);
icat2d = zeros(2,ndup); % allocate
icat2d(ind) = icat; % populate

con = tri2con(t,x,y); % connectivity
nn = sum(con~=0, 2); % number of neighbours for each node
nn = nn(freen(icat2d));

[~,inv] = max(nn); % row index of the most connected node in each column
inv = icat2d(sub2ind([2 ndup],inv,1:ndup));
ex(inv,:) = [];
it(inv) = [];
% ieex(inv) = [];
freen(inv) = [];
itm(inv) = [];
% ieexm(inv) = [];
freenm(inv) = [];


% do the exchange
itex = [it; itm]; % modifying these triangles
torig = t(itex,:); % save original triangles for undo
% triangles in it keep 1st node in each pair in ex; 2nd node goes to itm-triangles
% both free nodes go to both triangles (they are becoming connected nodes)
t1 = [freen freenm ex(:,1)];
t2 = [freen freenm ex(:,2)];
tnew = [t1; t2]; % all new triangles
t(itex,:) = tnew; % substitute new triangles