function [p,t,f,code] = grid_edge_split(p,t,noden,f,code)
% Split an edge defined by a pair of nodes.
%
% Each triangle containing the edge is split into two sub-triangles. 
% The new triangles are created by joining nodes introduced at the edge midpoints. 
%  p : Nx2 array of nodal XY coordinates, [x1,y1; x2,y2; etc]
%  t : Mx3 array of triangles as indices, [n11,n12,n13; n21,n22,n23; etc]
%  noden : 1x2 pair of node indices identifying the edge to split, [n1 n2]
%
% Functions defined on the nodes in P can also be refined using linear
% interpolation through an extra input:
%  f : NxK array of nodal function values. Each column in F corresponds to
%      a dependent function, F(:,1) = F1(P), F(:,2) = F2(P), etc.
%  code : Nx1 array of node codes as per Trigrid notation.
%
% It is often useful to smooth the modified mesh using SMOOTHMESH. Generally
% this will improve element quality.
%
% Usage:
%   [p,t] = grid_edge_split(p,t,noden);
%   [p,t,z] = grid_edge_split(p,t,noden,z);
%   [p,t,z,code] = grid_edge_split(p,t,noden,z,code);
%
%
% Bsed on grid_refine.m which is based on refine.m by Darren Engwirda.

switch nargin
    case 5
        gotCode = true;
        gotF = true;
    case 4
        gotCode = false;
        gotF = true;
    case 3
        gotCode = false;
        gotF = false;
    otherwise
        error('Wrong number of inputs');
end

if (gotCode&&(nargout>4)) || (~gotCode&&(nargout>3))
   error('Wrong number of outputs');
end
if (gotF && ~gotCode && (nargout>3)) || (~gotF && (nargout>2))
   error('Wrong number of outputs');
end
if numel(noden)~=2
   error('NODEN must be a 1x2 array of logical indices');
end
if gotF && ((size(f,1)~=size(p,1)) || (ndims(f)>2))
   error('F must be an NxK array');
end

% Trigrid code notation
cint = 0; % interior
csbe = 6; % solid boundary edge node
          % MUST BE DIFFERENT FROM THE SOLID BOUNDARY CODE!!!
          % set to either solid or liquid boundary code if there is no
          % distinct solid boundary edge code

% Ensure we start with a consistent mesh
pnoden = p(noden,:);
if gotCode
    pfun = [f code];
    [p,t,pfun] = fixmesh(p,t,pfun);
    f = pfun(:,1);
    code = pfun(:,2);
elseif gotF
    [p,t,f] = fixmesh(p,t,f); %mk [p,t,junk,f] = fixmesh(p,t,[],f);
else
    [p,t] = fixmesh(p,t);
end
noden = find(ismember(p,pnoden,'rows')); % adjust target indices after fixmesh

% Edge connectivity
numt = size(t,1);
vect = 1:numt;
e = [t(:,[1,2]); t(:,[2,3]); t(:,[3,1])];                                  % Edges - not unique
[e,j,j] = unique(sort(e,2),'rows');                                        % Unique edges
te = [j(vect), j(vect+numt), j(vect+2*numt)];                              % Unique edges in each triangle

if any(sum(ismember(t,noden),2)==3)
    error('Cannot split more than one edge per triangle.')
end

split1 = sum(ismember(t,noden),2)==2; % triangles to split
split = sum(ismember(e,noden),2)==2; % edges to split

% New nodes
np = size(p,1);
pm = 0.5*(p(e(split,1),:)+p(e(split,2),:)); % Split edge midpoints
p = [p; pm];

if gotCode
    code1 = code(e(split,1),:);
    code2 = code(e(split,2),:);
    codenew = cint*ones(size(code1)); % initialize new codes as interior
    
    lsame = code1==code2;
    
    % new nodes on same-coded edges receive this code
    codenew(lsame) = code1(lsame);
    
    % new nodes on differently-coded edges remain interior
%     codenew(~lsame) = cint;
    
    % exception is boundary edges containing solid boundary edge
%     if ~isnan(csbe)
        code1sbe = code1==csbe & code2~=cint;
        code2sbe = code2==csbe & code1~=cint;
        codenew(code1sbe) = code2(code1sbe);
        codenew(code2sbe) = code1(code2sbe);
%     end
    
    code = [code; codenew]; % append new codes
end

% Map E(SPLIT) to index PM
i = zeros(size(e,1),1);
i(split) = (1:length(find(split)))'+np;

tnew = t(~split1,:); % keep the triangles without changes

% New triangles
[row,col] = find(split(te(split1,:))); % Find split edges in tri's
if length(row)==1 % special case (when splitting a boundary edge)
    col = row;
    row = 1;
end

N1 = col;  % Transform so that the split is always between n1 & n2
N2 = col+1;
N3 = col+2;
N2(N2>3) = N2(N2>3)-3;
N3(N3>3) = N3(N3>3)-3;

n1 = 0*N1;
n2 = n1;
n3 = n1;
n4 = n1;

split1 = find(split1);
split1 = split1(row);
for k = 1:length(col)
  n1(k) = t(split1(k),N1(k));
  n2(k) = t(split1(k),N2(k));
  n3(k) = t(split1(k),N3(k));
  n4(k) = i(te(split1(k),col(k)));
end
tnew = [tnew; n1,n4,n3; n4,n2,n3]; % append new triangles

t = tnew;

% Linear interpolation to new nodes
if gotF
   f = [f; 0.5*(f(e(split,1),:)+f(e(split,2),:))];
end

% Ensure the consistent mesh
if gotCode
    pfun = [f code];
    [p,t,pfun] = fixmesh(p,t,pfun);
    f = pfun(:,1);
    code = pfun(:,2);
elseif gotF
    [p,t,f] = fixmesh(p,t,f); %mk [p,t,junk,f] = fixmesh(p,t,[],f);
else
    [p,t] = fixmesh(p,t);
end