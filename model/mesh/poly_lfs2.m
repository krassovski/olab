function [lfs,len,wm] = poly_lfs2(poly,range)
% poly_lfs Version 2.
% Estimate Local Feature Size (LFS) as the distance between the midpoint of each
% polyline edge and non-adjacent edges of the polyline.
% 
% Works for single-face multiply-connected (islands permitted) polygons.

% If the polyline is
% closed, only distances measured through the interior of the polygon are taken
% into account. The polygon may contain holes (islands).
% poly      polyline in the form of 2D array [x y] with NaN-separated segments
% range     range (relative to edge length) to look within (optional)

if ~exist('range','var') || isempty(range)
    range = Inf;
end

% make orientation of the polygon positive
% poly = poly_orient(poly); % node order may change!
o = poly_orient(poly,'report'); % orientation flags; no change in node order

len = sum(diff(poly).^2, 2).^.5; % edge lengths

rangel = range*len; % absolute range for each edge

wm = 0.5*(poly(1:end-1,:) + poly(2:end,:)); % edge midpoints
einv = isnan(wm(:,1)); % invalid edges (containing NaNs)

np = size(poly,1);
ne = np-1; % number of edges

lfs = zeros(ne,1);
for ie = 1:ne % loop over the edges
    if einv(ie)
        continue % process only valid (non-NaN) edges
    end
    disp(['poly_lfs:  ' num2str(ie) ' of ' num2str(ne)])
    
    % Replace current edge with NaNs, as well as copies of the nodes in the
    % current edge (necessary for the 1st and last edges of closed segments);
    % this removes the current and adjacent edges from the poly.
    
    % By ignoring the ajacent edges we avoid discretizing the corners but
    % discretize the channels.
    
    inv = ismember(poly,poly(ie:ie+1,:),'rows'); % nodes to invalidate
    polyn = poly; % copy of the original polygon
    polyn(inv,:) = NaN;
    
    % distance to every edge in polyn
    [distance, ~, projp] = point_seg_dist(polyn(1:end-1,:), polyn(2:end,:), wm(ie,:));
%     if nanmin(distance) < rangel(ie)
    if min(distance,[],'omitnan') < rangel(ie)
        wmr = repmat(wm(ie,:),np-1,1);
        theta = line2line(repmat(poly(ie,:),np-1,1),wmr,wmr,projp);
        if o(ie)>0 % the edge is positively oriented, "water" on the left
            iwater = theta>=0; % "on the edge" is good for both cases
        else
            iwater = theta<=0; % "on the edge" is good for both cases
        end
%         lfsc = nanmin(distance(iwater)); % LFS candidate
        lfsc = min(distance(iwater),[],'omitnan'); % LFS candidate
        if lfsc < rangel(ie)
            lfs(ie) = lfsc;
        end
    end
end
lfs(einv) = [];
len(einv) = [];
wm(einv,:) = [];
lfs(lfs==0) = len(lfs==0); % to avoid inf r in quadtree_mk