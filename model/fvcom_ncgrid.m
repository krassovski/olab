function [x,y,z,tri,ns,siglay,xc,yc,nbe] = fvcom_ncgrid(outfile)
%FVCOM_NCGRID -- Model grid from an output FVCOM nc file.
% 
% INPUTS:
%   outfile     name of an fvcom output file
% 
% OUTPUTS:
%   x,y     [n,1] model grid nodal coordinates
%   tri     [k,3] triangulation array
%   h       [n,1] model depths
%   ns      number of sigma-layers
%   siglay  [n,ns] sigma-layers
%   xc,yc   [k,1] grid element centroids
%   nbe     [k,3] elements surrounding each element (indices into rows of tri); 
%           obtained from the fvcom output variable 'nbe';
%           contains 1 zero value for each triangle with a boundary edge

finfo = ncinfo(outfile);
vn = {finfo.Variables.Name};

% FVCOM-specific netCDF file structure
% ncdisp(outfile) % to see the contents
ncid = netcdf.open(outfile,'NC_NOWRITE');

% model grid
x = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'x'),'double');
y = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'y'),'double');
tri = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'nv'),'double');
if all(ismember({'h','siglay'},vn))
    z = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'h'),'double');
    siglay = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'siglay'),'double');
    % FVCOM-specific output format (dimensions)
    [~,ns] = netcdf.inqDim(ncid,netcdf.inqDimID(ncid,'siglay')); % number of sigma-layers
else
    z = [];
    siglay = [];
    ns = 0;
end
if all(ismember({'xc','yc'},vn)) % FVCOM3
    xc = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'xc'),'double');
    yc = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'yc'),'double');
else
    xc = mean(x(tri),2);
    yc = mean(y(tri),2);
end
if ismember('nbe',vn)
    nbe = netcdf.getVar(ncid,netcdf.inqVarID(ncid,'nbe'),'double');
else
    olab_addpath('geometry/triangulation')
    nbe = tri_nbe(tri); % get bounding elements for each element in k
end

netcdf.close(ncid)