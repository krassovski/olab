function v = tri_smoothfield_lap(t,v,maxit,ifix)
%TRI_SMOOTHFIELD_LAP Smooth a node-based quantity using Laplacian smoothing 
% (average of neighbours).
%
%  t     : ntx3 array of triangles as indices, [n11,n12,n13; 
%                                              n21,n22,n23; etc].
%  v     : values at nodes
%  maxit : Maximum allowable iterations.
% ifix   : do not change values at these nodes (logical, size(v)), optional
%
% See also TRI_FILL_LAP

% Based on 
% Mesh2d v24/smoothmesh.m by Darren Engwirda - 2007

if ~exist('maxit','var') ||  isempty(maxit)
   maxit = 20;
end
if ~exist('ifix','var')
   ifix = [];
end

n = size(v,1);
S = sparse(t(:,[1,1,2,2,3,3]),t(:,[2,3,1,3,1,2]),1,n,n); % Sparse connectivity matrix

% S has 1's for boundary neighbours and 2's for internal neighbours
S(S==2) = 1; % replace 2's in S with 1's, so all neighbours have same weight

W = sum(S,2); % number of neighbours
if any(W==0)
   error('Invalid mesh. Hanging nodes found.');
end
W = W*ones(1,size(v,2));

vfix = v(ifix,:);

for iter = 1:maxit
    v = (S*v)./W;     % Laplacian smoothing; average of neighbour values
    v(ifix,:) = vfix; % Don't change fixed nodes
end