function [im2,cpr] = mk_imrotate(im1,thet,cp)
% Image rotation by nearest neigbour interpolation.
% cp    control points in the original image
% cpr   control points in the rotated image

[m,n,p] = size(im1);

R = [cos(thet) sin(thet); sin(-thet) cos(thet)]; % rotation matrix

%%% Code from rotate_image.m
% input and output size of matrices (output size is found by rotation of 4 corners)
in_mid_x        = (n-1) / 2;
in_mid_y        = (m-1) / 2;
in_corners_m    = [ [0,0,n-1,n-1] - in_mid_x;
                    [0,m-1,m-1,0] - in_mid_y ];
out_corners_m   = fix(R * in_corners_m);

% size of the output matrix
out_min_x = min(out_corners_m(1,:));
out_max_x = max(out_corners_m(1,:));
out_min_y = min(out_corners_m(2,:));
out_max_y = max(out_corners_m(2,:));
nn = out_max_x - out_min_x + 1;
mm = out_max_y - out_min_y + 1;

% translation of control points
if exist('cp','var') && ~isempty( cp )
    cpr = (R * [cp(1,:)-in_mid_x; cp(2,:)-in_mid_y]);
    cpr = [cpr(1,:)-out_min_x+1; cpr(2,:)-out_min_y+1];
else
    cpr = [];
end

t = repmat((1:mm)',1,nn);
s = repmat(1:nn,   mm,1);
io =  (t-mm/2)*cos(thet) + (s-nn/2)*sin(thet) + m/2;
jo = -(t-mm/2)*sin(thet) + (s-nn/2)*cos(thet) + n/2;
i = round(io); % nearest
j = round(jo);
iout = ~(i>0 & j>0 & i<=m & j<=n);
i(iout) = 1;
j(iout) = 1;
linearindex = sub2ind([m n], i, j);

% im2 = im1(linearindex);
% im2(iout) = 0;
im2 = cast(zeros(mm,nn,p), class(im1));
for k = 1:p
    im1k = im1(:,:,k);
    im2k = im1k(linearindex);
    im2k(iout) = 0;
    im2(:,:,k) = im2k;
end