function [xout,yout,cdataout] = imtransform(x,y,cdata,m)
% Transform image using transformation matrix m.

% % transformation matrix is obtained from two 3-point sets (polylines in VisualGrid)
% m = inv([r(1).x(1:end-1)' r(1).y(1:end-1)' ones(3,1)]) * [r(2).x(1:end-1)' r(2).y(1:end-1)' ones(3,1)];

% % coordinate transformation
% xy = inv(m)'*[x(:)'; y(:)'; ones(1,length(x))];
% 
% % inverse transformation
% xy = m'*[b.x; b.y; ones(1,length(b.x))];
% 
% ct.x = xy(1,:); ct.y = xy(2,:);

[ny,nx,nc] = size(cdata);

% % original resolution
% dx = (x(nx)-x(1))/(nx-1);
% dy = (y(ny)-y(1))/(ny-1);

out_corners = inv(m)'*[x([1 1 nx nx]); y([1 ny ny 1]); ones(1,4)];
dym = (out_corners(2,2)-out_corners(2,1))/(ny-1); % resolution along y of the transformed image
dxm = (out_corners(1,3)-out_corners(1,2))/(nx-1); % resolution along x of the transformed image
dm = min(dym,dxm); % unified resolution of the transformed image

% bounds of the output matrix
out_min_x = min(out_corners(1,:));
out_max_x = max(out_corners(1,:));
out_min_y = min(out_corners(2,:));
out_max_y = max(out_corners(2,:));

xout = out_min_x : dm : out_max_x;
yout = out_min_y : dm : out_max_y;

[xg,yg] = meshgrid(xout,yout);

xg = xg(:)';
yg = yg(:)';

% inverse transform of the output grid to the coord of the original image
xyg = m'*[xg; yg; ones(1,length(xg))];

nyout = length(yout);
nxout = length(xout);
cdataout = ones(nyout,nxout,nc);
cclass = class(cdata);
cfill = fill_color(cclass);
for k = 1:nc
    c = interp2(x,y,double(cdata(:,:,k)),xyg(1,:),xyg(2,:));
    c(isnan(c)) = cfill; % make expanded areas white
    cdataout(:,:,k) = reshape(c,[nyout nxout]);
end

cdataout = cast(cdataout, cclass);


function cfill = fill_color(cclass)
% Determine the color to use in blank areas of the transformed (rotated) image.

% See 'image' in help for image specification formats.
if strcmp(cclass,'double')
    % For true color (RGB) (m-by-n-by-3) array,
    % rgb=[1,1,1] is white; floating-point values in the range [0, 1].
    % For indexed (colormap) two-dimensional (m-by-n) array,
    % take 1st value in the colormap.
    cfill = 1; 
elseif strcmp(cclass,'uint8')
    cfill = 255; % array of integers in the range [0, 255] (uint8)
else % uint16
    cfill = 65535; % array of integers in the range [0, 65535] (uint16)
end