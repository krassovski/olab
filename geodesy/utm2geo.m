function  [Lat,Lon] = utm2geo(x,y,utmzone)
% Convert UTM coordinates to Lat-Lon (WGS84).
%
% Inputs:
%   x,y     arrays of UTM coordinates.
%   utmzone allowable formats:
%           (1) signed numeric value (negative for southern hemisphere).
%           (2) N�4 character array, first two characters being the zone
%               number followed by space and followed by zone letter (C to X);
%               in this case x,y should be vectors.
%           (3) cell array which can contain both numeric values as in (1)
%               and strings as in (2)
%           In case a single value is supplied for UTM zone, it is used to
%           convert all points in x,y.
%
% Outputs:
%   Lat: Latitude vector.   Degrees.  +ddd.ddddd  WGS84
%   Lon: Longitude vector.  Degrees.  +ddd.ddddd  WGS84
%
% Maxim Krassovski  
% 12-Sep-2011   vectorized the code and expanded acceptable formats for utmzone.
% 
% Based on utm2deg by Rafael Palacios; utm2deg uses parts of code
% extracted from UTMIP.m function by Gabriel Ruiz Martinez.

% Argument check
error(nargchk(3, 3, nargin)); % 3 arguments required
[n1,m1] = size(x);
[n2,m2] = size(y);
if n1~=n2 || m1~=m2
   error('x and y should be of same size.');
end

if n1 == 1 % row vectors supplied
    x = x';
    y = y';
    transposeflag = true;
else
    transposeflag = false;
end

if isnumeric(utmzone)
    zone = abs(utmzone); % strip the sign
    hemis = -sign(utmzone)/2 + 0.5; % -1 -> 1, 1 -> 0
elseif ischar(utmzone)
    if size(utmzone,2)~=4
       error('UTMZONE should be an array of four-character strings, e.g. ''09 U''');
    end
    utmzone = upper(utmzone);
    if any(utmzone(:,4)>'X' | utmzone(:,4)<'C')
       error('UTM zone letter should range from C to X');
    end
%     try
        zone = str2num(utmzone(:,1:2)); %#ok<ST2NM>
%     catch
%         error('Unrecognized values are supplied for UTM zone.')
%     end
    hemis = zeros(size(utmzone,1),1); % northern hemisphere
    hemis(utmzone(:,4)<='M') = 1; % southern hemisphere
    
%     utmzone = zone*(-hemis*2+1); % signed digital zone
elseif iscell(utmzone)
    ich = cellfun(@ischar,utmzone);
    if any(cellfun(@(s) (size(s,2)~=4), utmzone(ich)))
        error('UTMZONE should be an array of four-character strings, e.g. ''09 U''');
    end
    if any(cellfun(@(s) (upper(s(1,4))>'X' | upper(s(1,4))<'C'), utmzone(ich)))
        error('UTM zone letter should range from C to X');
    end
    idg = cellfun(@isnumeric,utmzone);
    zone = NaN(size(x)); % initialize
    zone(ich) = cellfun(@(s) (str2double(s(1,1:2))),utmzone(ich));
    zone(idg) = cellfun(@(s) (abs(s)),              utmzone(idg));
    hemis = NaN(size(x));
    hemis(ich) = cellfun(@(s) (upper(s(1,4))<='M'),utmzone(ich)); % 0 for northern, 1 for southern
    hemis(idg) = cellfun(@(s) (-sign(s)/2 + 0.5),  utmzone(idg)); % 0 for northern, 1 for southern
    if any(isnan(zone(:)))
        hemis(isnan(zone)) = NaN;
        warning('utm2geo:UnrecognizedUTMZone',...
            'UTM2GEO: Some of the values for UTMZONE are unrecognized; output will contain NaNs.')
    end
else
    error('Unrecognized variable type is supplied for UTM zone.')
end

% if a single value supplied for UTM zone, expand it to the size of x and y
if numel(zone)==1
    zone = repmat(zone,size(x));
    hemis = repmat(hemis,size(x));
else
    [n3,m3] = size(zone);
    if ~transposeflag && (n1~=n3 || m1~=m3) ||...
        transposeflag && (n1~=m3 || m1~=n3)
       error('utmzone array does not match the size of x and y.');
    end
end

% utmzone = cell2mat(utmzone(:)); % arrange in one column and convert to char array
% 
% utmzone = upper(utmzone);
% if size(utmzone,2)~=4 || any(utmzone(:,4)>'X' | utmzone(:,4)<'C')
%    error('utmzone should be an array of four-character strings, e.g. ''09 U''');
% end
% 
% hemis = zeros(size(x)); % northern hemisphere
% hemis(utmzone(:,4)<='M') = 1; % southern hemisphere
% 
% zone = str2num(utmzone(:,1:2)); %#ok<ST2NM>
% zone = reshape(zone,size(x));


% conversion
sa = 6378137.000000;
sb = 6356752.314245;

e2 = (sa^2 - sb^2)^0.5 / sb;
e2cuadrada = e2^2;
c = sa^2 / sb;

x = x - 500000;
y = y - hemis*10000000; % adjustment for southern hemisphere

S = zone*6 - 183; 
lat =  y / ( 6366197.724 * 0.9996 );                                    
v = 0.9996 * c ./ (1 + e2cuadrada.*cos(lat).^2).^0.5;
a = x./v;
a1 = sin(2*lat);
a2 = a1 .* cos(lat).^2;
j2 = lat + a1/2;
j4 = (3*j2 + a2)/4;
j6 = (5*j4 + a2.*cos(lat).^2) / 3;
alfa = 3/4*e2cuadrada;
beta = 5/3*alfa^2;
gama = 35/27*alfa^3;
Bm = 0.9996 * c * ( lat - alfa * j2 + beta * j4 - gama * j6 );
b = (y - Bm)./v;
Epsi = e2cuadrada * a.^2 ./ 2 .* cos(lat).^2;
Eps = a .* (1 - Epsi/3);
nab = b .* (1 - Epsi) + lat;
senoheps = (exp(Eps) - exp(-Eps))/2;
Delt = atan(senoheps./cos(nab));
TaO = atan(cos(Delt).*tan(nab));
Lon = Delt*180/pi + S;
Lat = (lat + (1 + e2cuadrada*(cos(lat).^2) - ...
   3/2*e2cuadrada.*sin(lat).*cos(lat).*(TaO - lat)) .* (TaO - lat))*180/pi;

if transposeflag % row vectors were supplied, transpose back
    Lon = Lon';
    Lat = Lat';
end