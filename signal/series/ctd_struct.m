function c = ctd_struct(n)
%CTD_STRUCT -- Initialize a CTD data structure with a struct array of size n-by-1 with empty fields.
% 
% Input:
%   n   length of the array; n=0 works too (useful for pre-loop initialization).
% 
% Output:
%   c   struct array
% 
% Convention for data storage:
%   p    mx1    pressure
%   t    mx1    temperature
%   s    mx1    salinity
%   time
%   lon
%   lat
%   mission
%   station
%   bottomdepth
%   datafile    original data file

if ~exist('n','var') || isempty(n)
    n = 1;
end

c = struct('p',[],'t',[],'s',[],'time',NaN,'lon',NaN,'lat',NaN,...
    'mission','','station','','bottomdepth',NaN,'datafile','');

c = repmat(c,n,1); % make an array of the specified size, n=0 works too