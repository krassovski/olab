function [it,w] = interpolant(t,ti)
% 1D linear interpolant to interpolate values at t to ti.
% 
% INPUTS:
%   t   a vector of points to interpolate from
%   ti  a vector of points to interpolate to
% 
% OTPUTS:
%   it  [nti x 2] indices into t
%   w   [nti x 2] corresponding weights
%       In case of matching values, it(k,2) is NaN and w(k,2) is 0

nti = length(ti);
w = zeros(nti,2);
it = zeros(nti,2);
for k = 1:nti % do in a loop to avoid memory overflow
    dt = t - ti(k);
    i1 = find(dt<=0,1,'last');
    i2 = find(dt>=0,1,'first');
    dt = abs(dt);
    
    if isempty(i1) % outside on the left
        i1 = i2; % always keep the 1st index valid
        i2 = NaN;
        w(k,:) = [1 0]; % nearest neighbour extrapolation
    elseif isempty(i2) || i1==i2 % outside on the right or exact match (both dt==0)
        i2 = NaN;
        w(k,:) = [1 0]; % nearest neighbour extrapolation
    else % between two tdiff
        w(k,:) = 1./dt([i1 i2]); % inverse distance weighting
    end
    
    it(k,:) = [i1 i2];
end

w = w./(sum(w,2)*[1 1]); % scale to sum to 1