function olab_register()
%OLAB_REGISTER Create a function pointing to olab path.
% Must be called once, after olab folder was created.

olab_folder = fileparts(mfilename('fullpath')); % path to this fuction

up = userpath;
if ~isempty(up) && (up(end)==';' || up(end)==':') % fix for matlab < R2016b
    up(end) = [];
end

fout = fopen(fullfile(up,olab_home_file_name),'w');
fprintf(fout,'%s\r\n','function p = olab_home()');
fprintf(fout,'%s\r\n','%OLAB_HOME Path to olab folder.');
fprintf(fout,'%s\r\n','% Created by olab_register.');
fprintf(fout,'%s',['p=''' olab_folder ''';']);
fclose(fout);