function [p,c,d] = prepnodes_poly(poly)
% Prep nodes and edges arrays from nan-separated polygon poly.

% p = [];
% c = [];
% d = [];
% 
% % terminate with NaNs if poly is not terminated
% if ~isnan(poly(end,1))
%     poly(end+1,:) = [NaN NaN];
% end
% 
% br = [0 find(isnan(poly(:,1)))]; % NaNs in the 1st column
% 
% for ix = 1:length(br)-1 % for each continous segment
%     x = poly(br(ix)+1:br(ix+1)-1,1); % skip trailing NaN
%     y = poly(br(ix)+1:br(ix+1)-1,2);
%     
% %     d = [d; sqrt(diff(x).^2+diff(y).^2)]; % edge length
%     
%     % remove duplicate nodes for closed segments
%     if x(1) == x(end) && y(1) == y(end)
%         x(end) = [];
%         y(end) = [];
%     end
%     
%     p = [p; x y]; % nodes
%     n = length(x);
%     ci = [(1:n-1)' (2:n)'; n 1]; %%%% this will close any segment
%     c = [c; ci+size(c,1)]; % connections
% %     d = [d; sqrt(diff(x).^2+diff(y).^2)]; % edge length
% end

%--------------------
% fully vectorized code
% for ~100 segments it is 30% slower than the code above

% accepts poly as a set of one or more NaN-terminated segments or a single
% non-terminated segment

p = poly;

% edge lengths, including the closing edges; at this stage it also includes
% unwanted edges (connecting to NaNs and between disjoint segments)
d = [sqrt(sum(diff(p).^2, 2)); 0]; % also add a dummy element so the sizes are consistent

lnan = isnan(poly(:,1)); % segment breaks: NaNs in the 1st column
if all(~lnan) % no terminating NaNs, assume it is a single segment
    p = [p; NaN NaN]; % trminate it to be consistent
    lnan = [lnan; true]; % update break marker array
    d = [d; 0];
end
l1st = circshift(lnan,1); % logical indices that point to the 1st node of each continuous segment
p(lnan,:) = [];  % remove terminating NaNs
d(lnan) = [];
l1st(lnan) = []; % and corresponding elements in l1st

llast = circshift(l1st,-1); % logical indices pointing to the last node of each segment

d(llast) = [];   % remove unwanted edge lengths (between disjoint segments)

% find repeating nodes (for closed segments)
pperm = NaN(size(p)); % temp array for comparison of 1st and last node in each segment
pperm(llast,:) = p(l1st,:); % assign 1st nodes to the last nodes ot the temp array
pperm(l1st,:) = p(llast,:); % assign last nodes to the 1st nodes ot the temp array
lrep = all(pperm == p, 2); % logical indices of the repeating nodes, both 1st and last

lrem = llast & lrep;
p(lrem,:) = []; % remove last nodes that repeat 1st nodes
l1st(lrem) = [];
llast = llast | circshift(lrem,-1); % move markers one spot to the head of the array
llast(lrem) = [];
lrep = lrep | circshift(lrem,-1); % move markers one spot to the head of the array
lrep(lrem) = [];
% p, l1st, llast, and lrep are of the same length now

np = size(p,1);
c = [(1:np-1)' (2:np)']; % raw connectivity array (indices into p)

% append one more edge for the last segment in case we need to close it
c = [c; np 0];

% for closed segments, replace connections between disjoit segnemts with
% connections between the last and 1st node
c(llast & lrep,2) = c(l1st & lrep,1); % add connection between last and 1st node of closed segments

% for open segments, remove unwanted connections (between disjoint segments)
c(llast & ~lrep,:) = [];