function [xv,yv,inv] = poly_validate(x,y)
%POLY_VALIDATE -- Remove consequitive and leading NaNs in a NaN-separated polyline.
%
% SYNTAX
%    [x,y,inv] = poly_validate(x,y)
%
% INPUT
%    x,y: coordinates of the polyline
%
% OUTPUT
%    xv,yv: coordinates of the validated polyline
%    inv: logical indices of removed values in the original x,y

inv = false(size(x));
inv(2:end) = (isnan(x(2:end)) & isnan(x(1:end-1)));
if isnan(x(1)) || isnan(y(1))
    inv(1) = true;
end
xv = x(~inv);
yv = y(~inv);