function varargout = intersectPolylines(poly1, varargin)
% INTERSECTPOLYLINES  find common points between 2 polylines or self-intersection
% of a polyline.
% [pts,pos1,pos2] = intersectPolylines(poly1, varargin)
%
% Substitute for David Legland's routine witht the same name in geom2d
% package. This version uses faster intersection test routine,
% lineSegmentIntersect by U. Murat Erdem

if isempty(varargin)
    poly2 = poly1;
    self = true;
else
    poly2 = varargin{1};
    self = false;
end

% intersection test
OUT = lineSegmentIntersect([poly1(1:end-1,:) poly1(2:end,:)],[poly2(1:end-1,:) poly2(2:end,:)]);

% % this can be used to pick intersections that are not at the nodes
% mcross = OUT.intAdjacencyMatrix & (1-OUT.intNormalizedDistance1To2)>1e-8 & OUT.intNormalizedDistance1To2>1e-8;
% isec = any(mcross,2); % connections that cross any coastline edge, but not at the connection node

mcross = OUT.intAdjacencyMatrix(:);
[n1,n2] = size(OUT.intAdjacencyMatrix);
if self
    mcross = mcross && ~eye(n1); % exclude same-node "intersections" for a self-intersection test
end
pts = [OUT.intMatrixX(:) OUT.intMatrixY(:)];
pts = pts(mcross,:);

% point coordinates
varargout{1} = pts;

% also extract positions if necessary
if nargout>1
    pos1 = OUT.intNormalizedDistance1To2 + (0:n1-1)'*ones(1,n2);
    pos1 = pos1(:);
    varargout{2} = pos1(mcross);
end
if nargout>2 && ~self
    pos2 = OUT.intNormalizedDistance2To1 + ones(n1,1)*(0:n2-1);
    pos2 = pos2(:);
    varargout{3} = pos2(mcross);
end