function [loops,lpos] = polylineLoops(poly)
%mk Based on:
%POLYGONLOOPS  divide a possibly self-intersecting polygon into a set of simple loops
%
%   LOOPS = polygonLoops(POLYGON);
%   POLYGON is a polygone defined by a series of vertices,
%   LOOPS is a cell array of polygons, containing the same vertices of the
%   original polygon, but no loop self-intersect, and no couple of loops
%   intersect each other.
%mk LPOS is a cell aray of positions for each loop used in polylineSubcurve to
%   break the original polygon. LPOS are indices into the original POLY!!!
%   
%   Redundant vertices are left in for mkExpandPolyline to work correctly.
%
%   Example:
%       poly = [0 0;0 10;20 10;20 20;10 20;10 0];
%       loops = polygonLoops(poly);
%       figure(1); hold on;
%       drawPolygon(loops);
%       polygonArea(loops)
%
%   See also
%   polygons2d, polygonSelfIntersections
%
%
% ------
% Author: David Legland
% e-mail: david.legland@grignon.inra.fr
% Created: 2009-06-15,    using Matlab 7.7.0.471 (R2008b)
% Copyright 2009 INRA - Cepia Software Platform.
% Licensed under the terms of the LGPL, see the file "license.txt"


% Initialisations

thr = 1e-10; % threshold for coordinate comparison
if any(sum(abs(diff(poly))<thr ,2)==2)
    warning('Input polygon contains consequitive duplicate nodes.')
end

% compute intersections
[inters pos1 pos2] = polylineSelfIntersections(poly);

% case of a polyline without self-intersection
if isempty(inters)
    loops = {poly};
    lpos = {0,size(poly,1)-1}; %%%%%test
    return
end

% array for storing loops
loops = cell(0, 1);
lpos = cell(0, 1); % and positions

positions = sortrows([pos1 pos2;pos2 pos1]);


% First loop

pos0 = 0;
loop = polylineSubcurve(poly, pos0, positions(1, 1));
lp = [pos0, positions(1, 1)];
pos = positions(1, 2);
positions(1, :) = [];

while true
    % index of next intersection point
    ind = find(positions(:,1)>pos, 1, 'first');
    
    % if not found, break
    if isempty(ind)
        break;
    end
    
    % add portion of curve
    loop = [loop;polylineSubcurve(poly, pos, positions(ind, 1))];
    lp = [lp; pos, positions(ind, 1)];
    
    % look for next intersection point
    pos = positions(ind, 2);
    positions(ind, :) = [];
end

% add the last portion of curve
% loop = [loop;polylineSubcurve(poly, pos, pos0)];
% lp = [lp; pos, pos0];
loop = [loop;polylineSubcurve(poly, pos, size(poly,1)-1)];
lp = [lp; pos, size(poly,1)-1];

% % remove redundant vertices
% % idel = sum(loop(1:end-1,:) == loop(2:end,:) ,2)==2;
% idel = sum(abs(diff(loop))<thr ,2)==2;
% loop(idel, :) = [];
% % lp(idel,:) = [];
% if sum(abs(diff(loop([1 end], :)))<thr)==2
%     loop(end, :) = [];
% %     lp(end, :) = [];
% end

% add current loop to the list of loops
loops{1} = loop;
lpos{1} = lp;


% Other loops

Nl = 1;
while ~isempty(positions)

    loop    = [];
    lp = [];
    pos0    = positions(1, 2);
    pos     = positions(1, 2);
    
    while true
        % index of next intersection point
        ind = find(positions(:,1)>pos, 1, 'first');

        % add portion of curve
        loop = [loop;polylineSubcurve(poly, pos, positions(ind, 1))];
        lp = [lp;pos, positions(ind, 1)];

        % look for next intersection point
        pos = positions(ind, 2);
        positions(ind, :) = [];

        % if not found, break
        if pos==pos0
            break;
        end
    end

%     % remove redundant vertices
%     idel = sum(abs(diff(loop))<thr ,2)==2;
%     loop(idel, :) = [];
% %     lp(idel, :) = [];
%     if sum(abs(diff(loop([1 end], :)))<thr)==2
%         loop(end, :) = [];
% %         lp(end, :) = [];
%     end

    % add current loop to the list of loops
    Nl = Nl + 1;
    loops{Nl} = loop;
    lpos{Nl} = lp;
end