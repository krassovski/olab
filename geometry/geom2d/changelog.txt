change log for geom2d


***************
geom2d_20090722
***************

* new features

- new functions for polygons:
    polygonPoint, polygonSubcurve, polygonLoops, distancePointPolygon, 
    distancePolygons, expandPolygon, polygonSelfIntersections,
    projPointOnPolygon, isPointInPolygon, reveresPolygon
    
- new functions for polylines:
    intersectPolylines, polylineSelfIntersections, distancePolylines,
    isPointOnPolyline, reveresPolyline

- projPointOnPolyline can also return the distance of the point to the polyline

- function 'edgeToLine' converts an edge to its supporting line


* Changes

- Renamed functions
    + subcurve      -> polylineSubCurve
    + curveCentroid -> polylineCentroid
    + invertLine    -> reverseLine
            
- Compatibility considerations
    + parallelLine: changed convention for signed distance

 * various bug fixes, and doc updates.

 
***************
geom2d_20090615
***************

* new features

- radicalAxis from 2 circles: 
- moment of a curve (polyline): curveMoment, curveCMoment, curveCSMoment
- new functions for polylines
    distancePointPolyline, drawPolyline, polylineLength, polylinePoint,
    polylineSubcurve, projPointOnPolyline
        
* changes

- changed some function names to avoid potential name conflicts, and to make
        function names more explicit:
    + rotation -> createRotation
    + scaling -> createScaling
    + translation -> createRotation
    + homothecy -> createHomothecy
    + lineSymmetry -> createLineReflection
    + inCircle -> isPointInCircle
    + onCircle -> isPointOnCircle
    + onEdge -> isPointOnEdge
    + onLine -> isPointOnLine
    + onRay -> isPointOnRay
    + normalize -> normalizeVector
    
    
* bug fixes

- fixed bug in intersectEdges
    
many updates in doc.    
     