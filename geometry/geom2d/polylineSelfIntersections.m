function [px,pos1,pos2] = polylineSelfIntersections(poly)
%mk A faster version of POLYLINESELFINTERSECTIONS in geom2d.
% Find self-intersections points of a polyline.
%
%   Also return the 2 positions of each intersection point (the position
%   when meeting point for first time, then position when meeting point
%   for the second time).

[x0,y0,gbg,pos1,pos2] = selfintersect(poly(:,1), poly(:,2));
px = [x0 y0];