function [pos,dist,proj] = projPointOnPolyline(p, poly)
%mk Faster substitute for projPointOnPolyline in geom2d.

% deal with each NaN-separated segment separately
br = find(isnan(poly(:,1)));
np = size(poly,1);
% if isempty(br)
%     br = [0; np+1];
% else
    br = [0; br; np+1];
% end
nbr = length(br)-1; % number of continuous segments
dist = ones(size(p,1),1)*Inf; % initialize
pos = dist; % initialize
proj = [pos pos]; % initialize
for ib = 1:nbr
    if br(ib+1)-br(ib) > 1 % at least one-node segment
        poly_ = poly(br(ib)+1 : br(ib+1)-1, :); % exclude NaNs

        [proj_,dist_,ta,t0] = distance2curve(poly_, p);
        % calculate positions in this segment
        if length(t0) == 1 % one-point poly_
            pos_ = ta; % should be all zeros
        else
            binb = t0; % bin borders
            % widen the last bin a bit to squeeze the last point into the last segment
            binb(binb==1) = binb(binb==1)+eps;
            [gbg,bin] = histc(ta,binb); % edge number in poly
            dt0 = diff(t0); % poly edge width relative to the entire poly length
            pos_ = bin-1 + (ta-t0(bin))./dt0(bin); % positions in this segment
        end
        
        pos_ = pos_ + br(ib); % positions in the entire poly contour

        % update values for points with lower distance than before
        irep = dist_<dist; % values to update
        dist(irep) = dist_(irep);
        proj(irep,:) = proj_(irep,:);
        pos(irep) = pos_(irep);
    end
end