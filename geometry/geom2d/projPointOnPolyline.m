function [pos,dist,proj,ta] = projPointOnPolyline(p, poly)
%mk Faster substitute for projPointOnPolyline in geom2d.
% Yet faster version: Deals with NaN-separated poly in one run.
% pos   projection position on the polyline
% dist  distance of each point to the polyline
% proj  projection coordinates
% ta    fractional arc length along the poly

% this must be in the calling routine and not here to avoid multiple calls:
% olab_addpath('geometry') % distance2curve

[proj,dist,ta,t0] = distance2curve(poly, p);

% calculate positions
if length(t0(~isnan(t0))) == 1 % one-point poly
    pos = ta; % should be all zeros
else
    binb = t0; % bin borders
    % widen the last bin a bit to squeeze the last point into the last segment
    binb(binb==1) = binb(binb==1)+eps;
    [gbg,bin] = histc(ta,binb); % edge number in poly
    dt0 = diff(t0); % poly edge width relative to the entire poly length
    pos = bin-1 + (ta-t0(bin))./dt0(bin); % positions in this segment
end