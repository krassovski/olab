function center = centroid(varargin)
%CENTROID compute centroid (center of mass) of a set of points
%
%   PTS = centroid(POINTS, MASS)
%   PTS = centroid(PTX, PTY, MASS)
%   Computes center of mass of POINTS, weighted by coefficient MASS.
%   POINTS is a [N*2] array, MASS is [N*1] array, and PTX and PTY are also
%   [N*1] arrays.
%   The result PTS has the same size as MASS.
%
%   PTS = centroid(POINTS)  or  PTS = centroid(PTX, PTY)
%   Assume equal weights for all points.
%
%   Example:
%   pts = [2 2;6 1;6 5;2 4];
%   centroid(pts)
%   ans =
%        4     3
%
%   See Also:
%   points2d, polygonCentroid
%   
%   ---------
%   author : David Legland 
%   INRA - TPV URPOI - BIA IMASTE
%   created the 07/04/2003.
%

%   HISTORY
%   22/06/2009 support for 3D points


% use empty mass by default
mass = [];

% extract input arguments
if nargin==1
    % give only array of points
    var = varargin{1};
    pts = var;
elseif nargin==2
    % either POINTS+MASS or PX+PY
    var = varargin{1};
    if size(var, 2)>1
        % coefs are POINTS, and MASS
        pts = var;
        mass = varargin{2};
    else
        % coefs are PX and PY
        pts = [var varargin{2}];
    end
elseif nargin==3
    % coefs are PX, PY, and MASS
    pts = [varargin{1} varargin{2}];
    mass = varargin{3};
end

% compute centroid
if isempty(mass)
    % no weight
    center = mean(pts);
else
    % chech dimension of weights
    if size(mass,2)>1
        mass=mass';
    end
    
    % compute weighted centroid
    center = mean(pts.*repmat(mass, 1, 3));
end
