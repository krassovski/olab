function intersects = intersectLinePolygon(line, points)
%INTERSECTLINEPOLYGON get intersection points between a line and a polygon
%
%   P = intersectLine(LINE, POLY)
%   Returns the intersection points of the lines LINE with polygon POLY. 
%   LINE is a 1x4 array containing parametric representation of the line
%   (in the form [x0 y0 dx dy], see createLine for details). 
%   POLY is a Nx2 array containing coordinate of polygon vertices
%   
%   See Also
%   lines2d, polygons2d, intersectLines
%
%   ---------
%
%   author : David Legland 
%   INRA - TPV URPOI - BIA IMASTE
%   created the 31/10/2003.
%

%   HISTORY
%   2008/11/24 rename 'pi' as 'intersects', update doc
%   2009/23/07 removed forgotten occurence of 'pi' variable (thanks to Bala
%       Krishnamoorthy)


% create the array of edges
N = length(points);
edges = [points(1:N-1,:) points(2:N, :)];
edges = [edges; points(N,:) points(1, :)];

% for each edge, compute intersection with the line, and append it to the
% list of intersection 'intersects'.
intersects = zeros(0,2);
for i=1:length(edges)
    p0 = intersectLines(line, createLine(edges(i,1:2), edges(i,3:4)));
    if isPointOnEdge(p0, edges(i,:), 1e-8) %mk: added tolerance of 1e-8 
        intersects = [intersects; p0];  %#ok<AGROW>
    end
end

