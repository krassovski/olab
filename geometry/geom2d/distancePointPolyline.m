function varargout = distancePointPolyline(point, poly, varargin)
%DISTANCEPOINTPOLYLINE  Compute shortest distance between a point and a polyline
%   output = distancePointPolyline(POINT, POLYLINE)
%
%   Example:
%       pt1 = [30 20];
%       pt2 = [30 5];
%       poly = [10 10;50 10;50 50;10 50];
%       distancePointPolyline([pt1;pt2], poly)
%       ans =
%           10
%            5
%
%   See also
%   polylines2d, points2d
%   distancePointEdge, projPointOnPolyline
%
% ------
% Author: David Legland
% e-mail: david.legland@grignon.inra.fr
% Created: 2009-04-30,    using Matlab 7.7.0.471 (R2008b)
% Copyright 2009 INRA - Cepia Software Platform.
% Licensed under the terms of the LGPL, see the file "license.txt"

%   HISTORY
%   23/06/2009 compute all distances in one call

% number of points
Np = size(point, 1);

%mk moved here from the loop
%mk deal with one-point and empty polylines
nnodes = size(poly,1);
switch nnodes
    case 0
        varargout{1} = [];
        return
    case 1
        minDist = min(distancePoints(point, poly));
    otherwise
        edges = [poly(1:end-1,:) poly(2:end,:)]; % construct the set of edges
        minDist = inf*ones(Np, 1); % allocate memory for the result
        for p=1:Np % process each point
            % compute min distance between current point and all edges
            minDist(p) = min(distancePointEdge(point(p, :), edges));
        end
end

% process output arguments
if nargout<=1
    varargout{1} = minDist;
end
