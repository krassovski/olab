function [point,loc1,loc2] = intersectEdges(edge1, edge2)
%INTERSECTEDGES return all intersections points of N edges in 2D
%
%   [P,loc1,loc2] = intersectEdges(E1, E2);
%
%mk Added   loc1,loc2   positions on edge1,edge2 to the output.
%mk For non-overlapping edges, loc1 is the position of the intersection
%mk point on edge1, loc2 is its' position on edge2.
%mk For overlapping edges, P contains Inf and loc1,loc2 are positions of
%mk egde2 endpoints on edge1.
%
%   returns the intersection point of edges E1 and E2. E1 and E2 are [1*4] 
%   arrays, containing parametric representation of each edge (in the form
%   [x1 y1 x2 y2], see 'createEdge' for details).
%   
%   In case of colinear edges, returns [Inf Inf].
%   In case of parallel but not colinear edges, returns [NaN NaN].
%
%   If each input is [N*4] array, the result is a [N*2] array containing
%   intersections of each couple of edges.
%   If one of the input has N rows and the other 1 row, the result is a
%   [N*2] array.
%
%   See also:
%   edges2d, intersectLines
%
%   ---------
%   author : David Legland 
%   INRA - TPV URPOI - BIA IMASTE
%   created the 31/10/2003.
%

%   HISTORY
%   19/02/2004: add support for multiple edges.
%   15/08/2005: rewrite a lot, and create unit tests
%   08/03/2007: update doc


% % Initialisations

% ensure input arrays are same size
N1  = size(edge1, 1);
N2  = size(edge2, 1);

% ensure input have same size
if N1~=N2
    if N1==1
        edge1 = repmat(edge1, [N2 1]);
        N1 = N2;
    elseif N2==1
        edge2 = repmat(edge2, [N1 1]);
    end
end

% tolerance for precision
tol = 1e-14;

% initialize result array with the default value for non-intersecting edges [NaN NaN]
x0  = NaN(N1, 1);
y0  = x0;
loc1 = x0; % 
loc2 = x0;


% % Process parallel and colinear cases

% indices of parallel edges
%par = abs(dx1.*dy2 - dx1.*dy2)<tol;
par = isParallel(edge1(:,3:4)-edge1(:,1:2), edge2(:,3:4)-edge2(:,1:2));

% indices of colinear edges
% equivalent to: |(x2-x1)*dy1 - (y2-y1)*dx1| < eps
col = abs(  (edge2(:,1)-edge1(:,1)).*(edge1(:,4)-edge1(:,2)) - ...
            (edge2(:,2)-edge1(:,2)).*(edge1(:,3)-edge1(:,1)) )<tol & par;

col = find(col); %mk

% colinear edges may have 0, 1 or infinite intersection
% Discrimnation based on position of edge2 vertices on edge1
if sum(col)>0
    % array for storing results of colinear edges
    resCol = Inf(size(col));

    % compute position of edge2 vertices wrt edge1 (position for edge1)
    t1 = edgePosition(edge2(col, 1:2), edge1(col, :));
    t2 = edgePosition(edge2(col, 3:4), edge1(col, :));
    
%     % control location of vertices
%     %mk do not swap -- the caller may need to preserve node order 
%     swap = t1>t2;
%     tmp = t1;
%     t1(swap)  = t2(swap);
%     t2(swap)  = tmp(swap);
    
    % edge totally before first vertex or totally after last vertex
%     resCol(col(t2<-eps))  = NaN;
%     resCol(col(t1>1+eps)) = NaN;
    resCol(t2<-eps & t1<-eps)  = NaN; %mk for unswapped t1,t2
    resCol(t1>1+eps & t2>1+eps) = NaN;

    % set up result into point coordinate
    x0(col) = resCol;
    y0(col) = resCol;
    loc1(col) = t1;
    loc2(col) = t2;

    % touches on first point of second edge
    ctouch = abs(t1)<1e-14 | abs(t1-1)<1e-14;
    touch = col(ctouch);
    x0(touch) = edge2(touch, 1);
    y0(touch) = edge2(touch, 2);
    loc1(touch) = t1(ctouch);
    %mk compute intersection point position for the second edge
    loc2(touch) = edgePosition([x0(touch) y0(touch)], edge2(touch, :));
    
    % touches on second point of second edge
    ctouch = abs(t2)<1e-14 | abs(t2-1)<1e-14;
    touch = col(ctouch);
    x0(touch) = edge2(touch, 3);
    y0(touch) = edge2(touch, 4);
    loc1(touch) = t2(ctouch);
    %mk compute intersection point position for the second edge
    loc2(touch) = edgePosition([x0(touch) y0(touch)], edge2(touch, :));
end


% % Process non parallel cases

% process intersecting edges whose interecting lines intersect
i = find(~par);

if sum(i)>0
    x1  = edge1(i,1);
    y1  = edge1(i,2);
    dx1 = edge1(i,3)-x1;
    dy1 = edge1(i,4)-y1;
    x2  = edge2(i,1);
    y2  = edge2(i,2);
    dx2 = edge2(i,3)-x2;
    dy2 = edge2(i,4)-y2;

    % compute intersection point
    delta = (dx2.*dy1-dx1.*dy2);
    x0(i) = ((y2-y1).*dx1.*dx2 + x1.*dy1.*dx2 - x2.*dy2.*dx1) ./ delta;
    y0(i) = ((x2-x1).*dy1.*dy2 + y1.*dx1.*dy2 - y2.*dx2.*dy1) ./ -delta;
    
%mk        
%      % compute position on each edge
%      t1  = ((y0(i)-y1)*dy1 + (x0(i)-x1)*dx1) / (dx1.*dx1+dy1.*dy1);
%      t2  = ((y0(i)-y2)*dy2 + (x0(i)-x2)*dx2) / (dx2.*dx2+dy2.*dy2);
%mk
    %mk compute position on each edge
    loc1(i)  = ((y0(i)-y1).*dy1 + (x0(i)-x1).*dx1) ./ (dx1.*dx1+dy1.*dy1);
    loc2(i)  = ((y0(i)-y2).*dy2 + (x0(i)-x2).*dx2) ./ (dx2.*dx2+dy2.*dy2);

    % check points 
    out = loc1(i)<-tol | loc1(i)>1+tol | loc2(i)<-tol | loc2(i)>1+tol;
%mk     
%     x0(out) = NaN;
%     y0(out) = NaN;
%mk
    x0(i(out)) = NaN; %mk
    y0(i(out)) = NaN;

end


% % format output arguments

point = [x0 y0];


%mk test
% [pi,t1,t2] = intersectEdges([2 1 -2 -1], [8 4 2 1; 0 2 3 8; 2 1 4 2; 0 1 1 0; -2 0 2 2; 4 2 8 4; 0 0 8 4])
% 
% pi =
% 
%     2.0000    1.0000
%        NaN       NaN
%     2.0000    1.0000
%     0.6667    0.3333
%        NaN       NaN
%        NaN       NaN
%        Inf       Inf
% 
% 
% t1 =
% 
%          0
%     0.8333
%          0
%     0.3333
%        NaN
%    -0.5000
%     0.5000
% 
% 
% t2 =
% 
%     1.0000
%    -0.4444
%          0
%     0.6667
%        NaN
%    -1.5000
%    -1.5000