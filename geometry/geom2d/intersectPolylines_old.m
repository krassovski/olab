function varargout = intersectPolylines_old(poly1, varargin)
% function [pts,pos1,pos2] = intersectPolylines(poly1, varargin)
%INTERSECTPOLYLINES  find common points between 2 polylines
%   output = intersectPolylines(input)
%
%   Example
%   intersectPolylines
%
%   See also
%
%
% ------
% Author: David Legland
% e-mail: david.legland@grignon.inra.fr
% Created: 2009-06-15,    using Matlab 7.7.0.471 (R2008b)
% Copyright 2009 INRA - Cepia Software Platform.
% Licensed under the terms of the LGPL, see the file "license.txt"

if isempty(varargin)
    pts = interX(poly1')';
else
    poly2 = varargin{1};
%     if size(poly1,1)==2 || size(poly2,1)==2
%         pts = intersectEdges([poly1(1:end-1,:) poly1(2:end,:)], [poly2(1:end-1,:) poly2(2:end,:)]);
%     else
        pts = interX(poly1', poly2')'; % interX doesn't work for some cases
%     end
end

%mk also compute positions if necessary
% point coordinates
varargout{1} = pts;

% point positions on polylines
if nargout>1
    varargout{2} = projPointOnPolyline(pts, poly1);
end
if nargout>2 && ~isempty(varargin)
    varargout{3} = projPointOnPolyline(pts, poly2);
end