function poly = geom_removeloops(poly)
% Remove loops resulting from self-intersections.

while true
    % remove repeating nodes
    poly(all(diff(poly)==0,2), :) = [];

    nn = size(poly,1);
%     [sx,sxpos1,sxpos2] = polylineSelfIntersections(poly);
    [sx,sxpos1,sxpos2] = selfintersect_bypiece(poly);
    sxpos1(sxpos1==0 | sxpos1==nn) = [];
    sxpos2(sxpos2==0 | sxpos2==nn) = [];
    
    %%%%%%%
    if ~isempty(sxpos1) && sxpos2(1)-sxpos1(1) > 3 % do not remove large loops (not to cut off islands)
        sxpos1(1) = []; % skip this loop
        sxpos2(1) = [];
    end
    
    if isempty(sxpos1) % exit if no more loops
        break
    end

    sxpos1 = sxpos1(1); % dealing with one loop at a time
    sxpos2 = sxpos2(1);
    
    %%%%% dealing with all non-nested loops at once will speed up the routine by
    %%%%% reducing the number self-intersection tests
    
    if abs(sxpos1-round(sxpos1))<1e-8 % within round-off error
        sxpos1 = round(sxpos1); % move to the node
    end
    if sxpos1 ~= floor(sxpos1)
        sxpos1 = floor(sxpos1(1)); % to avoid coincident nodes in the new polygon
    end
    poly = [polylineSubcurve(poly, 0, sxpos1); polylineSubcurve(poly, sxpos2, nn)];
end