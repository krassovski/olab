function [d, t, proj] = point_seg_dist(v1, v2, p)
% Distances and projections from a single location to a number of segments.
% Modified from seg_dist.
% Input:
%   v1, v2  [Nx2] segment start and end points [x y]
%   p       [1x2] test location [x y]
% Output:
%   d:      normal distance to each segment
%   proj:   projected points
%   t:      euclidean distance from projected point to v1

% MK, 2014

ns = size(v1,1); % number of segments

a = zeros(ns,1);
b = sum((v2-v1).^2, 2).^0.5; % segment lengths
u = (v2-v1)./[b b]; % unit vectors % segment is line v1+tu with t in [0,b]

pe = repmat(p,ns,1); % expand p to the number of segments 

% get projection index on line (not segment!) defined by each segment
t = sum((pe - v1).*u, 2);

% get projection index on segment
t = max(t,a);
t = min(t,b);

proj = v1 + [t t].*u;  % get projected points
d = sum((pe - proj).^2, 2).^0.5; % distances