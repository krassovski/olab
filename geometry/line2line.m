function theta = line2line(v11,v12,v21,v22)
% Angle between lines defined by vertices [v11 v12] and [v21 v22], each
% vertex is [x,y]. Theta is from -pi to pi.

v1 = v12 - v11; 
v2 = v22 - v21; 
x1 = v1(:,1); 
y1 = v1(:,2); 
x2 = v2(:,1); 
y2 = v2(:,2); 

theta = atan2(x1.*y2-y1.*x2, x1.*x2+y1.*y2);