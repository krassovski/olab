function [px,pos1,pos2] = selfintersect_bypiece(poly)
% Determine self-intersection points and positions of a polyline poly.

% to avoid memory overflow
ni = 3e3; % max total number of edges for one run of lineSegmentIntersect
np = size(poly,1);

px = []; pos1 = []; pos2 = [];
for ib1 = 1 : ni : np
%     disp(['---------- Segment 1 start node:  ' num2str(ib1) '   of ' num2str(np)])
    ie1 = min([ib1+ni np]);
    if np-ie1 == 1 % do not leave a single node for the last segment
        ie1 = np;
    end
    if ib1 == ie1 % quit if end of polyline have been reached (after the single-node adjustment)
        break
    end
    seg1 = poly(ib1:ie1,:);
    
    % find self-intersections of the current segment
%     % redundant routine: intersections.m does the job
%     [px_,pos1_,pos2_] = polylineSelfIntersections(seg1); 
    [px_,pos1_,pos2_] = intersections(seg1);
    pos1_ = pos1_ + ib1-1;
    pos2_ = pos2_ + ib1-1;
    
    px = [px; px_]; pos1 = [pos1; pos1_]; pos2 = [pos2; pos2_];
    
    % find intersections with other segments
    for ib2 = ie1 : ni : np
%         disp(['----- Segment 2 start node:  ' num2str(ib2) '   of ' num2str(np)])
        ie2 = min([ib2+ni np]);
        if np-ie2 == 1 % do not leave a single node for the last segment
            ie2 = np;
        end
        if ib2 == ie2 % quit if end of polyline have been reached (after the single-node adjustment)
            break
        end
        seg2 = poly(ib2:ie2,:);
        
        [px_,pos1_,pos2_] = intersections(seg1,seg2);
        irem = pos1_==size(seg1,1)-1 & pos2_==0; % discard intersection at the joining node
        px_(irem,:) = []; pos1_(irem) = []; pos2_(irem) = [];
        pos1_ = pos1_ + ib1-1;
        pos2_ = pos2_ + ib2-1;
        
        px = [px; px_]; pos1 = [pos1; pos1_]; pos2 = [pos2; pos2_]; 
    end
end