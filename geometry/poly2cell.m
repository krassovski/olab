function r = poly2cell(poly)
% Cell array of continuous polyline segments from nan-separated polygon poly.

% ensure poly has it 3 columns; append a column with zeros if necessary
[np,nc] = size(poly);

% terminate with NaNs if poly is not terminated
if ~isnan(poly(end,1))
    poly(end+1,:) = NaN(1,nc);
end

br = [0; find(isnan(poly(:,1)))]; % NaNs in the 1st column

nr = length(br)-1; % number of segments
r = cell(nr,1);
for ix = 1:nr % for each continous segment
    in = br(ix)+1:br(ix+1)-1; % indices for this segment, excluding the terminating NaN
    r{ix} = poly(in,:);
end