function isec = geom_crosstest(et,eo)
% Determine if edges in et cross any edge in eo, but not at the bounding node of
% corresponding edge et.

tol = 1e-9; % tolerance for comparison

% to avoid memory overflow
nmax = 2e6; % max total number of edges for one run of lineSegmentIntersect
ni = floor(nmax/size(eo,1));
nl = size(et,1);

isec = repmat(false,nl,1);
for ilb = 1 : ni : nl
    disp(['-------- Bunch start node:  ' num2str(ilb) '   of ' num2str(nl)])
    
    ile = min([ilb+ni-1 nl]);

    % current LW nodes
    et_ = et(ilb:ile,:);
    
    OUT = lineSegmentIntersect(et_,eo); % intersection test
    mcross = OUT.intAdjacencyMatrix & (1-OUT.intNormalizedDistance1To2)>tol & OUT.intNormalizedDistance1To2>tol;
    isec(ilb:ile) = any(mcross,2); % connections that cross any coastline edge, but not at the connection node
end