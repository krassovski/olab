function [xp,yp] = seg_parallel(x,y,sig,d)
% Coordinates of a segment parallel to a segment x(1),y(1) : x(2),y(2)

a = atan2(diff(x),diff(y));
xp = x - d*cos(a)*sig;
yp = y + d*sin(a)*sig;