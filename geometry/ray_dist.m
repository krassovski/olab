% Modified from seg_dist.
% v1 <- [x1; y1] % start point of the ray
% v2 <- [x2; y2] % 2nd defining point of the ray
% x  <- [x x x ...; y y y...] % points to test
% compute the sq distance between points x and the line segment from V1 to V2.
% d: sq distance
% p: projected point
% t: euclidean distance from projected point to v1
function [d, t, p] = ray_dist(v1, v2, x)

% ensure necessary array shapes
v1 = v1(:);
v2 = v2(:);
[m,n] = size(x);
if m~=2 && n==2
    x = x';
end

a = 0;
b = norm(v2-v1);
u = (v2-v1)/norm(b); % unit vector % segment is line v1+tu with t in [0,b]

% get projection index on line (not segment!)
t = (x'-repmat(v1',size(x,2),1))*u; 

% get projection index on the ray
t = max(t,a);
% t = min(t,b); % this is the only difference with seg_dist

p = repmat(v1',size(x,2),1) + t*u';  % get projected points
d=(x'-p);
d=d.*d;
d=sum(d,2);