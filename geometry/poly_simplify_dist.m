function [x,y,isub] = poly_simplify_dist(x,y,d)
% Radial distance polyline simplification.

n = length(x);
isub = false(size(x));
i = 1;
ir = 1;
while true
    isub(i) = true;
    
    if ir==n
        break
    end
    
    % ray start
    xr = x(i);
    yr = y(i);
    
    e = ((x(i+1:end)-xr).^2 + (y(i+1:end)-yr).^2).^0.5;
    ir = i + find(e>d,1,'first'); % 1st node out of range d
    if isempty(ir) % i+[] yields []
%         i = n; % last node is always a part of the resampled polyline
        
        % condition for the last node does not hold,
        % replace the last found node with the last node of the polyline;
        % last node is always a part of the resampled polyline
        isub(i) = false; 
        isub(end) = true; 
        break
    end
    i = ir;
end

x = x(isub);
y = y(isub);