import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import java.awt.image.BufferedImage;
import java.util.Vector;
import java.io.*;
import java.util.*;

public class SimpliPoly extends Applet {
    int M=1000;
    int WIDTH=600;
    int HEIGHT=500;
  
    //  Varivables use to store polygon
    //original control points
    int ctrlOriginPoints[][];
    //current number of control points
    int numOriginPoints=0;

    //control points M
    int ctrlpoints[][];
    //current number of control points
    int numPoints=0;

    //control points M
    double ctrlCurvature[];
    //control points
    String strCurvature[];
    //number of vertices in a sub part
    int numSub=5,numSub1=5;

    //Threshold
    //  double threshold=0;
    //  continute
    boolean cont=true;
    int focus=0;

    //  step of curve
    double step=0.001;
    // radius of Point
    int Rad=2;
    BufferedImage bimg;
    private boolean isStandalone = false;
    TextField txtThreshold = new TextField();
    Scrollbar sbThreshold = new Scrollbar();
    TextField txtError = new TextField();
    Scrollbar sbError = new Scrollbar();
    Button btnNew = new Button();
    Panel panel1 = new Panel();
    Panel panel2 = new Panel();
    Checkbox chkOriLine = new Checkbox();
    Checkbox chkCurLine = new Checkbox();
    Checkbox chkCurvature = new Checkbox();
    Checkbox chkError = new Checkbox();
    Label label1 = new Label();
    Label label2 = new Label();

    Label lblCurvature = new Label();
    Label lblCurvature1 = new Label();
    Label label3 = new Label();
    Label label4 = new Label();
    Label label5 = new Label();
    Label label6 = new Label();
    Label lblOri = new Label();
    Label lblCur = new Label();
    Label label7 = new Label();
    Label lblRec = new Label();
    Label label8 = new Label();
    Label label9 = new Label();

    Label label10 = new Label();
    Label label11 = new Label();
    Label label12 = new Label();
 

    //Get a parameter value
    public String getParameter(String key, String def) {
        return isStandalone ? System.getProperty(key, def) :
            (getParameter(key) != null ? getParameter(key) : def);
    }

    //Construct the applet
    public SimpliPoly() {
    }

    //Initialize the applet
    public void init() {
        try {
            jbInit();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    //Component initialization
    private void jbInit() throws Exception {

        this.setLayout(null);
        this.setBackground(Color.lightGray);
        this.addMouseListener(new SimpliPoly_this_mouseAdapter(this));

        panel1.setBackground(Color.gray);
        panel1.setFont(new java.awt.Font("MS Sans Serif", 0, 11));

        //panel1.setBounds(new Rectangle(502, 2, 120, 511));
        panel1.setBounds(new Rectangle(502, 2, 420, 511));
        panel1.setLayout(null);
        panel2.setForeground(Color.black);
        panel2.setBounds(new Rectangle(1, 514, 921, 44));
        panel2.setLayout(null);

        panel2.setBackground(Color.gray);
        panel2.setFont(new java.awt.Font("MS Sans Serif", 0, 11));

        chkOriLine.setForeground(Color.RED);
        chkOriLine.setLabel("Original Polyline");
        chkOriLine.setState(true);
        chkOriLine.setBounds(new Rectangle(5, 5, 300, 23));
        chkOriLine.addItemListener(new SimpliPoly_chkOriLine_itemAdapter(this));

        chkCurLine.setForeground(Color.GREEN);
        chkCurLine.setLabel("Simplified Polyline");
        chkCurLine.setState(true);
        chkCurLine.setBounds(new Rectangle(5, 30, 300, 23));
        chkCurLine.addItemListener(new SimpliPoly_chkCurLine_itemAdapter(this));

        chkCurvature.setLabel("Pseudo-curvatures");
        chkCurvature.setState(true);
        chkCurvature.setBounds(new Rectangle(5, 55, 300, 23));
        chkCurvature.addItemListener(new SimpliPoly_chkCurvature_itemAdapter(this));
    
        chkError.setLabel("Check Distance Error");
        chkError.setState(true);
        chkError.setBounds(new Rectangle(5, 80, 300, 23));
        chkError.addItemListener(new SimpliPoly_chkError_itemAdapter(this));

        label1.setText("Curvature Threshold");
        label1.setBounds(new Rectangle(5, 115, 300, 15));
    
        txtThreshold.setText("0");
        txtThreshold.setBounds(new Rectangle(15, 130, 100, 27));
        sbThreshold.setMaximum(10000);
        sbThreshold.setOrientation(0);
        sbThreshold.setUnitIncrement(1);
        sbThreshold.setBounds(new Rectangle(15, 157, 100, 27));
        sbThreshold.addAdjustmentListener(new SimpliPoly_sbThreshold_adjustmentAdapter(this));

        label2.setText("Distance Error Threshold");
        label2.setBounds(new Rectangle(5, 190, 300, 15));
        txtError.setBounds(new Rectangle(15, 205, 100, 27));
        txtError.setText("0");
        sbError.addAdjustmentListener(new SimpliPoly_sbError_adjustmentAdapter(this));
        sbError.setBounds(new Rectangle(15, 232, 100, 27));
        sbError.setUnitIncrement(1);
        sbError.setOrientation(0);
        sbError.setMaximum(10000);

        btnNew.setLabel("Clear");
        btnNew.setBounds(new Rectangle(20, 280, 80, 23));
        btnNew.addActionListener(new SimpliPoly_btnNew_actionAdapter(this));

        label3.setText("Original Vertices");
        label3.setBounds(new Rectangle(12, 315, 300, 15));
        lblOri.setText("0");
        lblOri.setForeground(Color.BLUE);
        lblOri.setBounds(new Rectangle(45, 327, 300, 15));
        label4.setText("Remaining Vertices");
        label4.setBounds(new Rectangle(12, 342, 300, 15));
        lblCur.setText("0");
        lblCur.setForeground(Color.BLUE);
        lblCur.setBounds(new Rectangle(45, 354, 81, 15));
        label7.setText("     Reduction");
        label7.setBounds(new Rectangle(12, 369, 300, 15));
        lblRec.setText("0%");
        lblRec.setForeground(Color.BLUE);
        lblRec.setBounds(new Rectangle(45, 381, 81, 15));

     
        lblCurvature1.setBounds(new Rectangle(12, 5, 600, 15));
        lblCurvature1.setLocale(java.util.Locale.getDefault());
        lblCurvature1.setText("Click Clear, click left mouse button to input points, then change Curvature and Distance Error Thresholds");
        lblCurvature1.setAlignment(Label.LEFT);
        lblCurvature1.setFont(new java.awt.Font("Dialog", 1, 11));

        label8.setBounds(new Rectangle(12, 20, 600, 15));
        label8.setLocale(java.util.Locale.getDefault());
        label8.setText("With initial default threshold values original and simplified polylines coincide.");
        label8.setAlignment(Label.LEFT);
        label8.setFont(new java.awt.Font("Dialog", 1, 11));


        panel1.add(chkOriLine, null);
        panel1.add(chkCurLine, null);
        panel1.add(chkCurvature, null);
        panel1.add(chkError, null);
        panel1.add(txtThreshold, null);
        panel1.add(sbThreshold, null);
        panel1.add(txtError, null);
        panel1.add(sbError, null);
        panel1.add(btnNew, null);
        panel1.add(label5, null);
        panel1.add(label1, null);
        panel1.add(label6, null);
        panel1.add(label2, null);
        panel2.add(label8, null);

        panel1.add(label3, null);
        panel1.add(label4, null);
        panel1.add(lblOri, null);
        panel1.add(lblCur, null);
        panel1.add(label7, null);
        panel1.add(lblRec, null);
        panel1.add(label10, null);
        panel1.add(label11, null);

        panel1.add(label12, null);
        this.add(panel2, null);
        panel2.add(lblCurvature, null);
        panel2.add(lblCurvature1, null);

        this.add(panel1, null);
        resetData();
    }

    public void resetData(){
        numOriginPoints=0;
        numPoints=0;
        txtThreshold.setText("0");
        sbThreshold.setValue(0);
        txtError.setText("0");
        sbError.setValue(0);

        ctrlOriginPoints=new int[M][3];
        ctrlpoints=new int[M][3];
        ctrlCurvature=new double[M];
        strCurvature=new String[M];
        lblOri.setText("0");
        lblCur.setText("0");
        lblRec.setText("0%");
        focus=0;
        cont=false;
    }
    public void recoveryData(){
        numPoints=numOriginPoints;
        for(int i=0;i<numPoints;i++){
            ctrlpoints[i][0]=ctrlOriginPoints[i][0];
            ctrlpoints[i][1]=ctrlOriginPoints[i][1];
            ctrlpoints[i][2]=ctrlOriginPoints[i][2];
        }
        generateCurvature();
    }
    //Get Applet information
    public String getAppletInfo() {
        return "Applet Information";
    }

    //Get parameter info
    public String[][] getParameterInfo() {
        return null;
    }
    /****************************************************************************/
    //Draw Curve
    ///////////////////////////////////////////////////

    // calculate the factorial of n
    int factorial(int n){
        int i;
        int result=1;
        if (n==0)
            return 1;
        else
            for (i=1;i<=n;i++){
                result*=i;
            }
        return result;
    }

    // calculate Combination
    int Combination(int n, int k){
        return factorial(n)/(factorial(k)*factorial(n-k));
    }

    //get x/y/z value on Bezier curve
    // u : 0 -> 1 (position)
    // *v: array of control points
    int getVertex(double u, int v[], int k){
        double result=0;
        int i;
        for(i=0;i<=k;i++){
            result+=Combination(k,i)*Math.pow(1-u,k-i)*Math.pow(u,i)*v[i];
        }
        return (int)result;
    }

    // draw Curve
    void drawCurve(Graphics2D g, int start, int num){
        int bufx[]=new int[num];
        int bufy[]=new int[num];
        int tmpX1,tmpY1,tmpX2,tmpY2;
        //  BasicStroke bs = new BasicStroke(1.0f);

        createBuffer(bufx,start,num,0);
        createBuffer(bufy,start,num,1);
        tmpX1 = ctrlOriginPoints[start][0];
        tmpY1 = ctrlOriginPoints[start][1];
        //      jplCanvas
        for(double t=step;t<=1;t=t+step){
            tmpX2 = getVertex(t, bufx, num - 1);
            tmpY2 = getVertex(t, bufy, num - 1);
            g.drawLine(tmpX1,tmpY1,tmpX2,tmpY2);
            tmpX1=tmpX2;
            tmpY1=tmpY2;
        }
    }

    public Graphics2D createGraphics2D(int w, int h) {
        Graphics2D g2 = null;
        if (bimg == null || bimg.getWidth() != w || bimg.getHeight() != h) {
            bimg = (BufferedImage) createImage(w, h);
        }
        g2 = bimg.createGraphics();
        //      g2.setBackground(Color.black);
        g2.clearRect(0, 0, w, h);
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
        return g2;
    }

    public void paint(Graphics g){
        Vector tmpV=new Vector();
        if (numOriginPoints<=0) return;
        displayNum();
        Graphics2D g2 = createGraphics2D(WIDTH, HEIGHT);
        float dash[] = {2.0f, 6.0f};
        BasicStroke bs = new BasicStroke (1.5f, BasicStroke.CAP_BUTT,
                         BasicStroke.JOIN_MITER, 1.0f, dash, 0.0f);
        g2.setStroke(bs);
        g2.setColor(Color.RED);
        if(chkOriLine.getState())
            for (int i = 1; i < numOriginPoints; i++) {
                g2.drawLine(ctrlOriginPoints[i - 1][0], ctrlOriginPoints[i - 1][1],
                    ctrlOriginPoints[i][0], ctrlOriginPoints[i][1]);
            }
            g2.setColor(Color.GREEN );
            bs = new BasicStroke (1.5f);
            g2.setStroke(bs);

        if( chkCurLine.getState())
            for (int i = 1; i < numPoints; i++) {
                g2.drawLine(ctrlpoints[i - 1][0], ctrlpoints[i - 1][1], ctrlpoints[i][0],
                    ctrlpoints[i][1]);
            }

        g2.setColor(Color.BLUE);
        //      if(chkCurve.getState())
        //        for (int i = 0; i < numOriginPoints - numSub1 + 1; i++)
        //          drawCurve(g2, i, numSub1);
        g2.setColor(Color.GRAY);
        for (int i = 0; i < numOriginPoints; i++) {
            g2.fillOval(ctrlOriginPoints[i][0] - Rad, ctrlOriginPoints[i][1] - Rad,
                2 * Rad, 2 * Rad);
        }
        g2.setColor(Color.ORANGE);
        for (int i = 0; i < numPoints; i++) {
            g2.fillOval(ctrlpoints[i][0] - Rad, ctrlpoints[i][1] - Rad, 2 * Rad,
                2 * Rad);
        }
        g2.setColor(Color.black);
        if(numOriginPoints>5)
        if(chkCurvature.getState())
            for(int i=1; i<numOriginPoints-1;i++){
                g2.drawString(String.valueOf(ctrlCurvature[i]).substring(0,8),ctrlOriginPoints[i][0],ctrlOriginPoints[i][1]);
            }

        g2.setColor(Color.BLUE);
        g2.fillOval(ctrlpoints[focus][0] - Rad, ctrlpoints[focus][1] - Rad, 3 * Rad,
                3 * Rad);
        
        g2.setColor(Color.MAGENTA);
        g2.fillOval(ctrlpoints[numPoints-1][0] - Rad, ctrlpoints[numPoints-1][1] - Rad, 3 * Rad,
                3 * Rad);
        

        g2.dispose();
        if (bimg != null)  {
        g.drawImage(bimg, 0, 0,Color.black, this);
        }
    }
    //create an one-dimensional array of values from
    //column-major of two-dimensional array (array of control points)
    //*buf: result
    //start: start index
    //num: number of elements
    //col: 1/2/3 (x/y/z)
    void createBuffer(int buf[], int start, int num, int col){
        int i;
        for(i=start;i<start+num;i++){
            buf[i-start]=ctrlOriginPoints[i][col];
        }
    }

    //calculate first - differential bezier equation
    // u : parameter of curve equation
    // v : array of control points
    // k : degree of bezier
    double getDiff1(double u, int v[],int k){
        double result=0;
        switch(k){
            case 1:
                result=1;
                break;
            case 2:
                result=-v[0]+v[1];
                break;
            case 3:
                result=-2*(1-u)*v[0]-2*u*v[1]+2*(1-u)*v[1]+2*u*v[2];
                break;
            case 4:
                result=-3*Math.pow(1-u,2)*v[0]-6*(1-u)*u*v[1]+3*Math.pow(1-u,2)*v[1]-3*Math.pow(u,2)*v[2]+6*(1-u)*u*v[2]+3*Math.pow(u,2)*v[3];
                break;
            case 5:
                result=-4*Math.pow(1-u,3)*v[0]-12*Math.pow(1-u,2)*u*v[1]+4*Math.pow(1-u,3)*v[1]
                    -12*(1-u)*Math.pow(u,2)*v[2]+12*Math.pow(1-u,2)*u*v[2]
                    -4*Math.pow(u,3)*v[3]+12*(1-u)*Math.pow(u,2)*v[3]+4*Math.pow(u,3)*v[4];
                break;
            case 6:
                result=0;
                break;
         }
         return result;
    }

    //calculate two - differential bezier equation
    // u : parameter of curve equation
    // v : array of control points
    // k : degree of bezier
    double getDiff2(double u, int v[],int k){
        double result=0;
        switch(k){
            case 1:
                result=1;
                break;
            case 2:
                result=0;
                break;
            case 3:
                result=2*v[0]-4*v[1]+2*v[2];
                break;
            case 4:
                result=6*(1-u)*v[0]+6*u*v[1]-6*(1-u)*v[1]-6*(1-u)*v[1]-6*u*v[2]-6*u*v[2]+6*(1-u)*v[2]+6*u*v[3];
                break;
            case 5:
                result=12*Math.pow(1-u,2)*v[0]+24*(1-u)*u*v[1]-24*Math.pow(1-u,2)*v[1]
                    +12*Math.pow(u,2)*v[2]-48*(1-u)*u*v[2]+12*Math.pow(1-u,2)*v[2]
                    -24*Math.pow(u,2)*v[3]+24*(1-u)*u*v[3]+12*Math.pow(u,2)*v[4];

                break;
            case 6:
                result=0;
                break;
        }
        return result;
    }

    //calculate the Curvature
    // u : 0 -> 1
    // x : x equation
    // y : y equation
    // c(u) = c(x) + c(y)
    // k : degree of bezier
    double getCurvature(double u, int x[], int y[], int k){
        double result=0;
        result=(getDiff1(u,x,k)*getDiff2(u,y,k) - getDiff2(u,x,k)*getDiff1(u,y,k))/
            Math.pow((Math.pow(getDiff1(u,x,k),2)+Math.pow(getDiff1(u,y,k),2)),1.5);
        if (result<0) result*=(-1);
            return result;
    }

    void generateCurvature1(int num){
        int i;
        float j;
        int bufx[]=new int[10];
        int bufy[]=new int[10];

        createBuffer(bufx,0,num,0);
        createBuffer(bufy,0,num,1);
        j=(float)1/(num-1);
        for(i=1;i<num-1;i++){
            ctrlCurvature[i]=getCurvature(j,bufx,bufy,num);
            strCurvature[i]=String.valueOf(ctrlCurvature[i]);
            j+=(float)1/(num-1);
        }
    }

    //calculate curvatures response to control points
    void generateCurvature(){
        int i,index;
        int numTmp=numOriginPoints;
        double j;
        int bufx[]=new int[10];
        int bufy[]=new int[10];

        double tmp;
        index=0;
        //get x, y of control points
        for(i=0;i<numTmp;i++){
            ctrlCurvature[i]=0;
            strCurvature[i]="";
        }
        if(numTmp>=5){
            for(i=0;i<=numTmp-numSub1;i++){
                createBuffer(bufx,i,numSub1,0);
                createBuffer(bufy,i,numSub1,1);
                //calulate curvature
                index=i+1;
                j=(double)1/(numSub1-1);
                while(j<1){
                    tmp=getCurvature(j,bufx,bufy,numSub1);
                    if(ctrlCurvature[index]!=0){
                        strCurvature[index]=strCurvature[index].concat("+");
                    }
                    String tmpStr=String.valueOf(tmp);
                    if(tmpStr.length()>8)
                        strCurvature[index]=strCurvature[index].concat(tmpStr.substring(0,8));
                    else
                        strCurvature[index]=strCurvature[index].concat(tmpStr);
                    ctrlCurvature[index]=ctrlCurvature[index]+tmp;
                    index++;
                    j+=(double)1/(numSub1-1);
                }
            }

            if(numTmp>5){
                ctrlCurvature[2]=(double)ctrlCurvature[2]/2;
                strCurvature[2]=strCurvature[2].concat(")/2=");
                strCurvature[2]=strCurvature[2].concat(String.valueOf(ctrlCurvature[2]).substring(0,8));

                ctrlCurvature[numTmp-3]=(double)ctrlCurvature[numTmp-3]/2;
                strCurvature[numTmp-3]=strCurvature[numTmp-3].concat(")/2=");
                strCurvature[numTmp-3]=strCurvature[numTmp-3].concat(String.valueOf(ctrlCurvature[numTmp-3]).substring(0,8));

                for(i=3;i<=numTmp-numSub1+1;i++){
                    ctrlCurvature[i]=(double)ctrlCurvature[i]/(numSub1-1);
                    strCurvature[i]=strCurvature[i].concat(")/3=");
                    strCurvature[i]=strCurvature[i].concat(String.valueOf(ctrlCurvature[i]).substring(0,8));
                }
            }
        }else{
            if(numTmp>1);
                generateCurvature1(numTmp);
        }
    }
    public void displayNum(){
        lblOri.setText(String.valueOf(numOriginPoints));
        lblCur.setText(String.valueOf(numPoints));
        lblRec.setText(String.valueOf(100*(numOriginPoints-numPoints)/numOriginPoints) + "%");
    }
    //remove one vertex i in array of control vertices
    void removeVertex(int i){
        int j;
        if (i > 0 && i < numPoints){
            for(j=i;j<numPoints-1;j++){
                ctrlpoints[j][0]=ctrlpoints[j+1][0];
                ctrlpoints[j][1]=ctrlpoints[j+1][1];
                ctrlpoints[j][2]=ctrlpoints[j+1][2];
            }
            numPoints--;
        }
    }
    //get Threshold to approximate - get min curvature
    double getThreshold(){
        double result = 0;
        int i;
        int numTmp = numOriginPoints;
        result = ctrlCurvature[1];
        for (i = 2; i < numTmp - 1; i++) {
            if (result >= ctrlCurvature[i]) {
                result = ctrlCurvature[i];
            }
        }
        return result;
    }
    double distance(double x1, double y1, double x2, double y2, double x0, double y0){
        double result=0;
        if((x2-x1+y2-y1)!=0)
            result= Math.abs((x2-x1)*(y1-y0)-(x1-x0)*(y2-y1))/(Math.sqrt(Math.pow((x2-x1),2) + Math.pow((y2-y1),2)));
        return result;
    }
    //approximate control vertices
    void removeVertices(){
        int i;
        i=numOriginPoints-2;
        double threshold=Double.valueOf(txtThreshold.getText()).doubleValue();
        double error=Double.valueOf(txtError.getText()).doubleValue();
        recoveryData();
        while(i>0){
            if(ctrlCurvature[i] <= threshold){
                if(!chkError.getState()){
                    removeVertex(i);
                }
                else{
                    if (distance(ctrlpoints[i - 1][0], ctrlpoints[i - 1][1],
                                 ctrlpoints[i + 1][0], ctrlpoints[i + 1][1],
                                 ctrlpoints[i][0], ctrlpoints[i][1]) <= error) {
                        removeVertex(i);
                    }
                }
            }
            i--;
        }
    }

    void this_mouseClicked(MouseEvent e) {
        int ev=e.getButton();
        switch(ev){
            case 1:
                if(cont==false){
                    resetData();
                    cont=true;
                }
                ctrlpoints[numPoints][0] = e.getX();
                ctrlOriginPoints[numPoints][0] = e.getX();
                ctrlpoints[numPoints][1] = e.getY();
                ctrlOriginPoints[numPoints][1] = e.getY();
                ctrlpoints[numPoints][2] = 0;
                ctrlOriginPoints[numPoints][2] = 0;

                ctrlCurvature[numPoints] = 0;

                numPoints++;
                numOriginPoints++;

                if (numPoints >= 5) generateCurvature();
                repaint();
                break;
            case 2:
                numPoints=0;
                numOriginPoints=0;
                break;
            case 3:
                cont=false;
                //         threshold=getThreshold();
                //         removeVertices();
                break;
        }
    }

    void sbThreshold_adjustmentValueChanged(AdjustmentEvent e) {
        txtThreshold.setText(String.valueOf((float)sbThreshold.getValue()/1000));
        removeVertices();
        cont=false;
        repaint();
    }

    void sbError_adjustmentValueChanged(AdjustmentEvent e) {
        txtError.setText(String.valueOf((float)sbError.getValue()/1000));
        removeVertices();
        cont=false;
        repaint();
    }

    public void showCurvature(){
        String tmp="(";
        if(focus<=1||focus>=numPoints-2 ) tmp="";
        if(focus==0||focus==numPoints-1)
            lblCurvature.setText(tmp.concat("0.000000"));
        else
            lblCurvature.setText(tmp.concat(strCurvature[focus]));
        repaint();

    }

    void btnApproximate_actionPerformed(ActionEvent e) {
        if (numOriginPoints<=0){
            return;
        }else{
            txtThreshold.setText(String.valueOf((float)sbThreshold.getValue()/1000));
            txtError.setText(String.valueOf((float)sbError.getValue()/1000));
            removeVertices();
            cont=false;
            repaint();
        }
    }

    void btnPrevious_actionPerformed(ActionEvent e) {
        if(focus>0)focus--;
        showCurvature();
    }

    void btnNew_actionPerformed(ActionEvent e) {
        resetData();
        repaint();
    }
  
    void chkOriLine_itemStateChanged(ItemEvent e) {
        repaint();
    }

    void chkCurLine_itemStateChanged(ItemEvent e) {
        repaint();
    }

    void chkCurve_itemStateChanged(ItemEvent e) {
        repaint();
    }
    void chkDP_itemStateChanged(ItemEvent e) {
        repaint();
    }

    void chkCurvature_itemStateChanged(ItemEvent e) {
        repaint();
    }
    void chkError_itemStateChanged(ItemEvent e) {
        removeVertices();
        repaint();
    }
}

class SimpliPoly_this_mouseAdapter extends java.awt.event.MouseAdapter {
    SimpliPoly adaptee;

    SimpliPoly_this_mouseAdapter(SimpliPoly adaptee) {
        this.adaptee = adaptee;
    }
    public void mouseClicked(MouseEvent e) {
        adaptee.this_mouseClicked(e);
    }
}

class SimpliPoly_sbThreshold_adjustmentAdapter implements java.awt.event.AdjustmentListener {
    SimpliPoly adaptee;

    SimpliPoly_sbThreshold_adjustmentAdapter(SimpliPoly adaptee) {
        this.adaptee = adaptee;
    }
    public void adjustmentValueChanged(AdjustmentEvent e) {
        adaptee.sbThreshold_adjustmentValueChanged(e);
    }
}

class SimpliPoly_sbError_adjustmentAdapter implements java.awt.event.AdjustmentListener {
    SimpliPoly adaptee;

    SimpliPoly_sbError_adjustmentAdapter(SimpliPoly adaptee) {
        this.adaptee = adaptee;
    }
    public void adjustmentValueChanged(AdjustmentEvent e) {
        adaptee.sbError_adjustmentValueChanged(e);
    }
}

class SimpliPoly_btnApproximate_actionAdapter implements java.awt.event.ActionListener {
    SimpliPoly adaptee;

    SimpliPoly_btnApproximate_actionAdapter(SimpliPoly adaptee) {
        this.adaptee = adaptee;
    }
    public void actionPerformed(ActionEvent e) {
        adaptee.btnApproximate_actionPerformed(e);
    }
}

class SimpliPoly_btnPrevious_actionAdapter implements java.awt.event.ActionListener {
    SimpliPoly adaptee;

    SimpliPoly_btnPrevious_actionAdapter(SimpliPoly adaptee) {
        this.adaptee = adaptee;
    }
    public void actionPerformed(ActionEvent e) {
        adaptee.btnPrevious_actionPerformed(e);
    }
}

class SimpliPoly_btnNew_actionAdapter implements java.awt.event.ActionListener {
    SimpliPoly adaptee;

    SimpliPoly_btnNew_actionAdapter(SimpliPoly adaptee) {
        this.adaptee = adaptee;
    }
    public void actionPerformed(ActionEvent e) {
        adaptee.btnNew_actionPerformed(e);
    }
}

class SimpliPoly_chkOriLine_itemAdapter implements java.awt.event.ItemListener {
    SimpliPoly adaptee;

    SimpliPoly_chkOriLine_itemAdapter(SimpliPoly adaptee) {
        this.adaptee = adaptee;
    }
    public void itemStateChanged(ItemEvent e) {
        adaptee.chkOriLine_itemStateChanged(e);
    }
}

class SimpliPoly_chkCurLine_itemAdapter implements java.awt.event.ItemListener {
    SimpliPoly adaptee;

    SimpliPoly_chkCurLine_itemAdapter(SimpliPoly adaptee) {
        this.adaptee = adaptee;
    }
    public void itemStateChanged(ItemEvent e) {
        adaptee.chkCurLine_itemStateChanged(e);
    }
}

class SimpliPoly_chkCurve_itemAdapter implements java.awt.event.ItemListener {
    SimpliPoly adaptee;

    SimpliPoly_chkCurve_itemAdapter(SimpliPoly adaptee) {
        this.adaptee = adaptee;
    }
    public void itemStateChanged(ItemEvent e) {
        adaptee.chkCurve_itemStateChanged(e);
    }
}

class SimpliPoly_chkDP_itemAdapter implements java.awt.event.ItemListener {
    SimpliPoly adaptee;

    SimpliPoly_chkDP_itemAdapter(SimpliPoly adaptee) {
        this.adaptee = adaptee;
    }
    public void itemStateChanged(ItemEvent e) {
        adaptee.chkDP_itemStateChanged(e);
    }
}


class SimpliPoly_chkCurvature_itemAdapter implements java.awt.event.ItemListener {
    SimpliPoly adaptee;

    SimpliPoly_chkCurvature_itemAdapter(SimpliPoly adaptee) {
        this.adaptee = adaptee;
    }
    public void itemStateChanged(ItemEvent e) {
        adaptee.chkCurvature_itemStateChanged(e);
    }
}
class SimpliPoly_chkError_itemAdapter implements java.awt.event.ItemListener {
    SimpliPoly adaptee;

    SimpliPoly_chkError_itemAdapter(SimpliPoly adaptee) {
        this.adaptee = adaptee;
    }
    public void itemStateChanged(ItemEvent e) {
        adaptee.chkError_itemStateChanged(e);
    }
}