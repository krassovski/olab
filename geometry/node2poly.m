function [x,y,isort] = node2poly(x,y,istart)
% Arrange a set of nodes into an ordered chain (polyline) based on
% pairwise closest distance criterion.

if ~exist('istart','var') || isempty(istart)
    istart = 1;
end

n = length(x);
isort = zeros(1,n);
isort(1) = istart;
unalloc = true(1,n); % mask for unallocated nodes
unalloc(istart) = false;
for k = 2:n
    d = ((x-x(istart)).^2 + (y-y(istart)).^2).^0.5;
    d(istart) = Inf;
    d(~unalloc) = Inf;
    istart = find(d==min(d), 1);
    isort(k) = istart;
    unalloc(istart) = false;
end

x = x(isort);
y = y(isort);