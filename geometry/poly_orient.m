function po = poly_orient(p,osign,multiface)
%POLY_ORIENT Orient polygon positively (CCW) or negatively (CW).
% Set or determine orientation (CW or CCW) for a single-faced
% multiply-connected polygon or multiple NaN-separated polygons.
% 
% SYNTAX
%   po = poly_orient(p,osign);
%   po = poly_orient(p); % orient positively by default
%   po = poly_orient(p,'report');
%   po = poly_orient(...,'multiface'); % replicate poly2ccw,poly2cw
% 
% INPUT
%   p       [N,2] polygon vertices;
%           can be multiply-connected, i.e. with holes, NaN-separated.
%           The polygon should be single-faced (only one outer polygon).
%   osign   -1, 1(default) or 'report' required orientation:
%           Positive means the interior is traversed counterclockwise.
%           Supply 'report' to determine the current orientation without
%           any changes.
%   multiface   if set to 'multiface', orient all segments same way,
%               without determining the outer segment.
%               This option allows multi-faced polygons
%               and should replicate behaviour of poly2ccw,poly2cw from
%               Matlab Mapping Toolbox. Default is false.
%
% OUTPUT
%   po      Polygon with required orientation 
%           or a vector of orientation indicators (-1 or 1, for each vertex)
% 
% SEE ALSO  poly2cw, poly2ccw

% Maxim Krassovski

olab_addpath('geometry/geom2d') % polygonArea

if nargin==2 && strcmp(osign,'multiface') % for the case: po = poly_orient(p,'multiface');
    multiface = osign;
    osign = [];
end

if ~exist('osign','var') || isempty(osign)
    osign = 1;
end

if exist('multiface','var') && strcmp(multiface,'multiface')
    multiface = true;
else
    multiface = false;
end

if ischar(osign) && strcmp(osign,'report')
    do_report = true;
    osign = 1;
else
    do_report = false;
end

if multiface
    b = [0; find(isnan(p(:,1)))]; % NaNs mark breaks between continuous loops
    lout = [];
    osign = -osign; % all segments are treated as outer loops
else
    [b,kout,lout] = poly_check(p);
end

if do_report
    po = ones(size(p,1),1); % initialize
else
    po = p;
end

% process the outer loop
if ~multiface
    if osign*poly_sign(p(kout,:))<0
        if do_report
            po([kout kout(end)+1]) = -1; % include the terminating NaN as well
        else
            po(kout,:) = flipud(p(kout,:));
        end
    end
end

% process the holes
for lk = setdiff(1:length(b)-1, lout)
    k = b(lk)+1 : b(lk+1)-1; % indices for the loop
    if ~isempty(k) && osign*poly_sign(p(k,:))>0 % empty k happens for empty segments (two consequitive NaNs)
        if do_report
            po([k k(end)+1]) = -1; % include the terminating NaN as well
        else
            po(k,:) = flipud(p(k,:));
        end
    end
end


function [b,kout,lout] = poly_check(p)
% Check if polygon is single-faced and get indices for the outer loop.
% 
% INPUT
%   p       [N,2] polygon vertices
% 
% OUTPUT
%   b       breaks between continuous segments
%   kout    indices of the vertices belonging to the outer loop
%   lout    index of the segment which is the outer loop

% continouous loop breaks
b = [0; find(isnan(p(:,1)))]; % NaNs mark breaks between continuous loops

% determine the outer loop and do a quick check on single-facedness
k1 = find(p(:,1)==min(p(:,1)),1);
k2 = find(p(:,1)==max(p(:,1)),1);
k3 = find(p(:,2)==min(p(:,2)),1);
k4 = find(p(:,2)==max(p(:,2)),1);

lout = find(b<k1, 1, 'last'); % outer loop index
lout2 = find(b<k2, 1, 'last'); % outer loop index
lout3 = find(b<k3, 1, 'last'); % outer loop index
lout4 = find(b<k4, 1, 'last'); % outer loop index

if lout ~= lout2 || lout ~= lout3 || lout ~= lout4
    error('Polygon is not single-faced. Supply each face separately.')
end

kout = b(lout)+1 : b(lout+1)-1; % indices for the loop

% robust check of single-facedness
pin = p(setdiff(1:size(p,1),kout),:); % island nodes
pin = pin(~isnan(pin(:,1)),:); % remove separators
in = inpolygon(pin(:,1),pin(:,2),p(kout,1),p(kout,2));
if ~all(in)
    error('Polygon is not single-faced. Supply each face separately.')
end


function s = poly_sign(p)
% Determine the sign of a singly-connected (no islands) polygon.
% Positive if the interior is traversed counterclockwise.

s = sign(polygonArea(p)); % is faster than sign(sum(line2line(...))); in geometry/geom2d
% s = sign(sum(line2line(p(1:end-2,:),p(2:end-1,:),p(2:end-1,:),p(3:end,:))));