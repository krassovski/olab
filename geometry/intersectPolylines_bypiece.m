function [apx,apos1,apos2] = intersectPolylines_bypiece(poly1,poly2,n)
% intersectPolylines done by n-point pieces of poly1 and poly2.

n1 = size(poly1,1);
n2 = size(poly2,1);

apx = [];
apos1 = [];
apos2 = [];

i1 = 1;
while true % loop over pieces in poly1
    disp(['----- Segment 1 start node:  ' num2str(i1) '   of ' num2str(n1)])
    i1e = min([i1+n n1]); % current end for poly1
    if n1-i1e == 1
        i1e = n1; % do not leave a single node to avoid 1-node polylines
    end
    c1 = poly1(i1:i1e,:);
    
    i2 = 1;
    while true % loop over pieces in poly2
        disp(['---------- Segment 2 start node:  ' num2str(i2) '   of ' num2str(n2)])
        i2e = min([i2+n n2]); % current end node for poly2
        if n2-i2e == 1
            i2e = n2; % do not leave a single node to avoid 1-node polylines
        end
        c2 = poly2(i2:i2e,:);
        
%         [px,pos1,pos2] = intersectPolylines(c1,c2);
        [px,pos1,pos2] = intersections(c1,c2); % faster
        apx = [apx; px];
        apos1 = [apos1; pos1+i1-1];
        apos2 = [apos2; pos2+i2-1];
        
        if i2e == n2; break; end % poly2 while-loop exit condition
        i2 = i2+n;
    end
    
    if i1e == n1; break; end % poly1 while-loop exit condition
    i1 = i1+n;
end