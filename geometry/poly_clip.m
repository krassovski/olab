function [xc,yc] = poly_clip(xc,yc,xp,yp)
% Clip polyline c [xc yc] by polygon p [xp yp], i.e. return part of c that is in p.
% Polygon p may be multiply-connected.
% No treatment of self-intersecting c or p.

olab_addpath('geometry') % intersections, prepnodes_poly
olab_addpath('model/mesh/Mesh2d v24') % inpoly

xc = xc(:);
yc = yc(:);
xp = xp(:);
yp = yp(:);

% intersection points
% [xi,yi,ii] = polyxpoly(xc(:),yc(:),xp,yp); % requires mapping toolbox
% ii = ii(:,1); % segment number in c
[xi,yi,ii] = intersections(xc(:),yc(:),xp,yp); % function from file exchange
ii = floor(ii);

% exclude intersections at nodes of c
[xy,iu] = setdiff([xi yi],[xc yc],'rows');
ii = ii(iu);

% insert intersection points in c
ii = ii + 0.5; % index new points in between their neighbours
ii = [(1:length(xc))'; ii]; % append new indices
xc = [xc; xy(:,1)]; % append new nodes
yc = [yc; xy(:,2)];
[~,isort] = sort(ii); % sort by index
xc = xc(isort);
yc = yc(isort);

% in-polygon test
[node,edge] = prepnodes_poly([xp yp]);
[in,on] = inpoly([xc yc],node,edge,1e-7);
% reltol is good to 1e-9 in some tests; default reltol of 1e-12 is too small
in = in | on;
xc(~in) = NaN; % replace outside nodes with NaNs
yc(~in) = NaN;

% remove repeating NaNs and the leading NaN if any
lnan = isnan(xc); % nodes to remove
lnan([0; diff(lnan)]==1) = false; % except first NaN in each continous series of NaNs
xc(lnan) = [];
yc(lnan) = [];