function [x,y,isub] = poly_simplify_opheim(x,y,dmin,dmax)
% Opheim polyline simplification.
% dmin  minimum edge length in the resulting polyline 
% dmax  (optional) maximum distance parameter
%
% For various simplification algorythms see
% http://www.codeproject.com/KB/recipes/PolylineSimplification.aspx

if ~exist('dmax','var') || isempty(dmax)
    dmax = Inf;
end

% ensure column vectors
x = x(:);
y = y(:);

n = length(x);
isub = false(size(x));
i = 1;
while true
%     disp([num2str(i) ' of ' num2str(n)])
    
    isub(i) = true;
    
    if i==n
        break
    end
    
    % ray start
    xr = x(i);
    yr = y(i);
    
    e = ((x(i+1:end)-xr).^2 + (y(i+1:end)-yr).^2).^0.5; % distance to the later nodes
    ir = i + find(e>dmin,1,'first'); % 1st node out of range d
    if isempty(ir) % i+[] yields []
        % condition for the last node does not hold,
        % replace the last found node with the last node of the polyline;
        % last node is always a part of the resampled polyline
        isub(i) = false; 
        isub(end) = true; 
        break
    end
    i = ir;
    
    [d, t] = ray_dist([xr yr], [x(i) y(i)], [x(i+1:end) y(i+1:end)]);
    ir = i + find(d>dmin | t>dmax, 1, 'first') - 1; % last node in the "sleave" and within the range dmax
    if ~isempty(ir)
        i = ir;
    end
end

x = x(isub);
y = y(isub);