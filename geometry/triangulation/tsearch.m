function k = tsearch(x,y,tri,xi,yi)
%TSEARCH Replacement for Matlab's native tsearch function.

if ~verLessThan('matlab','8.2') % MATLAB R2013a
    k = pointLocation(  triangulation(tri,x(:),y(:))  ,xi(:),yi(:)); % Introduced in R2013a
else
    %   Calls compiled mex file depending on the platform.
    if isunix
        % Call 
        % https://people.sc.fsu.edu/~jburkardt/m_src/tsearch/tsearch.html

        % This function is about as fast as MATLAB's tsearch, but does 
        % not have the restriction that the triangulation be a Delaunay 
        % triangulation.
        % 
        % Type "mex mytsearch" in Matlab to compile.
        % by David R. Martin, Boston College

        k = tsearch_mex(x,y,tri,xi,yi);
    else
    %     %TODO %%%% Compile tsearch_mex file for Windows.
    %     error('Compile tsearch_mex file for Windows.')

    %     olab_addpath('geometry/triangulation')
    %     k = tsearchsafe_mk(x, y, tri, xi, yi);

        % Solution from http://www.mathworks.com/matlabcentral/fileexchange/25555-mesh2d-automatic-mesh-generation
        % "Since Mathworks abandoned tsearch, simply calling tsearchn instead
        % works for me."
        k = tsearchn([x(:) y(:)],tri,[xi(:) yi(:)]); % for Delaunay only?
    end
end