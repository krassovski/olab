function z = tri_interp_3d(x,y,bx,by,bz,btri,extrap_dist,wrap)
% TRI_INTERP_3D Triangular interpolation for 3D field.
% Interpolate an 3D field bz at scattered nodes (bx,by) to nodes (x,y).
%
% Inputs:
%   x,y     nodes to interpolate to
%   bx,by   nodes to interpolate from
%   bz      [nb,nl,nt] values at nodes (bx,by)
%   btri    [nelem,3] (optional) triangulation of nodes (bx,by); 
%           if not supplied, (bx,by) are triangulated by delaunay
%   extrap_dist     if a non-zro value supplied, performs nearest neighbour
%                   extrapolation within this distance.
%   wrap    value to wrap around, e.g. to interpolate phases in degrees,
%           specify wrap=360; default - no wrap
% 
% Output:
%   z       [nx,nl,nt] interpolated values at (x,y)

% ensure column vectors
ncol = size(x,2);
if ncol ~= 1
    x = x(:);
    y = y(:);
end
bx = bx(:);
by = by(:);

if ~exist('wrap','var') || isempty(wrap)
    wrap = 0;
end

if ~exist('btri','var') || isempty(btri)
    btri = delaunay(bx,by);
end

% Introduced in R2013a
triobj = triangulation(btri,bx,by);
[k,barycentric] = pointLocation(triobj,x,y);
% barycentric are normalized weights for triangular interpolation

in = ~isnan(k);
btrikin = btri(k(in),:);
szbtrikin = size(btrikin);
sz = size(bz);
z = NaN([length(x), sz(2:3)]);

% triangular interpolation for points within triangles
vvv = NaN([szbtrikin, sz(2:3)]);
for kl = 1:sz(2)
    for kt = 1:sz(3)
        bz11 = bz(:,kl,kt);
        vv = bz11(btrikin); % values at vertices
        if wrap~=0 % need to wrap
            for k = 2:3
                d = vv(:,k)-vv(:,k-1);
                iwrap = abs(d) > wrap/2;
                vv(iwrap,k) = vv(iwrap,k) - sign(d(iwrap))*wrap;
            end
        end
        vvv(:,:,kl,kt) = vv;
    end
end
if verLessThan('matlab','9.1')
    vvvi = vvv.*repmat(barycentric(in,:),[1 1 sz(2:3)]);
else
    vvvi = vvv.*barycentric(in,:);
end
z(in,:,:) = squeeze(sum(vvvi, 2)); % this replaces Mesh2d_v24/tinterp

% extrapolate within specified distance threshold
if exist('extrap_dist','var') && ~isempty(extrap_dist) && ~isnan(extrap_dist) && extrap_dist~=0
    extrap = find(~in);
    [kn,d] = nearestNeighbor(triobj,x(extrap),y(extrap));
    ival = d<=extrap_dist;
    z(extrap(ival),:,:) = bz(kn(ival),:,:);
end