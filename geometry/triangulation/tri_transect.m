% function [pti,nid,w] = tri_transect(pt,p,tri,xc,yc,nbe)
function [pti,nid,w,ptie,eid,we,theta] = tri_transect(pt,p,tri,xc,yc,nbe)
% Transect of a triangular grid.
% Discretize transect edges with points of intersection with grid eges,
% i.e. find points of intersection of the transect polyline pt with grid
% edges and add them to pt.
%
% INPUTS:
%   pt    [nt,2] transect vertices, [x y]
%   p     [n,2] grid nodes, [x y]
%   tri   [k,3] triangulation array
%   xc,yc [k,1] grid element centroids
%   nbe   [k,3] elements surrounding each element (indices into rows of tri); 
%         can be obtained from fvcom output variable 'nbe';
%         contains 1 zero value for each triangle with a boundary edge
%
% OUTPUTS:
%   pti   [m,2] points of intersection, [x y]
%   nid   [m,3] grid node indices to use for further linear interpolation
%         (contains intersecting edges for pti in the 1st two columns)
%   w     [m,3] weights for linear interpolation
%         (intersection location on grid edge in the 2nd column)
%   ptie  [n,2] points of intersection for element-centered quantities,
%         same as pti but with duplicates at discontinuity points (at grid
%         edges and grid nodes)
%   eid   [n,4] grid element indices for interpolation of an element-centered quantity
%   we    [n,4] linear interpolant (weights) for an element-centered quantity

olab_addpath('geometry\geom2d') % intersectEdges

% [ebnd,eint] = grid_edge(tri);
[ebnd,eint,tib,tie] = tri_edgeb(tri);
e = [ebnd; eint]; % all grid edges
ti = [tib tib; tie]; % and their corresponding triangle indices (two-column array)
pti = []; % to collect points of intersection of the transect with the grid egdes
ei = [];  % to collect edges intersected by the transect
loc = []; % to collect location of intersection points on each edge 
          % (distance from 1st node relative to edge length)
tiic = []; % to collect triangle indices for the intersected grid edges
theta = []; % to collect transect orientation angle

pe = [p(e(:,1),:) p(e(:,2),:)]; % grid edge coords [Nx4]
for k = 1:size(pt,1)-1 % for each transect edge
    [pi,t1,t2] = intersectEdges([pt(k,:) pt(k+1,:)], pe); % geom2d
    
    % record transect orientation for subsequent calculation of normal
    % and tangential components of velocity as vn = v*cos(theta) - u*sin(theta);
    thetak = atan2(pt(k+1,2)-pt(k,2), pt(k+1,1)-pt(k,1));
    
    % extract intersection points for edges non-overlapping with the transect
    ii = ~isnan(pi(:,1)); %%% TODO: Use this line when colinear edges are dealt with properly
%     ii = ~isnan(pi(:,1)) & ~isinf(pi(:,1)); %%% TODO: Deal with colinear edges properly
    pix = pi(ii,:); % points to add to the transect
    t1x = t1(ii);   % position on the transect edge
    t2x = t2(ii);   % position on the grid edge
    tiix = ti(ii,:); % triangle indices for each intersected grid edge
    iix = find(ii); % intersected edge indices in the grid edge array e

    % Extract points for edges overlapping with the transect.
    % pi contains Inf for colinear overlapping edges;
    % for overlapping edges, t1,t2 are positions of edge2 vertices on edge1
    jj = isinf(pi(:,1));

    je = jj & t1>0 & t1<1; % node1 of grid edge is within the extent of the transect segment
    pix = [pix; pe(je,1:2)]; % node1
    t1x = [t1x; t1(je)];
    t2x = [t2x; zeros(sum(je),1)]; % start of the edge, pos=0
    tiix = [tiix; ti(je,:)];
    iix = [iix; find(je)];
    
    je = jj & t2>0 & t2<1; % node2 of grid edge is within the extent of the transect segment
    pix = [pix; pe(je,3:4)]; % node2
    t1x = [t1x; t1(je)];
    t2x = [t2x; ones(sum(je),1)]; % end of the edge, pos=1
    tiix = [tiix; ti(je,:)];
    iix = [iix; find(je)];
    
    je = jj & (t1<0 | t2<0); % node1 of the transect segment is within the grid edge
    if any(je)
        pix = [pix; pt(k,:)];
        t1x = [t1x; 0]; % start of transect edge, pos=0
        t2x = [t2x; edgePosition(pt(k,:), pe(je,:))]; % position on the grid edge
        tiix = [tiix; ti(je,:)];
        iix = [iix; find(je)];
        addnode1 = false; % no need to add Node 1 again at the end of the cycle
    else
        addnode1 = true; % need to add node 1 at the end of the cycle
    end
    
    addnode2 = false;
%     % add 2nd node only on the last iteration of the loop
%     if k == size(pt,1)-1 % if it's the last loop iteration
        % Add 2nd node for every segment: We need it duplicated since there
        % will be two different values for the normal velocity at the
        % junction point between two transect segments.
        je = jj & (t1>1 | t2>1); % node2 of the transect segment is within the grid edge
        if any(je)
            pix = [pix; pt(k+1,:)];
            t1x = [t1x; 1]; % end of transect edge, pos=1
            t2x = [t2x; edgePosition(pt(k+1,:), pe(je,:))]; % position on the grid edge
            tiix = [tiix; ti(je,:)];
            iix = [iix; find(je)];
        else
            addnode2 = true;
        end
%     end
    
    
    % order in the same direction as transect vertices
    [~,isort] = sort(t1x);
    pix = pix(isort,:);
    t2x = t2x(isort);
    tiix = tiix(isort,:);
    iix = iix(isort);
    
%     [pi,ikeep] = setdiff(pi,pt,'rows','stable'); % exclude any transect vertices
%     t2 = t2(ikeep);
%     tii = tii(ikeep,:);
%     ii = ii(ikeep);
    
    % order in the same direction as transect vertices
%     % after setdiff, point is sorted in ascending order by x then by y
%     % if xt(k) < xt(k+1) -- point is already sorted as needed
%     if pt(k,1) > pt(k+1,1)
%         pi = flipud(pi);
%     elseif pt(k,1) == pt(k+1,1) % transect edge along x, need to sort by y
%         if pt(k,2) < pt(k+1,2)
%             [~,isort] = sort(pi(:,2)); % ascend
%             pi = pi(isort,:);
%         elseif pt(k,2) > pt(k+1,2)
%             [~,isort] = sort(pi(:,2),'descend');
%             pi = pi(isort,:);
%         % yt(k) == yt(k+1) -- duplicated consecutive nodes in the transect
%         end
%     end
    
    if addnode1 % if 1st node of this segment hasn't been added as colinear
        pti = [pti; pt(k,:)];
        % transect vertices not on grid edges receive NaNs for edges and location;
        % for interpolation, they have to be dealt with by tinterp
        ei = [ei; 1 1]; % fill-in any valid node index, i.e. 1
        loc = [loc; NaN];
        k1 = tsearch(p(:,1),p(:,2),tri,pt(k,1),pt(k,2)); %%#ok<DGEO4>
        tiic = [tiic; k1 k1];
        theta = [theta; thetak];
    end
    pti = [pti; pix];
    ei = [ei; e(iix,:)];
    loc = [loc; t2x];
    tiic = [tiic; tiix];
    theta = [theta; repmat(thetak,size(pix,1),1)];
    % For a transect going through a grid node (t2==0 or t2==1), tiic
    % contains indices for all triangles sharing this node.
    if addnode2 % if 2nd node hasn't been added as colinear
        pti = [pti; pt(k+1,:)];
        % transect vertices not on grid edges receive NaNs for edges and location;
        % for interpolation, they have to be dealt with by tinterp
        ei = [ei; 1 1]; % fill-in any valid node index, i.e. 1
        loc = [loc; NaN];
        k2 = tsearch(p(:,1),p(:,2),tri,pt(k+1,1),pt(k+1,2)); %%#ok<DGEO4>
        tiic = [tiic; k2 k2];
        theta = [theta; thetak];
    end
end

% form output as 3-column node indices and weights
nj = length(loc); % number of nodes in the transect
nid = [ei ones(nj,1)]; % add 3rd column: any valid node index, i.e. 1
% w = [1-loc loc zeros(nj,1)]; % expand to 3-column array of weights: 3rd column - zero weights
col3 = zeros(nj,1);
col3(isnan(loc)) = NaN;
w = [1-loc loc col3]; % expand to 3-column array of weights: 3rd column - zero weights

% transect vertices not on grid edges receive NaNs for edges and location;
% for interpolation, they have to be dealt with by tinterp
kt = isnan(loc); % transect points not on grid edges
xt = pti(kt,1);
yt = pti(kt,2);
k = tsearch(p(:,1),p(:,2),tri,xt,yt); %%#ok<DGEO4> replacement in Tools\Geometry\Triangulation
in = ~isnan(k);
kt = find(kt);
[nid(kt(in),:),w(kt(in),:)] = tinterpolant(p,tri,[xt(in) yt(in)],k(in));


% linear interpolant for an element-centered quantity
% k = tiic(:); % contains NaNs for transect vertices outside the grid
k = tiic'; % contains NaNs for transect vertices outside the grid
k = k(:); % keep transect points sequential
xi = [pti(:,1) pti(:,1)]';
xi = xi(:);
yi = [pti(:,2) pti(:,2)]';
yi = yi(:);
theta = [theta theta]';
theta = theta(:);
if ~exist('xc','var') % assuming either both xc and yc are supplied or both not
    x = p(:,1);
    y = p(:,2);
    xc = mean(x(tri),2); % coordinates of centroids
    yc = mean(y(tri),2);
end
if ~exist('nbe','var') || isempty(nbe)
    nbe = tri_nbe(tri); % get bounding elements for each element in k
else
    nbe = double(nbe); % make sure fvcom output is converted to double
end
[eid,we] = tinterpolantc(xc,yc,xi,yi,k,nbe);
ptie = [xi yi];