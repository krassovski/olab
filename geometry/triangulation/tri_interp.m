function z = tri_interp(x,y,bx,by,bz,btri,extrap_dist,wrap)
% TRI_INTERP Triangular interpolation.
% Interpolate a field bz at scattered nodes (bx,by) to nodes (x,y).
%
% Inputs:
%   x,y     nodes to interpolate to
%   bx,by   nodes to interpolate from
%   bz      values at nodes (bx,by)
%   btri    [nelem,3] (optional) triangulation of nodes (bx,by); 
%           if not supplied, (bx,by) are triangulated by delaunay
%   extrap_dist     if a non-zro value supplied, performs nearest neighbour
%                   extrapolation within this distance.
%   wrap    value to wrap around, e.g. to interpolate phases in degrees,
%           specify wrap=360; default - no wrap
% 
% Output:
%   z       interpolated values at (x,y)

% ensure column vectors
ncol = size(x,2);
if ncol ~= 1
    x = x(:);
    y = y(:);
end
bx = bx(:);
by = by(:);
bz = bz(:);

if ~exist('wrap','var') || isempty(wrap)
    wrap = 0;
end

if ~exist('btri','var') || isempty(btri)
    btri = delaunay(bx,by);
end

% Introduced in R2013a
triobj = triangulation(double(btri),bx,by);
ival = ~isnan(x) & ~isnan(y);
n = length(x);
k = NaN(n,1);
barycentric = NaN(n,3);
[k(ival),barycentric(ival,:)] = pointLocation(triobj,x(ival),y(ival));
% barycentric are normalized weights for triangular interpolation

in = ~isnan(k);
z = NaN(size(x));

% triangular interpolation for points within triangles
vv = bz(btri(k(in),:)); % values at vertices
if wrap~=0 % need to wrap
    for k = 2:3
        d = vv(:,k)-vv(:,k-1);
        iwrap = abs(d) > wrap/2;
        vv(iwrap,k) = vv(iwrap,k) - sign(d(iwrap))*wrap;
    end
end
z(in) = sum(vv.*barycentric(in,:), 2); % this replaces Mesh2d_v24/tinterp

% extrapolate within specified distance threshold
if exist('extrap_dist','var') && ~isempty(extrap_dist) && ~isnan(extrap_dist) && extrap_dist~=0
    extrap = find(~in);
    [kn,d] = nearestNeighbor(triobj,x(extrap),y(extrap));
    ival = d<=extrap_dist;
    z(extrap(ival)) = bz(kn(ival));
end

% make output same shape as input x array
if ncol ~= 1
    z = z';
end