function [nid,w] = tinterpolant(p,t,pi,k)
% Interpolant for linear interpolation within a triangular grid.
% 
% INPUTS:
%   p  : Nx2 array of nodal XY coordinates, [x1,y1; x2,y2; etc]
%   t  : Mx3 array of triangles as indices, [n11,n12,n13; n21,n22,n23; etc]
%   pi : Jx2 matrix of interpolation points
%   k  : k = tsearch(p(:,1),p(:,2),t,pi(:,1),pi(:,2));
% 
% OUTPUTS:
%   nid   [Jx3] grid node indices to use for interpolation
%   w     [Jx3] weights for linear interpolation
%
%

% Code from tinterp by Darren Engwirda - 2005-2007
%  TINTERP: Triangle based linear interpolation.
%  fi = tinterp(p,t,f,pi,k);

%mk
% Does NOT perform nearest-neighbour extrapolation for points outside the
% triangulation.
%
% Linear interpolation of a scalar field(s) f:
% fi = (A1.*f(t1)+A2.*f(t2)+A3.*f(t3))./(A1+A2+A3);
% fi = w(:,1)*ones(1,nf).*f(nid(:,1),:) + ...
%      w(:,2)*ones(1,nf).*f(nid(:,2),:) + ...
%      w(:,3)*ones(1,nf).*f(nid(:,3),:);

% Corner nodes
t1 = t(k,1);
t2 = t(k,2);
t3 = t(k,3);

% Calculate areas
dp1 = pi-p(t1,:);
dp2 = pi-p(t2,:);
dp3 = pi-p(t3,:);
A3 = abs(dp1(:,1).*dp2(:,2)-dp1(:,2).*dp2(:,1));
A2 = abs(dp1(:,1).*dp3(:,2)-dp1(:,2).*dp3(:,1));
A1 = abs(dp3(:,1).*dp2(:,2)-dp3(:,2).*dp2(:,1));

% weights for interpolation
sa = (A1 + A2 + A3);
w = [A1./sa A2./sa A3./sa];

% grid node indices for interpolation
nid = [t1 t2 t3];