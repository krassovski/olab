function c = tri2con(tri,x,y)
%TRI2CON build a FEM neighbor list from element and node lists
% TRI2CON computes the neighbor list for a FEM mesh specified
% by the triangular element lists.  Each node's neighbor list
% is then sorted into counter-clockwise order.
%
% Used to output .ngh files (TRIGRID format).
%
% INPUT:   tri - [ne 3] triangulation array
%          x,y - (optional) node coordinates; if supplied, the neighbours
%          are ordered such that they go in CCW order around each node
% OUTPUT:  c - neighbor list
%
% CALL:    c = ele2nei(tri,x,y);
%          c = ele2nei(tri);

% Maxim Krassovski  11 Aug 2013
% 
% Based on ele2nei by Brian O. Blanton (Spring 2001) but with connectivity
% matrix constructed in a different way and with vectorized code.

if nargin<1 || nargin>3
   error('Call signatures: con = tri2con(tri,x,y) or con = tri2con(tri)');
end

tri = double(tri); % for sparse to work

[ebnd,eint] = grid_edge(tri); % unique edges

% Construct edge start- and end-point arrays which will serve as row
% and column indices, respectively, in the connectivity matrix.
i = [ebnd(:,1); eint(:,1)]; % "start" points for each edge
j = [ebnd(:,2); eint(:,2)]; % "end" points for each edge

% highest node index in tri
% it is the same as number of nodes if all nodes are referenced in tri
n = max(max(i),max(j));

% Connectivity matrix with connected node indices in each row.
% Each entry occurs only once since we've picked only unique edges.
% The connectivity matrix is composed from the sum of two matrices: 
% one contains neighbours which are end points in the egde the other
% contains neighbours which are start points.
c = sparse(i,j,j,n,n) + sparse(j,i,i,n,n); % neigbours are in rows
c = sort(c,2,'descend'); % move all non-zero entries to the left
c = full(c(:,any(c))); % remove all-zero columns and convert from sparse to full

% sort neighbours so they go in CCW order around each node
if exist('x','var') && ~isempty(x) && exist('y','var') && ~isempty(y)
    [np,nn] = size(c); % number of nodes (rows) and max number of neighbours (columns)
    xn = [x; NaN]; % add a fill-in value for zeros in c
    yn = [y; NaN];
    cn = c; % make a copy of c to replace zeros with NaNs
    cn(cn==0) = np+1; % make zero-elements point to NaNs in x,y arrays
    dx = xn(cn) - repmat(x,1,nn);
    dy = yn(cn) - repmat(y,1,nn);
    theta = atan2(dy,dx); % angle CCW from positive x direction
    %%% without wrapping the neighbours are arranged CCW from negative x direction
    % ineg = find(theta<0);
    % theta(ineg) = theta(ineg) + 2*pi; % wrap negative angles
    [~,icol] = sort(theta,2);  % sort within each row; icol is a column index into c
    irow = (1:np)'*ones(1,nn); % row indices into c
    c = c(sub2ind([np nn], irow, icol)); % sort each row in c according to theta; use linear indexing
end


% % Brian Blanton's code with my edits (%mk) to fix a bug. Some comments are added as well.
% % The for-loop is not vectorized and, therefore, is very slow.
%
% disp('Forming sparse connectivity matrix...')
% i=[e(:,1);e(:,2);e(:,3)]; %mk start points for each edge of all triangles
% j=[e(:,2);e(:,3);e(:,1)]; %mk end points for each edge of all triangles
% n = max(max(i),max(j)); %mk highest node index in e, same as number of nodes if all nodes are referenced in e
% 
% A = sparse(i,j,1,n,n); %mk sparse connectivity matrix; contains 0, 1, and 2; 2 for edges belonging to two triangles
% A = A+A'; %mk
% 
% [ia,ja] = find(A);
% [~,ii,jj] = unique(ja);
% 
% nnei=max(diff(ii));
% nei=zeros(n,nnei);
% % nei=zeros(n,5); % more columns will be added to nei if necessary
% % [g,h]=count(jj);
% 
% 
% disp('Sorting on interior angle...')
% for i=1:n
%    % get neighbors for Node i
% %    ntemp=find(A(i,:)); %mk %%%% wrong! We have to check both i-th row and
% %    i-th column!!! One way to fix this is to do A = A+A' above
% %    ntemp = unique([find(A(i,:))'; find(A(:,i))]); %mk % neighbours of the i-th node
%    ntemp = unique([ja(ia==i); ia(ja==i)]); %mk same
%    
%    % Sort local neighbors based on 
%    % angle CCW from 0-deg east.
%    dx=x(ntemp)-x(i);
%    dy=y(ntemp)-y(i);
%    theta=atan2(dy,dx);
%    ineg=find(theta<0);
%    theta(ineg)=theta(ineg)+2*pi;
%    [~,j]=sort(theta);
%    
%    %mk fill-in the neighbours for i-th node
%    try
% %    nei(i,1:h(i))=ntemp(j);
%    nei(i,1:length(ntemp)) = ntemp(j); %mk this will add more columns to nei if necessary
%    catch
%        ''
%    end
% end


%
%LabSig  Brian O. Blanton
%        Department of Marine Sciences
%        12-7 Venable Hall
%        CB# 3300
%        University of North Carolina
%        Chapel Hill, NC
%                 27599-3300
%
%        brian_blanton@unc.edu
%        March 1996
%
