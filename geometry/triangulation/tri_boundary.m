function [loop,errn,wrn] = tri_boundary(tri,n1)
% Extract boundary from a triangulation array tri (M-by-3 array).
% The output is a row vector of node indices in the form of a NaN-separated
% segments.
% n1    [optional] node to start from. Default: 1st node of the 1st edge.
%
% Get a multiply-connected (NaN-separated) polygon (N-by-2 array of
% x,y-coordinates) as 
% poly = NaN(length(c),2); poly(~isnan(c),:) = p(c(~isnan(c)),:);

if ~exist('n1','var')
    n1 = [];
elseif islogical(n1)
    n1 = find(n1,1,'first');
end

% ebnd = grid_edge(double(tri)); % boundary edges only
ebnd = tri_edgeb(double(tri)); % boundary edges only

loop = [];
while ~isempty(ebnd)
%     % find a non-branching node to avoid ambiguity
%     nstart = unique(ebnd);
%     is = 1;
%     while length(find(any(ebnd==nstart(is),2)))>2 % branching nodes belong to more than two edges
%         is = is+1;
%     end
%     nstart = nstart(is);

    [c,errn,wrn] = edge_extract_loop(ebnd,n1);
    if errn
        return
    end
%     [loopcell,hangcell,wrn] = extract_path(ebnd,ebnd(1,:),[]);
%     if ~isempty(hangcell)
%         wrn = ' Some unclosed paths found in the boundary. Check the grid.';
%     end
    
    loop = [loop c NaN];
    
    % remove edges with all nodes accounted for
    ebnd(any(ismember(ebnd,c),2), :) = [];
end


% function [c,errn,wrn] = extract_loop(e)
% % Extract a path connecting edges e starting from the end node in c.
% % e     N-by-2 array with each row containing a pair of node indices; e is not
% %       arranged consequitively.
% % Output is an array of consequitive node indices.
% 
% % ensure nodes are distinct for each edge
% idup = (e(:,1)-e(:,2)) == 0;
% if any(idup)
%     e(idup,:) = [];
%     wrn = 'Zero-length edges have been eliminated.';
% else
%     wrn = false;
% end
% 
% errn = false;
% 
% c = e(1,:);
% n1 = c(1); % marker for the start node of the current edge
% n2 = c(2); % marker for the start node of the current edge
% enext = e(any(e==n2,2) & all(e~=n1,2), :);
% n1 = n2;
% while true
%     if size(enext,1) == 1 % continuing a path without branching
%         n2 = enext(enext~=n1); % marker for the end node of the current edge
%         c(end+1) = n2;
%         
%         if n2==c(1) % back at the start node, return
%             return
%         end
% 
%         enext = e(any(e==n2,2) & all(e~=n1,2), :); % find next edge (do not go back)
%         n1 = n2; % move the start node marker
%     else
%         errn = n1;
%         return
%     end
% end



function [loopcell,stopcell,hangcell,wrn] = extract_path(e,nstart,nnot,nstop)
%%%%%%%%%% Needs debugging -- doesn't work correctly for branching boundaries.
% Recursively extract all branches starting from node nstart from edges array e,
% stop if edge nstop is encountered on the way.
% e     N-by-2 array with each row containing a pair of node indices; e is not
%       arranged consequitively.
% nstart    node to start from
% nnot      1st edge to go along should not contain nnot
% Output is two cell arrays of consequitive node indices, ccell for each loop
% and bcell for each branch.

% ensure nodes are distinct for each edge
idup = (e(:,1)-e(:,2)) == 0;
if any(idup)
    e(idup,:) = [];
    wrn = 'Zero-length edges have been eliminated.';
else
    wrn = false;
end

loopcell = {};
stopcell = {};
hangcell = {};

enext = e(any(e==nstart,2) & all(e~=nnot,2), :);
n1 = nstart; % marker for the start node of the current edge
c = n1; % array for current path
ie = 2; % edge counter for the current path
while true
    if size(enext,1) == 1 % continuing a path without branching
        n2 = enext(enext~=n1); % marker for the end node of the current edge
        c(ie,1) = n2; % make sure it's a column vector
        if any(nstop==n2) % any stop point is reached, return
            stopcell = {c};
            return
        end
        if n2==nstart % back at the start node, return
            loopcell = {c}; %%%%% this should never happen
            return
        end
        enext = e(any(e==n2,2) & all(e~=n1,2), :); % find next edge (do not go back)
        n1 = n2; % move the start node marker
        ie = ie+1;
    elseif isempty(enext) % end of a hanging path
        hangcell = {c};
        return
    else % the path branches
        start_is_not_closed = true;
        while ~isempty(enext) % do one branch at a time
            ethis = enext(1,:);
            enext(1,:) = [];
            n2 = ethis(ethis~=n1);
            
            [loop,stop,hang,wrn] = extract_path(e, n2, n1, [nstop n1]);
            
            if ~isempty(hang)
                if length(hang)==2 % connect two hanging branches at n1
                    % this case should bever happen when extracting boundary
                    % from a triangular grid
                    hang = {[flipud(hang{1}); n1; hang{2}]};
                    hangcell = [hangcell; hang];
                else
                    for is = 1:length(hang)
                        hangcell = [hangcell; {[n1; hang{is}]}];
                    end
                end
            end
            
            for is = 1:length(stop)
                if stop{is}(end) == nstart
                    loopcell = [loopcell; {[c; stop{is}]}]; % close the loop which came to nstart
                    start_is_not_closed = false;
                    enext(any(enext==stop{is}(end-1), 2), :) = []; % to avoid duplicating loops
                elseif stop{is}(end) == n1
                    loopcell = [loopcell; {[stop{is}; n2]}]; % close loops which started at n1
                    enext(any(enext==stop{is}(end-1), 2), :) = []; % to avoid duplicating loops
                else
                    stopcell = [stopcell; stop(is)];
                end
            end
            
            if ~isempty(loop) % append all loops
                loopcell = [loopcell; loop];
            end
        end
        if start_is_not_closed
            hangcell = [{c}; hangcell];
        end
        return
    end
end