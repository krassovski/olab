function fi = grid_interp_eval(nid,w,f)
% Interpolate a node-based quantity f with interpolant nid,w.
% 
% INPUTS:
%   nid   [nt,ni] indices into grid nodes array
%   w     [nt,ni] corresponding weights: all(sum(w,2))==1, where
%           nt -- number of transect points
%           ni -- number of grid nodes used for interpolation of each point
%   f     [nnodes,nfields] scalar field(s) defined on the grid nodes, one column per field.
% 
% OUTPUT:
%   fi    f interpolated to the transect points


% e.g. obtain interpolant for a transect of a triangular grid:
% [pti,nid,w] = tri_transect(double([xt yt]),double([x y]),double(tri));
% Interpolate a salinity field to the transect nodes xt,yt:
% sali = grid_interp_eval(nid,w,sal);

% Maxim Krassovski 2016-02-22

nf = size(f,2); % number of scalar fields (columns of f)

[nt,ni] = size(w);

fi = zeros(nt,nf); % initialize
% sum over each node used in interpolation
for k = 1:ni
    % points outside the grid get zeros
    % (needed for velocity, where boundary elements have one NaN)
    ival = ~isnan(w(:,k));
    fi(ival,:) = fi(ival,:) + w(ival,k)*ones(1,nf).*f(nid(ival,k),:);

%     % points outside the grid get NaNs
%     fi = fi + w(:,k)*ones(1,nf).*f(nid(:,k),:);
end

% points outside the grid get NaNs
inv = all(isnan(w),2); % outside the grid
fi(inv,:) = NaN;