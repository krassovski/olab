function [ebnd,eint,tib,tie] = tri_edgeb(t)
% Get the boundary and internal edges in a triangulation t.
% Based on getedges in meshpoly by Darren Engwirda.
% tib   indices of triangles in t corresponding to ebnd;
%       one-column array (one triangle per edge).
% tie   indices of triangles corresponding to eint; 
%       two-column array (two triangles per edge).
% %% mkcon also gives eINt edge indices in each triangle

% % use this to get all unique edges with each consequitive 3 edges being part of
% % same triangle:
% e = sort([t(:,[1,2]); t(:,[1,3]); t(:,[2,3])],2); % each edge is a row, sorted in both dimensions
% % e = sort(reshape(t(:,[1 2 2 3 3 1])',2,[])', 2); %%%%%mk: same, what is faster?
% eu = unique(e); % all unique edges; each consequitive 3 edges are part of same triangle
% e = sortrows(e);

% make each edge a row; array is sorted in both dimensions:
[e,esort] = sortrows( sort([t(:,[1,2]); t(:,[1,3]); t(:,[2,3])],2) );
% e = sortrows( sort(reshape(t(:,[1 2 2 3 3 1])',2,[])', 2) ); %%%%%mk: same, what is faster?

idx = all(diff(e,1)==0,2);     % Find shared edges
idx = [idx;false]|[false;idx]; % True for all shared edges
ebnd = e(~idx,:);              % Boundary edges
eint = e(idx,:);               % Internal edges
eint = eint(1:2:end-1,:);      % unique internal edges

% triange indices
ti = (1:size(t,1))';
ti = [ti; ti; ti];
ti = ti(esort);
tib = ti(~idx); % indices of triangles corresponding to ebnd
tie = ti(idx);  % indices of triangles corresponding to eint
tie = reshape(tie,2,[])'; % -> two-column array (two triangles per edge)