function [eid_out,w_out] = tinterpolantc(xc,yc,xi,yi,k,nbe)
% Linear interpolant for an element-centered quantity within a triangular grid.
% For FVCOM-like interpolation of an element-centered quantity to points xi,yi
% within a triangular grid.
%
% Used in tri_transect
% 
% INPUTS:
%   xc,yc coordinates of the element (triangle) centroids
%   xi,yi [ni] points to interpolate to
%   k     [ni] indices of central triangles 
%         no values for points outside of the grid bounds
%         N.B. Velocity field in FVCOM has discontinuities at element edges,
%         therefore, k has
%         1 value for points inside any triangle,
%         2 values for points on a triangle edge,
%         up to maxneigh values for points on a triangle vertex.
%         If all possible values at discontinuities are required, several
%         entries must be supplied for each xi,yi point, each with
%         corresponding k.
%         N.B. tsearch gives only one triangle for points on the edge or vertex
%         k = tsearch(x,y,tri,xi,yi); % where tri [n,3] triangles as indices into x,y
%   nbe   [n,3] indices into tri of surrounding triangles; 
%         can be obtained from fvcom output as the variable 'nbe';
%         contains 1 zero for each triangle with boundary edge
% 
% OUTPUTS:
%   eid   [ni,4] grid element indices to use for interpolation
%   w     [ni,4] weights for linear interpolation

% M.Krassovski, I.Fain 2016-03-21

% Does NOT perform nearest-neighbour extrapolation for points outside the
% triangulation.
%
% Linear interpolation of a scalar field f:
% fi = w(:,1)*ones(1,nf).*f(nid(:,1),:) + ...
%      w(:,2)*ones(1,nf).*f(nid(:,2),:) + ...
%      w(:,3)*ones(1,nf).*f(nid(:,3),:) + ...
%      w(:,4)*ones(1,nf).*f(nid(:,4),:);

ni = length(xi);
eid_out = NaN(ni,4);
w_out   = NaN(ni,4);
ival = ~isnan(k);
k = k(ival);
xi = xi(ival);
yi = yi(ival);

be = nbe(k,:); % indices of bounding triangles

%%%TODO: Halo elements for interpolation to the boundary?
%%% Need to consider different slip conditions for solid and open boundary.
% just ignore the missing bounding triange for boundary triangles
%%%% Does this work well?
ne = length(xc); % number of elements in the grid
% add a dummy element with NaN coordinates
xc(end+1) = NaN;
yc(end+1) = NaN;
be(be==0) = ne+1; % point to the dummy element

% zero-centre at each central triangle's centroid 
x = xc(be) - xc(k)*ones(1,3); % distances between bounding and central centroids
y = yc(be) - yc(k)*ones(1,3);
xi = xi - xc(k); % distances between bounding and central centroids
yi = yi - yc(k);

% sx2 = nansum(x.^2,2);
% sy2 = nansum(y.^2,2);
% sxy = nansum(x.*y,2);
sx2 = sum(x.^2,2,'omitnan');
sy2 = sum(y.^2,2,'omitnan');
sxy = sum(x.*y,2,'omitnan');

x1 = x(:,1);
x2 = x(:,2);
x3 = x(:,3);
y1 = y(:,1);
y2 = y(:,2);
y3 = y(:,3);

a1 = sy2.*x1 - sxy.*y1;
a2 = sy2.*x2 - sxy.*y2;
a3 = sy2.*x3 - sxy.*y3;

b1 = sx2.*y1 - sxy.*x1;
b2 = sx2.*y2 - sxy.*x2;
b3 = sx2.*y3 - sxy.*x3;

d = sx2.*sy2 - sxy.*sxy;

% w(:,1) = d - (xi.*(a1+a2+a3) + yi.*(b1+b2+b3));
% w(:,1) = d - (xi.*nansum([a1 a2 a3],2) + yi.*nansum([b1 b2 b3],2));
w(:,1) = d - (xi.*sum([a1 a2 a3],2,'omitnan') + yi.*sum([b1 b2 b3],2,'omitnan'));
w(:,2) = (xi.*a1 + yi.*b1);
w(:,3) = (xi.*a2 + yi.*b2);
w(:,4) = (xi.*a3 + yi.*b3);

w = w./(d*ones(1,4));

eid = [k be]; % some indices can point to the dummy element

w_out(ival,:) = w;
eid_out(ival,:) = eid;