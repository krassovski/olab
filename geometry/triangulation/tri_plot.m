function hout = tri_plot(x,y,tri,varargin)
%TRI_PLOT -- Triangulation "wire" plot.
% 
% Inputs:
%   x,y     node coordinates
%   tri     triangulation array
%   Also accepts any arguments for plot() funcion, e.g.
%   'Color',color,'LineWidth',width,'LineStyle',style,'Marker',marker,'LineSmoothing',linesmoothing
%
% Similar to Matlab's standard triplot(tri,x,y), but accepts single precision arguments.

[ebnd,eint] = tri_edgeb(tri);
e = [ebnd; eint];
xe = x(e);
ye = y(e);
xe = [x(e) NaN(size(xe,1),1)]';
ye = [y(e) NaN(size(ye,1),1)]';
xe = xe(:);
ye = ye(:);

h = plot(xe,ye,varargin{:});

if nargout>0
    hout = h;
end