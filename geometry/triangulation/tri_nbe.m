function nbe = tri_nbe(tie)
% Triangles surrounding each triangle (sharing same edges).
% N.B. Doesn't give same order as nbe in fvcom output.
%
% INPUTS:
%   tie   two options:
%         1. [nie,2] triangle indices for internal edges
%            can be obtained wtih [~,~,~,tie] = tri_edgeb(tri);
%         2. [ntri,3] triangulation array
% 
% OUTPUTS:
%   nbe   [ntri,3] triangles surrounding each triangle

if size(tie,2)==3
    [~,~,~,tie] = tri_edgeb(tie);
end
sz = [max(tie(:)) 1];
a1 = accumarray(tie(:,1), tie(:,2), sz, @(x) {x});
a2 = accumarray(tie(:,2), tie(:,1), sz, @(x) {x});
nbe = cellfun(@(x,y) [x; y], a1,a2,'UniformOutput',false);
% append zero for 2-element cells (boundary triangles) and 1-element cells (tip triangles)
nbe = cellfun(@(x) [x; zeros(3-length(x),1)], nbe,'UniformOutput',false);
nbe = cat(2,nbe{:})'; % now merge in [ntri 3] array