function tri = con2tri(con,long_flag)
% Convert connectivity array con (see readngh) to triangulation array tri (see dalaunay).
%
% Inputs:
%   con         [nnodes,nneighbours] connectivity array
%   long_flag   if true, uses less memory, but slower algorithm;
%               default is false
% Output:
%   tri         [ntri,3] triangulation array

if ~exist('long_flag','var') || ~long_flag
    [np,nc] = size(con);
    % build a tree

    % Level 1
    p1 = int32(1:np)'; % work with signed int: needed to make comparisons correctly

    % Level 2
    p2 = con(:);
    p1 = repmat(p1,1,nc); p1 = p1(:);
    % remove branches ending on 0 (no branches returning to the root at this stage)
    ir = ~p2;
    p1(ir) = [];
    p2(ir) = [];

    % Level 3
    p3 = con(p2,:);       p3 = p3(:);
    p1 = repmat(p1,1,nc); p1 = p1(:);
    p2 = repmat(p2,1,nc); p2 = p2(:);
    % remove branches ending on 0 and returning to the root
    ir = ~p3 | ~(p1-int32(p3));
    p1(ir) = [];
    p2(ir) = [];
    p3(ir) = [];

    % Level 4
    p4 = con(p3,:);       p4 = p4(:);
    p1 = repmat(p1,1,nc); p1 = p1(:);
    p2 = repmat(p2,1,nc); p2 = p2(:);
    p3 = repmat(p3,1,nc); p3 = p3(:);
    itri = ~(p1-int32(p4)); % triangle indices
    tri = [p1(itri) p2(itri) p3(itri)];
    % keep tri
    [~,iu] = unique(sort(tri,2),'rows');
    tri = tri(iu,:);
    % tri = tri(any((tri-circshift(tri,[0 1]))'),:);
    
else
    con = con'; % for find to work on rows rather than on columns

    % number of triangles:
    nt = length(find(con))/2/3; % num of non-zero elements, each edge occurs at least twice, 3 edges per triange 
    % interior edges occur four times
    tri = zeros(floor(nt),3); % preallocate excessive amount, cut later
    tricount = 0;
    inz = find(con);
    [col,row] = find(con);

    hwait = waitbar(0,'Converting to tri. 0% done. ETA: ','CreateCancelBtn','delete(gcbf)');
    tst = now;
    ninz = length(inz);
    for iinz = 1:ninz
    %     disp([num2str(iinz) ' of ' num2str(length(inz))]) %%%%%%%%%%
        ngb1 = row(iinz);
        ngb2 = con(col(iinz),ngb1);
        ngb3 = intersect(con(:,ngb1),con(:,ngb2));
    %     i1(~ngb3) = []; i2(~ngb3) = []; ngb3(~ngb3) = []; 
        ngb3(~ngb3) = []; 
        tricount = tricount+1;
%         try
        tri(tricount,:) = [ngb1 ngb2 ngb3(1)];
%         catch
%             ''
%         end

        if rem(iinz,1000)==0
            pdone = iinz/ninz;
            eta = tst + (now-tst)/pdone;
            waitbar(pdone,hwait,['Converting to tri... ' num2str(pdone*100,'%2.2f') '% done. ETA: ' datestr(eta)])
        end

    %     [garb,ingbngb] = intersect(con(:,ngb1),[ngb3(1) ngb2]);
    %     con(ingbngb,ngb1) = 0;
    %     [garb,ingbngb] = intersect(con(:,ngb2),[ngb3(1) ngb1]);
    %     con(ingbngb,ngb2) = 0;
    %     [garb,ingbngb] = intersect(con(:,ngb3(1)),[ngb1 ngb2]);
    %     con(ingbngb,ngb3(1)) = 0;
    end
    % tri = tri(1:tricount,:); % cut empty part
    tri = unique(tri,'rows'); % 
    delete(hwait);
end