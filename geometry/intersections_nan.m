function [px,pos1,pos2] = intersections_nan(poly1, poly2)
% Intersections of two polylines, both polylines can be NaN-separated.

nmax = 3e3; % max number of nodes to process in one run to avoid memory overflow

% find breaks between continuous segments in poly1
br1 = find(isnan(poly1(:,1)));
n1 = size(poly1,1);
br1 = [0; br1; n1+1];
nbr1 = length(br1)-1; % number of continuous segments in poly1

% find breaks between continuous segments in poly2
br2 = find(isnan(poly2(:,1)));
n2 = size(poly2,1);
br2 = [0; br2; n2+1];
nbr2 = length(br2)-1; % number of continuous segments in poly2

px = []; % initialize
pos1 = []; % initialize
pos2 = []; % initialize

for ib1 = 1:nbr1 % deal with each NaN-separated segment in poly1 separately
    if br1(ib1+1)-br1(ib1) > 2 % at least one-edge segment
        poly1_ = poly1(br1(ib1)+1 : br1(ib1+1)-1, :); % exclude NaNs
        for ib2 = 1:nbr2 % deal with each NaN-separated segment in poly2 separately
            if br2(ib2+1)-br2(ib2) > 2 % at least one-edge segment
                poly2_ = poly2(br2(ib2)+1 : br2(ib2+1)-1, :); % exclude NaNs
        %         [px_,pos1_,pos2_] = intersections(poly1, poly_);
                [px_,pos1_,pos2_] = intersectPolylines_bypiece(poly1_,poly2_,nmax);
                pos1_ = pos1_ + br1(ib1); % positions in the entire poly1 contour
                pos2_ = pos2_ + br2(ib2); % positions in the entire poly2 contour

                % accumulate intersection points
                px = [px; px_]; pos1 = [pos1; pos1_]; pos2 = [pos2; pos2_];
            end
        end
    end
end

% sort according to the position on poly1
[pos1,isort] = sort(pos1);
px = px(isort,:);
pos2 = pos2(isort);