function olab_install()
%OLAB_INSTALL Install olab on local machine.

olab_register % write olab_home.m
pause(.5)
addpath(olab_home)

% pdist2 and range are part of Statistics and Machine Learning Toolbox;
% add path to their replacements if the toolbox is not installed
if isempty(which('pdist2'))
    olab_addpath('toolboxfix/stat')
end

% % try to avoid nansuite: e.g. nanmax, nanmin have different signature than
% % Matlab's counterparts.
% if isempty(which('nansum'))
%     olab_addpath('versionfix/nansuite')
% end

savepath