function [on,not] = on_path(p)
% Determine if specified path(s) are on the Matlab path.

if ~iscell(p)
    p = strsplit(p,';');
end

mp = strsplit(path,';');

tf = ismember(p,mp);
on  = strjoin(p( tf),';');
not = strjoin(p(~tf),';');