function d = pdist2(a,b)
% Replacement for pdist2 in Statistics and Machine Learning Toolbox.
% The solution is from 
% https://stackoverflow.com/questions/7696734/pdist2-equivalent-in-matlab-version-7

d = sqrt( bsxfun(@plus,sum(a.^2,2),sum(b.^2,2)') - 2*(a*b') );