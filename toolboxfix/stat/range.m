function r = range(x,dim)
% Sub for standard function in Statistics Toolbox.

% Maxmi Krassovski 2017-07-20

if ~exist('dim','var') || isempty(dim)
    dim = 1;
end

% if vector, make sure it's a column
if sum(size(x)>1) <= 1
    x = x(:);
end

r = max(x,[],dim,'omitnan') - min(x,[],dim,'omitnan');