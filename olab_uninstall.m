function olab_uninstall()
%OLAB_UNINSTALL Uninstall olab from local machine.

installdir = olab_home;

rmpath(olab_home)
savepath

delete(olab_home_file_name) % remove olab_home.m
disp( 'olab_uninstall: olab code was not deleted. To delete use:')
disp(['                rmdir(' installdir ',''s'')'])