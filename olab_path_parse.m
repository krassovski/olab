function fullpath = olab_path_parse(relpath)
%OLAB_PATH_PARSE Parse input to olab_addpath and olab_rmpath.
%
% Inputs: 
%   relpath     path to remove, relative to olab_home;
%               either a string separated with ";" for multiple pathes (as
%               input to rmpath) or a cell array of individual pathes.
%               If omitted, all olab subfolders are removed except olab_home.

if ~exist('relpath','var')
    fullpath = olab_path; % add all olab subfolders
else
    if ~iscell(relpath)
        relpath = {relpath};
    end
    fullpath = cellfun(@(x) fsep(fullfile(olab_home,x)), relpath, 'UniformOutput',false);
    fullpath = strjoin(fullpath,';');
end