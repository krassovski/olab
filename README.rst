**********************************
OLAB - Oceanographic Matlab tools
**********************************

A set of tools for physical oceanographic data processing and numerical modelling.

Installation
============

Clone the repository: ::

    git clone git@gitlab.com:krassovski/olab.git olab.git

Install. In Matlab, make olab.git the current working folder, then do: ::

    olab_install
    
To avoid naming conflicts with other Matlab packages use 
``olab_addpath(folder_name)`` before using functions from that folder, e.g. ::

    olab_addpath('model/mesh') % path relative to olab folder

To add all olab folders to Matlab path: ::

    olab_addpath % paths added until the end of Matlab session

To make changes to the path permanent: ::

    savepath

Highlights
==========

VisualGrid -- A GUI to work with bathymetry and coastlines, as well as to 
generate and edit triangular grids: ::

    olab_addpath('model/mesh/VisualGrid')
    VisualGrid


Acknowledgements
================

OLAB depends on several third-party packages which are included in this repository:

 - Mesh2d v24 by Darren Engwirda. Generation of 2D FEM grids. The latest version is `here <https://www.mathworks.com/matlabcentral/fileexchange/25555-mesh2d-delaunay-based-unstructured-mesh-generation>`_.
 - `geom2d <https://www.mathworks.com/matlabcentral/fileexchange/7844-geom2d>`_  by David Legland. 2D geometry routines.
 - `m_map1.4 <https://www.eoas.ubc.ca/~rich/map.html>`_ by Rich Pawlowicz. Map  plotting.
 - `GUILayout  <https://www.mathworks.com/matlabcentral/fileexchange/47982-gui-layout-toolbox>`_  by David Sampson and Ben Tordoff.
 - A few other functions found on `Matlab File Exchange <https://www.mathworks.com/matlabcentral/fileexchange/>`_.

License
=======
Apache 2.0