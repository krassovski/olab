function p = olab_path()
%OLAB_PATH List pathes to all subfolders of olab_home.

p = genpath(olab_home);

% do not list .git and subfolders
c = strsplit(p,pathsep); % to cell array
c(strcmp(c,olab_home)) = []; % do not list olab_home itself
if verLessThan('matlab','9.1') % R2016b
    c(~cellfun(@isempty,strfind(c,[filesep '.git']))) = []; %#ok<STRCLFH>
else
    c(contains(c,[filesep '.git'])) = [];
end
p = strjoin(c,pathsep);