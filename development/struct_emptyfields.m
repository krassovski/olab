function r = struct_emptyfields(r)
% copy underlying structure fields

r = r(1); % need only one element in output

flds = fieldnames(r);
for ifl = 1:length(flds)
    r.(flds{ifl}) = [];
end