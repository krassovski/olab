function jsetuiprop(h,propname,propval)
% Set a property for an underlying Java objects of a set of Matlab handles
% h, e.g. for uimenues.

% May not work for disabled uicontrols. See
% http://undocumentedmatlab.com/blog/inactive-control-tooltips-event-chaining/
% for the workaround.

for ih = 1:length(h)
    set(findjobj(h(ih)),propname,propval);
end