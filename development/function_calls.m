function [fthirdparty,missing,mex_or_p,ftoolbox,fbuiltin,addp,addpr] = function_calls(fname)
%FUNCTION_CALLS Functions called by the specified function.
% NOTE: Doesn't determine function handles, @func_name.
%
% SYNTAX
%   [fthirdparty,missing,mex_or_p,ftoolbox,fbuiltin,addp,addpr] = function_calls(fname)
%
% INPUT
%    fname:     Name of the m-file to analyse
%
% OUTPUT
%    thirdparty: 3rd party functions (with full path)
%       missing: functions not found on the path
%       toolbox: functions in Matlab toolboxes (with full path)
%       builtin: built-in and standard Matlab functions (with full path)

% Author:       Maxim Krassovski       
% Matlab ver.:  9.3.0.713579 (R2017b)
% Date:         25-Jun-2018

% test:
% [fthirdparty,ftoolbox,fbuiltin,addp,addpr] = function_calls('/media/user/MyPassport/Max/Tools/Modelling/create_atm_hrdps.m');

olab_addpath('system') % listfiles

o = mtree(fname,'-file');
% o.dumptree
calls = mtfind(o,'Kind','CALL');

fcalls = calls.Left; % children of CALLS
ln = fcalls.lineno;
fcallname = (fcalls.strings)'; % called functions' names
[fcallnameu,iu] = unique(fcallname);
ln = ln(iu); %NOTE: picks only one occurence, according to unique
ibuiltin = cellfun(@(x) exist(x,'builtin'), fcallnameu) == 5; % builtins
% [fcallname num2cell(ifile) num2cell(ibuiltin)]

% determine addpath and olab_addpath calls
ap = mtfind(o,'String','addpath');
%%%% addpath can recieve not oly strings, but also expressions
addp = ap.Parent.Right.strings'; % strings supplied to addpath
apr = mtfind(o,'String','olab_addpath');
addpr = apr.Parent.Right.strings'; % strings supplied to olab_addpath

% addpathes
[fun_folder,fun_name] = fileparts(fname);
[~,finalfolder] = fileparts(fun_folder);
if ~strcmp(finalfolder,'private')
    addpath(fun_folder) % the folder of the specified function
    private_folder = '';
else % the specified function is in private folder
    private_folder = fun_folder;
end
for k = 1:length(addpr)
    % replace duplicated quotes with single quotes
%     olab_addpath(strrep(addpr{k},'''','')) % this works, but I don't understand why
    olab_addpath(addpr{k}(2:end-1)) % strip 1st and last quotes
end

fpath = cellfun(@which,fcallnameu,'UniformOutput',false); % full path to each function

% not all standard Matlab functions are caugh by builtins, e.g. mean, datestr, now;
% e.g. 'MException is a built-in method', 'toString is a Java method'
ibuiltin = ibuiltin | contains(fpath,' is a ');
% find also those on matlabroot
matlabfolder = fullfile(matlabroot,'toolbox','matlab');
imatlab = strncmp(fpath,matlabfolder,length(matlabfolder));
toolboxfolder = fullfile(matlabroot,'toolbox');
itoolbox = strncmp(fpath,toolboxfolder,length(toolboxfolder)) & ~imatlab;
ilocal = ismember(fcallnameu,o.Fname.strings); % among the names of functions in this file
i3rdparty = ~ibuiltin & ~imatlab & ~itoolbox & ~ilocal;

% check that each function call has been accounted for and only once
if ~(all(sum([i3rdparty ilocal itoolbox ibuiltin imatlab],2))==1)
    error('This is a bug. Check the code.')
end

fthirdparty_name = fcallnameu(i3rdparty);
fthirdparty = fpath(i3rdparty);
ln3rd = ln(i3rdparty);
imissing = cellfun(@isempty,fthirdparty);
missing = fthirdparty_name(imissing);
lnmissing = ln3rd(imissing);
fthirdparty(imissing) = [];

% private folder cannot be added to path, so check if there are any
% "missing" functions there
fl = dir(fun_folder);
if any(strcmp({fl.name},'private') & [fl.isdir]) % there is private folder
    private_folder = [fun_folder filesep 'private'];
end
if ~isempty(private_folder) % if specified function is in private folder or theere is private folder
    flp = listfiles(private_folder,'m'); % private m-files
    prfile = {flp.name}';
    [~,prname] = cellfun(@fileparts,prfile,'UniformOutput',false);
    [ipr,locpr] = ismember(missing,prname);
    fthirdparty = [fthirdparty; prfile(locpr(locpr~=0))];
    missing(ipr) = [];
    lnmissing(ipr) = [];
end

% prepend current function name
% % without line numbers
% missing = cellfun(@(x) [fun_name '/' x], missing,'UniformOutput',false);
% with line numbers
for k = 1:length(missing)
    missing{k} = [fun_name '(Line ' num2str(lnmissing(k)) '):' missing{k}];
end

% separate mex- and p-files from m-files
inotmfile = ismember(cellfun(@(x) exist(x,'file'), fthirdparty), [3 6]); % mex-file or p-file
mex_or_p = fthirdparty(inotmfile);
fthirdparty(inotmfile) = [];

ftoolbox = fpath(itoolbox);
fbuiltin = fpath(ibuiltin | imatlab);