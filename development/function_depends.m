function [deps,missing,mex_or_p] = function_depends(fname,dorecursive)
%FUNCTION_DEPENDS Dependencies for the specified function.
% List of dependencies all way deep, i.e. with all depencies of the dependencies.
%
% SYNTAX
%    [deps,missing,mex_or_p] = function_depends(fname);
%
% INPUT
%    fname:     File name for the function to analyse.
%
% OUTPUT
%        deps:  3rd party function found on the path.
%               NOTE: Pathes are added for olab_addpath calls, but not for
%               addpath calls.
%     missing:  Functions not found on the path.
%    mex_or_p:  MEX- and P-files on the path.
%
%
% M-FILES required: 
%    function_calls.m

% Author:       Maxim Krassovski
% Matlab ver.:  9.3.0.713579 (R2017b)
% Date:         28-Jun-2018

if ~exist('dorecursive','var') || isempty(dorecursive)
    dorecursive = true;
end

if ~exist(fname,'file')
    deps = {[fname ' -- doesn''t exist']};
%     deps = {fname};
    missing = {};
    mex_or_p = {};
    return
end

[fthirdparty,fmissing,fmex_or_p,ftoolbox,fbuiltin,addp,addpr] = function_calls(fname);

% don't do dependencies for olab_addpath 
[~,name] = cellfun(@fileparts,fthirdparty,'UniformOutput',false);
fthirdparty(strcmp(name,'olab_addpath')) = [];

deps = fthirdparty;
missing = fmissing;
mex_or_p = fmex_or_p;
if dorecursive
    for k = 1:length(fthirdparty)
    %     [fthirdparty,ftoolbox,fbuiltin,addp,addpr] = function_calls(fthirdparty{k});
        [depsk,missingk,mex_or_pk] = function_depends(fthirdparty{k});
        if any(strcmp(depsk,'islandcheck'))
            disp()
        end
        deps = [deps; depsk];
        missing = [missing; missingk];
        mex_or_p = [mex_or_p; mex_or_pk];
    end
end
deps = unique(deps);
missing = unique(missing);
mex_or_p = unique(mex_or_p);