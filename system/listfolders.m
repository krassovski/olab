function files = listfolders(foldername)
% List all subdirectories in a specified directory one level deep.
% files     struct array: Get names as files(i).name

files = dir(foldername);

files(~[files.isdir]) = [];   % remove non-directories

% Remove two pseudofiles (. and ..) from the file list
files(strcmp({files.name},'.')) = [];
files(strcmp({files.name},'..')) = [];

% add path to names
if ~strcmp(foldername,'.')
    for ifile = 1:length(files)
        files(ifile).name = [foldername filesep files(ifile).name];
    end
end