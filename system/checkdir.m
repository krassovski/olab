function checkdir(dirname)
% Checks if directory dirname exists, creates it if it doesn't.

% create outdir if doesn't exist
if ~exist(dirname,'dir')
%     if eval(['dos(''mkdir ',dirname,''')'])
%         error(['Can''t create  ',dirname])
%     end
    status = mkdir(dirname);
    if ~status
        error(['Can''t create  ',dirname])
    end
end