function files = listfiles(indir,ext,sub_flag)
% List files in the specified directory indir.
% ext   [optional] list only files with the specified extension.
%       for ext==[], list files with any extension (including files without extension);
%       for ext=='', list only files without extension.
% sub_flag  false (default): exclude subdirectories;
%           true:            include subdirectories all the way deep.
% Output is a struct array. Get names as files(i).name.

if ~exist('indir','var') || isempty(indir); indir = '.'; end
% remove trailing separators
while indir(end) == filesep
    indir(end) = [];
end
if ~exist('ext','var')
    ext = []; % default: all extensions (including files without extension)
elseif ischar(ext)
    ext = {ext}; % ensure cell array
end
if ~exist('sub_flag','var'); sub_flag = false; end

%TODO Started to code the case with wildcards. 
% % separate folder and wild-carded pattern
% if any(indir=='*') % if wildcards used
%     ks = find(indir==filesep,1,'last');
%     if isempty(ks) % searching current folder
%         pattern = indir;
%         path = '';
%     else
%         pattern = indir(ks+1:end);
%         path = indir(1:ks-1);
%     end
% else
%     path = indir;
% end

% get directory content
if isempty(ext)
    files = dir(indir);  % list files in main folder (including files without extension)
else
    files = [];
    for k = 1:length(ext)
        if isempty(ext{k})
            filesk = dir(indir);
            filesk(contains({filesk.name},'.')) = [];
            files = [files; filesk];
        else
            files = [files; dir([indir filesep '*.' ext{k}])]; % list files in main folder with specified extensions
        end
    end
end

% add path to names
if ~strcmp(indir,'.')
    for ifile = 1:length(files)
        files(ifile).name = [indir filesep files(ifile).name];
    end
end

if sub_flag     % search subdirectories
    dirs = listdirs(indir);       % list subfolders
    for idir = 1:length(dirs)
%         subfiles = mkListFilesDeep([indir filesep dirs(idir).name], ext);
        subfiles = listfiles([indir filesep dirs(idir).name],ext,sub_flag);
        files = [files; subfiles];  % append files in current subfolder to the list
    end
end

% remove directories
files([files.isdir]) = [];



function files = listdirs(dirName)
% returns the list of all subdirectories in a specified directory
% as struct array. Get names as files(i).name

files = dir(dirName);

files(~[files.isdir]) = [];   % remove non-directories

% Remove two psedofiles (. and ..) from the file list
files(strcmp({files.name},'.')) = [];
files(strcmp({files.name},'..')) = [];