function [err,h,warn] = shpread_header(fname)
% Read shapefiles according to ESRI format specification.
% M.Krassovski 09-Sep-2014 extract from shpread.m

err = 0;
warn = '';

try
fid = fopen(fname,'r');
catch err
    h = [];
%     err = 'Couldn''t open the file';
    return
end

% header
fcode = fread(fid, 1, 'int32', 0,'ieee-be');
if fcode ~= 9994
    warn = 'File code is not 9994';
end
fseek(fid,20,'cof');
h.fbytelength = fread(fid, 1, 'int32', 0,'ieee-be')*2; % ESRI has file length in 16-bit words
fversion = fread(fid, 1, 'int32');
if fversion ~= 1000
    warn = 'File version is not 1000';
end
h.shapetype = fread(fid, 1, 'int32');
h.boundbox.xmin = fread(fid, 1, 'double');
h.boundbox.ymin = fread(fid, 1, 'double');
h.boundbox.xmax = fread(fid, 1, 'double');
h.boundbox.ymax = fread(fid, 1, 'double');
h.boundbox.zmin = fread(fid, 1, 'double');
h.boundbox.zmax = fread(fid, 1, 'double');
h.boundbox.mmin = fread(fid, 1, 'double');
h.boundbox.mmax = fread(fid, 1, 'double');

fclose(fid);