function [h,x,y] = shpread_type1(fname,showwait)
% Hack to read Type 1 shapefiles faster than general-purpose shpread.m.
%
% M.Krassovski 29-Aug-2013

if ~exist('showwait','var') || ~islogical(showwait)
    showwait = false; % do not display waitbar by default
end

fid = fopen(fname,'r');

% header
fcode = fread(fid, 1, 'int32', 0,'ieee-be');
if fcode ~= 9994
    warning('File code is not 9994')
end
fseek(fid,20,'cof');
h.fbytelength = fread(fid, 1, 'int32', 0,'ieee-be')*2; % ESRI has file length in 16-bit words
fversion = fread(fid, 1, 'int32');
if fversion ~= 1000
    warning('File version is not 1000')
end
h.shapetype = fread(fid, 1, 'int32');
h.boundbox.xmin = fread(fid, 1, 'double');
h.boundbox.ymin = fread(fid, 1, 'double');
h.boundbox.xmax = fread(fid, 1, 'double');
h.boundbox.ymax = fread(fid, 1, 'double');
h.boundbox.zmin = fread(fid, 1, 'double');
h.boundbox.zmax = fread(fid, 1, 'double');
h.boundbox.mmin = fread(fid, 1, 'double');
h.boundbox.mmax = fread(fid, 1, 'double');

if showwait
    hwait = waitbar(0,['Reading ' fname '. 0% done. ETA: '],'CreateCancelBtn','delete(gcbf)');
    tst = now;
    
    dchk = h.fbytelength*0.0001;  % waitbar step in bytes
    chkdone = dchk; % next checkpoint for waitbar
end

reclen = 28; % 28-byte record lenght for Type 1 (Point) data: 
% 4*2(shape header) + 4(shape type) + 8*2(data)
n = (h.fbytelength-ftell(fid))/reclen;
x = zeros(n,1);
y = x;
for ii = 1:n
    fseek(fid,12,'cof');
    x(ii) = fread(fid, 1, 'double');
    y(ii) = fread(fid, 1, 'double');
    
    % update waitbar
    if showwait
        if ~ishandle(hwait) % waitbar Cancel pressed
            return
        end

        if ftell(fid)>chkdone
            pdone = ii/n;
            eta = tst + (now-tst)/pdone;
            waitbar(pdone,hwait,['Reading... ' num2str(pdone*100,'%2.2f') '% done. ETA: ' datestr(eta)])
            chkdone = chkdone+dchk;
        end
    end
end

fclose(fid);

if showwait
    delete(hwait);
end