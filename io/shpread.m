function [h,r,err,warn] = shpread(fname,showwait)
% Read shapefiles according to ESRI format specification.
% M.Krassovski 05-Nov-2008
% M.Krassovski 01-Feb-2011  Support for Type 5 (Polygon) is added.

if ~exist('showwait','var') || ~islogical(showwait)
    showwait = false; % do not display waitbar by default
end

h = '';
err = 0;
warn = '';

try
fid = fopen(fname,'r');
catch err
%     err = {'Couldn''t open the file'; err.Message};
    r = [];
    return
end

% header
fcode = fread(fid, 1, 'int32', 0,'ieee-be');
if fcode ~= 9994
%     warning('File code is not 9994')
    warn = 'File code is not 9994';
end
fseek(fid,20,'cof');
h.fbytelength = fread(fid, 1, 'int32', 0,'ieee-be')*2; % ESRI has file length in 16-bit words
fversion = fread(fid, 1, 'int32');
if fversion ~= 1000
%     warning('File version is not 1000')
    warn = 'File version is not 1000';
end
h.shapetype = fread(fid, 1, 'int32');
h.boundbox.xmin = fread(fid, 1, 'double');
h.boundbox.ymin = fread(fid, 1, 'double');
h.boundbox.xmax = fread(fid, 1, 'double');
h.boundbox.ymax = fread(fid, 1, 'double');
h.boundbox.zmin = fread(fid, 1, 'double');
h.boundbox.zmax = fread(fid, 1, 'double');
h.boundbox.mmin = fread(fid, 1, 'double');
h.boundbox.mmax = fread(fid, 1, 'double');

if showwait
    hwait = waitbar(0,['Reading ' fname '. 0% done. ETA: '],'CreateCancelBtn','delete(gcbf)');
    tst = now;
    
    dchk = h.fbytelength*0.0001;  % waitbar step in bytes
    chkdone = dchk; % next checkpoint for waitbar
end

% c = []; z = [];
while ftell(fid) < h.fbytelength
%     floor(ftell(fid)*0.000001) %%%%%%%%%%%%%%%%%%
    cr.number = fread(fid, 1, 'int32', 0,'ieee-be');
%     contentlen = fread(fid, 1, 'int32', 0,'ieee-be');
    fseek(fid,4,'cof'); %%%%%%%%% replaces the read of contentlen
    shtype = fread(fid, 1, 'int32');
    if shtype ~= h.shapetype
        warn = strvcat(warn,['In Record ' num2str(cr.number) ' the type is not consistent with file shape type. Skipping...']);
        continue
    end
    switch h.shapetype
        case 1 % Point
            cr.x = fread(fid, 1, 'double');
            cr.y = fread(fid, 1, 'double');
        case 3 % PolyLine
            cr.boundbox = fread(fid, 4, 'double');
            cr.numparts = fread(fid, 1, 'int32');
            cr.numpoints = fread(fid, 1, 'int32');
            % array index of 1st point for each part in cr.points:
            cr.parts = fread(fid, cr.numparts, 'int32');
            xy = fread(fid, cr.numpoints*2, 'double');
            cr.x = [xy(1:2:end)' NaN];
            cr.y = [xy(2:2:end)' NaN];
        case 5 % Polygon
            cr.boundbox = fread(fid, 4, 'double');
            cr.numparts = fread(fid, 1, 'int32');
            cr.numpoints = fread(fid, 1, 'int32');
            % array index of 1st point for each part in cr.points:
            cr.parts = fread(fid, cr.numparts, 'int32'); % indexing starts with 0
            xy = fread(fid, cr.numpoints*2, 'double');
            
            % insert terminating NaNs between parts
            nn = cr.numpoints+cr.numparts; % number of nodes including terminating NaNs
            xyn = NaN(nn,2);
            inan = cr.parts + (0:cr.numparts-1)';
            inan = [inan(2:end); nn];
            ival = setdiff((1:nn)',inan);
            xyn(ival,:) = reshape(xy,2,[])';
            cr.x = xyn(:,1)';
            cr.y = xyn(:,2)';
        case 11 % PointZ
            cr.x = fread(fid, 1, 'double');
            cr.y = fread(fid, 1, 'double');
            cr.z = fread(fid, 1, 'double');
            cr.m = fread(fid, 1, 'double');
        case 13 % PolyLineZ
            cr.boundbox = fread(fid, 4, 'double');
            cr.numparts = fread(fid, 1, 'int32');
            cr.numpoints = fread(fid, 1, 'int32');
            % array index of 1st point for each part in cr.points:
            cr.parts = fread(fid, cr.numparts, 'int32');
% %             cr.points = read_point(fid, cr.numpoints);
%             for ip = 1:cr.numpoints
%                 cr.points(ip).x = fread(fid, 1, 'double');
%                 cr.points(ip).y = fread(fid, 1, 'double');
%             end
            xy = fread(fid, cr.numpoints*2, 'double');
            cr.x = [xy(1:2:end)' NaN];
            cr.y = [xy(2:2:end)' NaN];

%             c = [c; [cr.x cr.y]];
%             c = [c; [NaN NaN]];
            
%             plot([cr.points.x],[cr.points.y]); hold on
            
            cr.zmin = fread(fid, 1, 'double');
            cr.zmax = fread(fid, 1, 'double');
            cr.z = [fread(fid, cr.numpoints, 'double')' NaN];

%             z = [z; [cr.z]];
%             z = [z; NaN];
            
            cr.mmin = fread(fid, 1, 'double');
            cr.mmax = fread(fid, 1, 'double');
            cr.m = fread(fid, cr.numpoints, 'double');
        otherwise
%             error('This shape type is not supported.')
            err = 'Unsupported shapefile type';
            r = [];
            return
    end
    if exist('r','var')
        r(end+1) = cr;
    else
        r = cr;
    end
    clear cr
    
    % update waitbar
    if showwait
        if ~ishandle(hwait) % waitbar Cancel pressed
            return
        end

        if ftell(fid)>chkdone
            pdone = ftell(fid)/h.fbytelength;
            eta = tst + (now-tst)/pdone;
            waitbar(pdone,hwait,['Reading... ' num2str(pdone*100,'%2.2f') '% done. ETA: ' datestr(eta)])
            chkdone = chkdone+dchk;
        end
    end
end
fclose(fid);

if showwait
    delete(hwait);
end


% function p = read_point(fid,np)
% % read ponts from shapefile
% 
% for ip = 1:np
%     p.number = fread(fid, 1, 'int32', 0,'ieee-be');
%     contentlen = fread(fid, 1, 'int32', 0,'ieee-be');
%     shtype = fread(fid, 1, 'int32');
%     if shtype ~= 1
%         error('Not a pont type')
%     end
%     p.x = fread(fid, 1, 'double');
%     p.y = fread(fid, 1, 'double');
% end