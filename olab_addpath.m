function olab_addpath(varargin)
%OLAB_ADDPATH Add olab subfolder(s) to Matlab path.
%
% Signatures:
%   olab_addpath % add all olab subfolders
%   olab_addpath(path_relative_to_olab_home) % add individual olab subfolder(s)
% 
% Inputs: 
%   relpath     path to add, relative to olab_home;
%               either a string separated with ";" for multiple pathes (as
%               input to addpath) or a cell array of individual pathes.
%               If omitted, all olab subfolders are added.
%               NOTE: If multiple pathes supplied, function_depends() and
%               function_calls() will not be able to trace the pathes added.

fullpath = olab_path_parse(varargin{:});
[~,not] = on_path(fullpath);
addpath(not)