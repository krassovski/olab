function f = olab_home_file_name()
%olab_home_file_name    Specifies the name for the file pointing to olab folder.

f = 'olab_home.m';