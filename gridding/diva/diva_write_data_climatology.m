function diva_write_data_climatology(outfile,lon,lat,v,w,ky,km,kd,kh,time,profile_id)
%DIVA_WRITE_DATA_CLIMATOLOGY -- Write data file for input to DIVA.
%
% SYNTAX
%    diva_write_data_climatology(outfile,lon,lat,v,w,ky,km,kd,kh,time,profile_id)
%
% INPUT
%       outfile: name for the output file
%           lon: 
%           lat: 
%             v: values for the variable 
%             w: probably weights?
%            ky: year from the start year in yearlist
%            km: month interval from the monthlist
%            kd: day
%            kh: hour (+1?)
%          time: time in datenum format
%    profile_id: cell array of strings
% 
% All input arrays are of the same size.
%
% OUTPUT
%   outfile is created
% 
% SEE ALSO
%   diva_read_data_climatology

% Author:       Maxim Krassovski
% Matlab ver.:  9.3.0.713579 (R2017b)

fid = fopen(outfile,'w');
n = length(v);
for k = 1:n
    %%% looks like 4th column is integers
    if any(w~=1)
        warning([outfile '   min(w)=' num2str(min(w))])
    end
    fprintf(fid,'%f %f %f %d %d %d %d %d %s %s\n',...
        lon(k),lat(k),v(k),w(k),ky(k),km(k),kd(k),kh(k),datestr(time(k),'yyyy-mm-ddTHH:MM'),profile_id{k});
end
fclose(fid);