function write_diva_grd(a,fillvalue,fname)
%WRITE_DIVA_GRD -- Write matrix a in a binary file according to diva example:
% diva-4.7.1/Example4D/input/topo.grd
%
% SYNTAX
%    write_diva_grd(a,fillvalue,fname)
%
% INPUT
%            a: should be oriented such that this command gives correctly oriented map:
%               figure;imagesc(a);set(gca,'YDir','normal')
%    fillvalue: to fill NaNs in a, e.g. -99
%        fname: name for the output file
%
% OUTPUT
%   output file is created
%
% Author:       Maxim Krassovski
% Matlab ver.:  9.3.0.713579 (R2017b)

a = a'; % orient to write by rows starting from the lower left corner

prec = 4; % single, 4-byte, output precision for a

a(isnan(a)) = fillvalue;

[imax,jmax,kmax] = size(a);
% nbmots = imax*jmax*kmax;

fid = fopen(fname,'w');

fwrite(fid, zeros(20,1),'int32'); % 20 empty values

fwrite(fid, 24,'int32'); %%% simulate fortran output: num bytes for each record before and after the record
fwrite(fid, [imax,jmax,kmax,prec,imax],'int32');
fwrite(fid, fillvalue,'real*4');
fwrite(fid, 24,'int32'); %%%

for k = 1:jmax
    fwrite(fid, imax*4,'int32'); %%%
    fwrite(fid, a(:,k),'real*4');
    fwrite(fid, imax*4,'int32'); %%%
end

fwrite(fid, 0,'int32'); %%% 1 empty value at the end of the file

fclose(fid);