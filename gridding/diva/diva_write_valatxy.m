function diva_write_valatxy(x,y,fname,prec)
%DIVA_WRITE_VALATXY -- Write valatxy.coord file for DIVA.
%
% SYNTAX
%    diva_write_valatxy(x,y,fname,prec)
%
% INPUT
%        x: coordinates
%        y: 
%    fname: output file name; default is valatxy.coord
%     prec: precision, the number of decimal places, 
%           e.g. 1 for coords in metres, 6 for geographic (default)
%
% OUTPUT
%   output file is created

% Author:       Maxim Krassovski
% Matlab ver.:  9.3.0.713579 (R2017b)

if ~exist('fname','var') || isempty(fname)
    fname = 'valatxy.coord';
end

if ~exist('prec','var') || isempty(prec)
    prec = 6;
end

precstr = num2str(prec);
fmt = ['%0.' precstr 'f %0.' precstr 'f\n'];

fout = fopen(fname,'w');
fprintf(fout,fmt,[x(:) y(:)]');
fclose(fout);