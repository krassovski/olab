function [lon,lat,v,w,ky,km,kd,kh,time,profile_id] = diva_read_data_climatology(fname)
%DIVA_READ_DATA_CLIMATOLOGY -- Read data file produced by diva after data extraction from ODV data file.
%
% Example of the records in the data file:
% -123.544667 49.375500 28.981 1 105 1 28 4 2004-04-28T03:02 Cruise:2004-07-NI07--123.544667-49.375500
% -123.549667 49.375000 24.981 1 106 1 26 25 2005-04-26T23:36 Cruise:2005-08-NIO7--123.549667-49.375000
% 
% SYNTAX
%    [lon,lat,v,w,ky,km,kd,kh,time,profile_id] = diva_read_data_climatology(fname)
%
% INPUT
%    fname:     file name
%
% OUTPUT
%           lon: 
%           lat: 
%             v: value
%             w: probably weight?
%            ky: year from the start year in yearlist
%            km: month interval from the monthlist
%            kd: day
%            kh: hour (+1?)
%          time: time in datenum format
%    profile_id: 

% Author:       Maxim Krassovski
% Matlab ver.:  9.3.0.713579 (R2017b)

fid = fopen(fname,'r');
c = textscan(fid,'%f %f %f %f %d %d %d %d %s %s');
fclose(fid);

lon = c{1};
lat = c{2};
v = c{3};
w = c{4}; %%% probably weight
% yr_rel = c{5}; % corresponds to the year from the start year in yearlist
% mon_rel = c{6}; % corresponds to the month interval from the monthlist
% day = c{7}; % corresponds to the month interval from the monthlist
% hour_rel = c{8}; % corresponds to the hour (+1?)
ky = c{5}; % corresponds to the year from the start year in yearlist
km = c{6}; % corresponds to the month interval from the monthlist
kd = c{7}; % corresponds to the day
kh = c{8}; % corresponds to the hour (+1?)

if isempty(c{9})
    time = v; % empty array, same as v
else
    time = datenum(c{9},'yyyy-mm-ddTHH:MM');
end
profile_id = c{10};