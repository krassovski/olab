function [x,y,tri] = diva_read_mesh(meshtoponame)
%DIVA_READ_MESH -- Read mesh from diva-generated mesh.dat and meshtopo files.
%
% SYNTAX
%    [x,y,tri] = diva_read_mesh(meshtoponame)
%
% INPUT
%    meshtoponame: name of the meshtopo file
%
% OUTPUT
%      x: coordinates
%      y: 
%    tri: [ntri,3] triangulation array

% Author:       Maxim Krassovski
% Matlab ver.:  9.3.0.713579 (R2017b)

[path,name,ext] = fileparts(meshtoponame);
n = load([path filesep 'mesh.dat' ext]);

fid = fopen(meshtoponame,'r');

nxy = fscanf(fid,'%d %f %f',n(1)*3);
nn  = fscanf(fid,'%d',n(2));
tri = fscanf(fid,'%d %d %d %d %d %d',n(3)*6);

fclose(fid);


nxy = reshape(nxy,[3 n(1)])';
tri = reshape(tri,[6 n(3)])';

x = nxy(:,2);
y = nxy(:,3);
tri = tri(:,[1 3 5]);