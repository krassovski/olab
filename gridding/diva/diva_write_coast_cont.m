function diva_write_coast_cont(c,fname,prec)
%DIVA_WRITE_COAST_CONT -- Write DIVA coast.cont file.
%
% SYNTAX
%    diva_write_coast_cont(c,fname,prec)
%
% INPUT
%        c: contour in Matlab ContourMatrix format (see nan2contour.m).
%           Must be negatively oriented (land is on the right).
%    fname: output file name; default is coast.cont
%     prec: precision, the number of decimal places, 
%           e.g. 1 for coords in metres, 6 for geographic
%
% OUTPUT
%   output file is created
% 
% M-FILES required: 
%    c2cell.m

% Author:       Maxim Krassovski
% Matlab ver.:  9.3.0.713579 (R2017b)
% Date:         2017-06-20

if ~exist('fname','var') || isempty(fname)
    fname = 'coast.cont';
end
if ~exist('prec','var') || isempty(prec)
    prec = 1; % for cartesian coords (in metres)
end

c = c2cell(c); % discarding contour values

fout = fopen(fname,'w');

ns = length(c);
fprintf(fout,'%d\n',ns);

fmt = ['%0.' num2str(prec) 'f %0.' num2str(prec) 'f\n'];
for k = 1:ns
    np = length(c{k});
    fprintf(fout,'%d\n',np);
%     fprintf(fout,'%10.6f %10.6f\n',c{k}'); % for coords in lon,lat
    fprintf(fout,fmt,c{k}');
end

fclose(fout);