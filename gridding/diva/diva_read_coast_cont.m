function c = diva_read_coast_cont(fname)
%DIVA_READ_COAST_CONT -- Read DIVA coast.cont file.
%
% SYNTAX
%    c = diva_read_coast_cont(fname)
%
% INPUT
%    fname: file name
%
% OUTPUT
%    c:     contour matrix (in contourc format)

% Author:       Maxim Krassovski
% Matlab ver.:  9.3.0.713579 (R2017b)

fid = fopen(fname,'r');

nc = fscanf(fid, '%d', 1);
c = [];
for k = 1:nc
    np = fscanf(fid, '%d', 1);
%     c = fscanf(fid, '%f %f', np);
    ck = textscan(fid, '%f %f',np);
    
    c = [c [[0; np] [ck{1} ck{2}]']];
end

fclose(fid);