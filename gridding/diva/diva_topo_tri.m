function h = diva_topo_tri(x,y,bx,by,bh,tri)
% Create topo files for DIVA from bathymetry on a triangular grid.
% 
% INPUTS:
%   x,y     target regular grid coordinates; vectors suitabe for input to meshgrid
%   bx,by   bathymetry grid coordinates
%   bh      bathymetry values
%   tri     (n-by-3) triangulation array
% 
% OUTPUT:
%   h       bathymetry on a regular grid

olab_addpath('geometry/triangulation') % tri_interp

if sum(size(x)~=1)>1 || sum(size(x)~=1)>1 % if not vectors
    error('Target grid coordinates should be vectors for input to meshgrid.')
end

[xx,yy] = meshgrid(x,y);

% produces NaNs outside of the bathymetry grid coverage
h = tri_interp(xx(:),yy(:), bx,by,bh,tri);

h = reshape(h,size(xx));

% h = topo_deepen_near_nodes(x,y,h,bx,by,bh);


function hd = topo_deepen_near_nodes(x,y,h,bx,by,bh)
% Deepen regular bathymetry grid (x,y,h) to the depths on the irregular
% nodes (bx,by,bh) such that all four grid nodes surrounding each
% bx,by location have at least the depth of the corresponding bh.
%
% Needed for DIVA to generate depth contours which cover all bx,by
% locations and depths. In this case, DIVA climatology values will
% interpolate to bx,by without NaNs.
%
% x,y           vectors (row or column)
% h             [ny,nx] matrix
% bx,by,bh      vectors of same size
% hd            h deepened

olab_addpath('signal') % interpolant

ix = interpolant(x,bx);
iy = interpolant(y,by);

% check if any locations are outside of the grid
iout = isnan(ix(:,1)) | isnan(iy(:,1));
if any(iout)
    warning([sum(iout) ' locations are outside of the grid.'])
end

% replace possible 2nd NaNs with 1st index (where (bx,by) hits a grid node)
ir = isnan(ix(:,2));
ix(ir,2) = ix(ir,1);
ir = isnan(iy(:,2));
iy(ir,2) = iy(ir,1);

% % each row has linear indices of 4 grid nodes surrounding each location
% ind = sub2ind(size(h),iy(:,[1 2 2 1]),ix(:,[1 1 2 2])); % [ni x 4]

ix = ix(:,[1 1 2 2]);
iy = iy(:,[1 2 2 1]);

% collect maximum depths for all grid nodes surrounding any location
% with NaNs for the rest of the grid nodes
hd = accumarray([iy(:) ix(:)],repmat(double(bh(:)),4,1),size(h),@max,NaN);

% deepen where needed
hd = max(h,hd,'omitnan');