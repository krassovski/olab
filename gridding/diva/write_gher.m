function write_gher(a,fillvalue,fname)
%WRITE_GHER -- Write matrix a in a binary file according to GHER format.
%
% SYNTAX
%    write_gher(a,fillvalue,fname)
%
% INPUT
%            a: array to write
%    fillvalue: to fill NaNs in a, e.g. -99
%        fname: name for the output file
%
% OUTPUT
%   output file is created

% Author:       Maxim Krassovski
% Matlab ver.:  9.3.0.713579 (R2017b)

prec = 4; % single, 4-byte, precision to output for a

[imax,jmax,kmax] = size(a);
nbmots = imax*jmax*kmax;

fid = fopen(fname,'w');

fwrite(fid, zeros(20,1),'int32'); % 20 empty values

fwrite(fid, 24,'int32'); %%% simulate fortran output: num bytes for each record before and after the record
fwrite(fid, [imax,jmax,kmax,prec,nbmots],'int32');
fwrite(fid, fillvalue,'real*4');
fwrite(fid, 24,'int32'); %%%

fwrite(fid, nbmots*4,'int32'); %%%
fwrite(fid, a,'real*4');
fwrite(fid, nbmots*4,'int32'); %%%

fwrite(fid, zeros(2,1),'int32'); %%% two empty values at the end of file

fclose(fid);