function a = read_gher(fname)
%READ_GHER -- Read an array from a binary file according to GHER format.
%
% SYNTAX
%    a = read_gher(fname)
%
% INPUT
%    fname: file name
%
% OUTPUT
%    a:     array of values

% Author:       Maxim Krassovski
% Matlab ver.:  9.3.0.713579 (R2017b)

% prec = 4; % single, 4-byte, precision to output for a
% [imax,jmax,kmax] = size(a);
% nbmots = imax*jmax*kmax;

fid = fopen(fname,'r');

fseek(fid, 21*4, 'bof');
% fread(fid,20,'int32'); % 20 empty values
% fwrite(fid, 24,'int32'); %%% simulate fortran output: num bytes for each record before and after the record
% fwrite(fid, [imax,jmax,kmax,prec,nbmots],'int32');
imax = fread(fid,1,'int32');
jmax = fread(fid,1,'int32');
kmax = fread(fid,1,'int32');
prec = fread(fid,1,'int32');
if prec~=4
    error('Adjust precision')
end
nbmots = fread(fid,1,'int32');
% fwrite(fid, fillvalue,'real*4');
fillvalue = fread(fid,1,'real*4');
% fwrite(fid, 24,'int32'); %%%
% fwrite(fid, nbmots*4,'int32'); %%%
fseek(fid, 2*4, 'cof');

% fwrite(fid, a,'real*4');
a = fread(fid,nbmots,'real*4'); %%% 2 extra numbers, which are actually int32 (same size in bytes as real*4)
% fwrite(fid, nbmots*4,'int32'); %%%
% fwrite(fid, zeros(2,1),'int32'); %%% two empty values at the end of file

fclose(fid);

a = reshape(a,[imax jmax kmax]);
% a(end-1:end,:,:) = []; % remove 2 extra numbers
a = a'; % transpose to orient, such that imagesc(a);set(gca,'YDir','normal') gives normally oriented map
a(a==fillvalue) = NaN;