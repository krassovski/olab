function [cv,v] = c2cell(c)
%C2CELL -- Convert contour matrix to cell array of contour vertices.
%
% NOTE: Can not plot the output contours with patch function - patches do
% not extend to the bottom.
% 
% SYNTAX
%    [cv,v] = c2cell(c)
%
% INPUT
%    c: contour matrix in contourc format
%
% OUTPUT
%    cv: cell array of vertices, one cell per contour
%     v: values for each contour

% Author:       Maxim Krassovski
% Matlab ver.:  9.3.0.713579 (R2017b)

cv = {};
v = [];
while ~isempty(c)
    n = c(2,1);
    cv{end+1} = c(:,2:n+1)';
    v(end+1) = c(1,1);
    c(:,1:n+1) = [];
end