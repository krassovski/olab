function olab_rmpath(varargin)
%OLAB_RMPATH Remove olab subfolder(s) from Matlab path.
%
% Signatures:
%   olab_rmpath % remove all olab subfolders except olab_home
%   olab_rmpath(path_relative_to_olab_home) % remove individual olab subfolder(s)
% 
% Inputs: 
%   relpath     path to remove, relative to olab_home;
%               either a string separated with ";" for multiple pathes (as
%               input to rmpath) or a cell array of individual pathes.
%               If omitted, all olab subfolders are removed except olab_home.

fullpath = olab_path_parse(varargin{:});
on = on_path(fullpath);
rmpath(on)