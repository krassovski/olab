function pathstr = fsep(pathstr)
% Ensure correct filesep for Windows and UNIX.
% M.Krassovski 2014-09-23

if ismember(computer,{'GLNXA64' 'MACI64'}) % UNIX
    pathstr = strrep(pathstr,'\',filesep);
else % Windows
    pathstr = strrep(pathstr,'/',filesep);
end